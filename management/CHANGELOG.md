# Changelog
All notable changes to this project will be documented in this file.

The format is based on [Keep a Changelog](https://keepachangelog.com/en/1.1.0/), and this project adheres to [Semantic Versioning](https://semver.org/spec/v2.0.0.html).

## [0.12.1] - 2023-07-10
### Changed
- Format the entire Java codebase.

## [0.12.0] - 2023-07-07
### Changed
- Update web API and websocket API URLs of the various IoT Broker services.

## [0.11.8] - 2023-07-04
### Changed
- Enable creation of executable and non-executable JARs.
  Previously, only executable JARs were created and published to the artifact repository without any artifact classifier.
  Now, executable JARs are published with the "exec" classifier, while non-executable JARs are published without any classifier.

## [0.11.7] - 2023-07-04
### Changed
- Enable generation of Javadoc and sources archive files.

## [0.11.6] - 2023-06-15
### Changed
- Update Spring Boot to 2.7.12.
- Update Spring Cloud Dependencies to 2021.0.7.
- Update Project Lombok to 1.18.28.
- Update OkHttp to 4.11.0.
- Update JaCoCo Maven Plugin to 0.8.10.
- Update License Maven Plugin to 2.1.0.
- Update Apache Maven Javadoc Plugin to 3.5.0.
- Update Apache Maven Checkstyle Plugin to 3.3.0.

## [0.11.5] - 2023-06-13
### Changed
- Update to Angular 15.

## [0.11.4] - 2023-04-17
### Changed
- Relicense under the OLFL-1.3.

## [0.11.3] - 2023-03-09
### Added
- Add an option to press "enter" in the device overviews and history message table filter fields instead of clicking on the button.
- Add automatic loading when selecting an option in the autofill select components.

## [0.11.2] - 2023-02-13
### Fixed
- Fix a bug where timestamps in the message history table were not formatted correctly.

## [0.11.1] - 2023-02-08
### Changed
- Apply optimizations to the frontend.
  - Improve consistency of german translations.
  - Improve consistency of UI element rendering across the application's views.
  - Display timestamps in table columns in a more human-readable format.

## [0.11.0] - 2023-02-07
### Added
- Add a filter for source and tenant to the device instance overview.
- Add a filter for source and identifier to the device type overview.

## [0.10.7] - 2023-01-11
### Changed
- Update trion/ng-cli-karma to 15.0.4.
- Update Node to 18.12.1.
- Update npm to 8.19.2.

## [0.10.6] - 2022-12-07
### Added
- Add initial loading of device types and instances.

## [0.10.5] - 2022-12-05
### Fixed
- Allow editing of the device type identifier of device instances in the device instance overview.

## [0.10.4] - 2022-12-05
### Fixed
- Fix loading of environment variables in the frontend which prevented access to the IoT Broker services (and their data) under some circumstances.

## [0.10.3] - 2022-11-25
### Fixed
- Fix the build of the frontend's docker image.

## [0.10.2] - 2022-11-25
### Changed
- Refactor the frontend project and adjust file structure for a better organization of classes and components.

## [0.10.1] - 2022-11-10
### Changed
- Change wordings and unify style elements on the frontend page.
- Add a dedicated provider class for localization of mat-paginators.

## [0.10.0] - 2022-10-26
### Changed
- Adjust how detailed information about device instances and device types is displayed when expanding a table entry in their respective views.
  Detailed information is now displayed using the same form as in the create/update dialog.
- Minor optimizations to the dialogs for creating and updating device instances and device types.

## [0.9.0] - 2022-09-28
### Changed
- Adjust the views for device types and device instances to display their respective elements using tables instead of tiles/cards.
  The tables provide support for sorting the displayed elements.

## [0.8.1] - 2022-09-28
### Fixed
- Fix a bug where the user was not able to edit a device type because the device type identifier was validated incorrectly.

## [0.8.0] - 2022-09-14
### Added
- Add descriptions for the IoT Broker in general and the components provided by the frontend application to the frontend's landing page.
- Add an entry for the landing page in the frontend's side menu.

## [0.7.1] - 2022-08-31
### Changed
- Add a devices tab in the sidebar to group the device instance overview and device type overview.
- Rename device overview to device instance overview.

## [0.7.0] - 2022-08-31
### Added
- Add a view for displaying the history of Sensor Data Messages (which is backed by the Sensor Data History Service).
  This view is similar to and complements the view for displaying live Sensor Data Messages (which in turn is backed by the Sensor Data Subscription Service).
- Add a messages tab in the sidebar to group the two views for Sensor Data Messages together.
- Add general components for autofill select and topic selection.
- Add a dedicated service for HTTP Requests to the Sensor Data History Service.

## [0.6.16] - 2022-08-23
### Changed
- Fix errors in test pipeline by adding missing imports.

## [0.6.15] - 2022-08-19
### Changed
- Add services for devices and device-types and refactor requests regarding those into them.
- Add application wide constants and validator files.
- Fix various ESLint warnings.

## [0.6.14] - 2022-08-19
### Changed
- Disable logging for stomp-js/rx-stomp in the frontend, which may otherwise expose sensitive data.

## [0.6.13] - 2022-08-10
### Changed
- Prevent loading fonts and icons from external sources in the frontend.
  Due to a German court decision it is forbidden to load data from an external source.

## [0.6.12] - 2022-08-08
### Changed
- Migrate from TSLint to ESLint in development setup.

## [0.6.11] - 2022-08-02
### Added
- Add a backing-off mechanism for the reconnection delay of the stomp connection after a defined number of attempts.

## [0.6.10] - 2022-07-21
### Fixed
- Generate new package-lock.json with current npm version.
- Fix dependency issues and npm package vulnerabilities.

## [0.6.9] - 2022-07-21
### Removed
- Remove the legacy websocket-service and sockjs-client dependency.

## [0.6.8] - 2022-07-19
### Changed
- Replace privacy disclaimer and imprint in the frontend with a placeholder.

## [0.6.7] - 2022-07-12
### Added
- Add a loading animation to the reload button in the device overview.

## [0.6.6] - 2022-07-11
### Changed
- Replace the favicon and the logo in the topbar.

## [0.6.5] - 2022-07-08
### Fixed
- Fix a layout bug where the paginators in the device, device type and message overview were not located at the bottom of the page.

## [0.6.4] - 2022-07-06
### Fixed
- Fix a bug where the latest device message was not shown in the device overview.

## [0.6.3] - 2022-07-06
### Changed
- Locate the paginators in the device and device type overview at the bottom of the pages.
- Delete Footer as it created layout problems.

## [0.6.2] - 2022-06-21
### Changed
- Update Angular and Angular Material versions to the latest ~13.3 version.
- Update base docker images and their node version accordingly.
- Run npm audit fix to fix npm package vulnerabilities.

## [0.6.1] - 2022-06-03
### Changed
- When creating new devices or device types via the frontend, required inputs are now correctly marked as such.

## [0.6.0] - 2022-05-20
### Added
- Add a dedicated API gateway application that provides access to IoT Broker services to the frontend application.
  The API gateway provides access not only to the Device Registry Service (like the backend before), but also to the Sensor Data History Service and the Sensor Data Subscription Service.

### Changed
- Remove the API gateway to the Device Registry Service from the backend.
  Now that there is a dedicated API gateway application, this functionality is no longer required in the backend.
- Adjust the frontend to use the API gateway application for requests to IoT Broker services.
  As a result, the frontend no longer needs to know the URLs of the individual services.
  This in turn means that the services themselves no longer need to be directly accessible from browsers running the frontend application.

## [0.5.0] - 2022-05-19
### Changed
- Rework the "Messages" view to display Sensor Data Messages received from the Sensor Data Subscription Service.
  For this, the view now provides a dialog for managing active subscriptions to the Sensor Data Subscription Service.
- Remove the backend's (now obsolete) WebSocket interface.
  This functionality is replaced by the Sensor Data Subscription Service's WebSocket interface.

## [0.4.4] - 2022-04-20
### Fixed
- In the device overview the footer is now placed correctly after opening device panels.

## [0.4.3] - 2022-04-06
### Fixed
- For the device overview and device type overview, the respective number of total elements is now shown correctly in the paginator when changing pages.

## [0.4.2] - 2022-04-01
### Changed
- Update Spring Boot to v2.5.12.
  This Spring Boot version addresses CVE-2022-22965.

## [0.4.1] - 2022-03-23
### Fixed
- Properly show the latest sensor data message for individual devices in the devices overview.

## [0.4.0] - 2022-03-15
### Added
- Add an overview for device types similar to the one for device instances.
  In this overview device types can be created, updated and deleted.

### Changed
- Extend the frontend's `HttpService` to provide access to the Device Registry Service's new `/deviceTypes` endpoints.
- Extend the backend to forward requests to the Device Registry Service's new `/deviceType` endpoints.

## [0.3.0] - 2022-03-15
This is a release that merely fixes the version number of the project.
With the 0.2.1 release, the minor version should have been increased because it introduced new features rather than fixing issues.
Version 0.3.0 and 0.2.1 are therefore identical in terms of the code base.

## [0.2.1] - 2022-03-09
### Changed
- Update the services used by the devices overview to use the Device Registry Service's new `/deviceInstances` endpoints.
- Update the devices overview and its components and dialogs to properly show and consider information about device instances.
- Update the backend to forward requests to the Device Registry Service's new `/deviceInstances` endpoints.

## [0.2.0] - 2022-01-12
### Added
- ElementWatcher Service for listening to size & position change events of HTMLElements.
- OverlayService for providing blocking overlays over specific frontend elements.
- Loading animations to frontend to indicate progress for the users.

## [0.1.9] - 2022-01-21
### Changed
- Update Spring Boot to v2.5.9.
  This Spring Boot version includes Log4j v2.17.1, which fixes a number of vulnerabilities that have been reported in recent weeks.
  For more information on the reported vulnerabilities, see the following links:
  - CVE-2021-44228, https://cve.mitre.org/cgi-bin/cvename.cgi?name=CVE-2021-44228
  - CVE-2021-45046, https://cve.mitre.org/cgi-bin/cvename.cgi?name=CVE-2021-45046
  - CVE-2021-45105, https://cve.mitre.org/cgi-bin/cvename.cgi?name=CVE-2021-45105
  - CVE-2021-44832, https://cve.mitre.org/cgi-bin/cvename.cgi?name=CVE-2021-44832

## [0.1.8] - 2022-01-04
### Changed
- Update Log4j to v2.17.1.
- Update Spring Boot to v2.5.8.

## [0.1.7] - 2021-12-14
### Changed
- Update Log4j dependencies (especially `log4j-core`) to v2.15.0.
  A vulnerability has been reported with CVE-2021-44228 against the `log4j-core` jar and has been fixed in Log4j v2.15.0.
  Until Log4j v2.15.0 is picked up by Spring Boot, which is planned for the Spring Boot v2.5.8 & v2.6.2 releases (due Dec 23, 2021), the Log4j dependencies are overridden manually.
  For more information on CVE-2021-44228, see https://nvd.nist.gov/vuln/detail/CVE-2021-44228.

## [0.1.6] - 2021-12-13
### Fixed
- Fix unit test for message display component.

## [0.1.5] - 2021-12-13
### Changed
- Adjust style of components to conform to the Silicon Economy Style Guide for web applications.

## [0.1.4] - 2021-11-19
### Fixed
- Add a new command to reload the message table whenever a new message is added.
  This fixes an issue where old messages are not displayed initially.

## [0.1.3] - 2021-11-18
### Changed
- Adjust and update docker image versions and dependencies in the frontend.
  - To ensure a stable CI/CD environment, the docker images used to build the Angular application itself (now trion/ng-cli-karma:11.2.12) and the corresponding docker image (now node:14-alpine3.14), are now explicitly specified by their version.
    Previously, the latest versions were always used in both cases.
  - Update all Angular dependencies to 11.2.12 to match the angular-cli version that is now used in the CI/CD pipeline.
  - Downgrade the angular/cli dependency to 0.1102.15 to match the major version of angular-cli that is now used in the CI/CD pipeline.

### Fixed
- Fix rules in the backend's and frontend's .gitlab-ci.yml which referred to the old "master" branch.
  The "master" branch has been renamed to "main".
  Refer to the default branch instead.

## [0.1.2] - 2021-11-17
### Changed
- Adjust the style of license headers in the backend.
  Use the slash-star style to avoid dangling Javadoc comments.
  Using this style also prevents the license headers from being affected when reformatting code via the IntelliJ IDEA IDE.

## [0.1.1] - 2021-10-27
### Added
- This CHANGELOG file to keep track of the changes in this project.

### Changed
- In the adapter overview in the frontend, display the start time of the first adapter instance instead of the creation time.
  This change corresponds to the fix in the backend.

### Fixed
- Fix the adapter information query in the backend.
  The "kube_pod_created" metric doesn't seem to be available via Prometheus anymore.
  As a replacement, the "kube_pod_start_time" metric is used instead.

## [0.1.0] - 2021-10-15
This is the first public release.

The IoT Broker Management application provides an interface to manage the IoT Broker and devices connected to it.

This release corresponds to IoT Broker v0.9.
