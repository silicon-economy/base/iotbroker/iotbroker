# IoT Broker Management application

The IoT Broker Management application provides an interface to manage the IoT Broker.
It is composed of the following components:

* Management API Gateway - Provides the Management Frontend access to the core services of the IoT Broker.
* Management Backend - Provides interfaces relevant to the administration and management of an IoT Broker that are not provided by the IoT Broker core services.
* Management Frontend - The IoT Broker’s own visualization intended for administrative purposes.
	It, among other things, accesses the IoT Broker core services via the Management API Gateway and provides, for example, an overview and allows management of IoT devices known to the IoT Broker.

## Documentation

For more information on the components in this project, please refer to the IoT Broker's arc42 documentation maintained in the `documentation` directory in this repository.

## Licenses of third-party dependencies

For information about licenses of third-party dependencies, please refer to the `README.md` files of the corresponding components.
