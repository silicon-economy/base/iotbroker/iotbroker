# OpenAPI 3 specification:
# - https://swagger.io/docs/specification/
# - https://github.com/OAI/OpenAPI-Specification/blob/master/versions/3.0.1.md
# Online validator:
# - https://apidevtools.org/swagger-parser/online/
openapi: 3.0.1
info:
  title: IoT Broker Management Backend API
  description: |-
    This is the IoT Broker Management's backend API.

    Besides the endpoints defined in this specification, the backend acts as an API gateway for the IoT Broker's Device Registry Service.
    It provides the same endpoints as the IoT Broker's Device Registry Service API and forwards requests to it and responses from it back to the client.
    For details on the Device Registry Service's API specification, please refer to: https://git.openlogisticsfoundation.org/silicon-economy/base/iotbroker/core/-/blob/master/device-registry-service/openapi.yaml.
  contact:
    name: Martin Grzenia
    email: martin.grzenia@iml.fraunhofer.de
  license:
    name: Open Logistics Foundation License, Version 1.3
  version: 0.1.0
servers:
  - url: http://localhost:8081
    description: Generated server url
tags:
  - name: Cluster Monitoring
    description: Endpoints for monitoring pods running in the IoT Broker's cluster.
paths:
  /cluster/monitoring/pods:
    get:
      tags:
        - Cluster Monitoring
      summary: Retrieves monitoring information about all pods running in the IoT Broker's cluster.
      responses:
        "200":
          description: OK
          content:
            application/json:
              schema:
                type: array
                items:
                  $ref: "#/components/schemas/PodMetrics"
  /cluster/monitoring/pods/{NAME}:
    get:
      tags:
        - Cluster Monitoring
      summary: Retrieves monitoring information about a single pod running in the IoT Broker's cluster.
      parameters:
        - name: NAME
          in: path
          description: The name of the pod.
          required: true
          schema:
            type: string
      responses:
        "200":
          description: OK
          content:
            application/json:
              schema:
                $ref: "#/components/schemas/PodMetrics"
        "404":
          description: Referencing pod that could not be found.
  /cluster/monitoring/pods/{NAME}/history/cpuUsage:
    get:
      tags:
        - Cluster Monitoring
      summary: Retrieves historical values for the CPU usage of a pod running in the IoT Broker's cluster.
      parameters:
        - name: NAME
          in: path
          description: The name of the pod.
          required: true
          schema:
            type: string
        - name: range
          in: query
          description: >-
            The range (in seconds) for which values are to be retrieved.
            The range is calculated based on the current time.
            Example: A range value of 600 will result in values being returned for the last 10 minutes.
          required: true
          schema:
            type: integer
            format: int32
        - name: stepWidth
          in: query
          description: >-
            The step width (in seconds) with which the values are to be retrieved.
            Example: A step width value of 60 will result in values being returned whose timestamps are each 1 minute apart.
          required: true
          schema:
            type: integer
            format: int32
      responses:
        "200":
          description: OK
          content:
            application/json:
              schema:
                $ref: "#/components/schemas/CpuUsageHistory"
        "404":
          description: Referencing pod that could not be found.
  /cluster/monitoring/pods/{NAME}/history/memoryUsage:
    get:
      tags:
        - Cluster Monitoring
      summary: Retrieves historical values for the memory usage of a pod running in the IoT Broker's cluster.
      parameters:
        - name: NAME
          in: path
          description: The name of the pod.
          required: true
          schema:
            type: string
        - name: range
          in: query
          description: >-
            The range (in seconds) for which values are to be retrieved.
            The range is calculated based on the current time.
            Example: A range value of 600 will result in values being returned for the last 10 minutes.
          required: true
          schema:
            type: integer
            format: int32
        - name: stepWidth
          in: query
          description: >-
            The step width (in seconds) with which the values are to be retrieved.
            Example: A step width value of 60 will result in values being returned whose timestamps are each 1 minute apart.
          required: true
          schema:
            type: integer
            format: int32
      responses:
        "200":
          description: OK
          content:
            application/json:
              schema:
                $ref: "#/components/schemas/MemoryUsageHistory"
        "404":
          description: Referencing pod that could not be found.
  /cluster/monitoring/pods/{NAME}/history/receiveBandwidth:
    get:
      tags:
        - Cluster Monitoring
      summary: Retrieves historical values for the bandwidth of incoming traffic of a pod running in the IoT Broker's cluster.
      parameters:
        - name: NAME
          in: path
          description: The name of the pod.
          required: true
          schema:
            type: string
        - name: range
          in: query
          description: >-
            The range (in seconds) for which values are to be retrieved.
            The range is calculated based on the current time.
            Example: A range value of 600 will result in values being returned for the last 10 minutes.
          required: true
          schema:
            type: integer
            format: int32
        - name: stepWidth
          in: query
          description: >-
            The step width (in seconds) with which the values are to be retrieved.
            Example: A step width value of 60 will result in values being returned whose timestamps are each 1 minute apart.
          required: true
          schema:
            type: integer
            format: int32
      responses:
        "200":
          description: OK
          content:
            application/json:
              schema:
                $ref: "#/components/schemas/ReceiveBandwidthHistory"
        "404":
          description: Referencing pod that could not be found.
  /cluster/monitoring/pods/{NAME}/history/transmitBandwidth:
    get:
      tags:
        - Cluster Monitoring
      summary: Retrieves historical values for the bandwidth of outgoing traffic of a pod running in the IoT Broker's cluster.
      parameters:
        - name: NAME
          in: path
          description: The name of the pod.
          required: true
          schema:
            type: string
        - name: range
          in: query
          description: >-
            The range (in seconds) for which values are to be retrieved.
            The range is calculated based on the current time.
            Example: A range value of 600 will result in values being returned for the last 10 minutes.
          required: true
          schema:
            type: integer
            format: int32
        - name: stepWidth
          in: query
          description: >-
            The step width (in seconds) with which the values are to be retrieved.
            Example: A step width value of 60 will result in values being returned whose timestamps are each 1 minute apart.
          required: true
          schema:
            type: integer
            format: int32
      responses:
        "200":
          description: OK
          content:
            application/json:
              schema:
                $ref: "#/components/schemas/TransmitBandwidthHistory"
        "404":
          description: Referencing pod that could not be found.
  /cluster/monitoring/adapters:
    get:
      tags:
        - Cluster Monitoring
      summary: Retrieves information about all adapters running in the IoT Broker's cluster.
      responses:
        "200":
          description: OK
          content:
            application/json:
              schema:
                type: array
                items:
                  $ref: "#/components/schemas/AdapterInformation"
components:
  parameters:
    PodFilter:
      in: query
      name: filter
      required: true
      schema:
        type: string
        enum: [ ALL, ADAPTERS, CONNECTORS, MONGODB, RABBITMQ ]
      description: >-
        A filter to constrain the type of pods to retrieve information for:
          * `ALL` - Retrieve the information for all pods.
          * `ADAPTERS` - Retrieve the information for all adapter instances.
          * `CONNECTORS` - Retrieve the information for all connector instances.
          * `MONGODB` - Retrieve the information for all MongoDB instances.
          * `RABBITMQ` - Retrieve the information for all RabbitMQ instances.
  schemas:
    PodMetrics:
      type: object
      description: Information about different metrics of a single pod.
      properties:
        name:
          type: string
          description: The name of the pod the metrics belong to.
          example: mongodb-2
        label:
          type: string
          description: The label describing the pod's type.
          example: mongodb
        values:
          $ref: "#/components/schemas/PodMetricsValues"
        resourceLimits:
          $ref: "#/components/schemas/PodResourceLimits"
    PodMetricsValues:
      type: object
      description: The actual metric values for a single pot at a specific point in time.
      properties:
        cpuUsage:
          type: number
          format: double
          description: The CPU usage value (in number of CPU cores).
          example: 0.0421800430486096
        memoryUsage:
          type: integer
          format: int64
          description: The memory usage value (in bytes).
          example: 114929664
        receiveBandwidth:
          type: number
          format: double
          description: The bandwidth value (in bytes per second).
          example: 4003.5511848664887
        transmitBandwidth:
          type: number
          format: double
          description: The bandwidth value (in bytes per second).
          example: 3182.4115424252836
        timestamp:
          type: string
          format: date-time
          description: The timestamp associated with the values.
          example: "2020-11-12T12:56:55Z"
    PodResourceLimits:
      type: object
      description: The resource limits configured for a pod.
      properties:
        cpu:
          type: number
          nullable: true
          format: double
          description: >-
            The number of CPU cores a pod is limited to.
            May be `null` if a CPU core limit isn't configured.
          example: 1.0
        memory:
          type: integer
          nullable: true
          format: int64
          description: >-
            The amount of memory (in bytes) a pod is limited to.
            May be `null` if a memory limit isn't configured.
          example: 2147483648
    AdapterInformation:
      type: object
      description: Information about a specific type of adapter.
      properties:
        adapterType:
          type: string
          description: The type of adapter the information belongs to.
          example: test-adapter
        instanceCount:
          type: integer
          format: int32
          description: The number of adapter instances running (i.e. the number of pods).
          example: 1
        instances:
          type: array
          description: Detailed information about the adapter instances running.
          items:
            $ref: "#/components/schemas/AdapterInstanceDetails"
    AdapterInstanceDetails:
      type: object
      description: Detailed information about a specific adapter instance.
      properties:
        instanceName:
          type: string
          description: The name of the adapter instance running (i.e. the name of the pod).
          example: "test-adapter-20-2jt78"
        timeStarted:
          type: string
          format: date-time
          description: The time at which the adapter instance was started.
          example: "2020-11-12T12:56:55Z"
    CpuUsageHistory:
      type: object
      description: Historical values for the CPU usage of a single pod.
      properties:
        name:
          type: string
          description: The name of the pod the values belong to.
          example: mongodb-2
        values:
          type: array
          description: A list of timestamp and CPU usage pairs.
          items:
            $ref: "#/components/schemas/CpuUsageHistoryValue"
    CpuUsageHistoryValue:
      type: object
      description: A timestamp and CPU usage pair.
      properties:
        value:
          type: number
          format: double
          description: The CPU usage value (in CPUs).
          example: 0.0421800430486096
        timestamp:
          type: string
          format: date-time
          description: The timestamp associated with the value.
          example: "2020-11-12T12:56:55Z"
    MemoryUsageHistory:
      type: object
      description: Historical values for the memory usage of a single pod.
      properties:
        name:
          type: string
          description: The name of the pod the values belong to.
          example: mongodb-2
        values:
          type: array
          description: A list of timestamp and memory usage pairs.
          items:
            $ref: "#/components/schemas/MemoryUsageHistoryValue"
    MemoryUsageHistoryValue:
      type: object
      description: A timestamp and memory usage pair.
      properties:
        value:
          type: integer
          format: int64
          description: The memory usage value (in bytes).
          example: 114929664
        timestamp:
          type: string
          format: date-time
          description: The timestamp associated with the value.
          example: "2020-11-12T12:56:55Z"
    ReceiveBandwidthHistory:
      type: object
      description: Historical values for the bandwidth of incoming traffic of a single pod.
      properties:
        name:
          type: string
          description: The name of the pod the values belong to.
          example: mongodb-2
        values:
          type: array
          description: A list of timestamp and receive bandwidth pairs.
          items:
            $ref: "#/components/schemas/ReceiveBandwidthHistoryValue"
    ReceiveBandwidthHistoryValue:
      type: object
      description: A timestamp and receive bandwidth pair.
      properties:
        value:
          type: number
          format: double
          description: The bandwidth value (in bytes per second).
          example: 4003.5511848664887
        timestamp:
          type: string
          format: date-time
          description: The timestamp associated with the value.
          example: "2020-11-12T12:56:55Z"
    TransmitBandwidthHistory:
      type: object
      description: Historical values for the bandwidth of outgoing traffic of a single pod.
      properties:
        name:
          type: string
          description: The name of the pod the values belong to.
          example: mongodb-2
        values:
          type: array
          description: A list of timestamp and transmit bandwidth pairs.
          items:
            $ref: "#/components/schemas/TransmitBandwidthHistoryValue"
    TransmitBandwidthHistoryValue:
      type: object
      description: A timestamp and transmit bandwidth pair.
      properties:
        value:
          type: number
          format: double
          description: The bandwidth value (in bytes per second).
          example: 3182.4115424252836
        timestamp:
          type: string
          format: date-time
          description: The timestamp associated with the value.
          example: "2020-11-12T12:56:55Z"
