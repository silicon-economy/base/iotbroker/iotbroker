This file contains third-party licenses that cannot be determined automatically by the license tool used in this project.
For each dependency and its license a single line is to be added in the format:
(<license-name>) <project-name> <groupId>:<artifactId>:<version> - <project-url>
