max by (adapterType, pod) (
  label_replace(
    kube_pod_labels{label_sele_service=~'.+', namespace='iot-broker', pod=~'.*adapter.*'},
    'adapterType', '$1', 'label_sele_service', '(.*)'
  )
) * on(pod) group_right(adapterType)
(
  sum by (metricName, pod) (
    label_replace(
      kube_pod_start_time,
      'metricName', 'TIME_STARTED', '', ''
    )
  )
)
