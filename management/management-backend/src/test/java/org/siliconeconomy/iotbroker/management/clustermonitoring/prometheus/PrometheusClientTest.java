/*
 * Copyright Open Logistics Foundation
 *
 * Licensed under the Open Logistics Foundation License 1.3.
 * For details on the licensing terms, see the LICENSE file.
 * SPDX-License-Identifier: OLFL-1.3
 */
package org.siliconeconomy.iotbroker.management.clustermonitoring.prometheus;

import okhttp3.*;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.mockito.ArgumentCaptor;
import org.mockito.InjectMocks;
import org.mockito.MockitoAnnotations;
import org.siliconeconomy.iotbroker.management.config.OkHttpClientConfig;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.boot.test.mock.mockito.MockBean;
import org.springframework.test.context.ContextConfiguration;

import java.net.URLEncoder;
import java.nio.charset.StandardCharsets;

import static org.hamcrest.MatcherAssert.assertThat;
import static org.hamcrest.Matchers.is;
import static org.mockito.ArgumentMatchers.any;
import static org.mockito.Mockito.verify;
import static org.mockito.Mockito.when;

@SpringBootTest(
    classes = {PrometheusClient.class},
    properties = {
        "cluster.monitoring.prometheus.url=https://localhost",
        "cluster.monitoring.prometheus.endpoint.query=/api/v1/query",
        "cluster.monitoring.prometheus.endpoint.queryRange=/api/v1/query_range",
        "cluster.monitoring.prometheus.token=some-token"
    }
)
@ContextConfiguration(classes = {OkHttpClientConfig.class})
class PrometheusClientTest {

  @MockBean
  private OkHttpClient okHttpClient;

  @MockBean
  private Call call;

  @Autowired
  @InjectMocks
  private PrometheusClient prometheusClient;

  @BeforeEach
  public void init() {
    MockitoAnnotations.openMocks(this);
  }

  @Test
  void testQuery()
      throws Exception {
    when(call.execute()).thenReturn(dummyResponse());
    when(okHttpClient.newCall(any())).thenReturn(call);

    prometheusClient.query("some string to be encoded");

    ArgumentCaptor<Request> requestCaptor = ArgumentCaptor.forClass(Request.class);
    verify(okHttpClient).newCall(requestCaptor.capture());
    assertThat(requestCaptor.getValue().url().isHttps(), is(true));
    assertThat(requestCaptor.getValue().url().host(), is("localhost"));
    assertThat(requestCaptor.getValue().url().encodedPath(), is("/api/v1/query"));
    assertThat(
        requestCaptor.getValue().url().encodedQuery(),
        is("query=" + URLEncoder.encode("some string to be encoded", StandardCharsets.UTF_8))
    );
    assertThat(requestCaptor.getValue().headers().get("Authorization"), is("Bearer some-token"));
  }

  @Test
  void testQueryRange()
      throws Exception {
    when(call.execute()).thenReturn(dummyResponse());
    when(okHttpClient.newCall(any())).thenReturn(call);

    prometheusClient.queryRange("some string to be encoded", 123, 456, (float) 7.89);

    ArgumentCaptor<Request> requestCaptor = ArgumentCaptor.forClass(Request.class);
    verify(okHttpClient).newCall(requestCaptor.capture());
    assertThat(requestCaptor.getValue().url().isHttps(), is(true));
    assertThat(requestCaptor.getValue().url().host(), is("localhost"));
    assertThat(requestCaptor.getValue().url().encodedPath(), is("/api/v1/query_range"));
    assertThat(
        requestCaptor.getValue().url().encodedQuery(),
        is("query="
               + URLEncoder.encode("some string to be encoded", StandardCharsets.UTF_8)
               + "&start=123&end=456&step=7.89")
    );
    assertThat(requestCaptor.getValue().headers().get("Authorization"), is("Bearer some-token"));
  }

  private Response dummyResponse() {
    return new Response.Builder()
        .request(
            new Request.Builder()
                .url("https://localhost")
                .build())
        .code(200)
        .protocol(Protocol.HTTP_2)
        .message("Dummy message content")
        .body(ResponseBody.create("Dummy body content", MediaType.get("text/plain")))
        .build();
  }
}
