/*
 * Copyright Open Logistics Foundation
 *
 * Licensed under the Open Logistics Foundation License 1.3.
 * For details on the licensing terms, see the LICENSE file.
 * SPDX-License-Identifier: OLFL-1.3
 */
package org.siliconeconomy.iotbroker.management.clustermonitoring.prometheus.query;

import org.junit.jupiter.api.Test;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Qualifier;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.core.io.ResourceLoader;

import java.io.IOException;
import java.nio.file.Files;

import static org.hamcrest.MatcherAssert.assertThat;
import static org.hamcrest.Matchers.is;

@SpringBootTest(
    classes = {AdaptersQuery.class},
    properties = {
        "cluster.monitoring.pod.label.component=label_sele_service",
        "cluster.monitoring.namespace=iot-broker"
    }
)
class AdaptersQueryTest {

  @Autowired
  @Qualifier("webApplicationContext")
  private ResourceLoader resourceLoader;

  @Autowired
  private AdaptersQuery query;

  @Test
  void testAdapterAllInstances()
      throws IOException {
    String expected = removeLineBreaksAndDuplicateWhitespace(loadResourceAsString(
        "testdata/AdapterQueryTest_prometheusRequest_adapterAllInstances.txt"
    ));
    String result = removeLineBreaksAndDuplicateWhitespace(query.adapterAllInstances());

    assertThat(result, is(expected));
  }

  private String removeLineBreaksAndDuplicateWhitespace(String source) {
    return source
        .trim()
        .replaceAll("\n", "")
        .replaceAll(" +", " ");
  }

  private String loadResourceAsString(String resoucePath)
      throws IOException {
    return new String(
        Files.readAllBytes(
            resourceLoader.getResource("classpath:" + resoucePath).getFile().toPath()
        )
    );
  }
}
