/*
 * Copyright Open Logistics Foundation
 *
 * Licensed under the Open Logistics Foundation License 1.3.
 * For details on the licensing terms, see the LICENSE file.
 * SPDX-License-Identifier: OLFL-1.3
 */
package org.siliconeconomy.iotbroker.management.clustermonitoring;

import com.fasterxml.jackson.databind.JsonMappingException;
import org.hamcrest.Matchers;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.mockito.InjectMocks;
import org.mockito.MockitoAnnotations;
import org.siliconeconomy.iotbroker.management.clustermonitoring.data.AdapterInformation;
import org.siliconeconomy.iotbroker.management.clustermonitoring.data.PodMetricHistory;
import org.siliconeconomy.iotbroker.management.clustermonitoring.data.PodMetrics;
import org.siliconeconomy.iotbroker.management.clustermonitoring.prometheus.PrometheusClient;
import org.siliconeconomy.iotbroker.management.clustermonitoring.prometheus.query.AdaptersQuery;
import org.siliconeconomy.iotbroker.management.clustermonitoring.prometheus.query.Metric;
import org.siliconeconomy.iotbroker.management.clustermonitoring.prometheus.query.PodMetricHistoryQuery;
import org.siliconeconomy.iotbroker.management.clustermonitoring.prometheus.query.PodMetricsQuery;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Qualifier;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.boot.test.mock.mockito.MockBean;
import org.springframework.core.io.ResourceLoader;

import java.io.IOException;
import java.nio.file.Files;
import java.time.Instant;
import java.util.List;

import static org.hamcrest.MatcherAssert.assertThat;
import static org.hamcrest.Matchers.is;
import static org.junit.jupiter.api.Assertions.assertEquals;
import static org.junit.jupiter.api.Assertions.assertThrows;
import static org.mockito.ArgumentMatchers.*;
import static org.mockito.Mockito.when;

@SpringBootTest(classes = {ClusterMonitoringService.class})
class ClusterMonitoringServiceTest {

  @MockBean
  private PrometheusClient prometheusClient;
  @MockBean
  private PodMetricsQuery proPodMetricsQuery;
  @MockBean
  private PodMetricHistoryQuery podMetricHistoryQuery;
  @MockBean
  private AdaptersQuery adaptersQuery;
  @Autowired
  @Qualifier("webApplicationContext")
  private ResourceLoader resourceLoader;
  @Autowired
  @InjectMocks
  private ClusterMonitoringService clusterMonitoringService;

  @BeforeEach
  public void init() {
    MockitoAnnotations.openMocks(this);
  }

  @Test
  void testGetPodMetrics()
      throws Exception {
    String prometheusResponse
        = loadResourceAsString("testdata/ClusterMonitoringServiceTest_validPrometheusResponse_onePod.json");
    when(prometheusClient.query(any())).thenReturn(prometheusResponse);

    List<PodMetrics> result = clusterMonitoringService.getPodMetrics();

    assertThat(result, Matchers.hasSize(1));
    assertThat(result.get(0).getName(), is("test-data-generator-7-ctqxh"));
    assertThat(result.get(0).getLabel(), is("test-data-generator"));
    assertThat(result.get(0).getValues().getCpuUsage(), is(0.0018810523974694016));
    assertThat(result.get(0).getValues().getMemoryUsage(), is(3507994624L));
    assertThat(result.get(0).getValues().getReceiveBandwidth(), is(696.6915901873016));
    assertThat(result.get(0).getValues().getTransmitBandwidth(), is(1721.3233639250795));
    assertThat(result.get(0).getResourceLimits().getCpu(), is(1.0));
    assertThat(result.get(0).getResourceLimits().getMemory(), is(2147483648L));
    assertEquals(Instant.parse("2020-11-11T11:39:52Z"), result.get(0).getValues().getTimestamp());
  }

  @Test
  void testGetPodMetricsByPodName()
      throws Exception {
    String prometheusResponse
        = loadResourceAsString("testdata/ClusterMonitoringServiceTest_validPrometheusResponse_onePod.json");
    when(prometheusClient.query(any())).thenReturn(prometheusResponse);

    PodMetrics result = clusterMonitoringService
        .getPodMetricsByPodName("test-data-generator-7-ctqxh");

    assertThat(result.getName(), is("test-data-generator-7-ctqxh"));
    assertThat(result.getLabel(), is("test-data-generator"));
    assertThat(result.getValues().getCpuUsage(), is(0.0018810523974694016));
    assertThat(result.getValues().getMemoryUsage(), is(3507994624L));
    assertThat(result.getValues().getReceiveBandwidth(), is(696.6915901873016));
    assertThat(result.getValues().getTransmitBandwidth(), is(1721.3233639250795));
    assertThat(result.getResourceLimits().getCpu(), is(1.0));
    assertThat(result.getResourceLimits().getMemory(), is(2147483648L));
    assertEquals(Instant.parse("2020-11-11T11:39:52Z"), result.getValues().getTimestamp());
  }

  @Test
  void testGetPodMetricsByPodName_missingPod()
      throws Exception {
    String prometheusResponse
        = loadResourceAsString("testdata/ClusterMonitoringServiceTest_validPrometheusResponse_emptyResult.json");
    when(prometheusClient.query(any())).thenReturn(prometheusResponse);

    assertThrows(
        PodNotFoundException.class,
        () -> clusterMonitoringService.getPodMetricsByPodName("some-pod-name")
    );
  }

  @Test
  void testGetAdapterInformation()
      throws Exception {
    String prometheusResponse
        = loadResourceAsString("testdata/ClusterMonitoringServiceTest_validPrometheusResponse_twoAdapterInstances.json");

    when(prometheusClient.query(any())).thenReturn(prometheusResponse);

    List<AdapterInformation> result = clusterMonitoringService.getAdapterInformation();

    assertThat(result.size(), is(1));
    assertThat(result.get(0).getAdapterType(), is("test-adapter"));
    assertThat(result.get(0).getInstanceCount(), is(2));
    assertThat(result.get(0).getInstances().get(0).getInstanceName(), is("test-adapter-20-2jt78"));
    assertThat(result.get(0).getInstances().get(0).getTimeStarted(), is(Instant.ofEpochSecond(1608039244)));
    assertThat(result.get(0).getInstances().get(1).getInstanceName(), is("test-adapter-20-6rz7j"));
    assertThat(result.get(0).getInstances().get(1).getTimeStarted(), is(Instant.ofEpochSecond(1608195360)));
  }

  @Test
  void testGetPodMetricHistory_doubleValues()
      throws Exception {
    String prometheusResponse
        = loadResourceAsString("testdata/ClusterMonitoringServiceTest_validPrometheusResponse_doubleValues.json");

    when(prometheusClient.queryRange(any(), anyLong(), anyLong(), eq(60.0f))).thenReturn(prometheusResponse);

    PodMetricHistory result = clusterMonitoringService.getPodMetricHistory(
        "management-backend-24-pp48t",
        Metric.CPU_USAGE,
        600,
        60
    );

    assertThat(result.getName(), is("management-backend-24-pp48t"));
    assertThat(result.getValues().size(), is(11));
    assertThat(result.getValues().get(0).getTimestamp(), is(Instant.ofEpochSecond(1607505859)));
    assertThat(result.getValues().get(0).getValue(), is(0.0005807624946977528));
  }

  @Test
  void testGetPodMetricHistory_longValues()
      throws Exception {
    String prometheusResponse
        = loadResourceAsString("testdata/ClusterMonitoringServiceTest_validPrometheusResponse_longValues.json");

    when(prometheusClient.queryRange(any(), anyLong(), anyLong(), eq(60.0f))).thenReturn(prometheusResponse);

    PodMetricHistory result = clusterMonitoringService.getPodMetricHistory(
        "management-backend-24-pp48t",
        Metric.MEMORY_USAGE,
        600,
        60
    );

    assertThat(result.getName(), is("management-backend-24-pp48t"));
    assertThat(result.getValues().size(), is(11));
    assertThat(result.getValues().get(0).getTimestamp(), is(Instant.ofEpochSecond(1607505859)));
    assertThat(result.getValues().get(0).getValue(), is((long) 199913472));
  }

  @Test
  void testGetPodMetricHistory_missingPod()
      throws Exception {
    String prometheusResponse
        = loadResourceAsString("testdata/ClusterMonitoringServiceTest_validPrometheusResponse_emptyResult_queryRange.json");
    when(prometheusClient.queryRange(any(), anyLong(), anyLong(), anyFloat())).thenReturn(prometheusResponse);

    assertThrows(
        PodNotFoundException.class,
        () -> clusterMonitoringService.getPodMetricHistory("some-pod-name",
                                                           Metric.CPU_USAGE,
                                                           600,
                                                           60)
    );
  }

  @Test
  void testInvalidPrometheusResponse()
      throws Exception {
    when(prometheusClient.query(any())).thenReturn("");

    assertThrows(
        InvalidPrometheusResponeException.class,
        () -> clusterMonitoringService.getPodMetrics()
    );
  }

  @Test
  void testMissingResultInPrometheusResponse()
      throws Exception {
    String prometheusResponse = "{\n" +
        "  \"status\": \"success\",\n" +
        "  \"data\": {\n" +
        "  }\n" +
        "}";
    when(prometheusClient.query(any())).thenReturn(prometheusResponse);

    assertThrows(
        InvalidPrometheusResponeException.class,
        () -> clusterMonitoringService.getPodMetrics()
    );
  }

  @Test
  void testWrongFormatOfPrometheusResponse()
      throws Exception {
    String prometheusResponse = "{\n" +
        "  \"status\": \"success\",\n" +
        "  \"data\": {\n" +
        "    \"resultType\": \"string\",\n" +
        "    \"result\": \"some result\"\n" +
        "  }\n" +
        "}";
    when(prometheusClient.query(any())).thenReturn(prometheusResponse);

    assertThrows(
        JsonMappingException.class,
        () -> clusterMonitoringService.getPodMetrics()
    );
  }


  @Test
  void testWrongContentInPrometheusResponse()
      throws Exception {
    String prometheusResponse = "{\n" +
        "  \"status\": \"success\",\n" +
        "  \"data\": {\n" +
        "    \"resultType\": \"vector\",\n" +
        "    \"result\": [\n" +
        "      {\n" +
        "        \"metric\": {\n" +
        "          \"namespace\": \"iot-broker\",\n" +
        "          \"pood\": \"test-adapter-4-7ddlr\"\n" +
        "        },\n" +
        "        \"value\": [\n" +
        "          1605094909.313\n" +
        "        ]\n" +
        "      }\n" +
        "    ]\n" +
        "  }\n" +
        "}";
    when(prometheusClient.query(any())).thenReturn(prometheusResponse);

    assertThrows(
        JsonMappingException.class,
        () -> clusterMonitoringService.getPodMetrics()
    );
  }

  private String loadResourceAsString(String resoucePath)
      throws IOException {
    return new String(
        Files.readAllBytes(
            resourceLoader.getResource("classpath:" + resoucePath).getFile().toPath()
        )
    );
  }
}
