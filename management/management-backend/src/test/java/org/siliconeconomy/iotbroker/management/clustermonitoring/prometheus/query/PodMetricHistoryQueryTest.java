/*
 * Copyright Open Logistics Foundation
 *
 * Licensed under the Open Logistics Foundation License 1.3.
 * For details on the licensing terms, see the LICENSE file.
 * SPDX-License-Identifier: OLFL-1.3
 */
package org.siliconeconomy.iotbroker.management.clustermonitoring.prometheus.query;

import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.mockito.MockitoAnnotations;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Qualifier;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.core.io.ResourceLoader;

import java.io.IOException;
import java.nio.file.Files;

import static org.hamcrest.MatcherAssert.assertThat;
import static org.hamcrest.Matchers.is;

@SpringBootTest(
    classes = {PodMetricHistoryQuery.class},
    properties = {
        "cluster.monitoring.namespace=iot-broker"
    }
)
class PodMetricHistoryQueryTest {

  @Autowired
  @Qualifier("webApplicationContext")
  private ResourceLoader resourceLoader;

  @Autowired
  private PodMetricHistoryQuery query;

  @BeforeEach
  public void init() {
    MockitoAnnotations.openMocks(this);
  }

  @Test
  void testMetricHistory()
      throws IOException {
    String expected = removeLineBreaksAndDuplicateWhitespace(loadResourceAsString(
        "testdata/PodMetricHistroyQueryTest_prometheusRequest_metricHistory.txt"
    ));
    String result
        = removeLineBreaksAndDuplicateWhitespace(query.metricHistory("pod-name",
                                                                     Metric.CPU_USAGE));
    assertThat(result, is(expected));
  }

  private String removeLineBreaksAndDuplicateWhitespace(String source) {
    return source
        .trim()
        .replaceAll("\n", "")
        .replaceAll(" +", " ");
  }

  private String loadResourceAsString(String resoucePath)
      throws IOException {
    return new String(
        Files.readAllBytes(
            resourceLoader.getResource("classpath:" + resoucePath).getFile().toPath()
        )
    );
  }
}
