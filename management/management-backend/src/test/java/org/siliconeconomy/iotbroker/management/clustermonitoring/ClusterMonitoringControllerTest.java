/*
 * Copyright Open Logistics Foundation
 *
 * Licensed under the Open Logistics Foundation License 1.3.
 * For details on the licensing terms, see the LICENSE file.
 * SPDX-License-Identifier: OLFL-1.3
 */
package org.siliconeconomy.iotbroker.management.clustermonitoring;

import com.fasterxml.jackson.databind.JsonMappingException;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.mockito.InjectMocks;
import org.mockito.MockitoAnnotations;
import org.siliconeconomy.iotbroker.management.RequestExceptionHandler;
import org.siliconeconomy.iotbroker.management.clustermonitoring.prometheus.query.Metric;
import org.siliconeconomy.iotbroker.management.config.WebConfig;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.boot.test.mock.mockito.MockBean;
import org.springframework.test.web.servlet.MockMvc;
import org.springframework.test.web.servlet.request.MockMvcRequestBuilders;
import org.springframework.test.web.servlet.result.MockMvcResultMatchers;
import org.springframework.test.web.servlet.setup.MockMvcBuilders;
import org.springframework.web.servlet.config.annotation.EnableWebMvc;

import static org.mockito.Mockito.*;

@SpringBootTest(classes = {ClusterMonitoringController.class, RequestExceptionHandler.class,
    WebConfig.class})
@EnableWebMvc
class ClusterMonitoringControllerTest {

  private MockMvc mockMvc;

  @MockBean
  private ClusterMonitoringService clusterMonitoringService;

  @Autowired
  private RequestExceptionHandler exceptionHandler;

  @Autowired
  @InjectMocks
  private ClusterMonitoringController clusterMonitoringController;

  @BeforeEach
  public void init() {
    this.mockMvc = MockMvcBuilders
        .standaloneSetup(clusterMonitoringController)
        .setControllerAdvice(exceptionHandler)
        .build();
    MockitoAnnotations.openMocks(this);
  }

  @Test
  void testInvalidPrometheusResponse()
      throws Exception {
    when(clusterMonitoringService.getPodMetrics())
        .thenThrow(InvalidPrometheusResponeException.class);

    mockMvc.perform(MockMvcRequestBuilders
                        .get("/cluster/monitoring/pods"))
        .andExpect(MockMvcResultMatchers.status().is5xxServerError());
  }

  @Test
  void testWrongFormatOfPrometheusResponse()
      throws Exception {
    when(clusterMonitoringService.getPodMetrics()).thenThrow(JsonMappingException.class);

    mockMvc.perform(MockMvcRequestBuilders
                        .get("/cluster/monitoring/pods"))
        .andExpect(MockMvcResultMatchers.status().is5xxServerError());
  }

  @Test
  void testGetPods()
      throws Exception {
    mockMvc.perform(MockMvcRequestBuilders
                        .get("/cluster/monitoring/pods"))
        .andDo(mvcResult -> {
          verify(clusterMonitoringService, times(1)).getPodMetrics();
        });
  }

  @Test
  void testGetPod()
      throws Exception {
    mockMvc.perform(MockMvcRequestBuilders
                        .get("/cluster/monitoring/pods/some-name"))
        .andDo(mvcResult -> {
          verify(clusterMonitoringService, times(1)).getPodMetricsByPodName("some-name");
        });
  }

  @Test
  void testGetAdapterInformation()
      throws Exception {
    mockMvc.perform(MockMvcRequestBuilders
                        .get("/cluster/monitoring/adapters"))
        .andDo(mvcResult -> {
          verify(clusterMonitoringService, times(1)).getAdapterInformation();
        });
  }

  @Test
  void testGetCpuUsageHistory()
      throws Exception {
    mockMvc.perform(
            MockMvcRequestBuilders
                .get("/cluster/monitoring/pods/some-name/history/cpuUsage?range=600&stepWidth=60")
        )
        .andDo(mvcResult -> {
          verify(clusterMonitoringService, times(1))
              .getPodMetricHistory("some-name", Metric.CPU_USAGE, 600, 60);
        });
  }

  @Test
  void testGetMemoryUsageHistory()
      throws Exception {
    mockMvc.perform(
            MockMvcRequestBuilders
                .get("/cluster/monitoring/pods/some-name/history/memoryUsage?range=600&stepWidth=60")
        )
        .andDo(mvcResult -> {
          verify(clusterMonitoringService, times(1))
              .getPodMetricHistory("some-name", Metric.MEMORY_USAGE, 600, 60);
        });
  }

  @Test
  void testGetReceiveBandwidthHistory()
      throws Exception {
    mockMvc.perform(
            MockMvcRequestBuilders
                .get("/cluster/monitoring/pods/some-name/history/receiveBandwidth?range=600&stepWidth=60")
        )
        .andDo(mvcResult -> {
          verify(clusterMonitoringService, times(1))
              .getPodMetricHistory("some-name", Metric.BANDWIDTH_RX, 600, 60);
        });
  }

  @Test
  void testGetTransmitBandwidthHistory()
      throws Exception {
    mockMvc.perform(
            MockMvcRequestBuilders
                .get("/cluster/monitoring/pods/some-name/history/transmitBandwidth?range=600&stepWidth=60")
        )
        .andDo(mvcResult -> {
          verify(clusterMonitoringService, times(1))
              .getPodMetricHistory("some-name", Metric.BANDWIDTH_TX, 600, 60);
        });
  }
}
