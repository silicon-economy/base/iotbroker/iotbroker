/*
 * Copyright Open Logistics Foundation
 *
 * Licensed under the Open Logistics Foundation License 1.3.
 * For details on the licensing terms, see the LICENSE file.
 * SPDX-License-Identifier: OLFL-1.3
 */
package org.siliconeconomy.iotbroker.management.config;

import org.springframework.beans.factory.annotation.Value;
import org.springframework.context.annotation.Configuration;
import org.springframework.web.servlet.config.annotation.CorsRegistry;
import org.springframework.web.servlet.config.annotation.WebMvcConfigurer;

/**
 * Configuration of Spring MVC.
 * Enables CORS for the endpoints provided by the backend and adds the frontend's URL to the list of allowed origins.
 *
 * @author M. Grzenia
 * @version 1.0
 */
@Configuration
public class WebConfig
    implements WebMvcConfigurer {

  @Value("${cors.allowedOrigin.frontend.url}")
  private String managementFrontendUrl;

  @Override
  public void addCorsMappings(CorsRegistry registry) {
    registry.addMapping("/cluster/**")
        .allowedOrigins(managementFrontendUrl)
        .allowedMethods("*");
    registry.addMapping("/deviceInstances/**")
        .allowedOrigins(managementFrontendUrl)
        .allowedMethods("*");
    registry.addMapping("/deviceTypes/**")
        .allowedOrigins(managementFrontendUrl)
        .allowedMethods("*");
  }
}
