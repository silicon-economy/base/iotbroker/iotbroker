/*
 * Copyright Open Logistics Foundation
 *
 * Licensed under the Open Logistics Foundation License 1.3.
 * For details on the licensing terms, see the LICENSE file.
 * SPDX-License-Identifier: OLFL-1.3
 */
package org.siliconeconomy.iotbroker.management.clustermonitoring.data;

import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;
import lombok.experimental.Accessors;

import java.time.Instant;

/**
 * This DTO encapsulates a value and timestamp pair for a particular pod metric at some point in time.
 * Since different pod metrics have different value types, the value is represented by a generic {@link Object}.
 *
 * @author M. Grzenia
 * @version 1.0
 */
@Getter
@Setter
@NoArgsConstructor
@Accessors(chain = true)
public class PodMetricHistoryValue {

  /**
   * The actual metric value.
   */
  private Object value;
  /**
   * The timestamp at which the value applied.
   */
  private Instant timestamp;
}
