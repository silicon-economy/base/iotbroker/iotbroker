/*
 * Copyright Open Logistics Foundation
 *
 * Licensed under the Open Logistics Foundation License 1.3.
 * For details on the licensing terms, see the LICENSE file.
 * SPDX-License-Identifier: OLFL-1.3
 */
package org.siliconeconomy.iotbroker.management.clustermonitoring.data;

import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;

import java.time.Instant;

/**
 * This DTO encapsulates the actual metric values for a single pod at a specific point in time.
 *
 * @author M. Grzenia
 * @version 1.0
 */
@Getter
@Setter
@NoArgsConstructor
public class PodMetricsValues {

  /**
   * The CPU usage value (in number of CPU cores).
   */
  private double cpuUsage;
  /**
   * The memory usage value (in bytes).
   */
  private long memoryUsage;
  /**
   * The bandwidth value (in bytes per second).
   */
  private double receiveBandwidth;
  /**
   * The bandwidth value (in bytes per second).
   */
  private double transmitBandwidth;
  /**
   * The timestamp at which the values apply.
   */
  private Instant timestamp;
}
