/*
 * Copyright Open Logistics Foundation
 *
 * Licensed under the Open Logistics Foundation License 1.3.
 * For details on the licensing terms, see the LICENSE file.
 * SPDX-License-Identifier: OLFL-1.3
 */
package org.siliconeconomy.iotbroker.management.clustermonitoring.prometheus.query;

import org.springframework.beans.factory.annotation.Value;
import org.springframework.context.annotation.Configuration;

/**
 * Defines PromQL query strings for querying pod metrics via the Prometheus HTTP API.
 * For this, Prometheus' instant query endpoint is used.
 * <p>
 * The query is designed to return consistent result structures across all metrics. For a single
 * metric the result will look like this:
 * <pre>{@code
 * {
 *   "metric": {
 *     "labelName": "<LABEL_VALUE>",
 *     "metricName": "<METRIC_NAME>",
 *     "pod": "<POD_NAME>"
 *   },
 *   "value": [
 *     <METRIC_TIMESTAMP>,
 *     "<METRIC_VALUE>"
 *   ]
 * }
 * }</pre>
 *
 * @author M. Grzenia
 * @version 1.0
 */
@Configuration
public class PodMetricsQuery {

  /**
   * The placeholder string to be replaced with the label key to filter for.
   * The query result will only contain pods with the corresponding label (key) set.
   */
  private static final String LABEL_KEY = "<LABEL_KEY>";
  /**
   * The placeholder string to be replaced with the label value to filter for.
   * The string used as a replacement is evaluated as a regular expression.
   * The query result will only contain pods that match the corresponding label value/regular
   * expression.
   */
  private static final String LABEL_VALUE = "<LABEL_VALUE>";
  /**
   * The placeholder string to be replaced with the pod name to filter for.
   * The string used as a replacement is evaluated as a regular expression.
   * The query result will only contain pods that match the corresponding pod name/regular
   * expression.
   */
  private static final String POD_FILTER = "<POD_FILTER>";
  /**
   * The placeholder string to be replaced with the namespace to filter for.
   * The query result will only contain pods that match the corresponding namespace.
   */
  private static final String NAMESPACE_FILTER = "<NAMESPACE_FILTER>";
  /**
   * The regular expression for filtering all pods.
   */
  private static final String REGEX_POD_FILTER_ALL_PODS = ".*";
  /**
   * The regular expression for filtering all label values.
   */
  private static final String REGEX_LABEL_VALUE_ALL_LABELS = ".+";
  /**
   * The label key used for pods related to the infrastructure.
   */
  @Value("${cluster.monitoring.pod.label.infrastructure}")
  private String infrastructureLabelKey;
  /**
   * The label key used for pods not related to the infrastructure.
   */
  @Value("${cluster.monitoring.pod.label.component}")
  private String componentLabelKey;
  /**
   * The namespace for which to get the pod metrics.
   */
  @Value("${cluster.monitoring.namespace}")
  private String namespace;

  /**
   * Returns the string for querying the metrics of all pods related to the infrastructure.
   *
   * @return The query string.
   */
  public String infrastructureAllPods() {
    return buildQueryTemplate()
        .replace(NAMESPACE_FILTER, namespace)
        .replace(LABEL_KEY, infrastructureLabelKey)
        .replace(LABEL_VALUE, REGEX_LABEL_VALUE_ALL_LABELS)
        .replace(POD_FILTER, REGEX_POD_FILTER_ALL_PODS);
  }

  /**
   * Returns the string for querying the metrics of a single pod that is related to the
   * infrastructure.
   * The pod is selected by the given pod name.
   *
   * @param podName The pod name.
   * @return The query string.
   */
  public String infrastructureSinglePod(String podName) {
    return buildQueryTemplate()
        .replace(NAMESPACE_FILTER, namespace)
        .replace(LABEL_KEY, infrastructureLabelKey)
        .replace(LABEL_VALUE, REGEX_LABEL_VALUE_ALL_LABELS)
        .replace(POD_FILTER, podName);
  }

  /**
   * Returns the string for querying the metrics of all pods not related to the infrastructure.
   *
   * @return The query string.
   */
  public String componentAllPods() {
    return buildQueryTemplate()
        .replace(NAMESPACE_FILTER, namespace)
        .replace(LABEL_KEY, componentLabelKey)
        .replace(LABEL_VALUE, REGEX_LABEL_VALUE_ALL_LABELS)
        .replace(POD_FILTER, REGEX_POD_FILTER_ALL_PODS);
  }

  /**
   * Returns the string for querying the metrics of a single pod that is not related to the
   * infrastructure.
   * The pod is selected by the given pod name.
   *
   * @param podName The pod name.
   * @return The query string.
   */
  public String componentSinglePod(String podName) {
    return buildQueryTemplate()
        .replace(NAMESPACE_FILTER, namespace)
        .replace(LABEL_KEY, componentLabelKey)
        .replace(LABEL_VALUE, REGEX_LABEL_VALUE_ALL_LABELS)
        .replace(POD_FILTER, podName);
  }

  /**
   * Builds a template for the actual query string.
   * The template contains some placeholder strings that are to be replaces depending on the pods
   * of interest.
   *
   * @return The query string.
   */
  private String buildQueryTemplate() {
    return podsByLabelQuery() +
        " * on(pod) group_right(labelName)" +
        "(" +
        " " + singleMetricQuery(Metric.BANDWIDTH_RX.name(), Metric.BANDWIDTH_RX.getQuery()) +
        " or " + singleMetricQuery(Metric.BANDWIDTH_TX.name(), Metric.BANDWIDTH_TX.getQuery()) +
        " or " + singleMetricQuery(Metric.MEMORY_USAGE.name(), Metric.MEMORY_USAGE.getQuery()) +
        " or " + singleMetricQuery(Metric.CPU_USAGE.name(), Metric.CPU_USAGE.getQuery()) +
        " or " + singleMetricQuery(ResourceLimit.CPU_CORES_LIMIT.name(), ResourceLimit.CPU_CORES_LIMIT.getQuery()) +
        " or " + singleMetricQuery(ResourceLimit.MEMORY_LIMIT.name(), ResourceLimit.MEMORY_LIMIT.getQuery()) +
        ")";
  }

  /**
   * Builds a query string for retrieving pods with a specific label set.
   * The query string is populated with placeholder strings that are intended to be replaced
   * depending on the pods of interest. Additionally, the query will result in the response's result
   * to contain information about the specific label.
   *
   * @return The query string.
   */
  private String podsByLabelQuery() {
    return String.format("" +
                             "max by (pod, labelName) (" +
                             "  label_replace(" +
                             "    kube_pod_labels{%s=~'%s', namespace='%s', pod=~'%s'}," +
                             "    'labelName', '$1', '%s', '(.*)'" +
                             "  )" +
                             ")", LABEL_KEY, LABEL_VALUE, NAMESPACE_FILTER, POD_FILTER, LABEL_KEY);
  }

  /**
   * Builds a query string for retrieving a single metric (e.g. cpu usage, memory usage, etc.) for
   * any pod.
   * Since the query string produced by {@link #buildQueryTemplate()} will result in a response from
   * the Prometheus HTTP API that contains values for different types of metrics, the given metric
   * name is added to a single metric's result so that the different types of metrics can be
   * distinguished from each other easily.
   *
   * @param metricName   The name (i.e. type) of the metric.
   * @param instantQuery The actual instant query which provides the metric value.
   * @return The query string.
   */
  private String singleMetricQuery(String metricName, String instantQuery) {
    return String.format("sum by (metricName, pod) ( label_replace( %s, 'metricName', '%s', '', '' ) )",
                         instantQuery,
                         metricName);
  }
}
