/*
 * Copyright Open Logistics Foundation
 *
 * Licensed under the Open Logistics Foundation License 1.3.
 * For details on the licensing terms, see the LICENSE file.
 * SPDX-License-Identifier: OLFL-1.3
 */
package org.siliconeconomy.iotbroker.management.clustermonitoring.prometheus.query;

import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;

/**
 * This DTO contains contextual information about a pod's metrics.
 *
 * @author M. Grzenia
 * @version 1.0
 */
@Getter
@Setter
@NoArgsConstructor
public class PodMetricDescription {

  /**
   * The name of the label that is specific to the pod type.
   */
  private String labelName;
  /**
   * The name describing the metric.
   */
  private String metricName;
  /**
   * The name of the pod the metric belongs to.
   */
  private String pod;
}
