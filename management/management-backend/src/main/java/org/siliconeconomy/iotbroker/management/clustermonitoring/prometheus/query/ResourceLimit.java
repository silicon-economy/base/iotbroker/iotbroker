/*
 * Copyright Open Logistics Foundation
 *
 * Licensed under the Open Logistics Foundation License 1.3.
 * For details on the licensing terms, see the LICENSE file.
 * SPDX-License-Identifier: OLFL-1.3
 */
package org.siliconeconomy.iotbroker.management.clustermonitoring.prometheus.query;

/**
 * Defines pod resource limits with their corresponding (PromQL) query strings that are used to
 * query the Prometheus HTTP API for the corresponding configured resource limit.
 *
 * @author M. Grzenia
 * @version 1.0
 */
public enum ResourceLimit {

  /**
   * The limit of CPU cores.
   */
  CPU_CORES_LIMIT("kube_pod_container_resource_limits_cpu_cores"),
  /**
   * The memory limit.
   */
  MEMORY_LIMIT("kube_pod_container_resource_limits_memory_bytes");

  /**
   * The (PromQL) query string that is used to query the corresponding resource limit.
   */
  private String query;

  ResourceLimit(String query) {
    this.query = query;
  }

  /**
   * Returns the query string that is used to query the Prometheus HTTP API for the corresponding
   * resource limit.
   *
   * @return The query string.
   */
  public String getQuery() {
    return query;
  }
}
