/*
 * Copyright Open Logistics Foundation
 *
 * Licensed under the Open Logistics Foundation License 1.3.
 * For details on the licensing terms, see the LICENSE file.
 * SPDX-License-Identifier: OLFL-1.3
 */
package org.siliconeconomy.iotbroker.management.clustermonitoring.prometheus.query;

/**
 * Defines adapter information with their corresponding (PromQL) query strings that are used to
 * query the Prometheus HTTP API for the corresponding adapter information.
 *
 * @author M. Grzenia
 * @version 1.0
 */
public enum AdapterInfo {

  /**
   * The point in time at which a pos was started.
   */
  TIME_STARTED("kube_pod_start_time");

  /**
   * The (PromQL) query string that is used to query the corresponding information.
   */
  private String query;

  AdapterInfo(String query) {
    this.query = query;
  }

  /**
   * Returns the query string that is used to query the Prometheus HTTP API for the corresponding
   * adapter information.
   *
   * @return The query string.
   */
  public String getQuery() {
    return query;
  }
}
