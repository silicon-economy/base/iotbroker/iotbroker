/*
 * Copyright Open Logistics Foundation
 *
 * Licensed under the Open Logistics Foundation License 1.3.
 * For details on the licensing terms, see the LICENSE file.
 * SPDX-License-Identifier: OLFL-1.3
 */
package org.siliconeconomy.iotbroker.management.clustermonitoring.prometheus;

import okhttp3.HttpUrl;
import okhttp3.OkHttpClient;
import okhttp3.Request;
import okhttp3.Response;
import org.siliconeconomy.iotbroker.management.config.OkHttpClientConfig;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.stereotype.Service;

import java.io.IOException;
import java.net.URLEncoder;
import java.nio.charset.StandardCharsets;

/**
 * Uses a HTTP client to access/query the Prometheus HTTP API.
 *
 * @author M. Grzenia
 * @version 1.0
 * @see OkHttpClientConfig
 */
@Service
public class PrometheusClient {

  /**
   * This class's logger.
   */
  public static final Logger LOGGER = LoggerFactory.getLogger(PrometheusClient.class);
  /**
   * The HTTP client to use for the queries.
   */
  private final OkHttpClient client;
  /**
   * The URL at which to access the Prometheus HTTP API.
   */
  @Value("${cluster.monitoring.prometheus.url}")
  private String url;
  /**
   * The endpoint at which instant queries are evaluated.
   */
  @Value("${cluster.monitoring.prometheus.endpoint.query}")
  private String queryEndpoint;
  /**
   * The endpoint at which range queries are evaluated.
   */
  @Value("${cluster.monitoring.prometheus.endpoint.queryRange}")
  private String queryRangeEndpoint;
  /**
   * The token to be used for authorizing access to the Prometheus HTTP API.
   */
  @Value("${cluster.monitoring.prometheus.token}")
  private String token;

  @Autowired
  public PrometheusClient(OkHttpClient client) {
    this.client = client;
  }

  /**
   * Queries the Prometheus HTTP API at a single instant.
   *
   * @param query The actual query to use.
   * @return The body of the HTTP response.
   * @throws IOException If there was an error when querying the Prometheus HTTP API.
   */
  public String query(String query)
      throws IOException {
    HttpUrl httpUrl = HttpUrl.get(url + queryEndpoint)
        .newBuilder()
        .addEncodedQueryParameter("query", URLEncoder.encode(query, StandardCharsets.UTF_8))
        .build();

    Response response = client.newCall(buildRequest(httpUrl)).execute();
    return response.body().string();
  }

  /**
   * Queries the Prometheus HTTP API over a range of time.
   *
   * @param query The actual query to use.
   * @param start The start timestamp (as a unix timestamp).
   * @param end   The end timestamp (as a unix timestamp).
   * @param step  Query resolution step width (in number of seconds).
   * @return The body of the HTTP response.
   * @throws IOException If there was an error when querying the Prometheus HTTP API.
   */
  public String queryRange(String query, long start, long end, float step)
      throws IOException {
    HttpUrl httpUrl = HttpUrl.get(url + queryRangeEndpoint)
        .newBuilder()
        .addEncodedQueryParameter("query", URLEncoder.encode(query, StandardCharsets.UTF_8))
        .addEncodedQueryParameter("start", String.valueOf(start))
        .addEncodedQueryParameter("end", String.valueOf(end))
        .addEncodedQueryParameter("step", String.valueOf(step))
        .build();

    Response response = client.newCall(buildRequest(httpUrl)).execute();
    return response.body().string();
  }

  private Request buildRequest(HttpUrl url) {
    return new Request.Builder()
        .url(url)
        .addHeader("Authorization", "Bearer " + token)
        .build();
  }
}
