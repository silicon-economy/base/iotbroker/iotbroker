/*
 * Copyright Open Logistics Foundation
 *
 * Licensed under the Open Logistics Foundation License 1.3.
 * For details on the licensing terms, see the LICENSE file.
 * SPDX-License-Identifier: OLFL-1.3
 */
package org.siliconeconomy.iotbroker.management;

import com.fasterxml.jackson.core.JsonParseException;
import com.fasterxml.jackson.databind.JsonMappingException;
import org.siliconeconomy.iotbroker.management.clustermonitoring.InvalidPrometheusResponeException;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.http.HttpStatus;
import org.springframework.web.bind.annotation.ExceptionHandler;
import org.springframework.web.bind.annotation.ResponseStatus;
import org.springframework.web.bind.annotation.RestControllerAdvice;

import java.io.IOException;

/**
 * Generates responses in case an exception occurs during processing of requests.
 * It is marked as {@link RestControllerAdvice} so that it will be used by all rest controller
 * classes.
 *
 * @author M. Grzenia
 * @version 1.0
 */
@RestControllerAdvice
public class RequestExceptionHandler {

  private static final Logger LOGGER = LoggerFactory.getLogger(RequestExceptionHandler.class);

  @ExceptionHandler({InvalidPrometheusResponeException.class, IOException.class,
      JsonParseException.class, JsonMappingException.class})
  @ResponseStatus(HttpStatus.INTERNAL_SERVER_ERROR)
  public String handleErrorDuringClusterMonitoringRequest(Exception exception) {
    LOGGER.error(exception.getMessage(), exception);
    return "An internal error occurred while fetching/processing the monitoring data: "
        + exception.getMessage();
  }
}
