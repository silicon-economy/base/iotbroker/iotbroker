/*
 * Copyright Open Logistics Foundation
 *
 * Licensed under the Open Logistics Foundation License 1.3.
 * For details on the licensing terms, see the LICENSE file.
 * SPDX-License-Identifier: OLFL-1.3
 */
package org.siliconeconomy.iotbroker.management.clustermonitoring.prometheus.query;

import org.springframework.beans.factory.annotation.Value;
import org.springframework.context.annotation.Configuration;

/**
 * Defines PromQL query strings for querying historical values for a single metric of a pod via the
 * Prometheus HTTP API.
 * For this, Prometheus' range query endpoint is used.
 * <p>
 * The query is designed to return consistent result structures across all metrics. For a specific
 * metric the result will look like this:
 * <pre>{@code
 * {
 *   "metric": {
 *     "labelName": "dummy-label",
 *     "metricName": "<METRIC_NAME>",
 *     "pod": "<POD_NAME>"
 *   },
 *   "values": [
 *     [
 *       <METRIC_TIMESTAMP>,
 *       "<METRIC_VALUE>"
 *     ]
 *   ]
 * }
 * }</pre>
 * Note that a "dummy-label" is added to the result merely to be able to use
 * {@link PodMetricDescription} when mapping the received JSON response to objects.
 *
 * @author M. Grzenia
 * @version 1.0
 */
@Configuration
public class PodMetricHistoryQuery {

  /**
   * The placeholder string to be replaced with the pod name to filter for.
   * The query result will only contain the pod that matches the corresponding pod name.
   */
  private static final String POD_FILTER = "<POD_FILTER>";
  /**
   * The placeholder string to be replaced with the namespace to filter for.
   * The query result will only contain pods that match the corresponding namespace.
   */
  private static final String NAMESPACE_FILTER = "<NAMESPACE_FILTER>";
  /**
   * The namespace for which to get the pod metrics.
   */
  @Value("${cluster.monitoring.namespace}")
  private String namespace;

  public String metricHistory(String podName, Metric metric) {
    return buildQueryTemplate(metric)
        .replace(NAMESPACE_FILTER, namespace)
        .replace(POD_FILTER, podName);
  }

  /**
   * Builds a template for the actual query string.
   * The template contains some placeholder strings that are to be replaces depending on the pod of
   * interest.
   *
   * @return The query string.
   */
  private String buildQueryTemplate(Metric metric) {
    return podByNamespaceAndNameQuery() +
        " * on(pod) group_right(labelName)" +
        "(" +
        " " + singleMetricQuery(metric.name(), metric.getQuery()) +
        ")";
  }

  /**
   * Builds a query string for retrieving a pod with a specific name in a specific namespace.
   * The query string is populated with placeholder strings that are intended to be replaced
   * depending on the pod of interest. Additionally, the query will result in the response's result
   * to contain a "dummy-label".
   *
   * @return The query string.
   */
  private String podByNamespaceAndNameQuery() {
    return String.format("" +
                             "max by (pod, labelName) (" +
                             "  label_replace(" +
                             "    kube_pod_labels{namespace='%s', pod='%s'}," +
                             "    'labelName', 'dummy-label', '', ''" +
                             "  )" +
                             ")", NAMESPACE_FILTER, POD_FILTER);
  }

  /**
   * Builds a query string for retrieving a single metric (e.g. cpu usage, memory usage, etc.) for
   * any pod.
   * The given metric name is added to a single metric's result so that the different types of
   * metrics can be distinguished from each other easily.
   *
   * @param metricName   The name (i.e. type) of the metric.
   * @param instantQuery The actual instant query which provides the metric value.
   * @return The query string.
   */
  private String singleMetricQuery(String metricName, String instantQuery) {
    return String.format("sum by (metricName, pod) ( label_replace( %s, 'metricName', '%s', '', '' ) )",
                         instantQuery,
                         metricName);
  }
}
