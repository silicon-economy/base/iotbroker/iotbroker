/**
 * The root package of the java-backend application of the IoT broker's management component.
 * Contains the main class.
 */
package org.siliconeconomy.iotbroker.management;