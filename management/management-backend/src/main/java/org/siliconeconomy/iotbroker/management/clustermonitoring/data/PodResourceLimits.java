/*
 * Copyright Open Logistics Foundation
 *
 * Licensed under the Open Logistics Foundation License 1.3.
 * For details on the licensing terms, see the LICENSE file.
 * SPDX-License-Identifier: OLFL-1.3
 */
package org.siliconeconomy.iotbroker.management.clustermonitoring.data;

import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;

/**
 * This DTO encapsulates the resource limits for which a pod is configured.
 *
 * @author M. Grzenia
 * @version 1.0
 */
@Getter
@Setter
@NoArgsConstructor
public class PodResourceLimits {

  /**
   * The number of CPU cores a pod is limited to.
   * May be {@code null} if a CPU core limit isn't configured.
   */
  private Double cpu;
  /**
   * The amount of memory (in bytes) a pod is limited to.
   * May be {@code null} if a memory limit isn't configured.
   */
  private Long memory;
}
