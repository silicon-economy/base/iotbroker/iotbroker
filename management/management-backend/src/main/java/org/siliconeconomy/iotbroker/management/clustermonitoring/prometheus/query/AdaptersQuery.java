/*
 * Copyright Open Logistics Foundation
 *
 * Licensed under the Open Logistics Foundation License 1.3.
 * For details on the licensing terms, see the LICENSE file.
 * SPDX-License-Identifier: OLFL-1.3
 */
package org.siliconeconomy.iotbroker.management.clustermonitoring.prometheus.query;

import org.springframework.beans.factory.annotation.Value;
import org.springframework.context.annotation.Configuration;

/**
 * Defines PromQL query strings for querying information about running adapter instances via the
 * Prometheus HTTP API.
 * For this, Prometheus' instant query endpoint is used.
 * <p>
 * The query is designed to return consistent result structures across all metrics. For a single
 * metric the result will look like this:
 * <pre>{@code
 * {
 *   "metric": {
 *     "adapterType": "<ADAPTER_TYPE>",
 *     "metricName": "<METRIC_NAME>",
 *     "pod": "<POD_NAME>"
 *   },
 *   "value": [
 *     <METRIC_TIMESTAMP>,
 *     "<METRIC_VALUE>"
 *   ]
 * }
 * }</pre>
 *
 * @author M. Grzenia
 * @version 1.0
 */
@Configuration
public class AdaptersQuery {

  /**
   * The regular expression for filtering all label values.
   */
  private static final String REGEX_ALL_LABELS = ".+";
  /**
   * The regular expression for filtering all adapter pods.
   */
  private static final String REGEX_ALL_ADAPTER_PODS = ".*adapter.*";
  /**
   * The label key used for pods not related to the infrastructure. (e.g. adapter pods)
   */
  @Value("${cluster.monitoring.pod.label.component}")
  private String componentLabelKey;
  /**
   * The namespace for which to get the pod information.
   */
  @Value("${cluster.monitoring.namespace}")
  private String namespace;

  /**
   * Returns the string for querying all running adapter instances.
   *
   * @return The query string.
   */
  public String adapterAllInstances() {
    return adapterAllInstancesQuery() +
        " * on(pod) group_right(adapterType)" +
        "(" +
        " " + singleMetricQuery(AdapterInfo.TIME_STARTED.name(), AdapterInfo.TIME_STARTED.getQuery()) +
        ")";
  }

  /**
   * Builds a query string for retrieving all adapter instances running in the IoT broker's cluster.
   *
   * @return The query string.
   */
  private String adapterAllInstancesQuery() {
    return String.format("" +
                             "max by (adapterType, pod) (" +
                             "  label_replace(" +
                             "    kube_pod_labels{%s=~'%s', namespace='%s', pod=~'%s'}," +
                             "    'adapterType', '$1', '%s', '(.*)'" +
                             "  )" +
                             ")",
                         componentLabelKey,
                         REGEX_ALL_LABELS,
                         namespace,
                         REGEX_ALL_ADAPTER_PODS,
                         componentLabelKey);
  }

  /**
   * Builds a query string for retrieving a single metric (e.g. creation time) for any adapter
   * instance.
   * Since the query string produced by {@link #adapterAllInstances()} will result in a response
   * from the Prometheus HTTP API that contains values for different types of metrics, the given
   * metric name is added to a single metric's result so that the different types of metrics can be
   * distinguished from each other easily.
   *
   * @param metricName   The name (i.e. type) of the metric.
   * @param instantQuery The actual instant query which provides the metric value.
   * @return The query string.
   */
  private String singleMetricQuery(String metricName, String instantQuery) {
    return String.format("sum by (metricName, pod) ( label_replace( %s, 'metricName', '%s', '', '' ) )",
                         instantQuery,
                         metricName);
  }
}
