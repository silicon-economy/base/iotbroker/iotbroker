/*
 * Copyright Open Logistics Foundation
 *
 * Licensed under the Open Logistics Foundation License 1.3.
 * For details on the licensing terms, see the LICENSE file.
 * SPDX-License-Identifier: OLFL-1.3
 */
package org.siliconeconomy.iotbroker.management.clustermonitoring.mapper;

import org.siliconeconomy.iotbroker.management.clustermonitoring.data.AdapterInformation;
import org.siliconeconomy.iotbroker.management.clustermonitoring.prometheus.query.AdapterInfo;
import org.siliconeconomy.iotbroker.management.clustermonitoring.prometheus.query.AdaptersQuery;
import org.siliconeconomy.iotbroker.management.clustermonitoring.prometheus.query.AdaptersResult;

import java.time.Instant;
import java.util.ArrayList;
import java.util.List;
import java.util.Map;
import java.util.stream.Collectors;

import static java.util.Objects.requireNonNull;

/**
 * Used to map results as received by the Prometheus HTTP API to {@link AdapterInformation}
 * instances.
 *
 * @author M. Grzenia
 * @version 1.0
 */
public class AdapterInformationMapper {

  /**
   * Maps a list of {@link AdaptersResult}s which contains entries for a specific adapter type as
   * received by a {@link AdaptersQuery} to a single {@link AdapterInformation} instance.
   * Each {@link AdaptersResult} represents a metric (with a value) for a specific adapter instance.
   *
   * @param results {@link AdaptersResult}s for a specific adapter type.
   * @return The results mapped to a single {@link AdapterInformation} instance.
   */
  public AdapterInformation map(List<AdaptersResult> results) {
    requireNonNull(results, "results");

    Map<String, List<AdaptersResult>> resultsByInstance = results.stream()
        .collect(Collectors.groupingBy(result -> result.getMetric().getPod()));

    List<AdapterInformation.AdapterInstance> adapterInstances = new ArrayList<>();

    // For every instance we want to map the different types of metrics
    for (Map.Entry<String, List<AdaptersResult>> entry : resultsByInstance.entrySet()) {
      Map<String, List<AdaptersResult>> metricsByName = entry.getValue().stream()
          .collect(Collectors.groupingBy(result -> result.getMetric().getMetricName()));

      AdapterInformation.AdapterInstance instance = new AdapterInformation.AdapterInstance();
      instance.setInstanceName(entry.getKey());
      instance.setTimeStarted(mapTimeStarted(metricsByName));
      adapterInstances.add(instance);
    }

    return new AdapterInformation()
        .setAdapterType(mapAdapterType(results))
        .setInstanceCount(adapterInstances.size())
        .setInstances(adapterInstances);
  }

  private String mapAdapterType(List<AdaptersResult> results) {
    if (results.isEmpty()) {
      return "";
    }

    return results.get(0).getMetric().getAdapterType();
  }

  private Instant mapTimeStarted(Map<String, List<AdaptersResult>> metricsByName) {
    if (!metricsByName.containsKey(AdapterInfo.TIME_STARTED.name())) {
      return Instant.ofEpochSecond(0);
    }

    return Instant.ofEpochSecond(
        (long) Double.parseDouble(
            metricsByName.get(AdapterInfo.TIME_STARTED.name()).get(0).getValue().get(1)
        )
    );
  }
}
