/*
 * Copyright Open Logistics Foundation
 *
 * Licensed under the Open Logistics Foundation License 1.3.
 * For details on the licensing terms, see the LICENSE file.
 * SPDX-License-Identifier: OLFL-1.3
 */
package org.siliconeconomy.iotbroker.management.clustermonitoring;

import org.siliconeconomy.iotbroker.management.clustermonitoring.data.AdapterInformation;
import org.siliconeconomy.iotbroker.management.clustermonitoring.data.PodMetricHistory;
import org.siliconeconomy.iotbroker.management.clustermonitoring.data.PodMetrics;
import org.siliconeconomy.iotbroker.management.clustermonitoring.prometheus.query.Metric;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.http.HttpStatus;
import org.springframework.web.bind.annotation.*;

import java.io.IOException;
import java.util.List;

/**
 * Provides REST endpoints for monitoring the IoT broker's kubernetes cluster.
 * This controller utilizes the Prometheus HTTP API to fetch the actual monitoring data, extracts
 * the data of interest and maps it to object types that correspond to the respective data.
 *
 * @author M. Grzenia
 * @version 1.0
 */
@RestController
@RequestMapping("/cluster/monitoring")
public class ClusterMonitoringController {

  /**
   * Used to query monitoring information about the IoT broker's cluster.
   */
  private final ClusterMonitoringService clusterMonitoringService;
  /**
   * The namespace to use for the Prometheus HTTP queries.
   */
  @Value("${cluster.monitoring.namespace}")
  private String namespace;

  @Autowired
  public ClusterMonitoringController(ClusterMonitoringService clusterMonitoringService) {
    this.clusterMonitoringService = clusterMonitoringService;
  }

  /**
   * Returns metrics for all pods running in the IoT broker's cluster.
   *
   * @return A list of metrics for all pods running in the IoT broker's cluster.
   * @throws IOException If the JSON response received from the Prometheus HTTP API could not be
   *                     parsed to JSON.
   */
  @GetMapping("pods")
  @ResponseStatus(HttpStatus.OK)
  public List<PodMetrics> getPods()
      throws IOException {
    return clusterMonitoringService.getPodMetrics();
  }

  /**
   * Returns the metrics for a the pod with the given name running in the IoT broker's cluster.
   *
   * @param name The name of the pod to get the metrics for.
   * @return The pod metrics.
   * @throws IOException If the JSON response received from the Prometheus HTTP API could not be
   *                     parsed to JSON.
   */
  @GetMapping("pods/{name}")
  @ResponseStatus(HttpStatus.OK)
  public PodMetrics getPod(@PathVariable("name") String name)
      throws IOException {
    return clusterMonitoringService.getPodMetricsByPodName(name);
  }

  /**
   * Returns information about adapters running in the IoT broker's cluster.
   *
   * @return A list of information about adapters running in the IoT broker's cluster.
   * @throws IOException If the JSON response received from the Prometheus HTTP API could not be
   *                     parsed to JSON.
   */
  @GetMapping("adapters")
  @ResponseStatus(HttpStatus.OK)
  public List<AdapterInformation> getAdapterInformation()
      throws IOException {
    return clusterMonitoringService.getAdapterInformation();
  }

  /**
   * Returns values for the cpu usage of the pod with the given name within the given range.
   *
   * @param name      The name of the pod.
   * @param range     The range (in seconds) for which values are to be returned. The range is
   *                  calculated based on the current time.
   * @param stepWidth The step width (i.e. resolution) to use for the values.
   * @return The history values.
   * @throws IOException If the JSON response received from the Prometheus HTTP API could not be
   *                     parsed to JSON.
   */
  @GetMapping("pods/{name}/history/cpuUsage")
  @ResponseStatus(HttpStatus.OK)
  public PodMetricHistory getCpuUsageHistory(@PathVariable("name") String name,
                                             @RequestParam("range") int range,
                                             @RequestParam("stepWidth") int stepWidth)
      throws IOException {
    return clusterMonitoringService.getPodMetricHistory(name, Metric.CPU_USAGE, range, stepWidth);
  }

  /**
   * Returns values for the memory usage of the pod with the given name within the given range.
   *
   * @param name      The name of the pod.
   * @param range     The range (in seconds) for which values are to be returned. The range is
   *                  calculated based on the current time.
   * @param stepWidth The step width (i.e. resolution) to use for the values.
   * @return The history values.
   * @throws IOException If the JSON response received from the Prometheus HTTP API could not be
   *                     parsed to JSON.
   */
  @GetMapping("pods/{name}/history/memoryUsage")
  @ResponseStatus(HttpStatus.OK)
  public PodMetricHistory getMemoryUsageHistory(@PathVariable("name") String name,
                                                @RequestParam("range") int range,
                                                @RequestParam("stepWidth") int stepWidth)
      throws IOException {
    return clusterMonitoringService.getPodMetricHistory(name,
                                                        Metric.MEMORY_USAGE,
                                                        range,
                                                        stepWidth);
  }

  /**
   * Returns values for the bandwidth of incoming traffic of the pod with the given name within the
   * given range.
   *
   * @param name      The name of the pod.
   * @param range     The range (in seconds) for which values are to be returned. The range is
   *                  calculated based on the current time.
   * @param stepWidth The step width (i.e. resolution) to use for the values.
   * @return The history values.
   * @throws IOException If the JSON response received from the Prometheus HTTP API could not be
   *                     parsed to JSON.
   */
  @GetMapping("pods/{name}/history/receiveBandwidth")
  @ResponseStatus(HttpStatus.OK)
  public PodMetricHistory getReceiveBandwidthHistory(@PathVariable("name") String name,
                                                     @RequestParam("range") int range,
                                                     @RequestParam("stepWidth") int stepWidth)
      throws IOException {
    return clusterMonitoringService.getPodMetricHistory(name,
                                                        Metric.BANDWIDTH_RX,
                                                        range,
                                                        stepWidth);
  }

  /**
   * Returns values for the bandwidth of outgoing traffic of the pod with the given name within the
   * given range.
   *
   * @param name      The name of the pod.
   * @param range     The range (in seconds) for which values are to be returned. The range is
   *                  calculated based on the current time.
   * @param stepWidth The step width (i.e. resolution) to use for the values.
   * @return The history values.
   * @throws IOException If the JSON response received from the Prometheus HTTP API could not be
   *                     parsed to JSON.
   */
  @GetMapping("pods/{name}/history/transmitBandwidth")
  @ResponseStatus(HttpStatus.OK)
  public PodMetricHistory getTransmitBandwidthHistory(@PathVariable("name") String name,
                                                      @RequestParam("range") int range,
                                                      @RequestParam("stepWidth") int stepWidth)
      throws IOException {
    return clusterMonitoringService.getPodMetricHistory(name,
                                                        Metric.BANDWIDTH_TX,
                                                        range,
                                                        stepWidth);
  }
}
