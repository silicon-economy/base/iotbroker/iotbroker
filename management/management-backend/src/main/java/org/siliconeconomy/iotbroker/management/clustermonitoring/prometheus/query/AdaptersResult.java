/*
 * Copyright Open Logistics Foundation
 *
 * Licensed under the Open Logistics Foundation License 1.3.
 * For details on the licensing terms, see the LICENSE file.
 * SPDX-License-Identifier: OLFL-1.3
 */
package org.siliconeconomy.iotbroker.management.clustermonitoring.prometheus.query;

import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;

import java.util.List;

/**
 * This DTO represents a single result entry for the queries provided by {@link AdaptersQuery} as received by the
 * Prometheus HTTP API.
 *
 * @author M. Grzenia
 * @version 1.0
 */
@Getter
@Setter
@NoArgsConstructor
public class AdaptersResult {

  /**
   * The metric property of a single result entry.
   * Contains information about a specific adapter instance.
   */
  private Metric metric;
  /**
   * The actual metric value.
   * The list contains two elements:
   * <ul>
   * <li>First element: The unix timestamp (that is to be parsed as a double)</li>
   * <li>Second element: The actual value (parsing depends on the metric type)</li>
   * </ul>
   */
  private List<String> value;

  /**
   * Represents the {@code metric} property of a single result entry.
   */
  @Getter
  @Setter
  @NoArgsConstructor
  public static class Metric {

    /**
     * The adapter type.
     */
    private String adapterType;
    /**
     * The name describing the metric.
     */
    private String metricName;
    /**
     * The name of the pod (i.e. the name of the adapter instance).
     */
    private String pod;
  }
}
