/*
 * Copyright Open Logistics Foundation
 *
 * Licensed under the Open Logistics Foundation License 1.3.
 * For details on the licensing terms, see the LICENSE file.
 * SPDX-License-Identifier: OLFL-1.3
 */
package org.siliconeconomy.iotbroker.management.clustermonitoring;

/**
 * Thrown to indicate that a response received from the Prometheus HTTP API is invalid due to either an incorrect
 * format or incorrect content.
 *
 * @author M. Grzenia
 * @version 1.0
 */
public class InvalidPrometheusResponeException
    extends RuntimeException {

  public InvalidPrometheusResponeException(String message) {
    super(message);
  }
}
