/*
 * Copyright Open Logistics Foundation
 *
 * Licensed under the Open Logistics Foundation License 1.3.
 * For details on the licensing terms, see the LICENSE file.
 * SPDX-License-Identifier: OLFL-1.3
 */
package org.siliconeconomy.iotbroker.management.config;

import okhttp3.OkHttpClient;
import org.siliconeconomy.iotbroker.management.clustermonitoring.prometheus.PrometheusClient;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;

import javax.net.ssl.SSLContext;
import javax.net.ssl.SSLSocketFactory;
import javax.net.ssl.TrustManager;
import javax.net.ssl.X509TrustManager;
import java.security.SecureRandom;
import java.security.cert.CertificateException;
import java.security.cert.X509Certificate;

/**
 * Configuration of the {@link OkHttpClient} that is used to call the Prometheus HTTP API.
 *
 * @author M. Grzenia
 * @version 1.0
 * @see PrometheusClient
 */
@Configuration
public class OkHttpClientConfig {

  @Bean
  public OkHttpClient okHttpClient() {
    return createUnsafeOkHttpClient();
  }

  /**
   * Creates a {@link OkHttpClient} instance that doesn't validate any certificates and therefore
   * accepts self-signed certificates.
   * <p>
   * Reason for suppressing all warnings:
   * <p>
   * The Prometheus HTTP API is accessible via two URLs:
   * <ul>
   * <li>A public URL that is backed by a load balancer</li>
   * <li>A cluster-internal URL that is accessible only for components running within the K8s
   * cluster</li>
   * </ul>
   * Since this backend is running within the K8s cluster, using the public URL would lead to
   * unnecessary overhead. In order to avoid this overhead it is desirable to use the
   * cluster-internal URL. However, this URL is secured by an ephemeral certificate that is being
   * generated and signed by K8s itself and that might be re-generated over time. For this reason,
   * calls to the cluster-internal URL would fail, since self-signed certificates are usually
   * rejected by default.
   * <p>
   * To be able to use the cluster-internal URL, it was decided that for calls from one service
   * within the K8s cluster to another, it is acceptable to use a HTTP client that doesn't validate
   * any certificates.
   *
   * @return
   */
  @SuppressWarnings("all")
  private OkHttpClient createUnsafeOkHttpClient() {
    try {
      // Create a trust manager that doesn't validate certificate chains
      TrustManager[] trustAllCerts = new TrustManager[]{new X509TrustManager() {
        @Override
        public void checkClientTrusted(X509Certificate[] chain, String authType)
            throws CertificateException {
        }

        @Override
        public void checkServerTrusted(X509Certificate[] chain, String authType)
            throws CertificateException {
        }

        @Override
        public X509Certificate[] getAcceptedIssuers() {
          return new X509Certificate[0];
        }
      }};

      // Install the all-trusting trust manager
      SSLContext sslContext = SSLContext.getInstance("SSL");
      sslContext.init(null, trustAllCerts, new SecureRandom());
      // Create an ssl socket factory with the all-trusting manager
      final SSLSocketFactory sslSocketFactory = sslContext.getSocketFactory();

      return new OkHttpClient.Builder()
          .sslSocketFactory(sslSocketFactory, (X509TrustManager) trustAllCerts[0])
          .hostnameVerifier((hostname, session) -> true)
          .build();
    } catch (Exception e) {
      throw new RuntimeException(e);
    }
  }
}
