/*
 * Copyright Open Logistics Foundation
 *
 * Licensed under the Open Logistics Foundation License 1.3.
 * For details on the licensing terms, see the LICENSE file.
 * SPDX-License-Identifier: OLFL-1.3
 */
package org.siliconeconomy.iotbroker.management.clustermonitoring.prometheus.query;

import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;

import java.util.List;

/**
 * This DTO represents the result of a single metric (e.g. cpu usage or receive bandwidth) for a
 * single pod as received by the Prometheus HTTP API. The results of the queries provided by
 * {@link PodMetricsQuery} are mapped to instances of this class.
 *
 * @author M. Grzenia
 * @version 1.0
 */
@Getter
@Setter
@NoArgsConstructor
public class PodMetricsResult {

  /**
   * A description of the metric.
   */
  private PodMetricDescription metric;
  /**
   * The actual metric value.
   * The list contains two elements:
   * <ul>
   * <li>First element: The unix timestamp (that is to be parsed as a double)</li>
   * <li>Second element: The actual value (parsing depends on the metric type)</li>
   * </ul>
   */
  private List<String> value;
}
