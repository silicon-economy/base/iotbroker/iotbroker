/**
 * @license
 * Copyright Open Logistics Foundation
 *
 * Licensed under the Open Logistics Foundation License 1.3.
 * For details on the licensing terms, see the LICENSE file.
 * SPDX-License-Identifier: OLFL-1.3
 */

import { Component, Injectable } from '@angular/core';
import { ConnectedPosition, Overlay, OverlayRef } from '@angular/cdk/overlay';
import { ElementWatcher, ElementWatcherService } from 'src/app/core/services/element-watcher.service';
import { ComponentPortal } from '@angular/cdk/portal';

/**
 * Service for creating blocking Overlays over certain HTML Elements.
 * Use setup() before calling startLoadingAnimation()
 * Use stopLeadingAnimation() for shutting down the overlay
 * Call cleanup() within ngDestroy() to ensure the cleanup of references
 */
@Injectable({
  providedIn: 'root'
})
export class OverlayService {

  loadingAnimOverlayRef: OverlayRef;
  watcherRef: ElementWatcher;
  elementRef: HTMLElement;

  constructor(public overlay: Overlay, private watcherService: ElementWatcherService) {
  }

  /**
   * Sets the HTMLElement for the overlay
   * @param element HTMLElement to use
   */
  setup(element: HTMLElement) {
    this.elementRef = element;
  }

  /**
   * Internal Method for creating the overlay
   */
  _initOverlay() {
    // Only one Overlay should be active at a given time, otherwise the reference is lost and can't be canceled
    this.cleanup();
    this.loadingAnimOverlayRef = this.overlay.create({
      hasBackdrop: false,
      height: this.elementRef.clientHeight,
      width: this.elementRef.clientWidth,
      panelClass: 'loading-animation-backdrop',
      scrollStrategy: this.overlay.scrollStrategies.reposition({ scrollThrottle: 10 }),
      positionStrategy: this.overlay.position()
          .flexibleConnectedTo(this.elementRef)
          .withPositions([{
            originX: 'start',
            originY: 'top',
            overlayX: 'start',
            overlayY: 'top'
          } as ConnectedPosition])
          .withPush(false)
    });
    this.watcherRef = this.watcherService.createWatcher(this.elementRef, 0);
    this.watcherRef.change$.subscribe(() => {
      this._repositionOverlay();
    });
  }

  /**
   * Internal method for repositioning the overlay after a change is detected
   */
  _repositionOverlay() {
    if (this.loadingAnimOverlayRef?.hasAttached()) {
      this.loadingAnimOverlayRef.updateSize({
        height: this.elementRef.clientHeight,
        width: this.elementRef.clientWidth,
      });
      this.loadingAnimOverlayRef.updatePosition();
    }
  }

  /**
   * Starts the Overlay and Spinner Animation
   * Use only after a first call to setup()
   */
  startLoadingAnimation() {
    this._initOverlay();
    this.loadingAnimOverlayRef.attach(new ComponentPortal(CustomSpinnerComponent));
  }

  /**
   * Shuts down the overlay and cleans up overlay references
   */
  stopLoadingAnimation() {
    this.loadingAnimOverlayRef?.detach();
    this.loadingAnimOverlayRef?.dispose();
  }

  /**
   * Call within ngDestroy() to stop the overlay and dispose remaining references on component switch
   */
  cleanup() {
    this.stopLoadingAnimation();
    this.watcherRef?.dispose();
  }
}

/**
 * Custom component for the mat-spinner within the overlay
 */
@Component({
  selector: 'app-custom-spinner',
  template: '<mat-spinner></mat-spinner>',
  styles: [
    ':host {width: inherit; height: inherit}',
    'mat-spinner {top: calc(50% - 50px); left: calc(50% - 50px)}'
  ]
})
export class CustomSpinnerComponent {
}
