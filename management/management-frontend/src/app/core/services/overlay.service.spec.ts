/**
 * @license
 * Copyright Open Logistics Foundation
 *
 * Licensed under the Open Logistics Foundation License 1.3.
 * For details on the licensing terms, see the LICENSE file.
 * SPDX-License-Identifier: OLFL-1.3
 */

import { TestBed } from '@angular/core/testing';

import { OverlayService } from 'src/app/core/services/overlay.service';
import { ElementWatcherService } from 'src/app/core/services/element-watcher.service';
import { Overlay } from '@angular/cdk/overlay';
import { of } from 'rxjs';
import createSpyObj = jasmine.createSpyObj;

describe('OverlayService', () => {
  let service: OverlayService;
  const spyElementService = jasmine.createSpyObj('ElementRulerService', ['createWatcher']);
  const spyOverlay = jasmine.createSpyObj('Overlay', ['create', 'position', 'flexibleConnectedTo', 'withPositions', 'withPush']);


  beforeEach(() => {
    TestBed.configureTestingModule({
      providers: [
        OverlayService,
        { provide: ElementWatcherService, useValue: spyElementService },
        { provide: Overlay, useValue: spyOverlay }
      ]
    });
    service = TestBed.inject(OverlayService);
    service.overlay.scrollStrategies = jasmine.createSpyObj(['reposition']);
    spyOverlay.position.and.returnValue(spyOverlay);
    spyOverlay.flexibleConnectedTo.and.returnValue(spyOverlay);
    spyOverlay.withPositions.and.returnValue(spyOverlay);
    spyElementService.createWatcher.and.returnValue({
      dispose: () => {
        return;
      },
      change$: of({ width: 0, height: 0, posX: 0, posY: 0 })
    });
  });

  it('should be created', () => {
    expect(service).toBeTruthy();
  });

  it('should set element', () => {
    const element = document.createElement('div') as HTMLElement;
    service.setup(element);
    expect(service.elementRef).toEqual(element);
  });

  it('should call initializing methods', () => {
    const element = document.createElement('div') as HTMLElement;
    service.setup(element);
    service._initOverlay();
    expect(spyElementService.createWatcher).toHaveBeenCalled();
    expect(spyOverlay.position).toHaveBeenCalled();
    expect(spyOverlay.flexibleConnectedTo).toHaveBeenCalled();
    expect(spyOverlay.withPositions).toHaveBeenCalled();
  });

  it('should call stopping methods', () => {
    spyOn(service, 'stopLoadingAnimation').and.callThrough();
    service.loadingAnimOverlayRef = createSpyObj('loadingAnimOverlayRef', ['dispose', 'detach']);
    service.cleanup();
    expect(service.loadingAnimOverlayRef.dispose).toHaveBeenCalled();
    expect(service.loadingAnimOverlayRef.detach).toHaveBeenCalled();
    expect(service.stopLoadingAnimation).toHaveBeenCalled();
  });

  it('should call start methods', () => {
    spyOn(service, '_initOverlay');
    service.loadingAnimOverlayRef = createSpyObj('loadingAnimOverlayRef', ['attach']);
    service.startLoadingAnimation();
    expect(service.loadingAnimOverlayRef.attach).toHaveBeenCalled();
    expect(service._initOverlay).toHaveBeenCalled();
  });

});
