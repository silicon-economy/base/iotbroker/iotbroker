/**
 * @license
 * Copyright Open Logistics Foundation
 *
 * Licensed under the Open Logistics Foundation License 1.3.
 * For details on the licensing terms, see the LICENSE file.
 * SPDX-License-Identifier: OLFL-1.3
 */

import { TestBed } from '@angular/core/testing';

import { ElementWatcher, ElementWatcherService } from 'src/app/core/services/element-watcher.service';
import { NgZone } from '@angular/core';

describe('ElementRulerService', () => {
  let service: ElementWatcherService;


  beforeEach(() => {
    TestBed.configureTestingModule({
      providers: []
    });
    service = TestBed.inject(ElementWatcherService);
    const ngZone = TestBed.inject(NgZone);
    spyOn(ngZone, 'runOutsideAngular').and.callFake((fn: Function) => fn());


  });

  it('should be created', () => {
    expect(service).toBeTruthy();
  });

  it('should create an ElementWatcher', () => {
    const element = document.createElement('div') as HTMLElement;
    element.setAttribute('clientHeight', '0');
    element.setAttribute('clientHeight', '0');
    const watcher: ElementWatcher = service.createWatcher(element, 0);
    const watcher2: ElementWatcher = service.createWatcher(element, 50);
    expect(watcher).toBeTruthy();
    expect(watcher2).toBeTruthy();
  });
});
