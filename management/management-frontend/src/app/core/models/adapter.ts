/**
 * @license
 * Copyright Open Logistics Foundation
 *
 * Licensed under the Open Logistics Foundation License 1.3.
 * For details on the licensing terms, see the LICENSE file.
 * SPDX-License-Identifier: OLFL-1.3
 */

import { AdapterInstance } from 'src/app/core/models/adapter-instance';

export class Adapter {
  adapterType: string;
  instanceCount: number;
  instances: AdapterInstance[];
}
