/**
 * @license
 * Copyright Open Logistics Foundation
 *
 * Licensed under the Open Logistics Foundation License 1.3.
 * For details on the licensing terms, see the LICENSE file.
 * SPDX-License-Identifier: OLFL-1.3
 */

export class DeviceInstance {
  id?: string;
  source: string;
  tenant: string;
  deviceTypeIdentifier: string;
  registrationTime: string;
  lastSeen: string;
  enabled: boolean;
  description: string;
  hardwareRevision: string;
  firmwareVersion: string;
}
