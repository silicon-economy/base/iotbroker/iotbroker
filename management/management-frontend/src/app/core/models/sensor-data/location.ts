/**
 * @license
 * Copyright Open Logistics Foundation
 *
 * Licensed under the Open Logistics Foundation License 1.3.
 * For details on the licensing terms, see the LICENSE file.
 * SPDX-License-Identifier: OLFL-1.3
 */

import { LatitudeLongitudeDetails } from 'src/app/core/models/sensor-data/latitude-longitude-details';
import { MapCodeDetails } from 'src/app/core/models/sensor-data/map-code-details';
import { SimpleDetails } from 'src/app/core/models/sensor-data/simple-details';
import { LocationEncodingType } from 'src/app/core/models/sensor-data/location-encoding-type.enum';

export class Location {
  name: string;
  description: string;
  encodingType: LocationEncodingType;
  details: LatitudeLongitudeDetails | MapCodeDetails | SimpleDetails;
}
