/**
 * @license
 * Copyright Open Logistics Foundation
 *
 * Licensed under the Open Logistics Foundation License 1.3.
 * For details on the licensing terms, see the LICENSE file.
 * SPDX-License-Identifier: OLFL-1.3
 */

import { Datastream } from 'src/app/core/models/sensor-data/datastream';
import { Location } from 'src/app/core/models/sensor-data/location';

export class SensorDataMessage {
  id: string;
  originId: string;
  source: string;
  tenant: string;
  timestamp: string;
  location: Location;
  datastreams: Datastream[];

  constructor(obj) {
    Object.assign(this, obj);
  }
}
