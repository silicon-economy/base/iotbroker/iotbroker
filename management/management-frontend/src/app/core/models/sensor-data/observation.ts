/**
 * @license
 * Copyright Open Logistics Foundation
 *
 * Licensed under the Open Logistics Foundation License 1.3.
 * For details on the licensing terms, see the LICENSE file.
 * SPDX-License-Identifier: OLFL-1.3
 */

import { Location } from 'src/app/core/models/sensor-data/location';

export class Observation {
  timestamp: string;
  result: any;
  location: Location;
  parameters: Map<string, string>;
}
