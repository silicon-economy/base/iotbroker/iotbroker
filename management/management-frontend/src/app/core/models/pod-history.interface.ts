/**
 * @license
 * Copyright Open Logistics Foundation
 *
 * Licensed under the Open Logistics Foundation License 1.3.
 * For details on the licensing terms, see the LICENSE file.
 * SPDX-License-Identifier: OLFL-1.3
 */

import { PodHistoryValue } from 'src/app/core/models/pod-history-value.interface';

export interface PodHistory {
  name: string;
  values: PodHistoryValue[];
}
