/**
 * @license
 * Copyright Open Logistics Foundation
 *
 * Licensed under the Open Logistics Foundation License 1.3.
 * For details on the licensing terms, see the LICENSE file.
 * SPDX-License-Identifier: OLFL-1.3
 */

export class DeviceType {
  id?: string;
  source: string;
  identifier: string;
  providedBy: string[];
  description: string;
  enabled: boolean;
  autoRegisterDeviceInstances: boolean;
  autoEnableDeviceInstances: boolean;
}
