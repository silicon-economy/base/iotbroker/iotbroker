/**
 * @license
 * Copyright Open Logistics Foundation
 *
 * Licensed under the Open Logistics Foundation License 1.3.
 * For details on the licensing terms, see the LICENSE file.
 * SPDX-License-Identifier: OLFL-1.3
 */

import { UntypedFormControl, ValidationErrors } from '@angular/forms';

/*
 * Validator to check if a string consists of only whitespace or is empty.
 */
export function isNotEffectivelyEmptyValidator(control: UntypedFormControl): ValidationErrors | null {
    const isValid = (control.value || '').trim().length > 0;
    return isValid ? null : { 'emptyOrOnlyWhitespace': true};
}
