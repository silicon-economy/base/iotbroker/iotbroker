/**
 * @license
 * Copyright Open Logistics Foundation
 *
 * Licensed under the Open Logistics Foundation License 1.3.
 * For details on the licensing terms, see the LICENSE file.
 * SPDX-License-Identifier: OLFL-1.3
 */

import { Injectable } from '@angular/core';
import { MatPaginatorIntl } from '@angular/material/paginator';
import { Subject } from 'rxjs';
import { TranslateService } from '@ngx-translate/core';

@Injectable()
export class PaginatorIntl implements MatPaginatorIntl {

  readonly changes = new Subject<void>();

  rangeLabel = '';
  itemsPerPageLabel = '';
  nextPageLabel = '';
  previousPageLabel = '';
  firstPageLabel = '';
  lastPageLabel = '';


  constructor(private translate: TranslateService) {
    this.translate.stream('paginator.items_per_page').subscribe(text => {
      this.itemsPerPageLabel = text;
      this.changes.next();
    });
    this.translate.stream('paginator.first_page').subscribe(text => {
      this.firstPageLabel = text;
      this.changes.next();
    });
    this.translate.stream('paginator.last_page').subscribe(text => {
      this.lastPageLabel = text;
      this.changes.next();
    });
    this.translate.stream('paginator.next_page').subscribe(text => {
      this.nextPageLabel = text;
      this.changes.next();
    });
    this.translate.stream('paginator.previous_page').subscribe(text => {
      this.previousPageLabel = text;
      this.changes.next();
    });
    this.translate.stream('paginator.range_label').subscribe(text => {
      this.rangeLabel = text;
      this.changes.next();
    });
  }


  getRangeLabel(page: number, pageSize: number, length: number): string {
    if (length === 0) {
      return this.rangeLabel.replace('{{i}}', '1').replace('{{j}}', '1');
    }
    const amountPages = Math.ceil(length / pageSize);
    return this.rangeLabel.replace('{{i}}', '' + (page + 1)).replace('{{j}}', '' + amountPages);
  }
}
