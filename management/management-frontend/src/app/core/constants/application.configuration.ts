/**
 * @license
 * Copyright Open Logistics Foundation
 *
 * Licensed under the Open Logistics Foundation License 1.3.
 * For details on the licensing terms, see the LICENSE file.
 * SPDX-License-Identifier: OLFL-1.3
 */

/*
 * Duration of snackbars shown after errors occur
 */
export const SNACKBAR_ERROR_DURATION = 5000;
