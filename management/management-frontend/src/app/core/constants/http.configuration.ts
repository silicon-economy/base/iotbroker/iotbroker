/**
 * @license
 * Copyright Open Logistics Foundation
 *
 * Licensed under the Open Logistics Foundation License 1.3.
 * For details on the licensing terms, see the LICENSE file.
 * SPDX-License-Identifier: OLFL-1.3
 */

import { HttpHeaders } from '@angular/common/http';

export const JSON_HEADER = new HttpHeaders({ 'Content-Type': 'application/json' });
export const HTTP_OPTIONS_JSON_HEADER = { headers: JSON_HEADER };
