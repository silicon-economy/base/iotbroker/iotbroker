/**
 * @license
 * Copyright Open Logistics Foundation
 *
 * Licensed under the Open Logistics Foundation License 1.3.
 * For details on the licensing terms, see the LICENSE file.
 * SPDX-License-Identifier: OLFL-1.3
 */

import { NgModule } from '@angular/core';
import { RouterModule, Routes } from '@angular/router';
import { PrivacyComponent } from 'src/app/features/privacy/privacy.component';
import { AdapterOverviewComponent } from 'src/app/features/adapter-overview/adapter-overview.component';
import { LoadPageComponent } from 'src/app/features/load-page/load-page.component';
import { DeviceInstanceOverviewComponent } from 'src/app/features/devices/pages/device-instance-overview/device-instance-overview.component';
import { LiveMessageDisplayComponent } from 'src/app/features/device-messages/pages/live-message-display/live-message-display.component';
import { WelcomePageComponent } from 'src/app/features/welcome-page/welcome-page.component';
import { ImprintLegalComponent } from 'src/app/features/imprint-legal/imprint-legal.component';
import { ErrorComponent } from 'src/app/core/components/error/error.component';
import { DeviceTypeOverviewComponent } from 'src/app/features/devices/pages/device-type-overview/device-type-overview.component';
import { HistoryMessageDisplayComponent } from 'src/app/features/device-messages/pages/history-message-display/history-message-display.component';

const routes: Routes = [
  { path: '', pathMatch: 'full', redirectTo: 'home' },
  { path: 'adapter-overview', component: AdapterOverviewComponent },
  { path: 'load-overview', component: LoadPageComponent },
  { path: 'devices/instances', component: DeviceInstanceOverviewComponent },
  { path: 'devices/types', component: DeviceTypeOverviewComponent },
  { path: 'live-message-display', component: LiveMessageDisplayComponent },
  { path: 'history-message-display', component: HistoryMessageDisplayComponent },
  { path: 'home', component: WelcomePageComponent },
  { path: 'privacy', component: PrivacyComponent },
  { path: 'imprint-legal', component: ImprintLegalComponent },
  { path: '**', component: ErrorComponent },
];

@NgModule({
  imports: [RouterModule.forRoot(routes)],
  exports: [RouterModule]
})
export class AppRoutingModule {
}
