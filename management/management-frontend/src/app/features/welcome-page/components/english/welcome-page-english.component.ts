/**
 * @license
 * Copyright Open Logistics Foundation
 *
 * Licensed under the Open Logistics Foundation License 1.3.
 * For details on the licensing terms, see the LICENSE file.
 * SPDX-License-Identifier: OLFL-1.3
 */

import { Component } from '@angular/core';

@Component({
  selector: 'app-welcome-page-english',
  templateUrl: 'welcome-page-english.component.html',
  styleUrls: ['../../welcome-page.component.scss']
})
export class WelcomePageEnglishComponent {
}
