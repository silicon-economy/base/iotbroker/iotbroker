/**
 * @license
 * Copyright Open Logistics Foundation
 *
 * Licensed under the Open Logistics Foundation License 1.3.
 * For details on the licensing terms, see the LICENSE file.
 * SPDX-License-Identifier: OLFL-1.3
 */

import { Component, OnInit, ViewChild } from '@angular/core';
import { MatDialogRef } from '@angular/material/dialog';
import { LiveMessageDisplayComponent } from 'src/app/features/device-messages/pages/live-message-display/live-message-display.component';
import { MatSnackBar } from '@angular/material/snack-bar';
import { SubscriptionService } from 'src/app/features/device-messages/services/subscription.service';
import { DeviceInstanceSelectionComponent } from 'src/app/shared/components/device-instance-selection/device-instance-selection.component';
import { DeviceInstanceService } from 'src/app/features/devices/services/device-instance.service';
import { SNACKBAR_ERROR_DURATION } from 'src/app/core/constants/application.configuration';

@Component({
  selector: 'app-topic-subscription',
  templateUrl: './topic-subscription.component.html',
  styleUrls: ['./topic-subscription.component.scss']
})
export class TopicSubscriptionComponent implements OnInit {

  @ViewChild(DeviceInstanceSelectionComponent) topicSelection: DeviceInstanceSelectionComponent;

  // subscriptions
  public topics: string[] = [];

  constructor(private subscriptionService: SubscriptionService,
              private deviceService: DeviceInstanceService,
              public dialogRef: MatDialogRef<LiveMessageDisplayComponent>,
              private snackBar: MatSnackBar) {
  }

  /**
   * loads sources and active subscriptions
   */
  public ngOnInit(): void {
    this.showSubscriptions();
  }

  private showSubscriptions(): void {
    this.topics = this.subscriptionService.getTopics();
  }

  /**
   * exit the dialog
   */
  public exit(): void {
    this.dialogRef.close(false);
  }

  /**
   * subscribe to a topic through the {@link SubscriptionService}
   * updates {@link topics}
   * this method is called, when the submit button in {@link topicSelection} is clicked
   */
  public subscribeToTopic(): void {
    if(this.topicSelection.elementsAreValid()) {
      const tenant = this.topicSelection.getTenant().value;
      const source = this.topicSelection.getSource().value;

      const result = this.subscriptionService.subscribeToTopic(source, tenant);

      if (result === null) {
        this.snackBar.open('This topic already exists.', 'Close', {
          duration: SNACKBAR_ERROR_DURATION,
        });
      } else {
        this.topics.push(result);
      }
    }
  }


  /**
   * unsubscribe to a topic through the {@link SubscriptionService}
   * updated {@link topics}
   */
  public unsubscribe(topic: string): void {
    this.subscriptionService.unsubscribe(topic);
    const index = this.topics.indexOf(topic);
    if (index > -1) {
      this.topics.splice(index, 1);
    }
  }

}
