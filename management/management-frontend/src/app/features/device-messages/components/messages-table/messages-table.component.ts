/**
 * @license
 * Copyright Open Logistics Foundation
 *
 * Licensed under the Open Logistics Foundation License 1.3.
 * For details on the licensing terms, see the LICENSE file.
 * SPDX-License-Identifier: OLFL-1.3
 */

import { Component, ViewChild } from '@angular/core';
import { MatSort } from '@angular/material/sort';
import { MatPaginator } from '@angular/material/paginator';
import { ExpandableTableComponent } from 'src/app/shared/components/expandable-table/expandable-table.component';
import { TableSensorDataMessage } from 'src/app/features/device-messages/models/table-sensor-data-message';
import { TableColumnConfig } from 'src/app/shared/components/expandable-table/models/table-column-config';

@Component({
  selector: 'app-messages-table',
  templateUrl: './messages-table.component.html',
  styleUrls: ['./messages-table.component.scss']
})
export class MessagesTableComponent {

  @ViewChild(MatSort) sort: MatSort;
  @ViewChild(MatPaginator) paginator: MatPaginator;
  @ViewChild(ExpandableTableComponent) table: ExpandableTableComponent<TableSensorDataMessage>;

  // the attributes of the table objects that are displayed
  public tableColumnConfig: TableColumnConfig[] = [
    new TableColumnConfig('source', 'historymessagedisplay.th_source'),
    new TableColumnConfig('tenant', 'historymessagedisplay.th_tenant'),
    new TableColumnConfig('timestamp', 'historymessagedisplay.th_timestamp'),
    new TableColumnConfig('datastreams', 'historymessagedisplay.th_datastreams'),
    new TableColumnConfig('observations', 'historymessagedisplay.th_observations'),
  ];

  public addMessages(messages): void {
    for (const message of messages) {
      this.table.addElementToBeginning(TableSensorDataMessage.transformSensorDataMessageIntoTableMessage(message));
    }
  }

  public clearMessages(): void {
    this.table.clearElements();
  }

}
