/**
 * @license
 * Copyright Open Logistics Foundation
 *
 * Licensed under the Open Logistics Foundation License 1.3.
 * For details on the licensing terms, see the LICENSE file.
 * SPDX-License-Identifier: OLFL-1.3
 */

import { Injectable } from '@angular/core';
import { Observable } from 'rxjs';
import { HttpClient } from '@angular/common/http';
import { environment } from 'src/app/environments/environment';

@Injectable({
  providedIn: 'root'
})
export class SensorDataService {

  private static readonly SENSOR_DATA_HISTORY_SERVICE_URL = `${environment.sensorDataHistoryServiceUrl}`;

  constructor(private http: HttpClient) { }

  /**
   * This method retrieves the latest message of a specific device
   * @param source The source of the device
   * @param tenant The tenant of the device
   * @return The latest device message as an observable
   */
  getLatestDeviceMessage(source, tenant): Observable<object> {
    const url = SensorDataService.SENSOR_DATA_HISTORY_SERVICE_URL + `/v1/messages/sources/${source}/tenants/${tenant}/latest`;
    return this.http.get<object>(url);
  }

  /**
   * This method retrieves all messages of a specific device
   * @param source The source of the device
   * @param tenant The tenant of the device
   * @return The messages as an observable
   */
  getDeviceMessages(source, tenant): Observable<object[]> {
    const url = SensorDataService.SENSOR_DATA_HISTORY_SERVICE_URL + `/v1/messages/sources/${source}/tenants/${tenant}`;
    return this.http.get<object[]>(url);
  }
}
