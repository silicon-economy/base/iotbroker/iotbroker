/**
 * @license
 * Copyright Open Logistics Foundation
 *
 * Licensed under the Open Logistics Foundation License 1.3.
 * For details on the licensing terms, see the LICENSE file.
 * SPDX-License-Identifier: OLFL-1.3
 */

import { Injectable } from '@angular/core';
import { StompService } from 'src/app/features/device-messages/services/stomp/stomp.service';
import { Subject, Subscription } from 'rxjs';
import { SensorDataMessage } from 'src/app/core/models/sensor-data/sensor-data-message';
import { map } from 'rxjs/operators';

@Injectable({
  providedIn: 'root'
})
export class SubscriptionService {

  public subscriptions: Map<string, Subscription> = new Map();

  public topicCountNotifier = new Subject<number>();
  public messageNotifier = new Subject<SensorDataMessage[]>();

  /**
   * Whenever the subscribed topics change the observers are notified
   * @param topicCount The number of the current topics
   */
  public topicChanged(topicCount: number) {
    this.topicCountNotifier.next(topicCount);
  }

  /**
   * Whenever a new message is received the observers are notified
   * @param messages The received message
   */
  public newMessages(messages: SensorDataMessage[]) {
    this.messageNotifier.next(messages);
  }

  constructor(private stompService: StompService) {
  }

  /**
   * subscribes to a topic through the stomp protocol
   * the topic can consist of a source or source and tenant
   * if the topic is already subscribed, the method will return false
   * notifies the observers of {@link topicCountNotifier}
   * when a new message arrives the observers of {@link messageNotifier} are notified
   * @param source The source of the devices
   * @param tenant The tenant of the specific device (optional)
   */
  public subscribeToTopic(source: string, tenant: string): string {
    if (tenant === null || tenant === undefined) {
      tenant = '';
    }
    const topic = `sources/${source}/tenants/${tenant}`;
    if (this.getTopics().includes(topic) || source === null || source === undefined) {
      return null;
    }
    const subscription = this.stompService.subscribeToTopic(`/v1/messages/${topic}`).pipe(map(result => {
      const messages: SensorDataMessage[] = [];
      if (result !== null) {
        for (const message of JSON.parse(result.body)) {
          messages.push(new SensorDataMessage(message));
        }
      }
      return messages;
    }));

    this.subscriptions.set(topic, subscription.subscribe(result => this.newMessages(result)));
    this.topicChanged(this.getTopics().length);

    return topic;
  }

  /**
   * unsubscribes from a Subscription
   * deletes the element from {@link subscriptions}
   * notifies the Observers of {@link topicCountNotifier}
   * @param topic The topic from the subscription
   */
  public unsubscribe(topic: string): void {
    this.stompService.unsubscribeFromTopic(this.subscriptions.get(topic));
    this.subscriptions.delete(topic);

    // notify about deleted topic
    this.topicChanged(this.getTopics().length);
  }

  /**
   * @return the topics from {@link subscriptions}
   */
  public getTopics(): string[] {
    return Array.from(this.subscriptions.keys());
  }

}
