/**
 * @license
 * Copyright Open Logistics Foundation
 *
 * Licensed under the Open Logistics Foundation License 1.3.
 * For details on the licensing terms, see the LICENSE file.
 * SPDX-License-Identifier: OLFL-1.3
 */

import { TestBed } from '@angular/core/testing';

import { SensorDataService } from 'src/app/features/device-messages/services/sensor-data.service';
import { HttpClientTestingModule, HttpTestingController } from '@angular/common/http/testing';
import { DeviceInstance } from 'src/app/core/models/device-instance';

describe('SensorDataService', () => {
  let service: SensorDataService;

  let httpMock: HttpTestingController;

  const sensorDataHistoryServiceUrl = 'http://localhost:8081';

  // test data
  const testDevice: DeviceInstance = {
    id: '123',
    source: 'source1',
    tenant: 'tenant1',
    deviceTypeIdentifier: 'ID1234',
    registrationTime: '2020-11-12T12:56:55Z',
    lastSeen: '2020-11-12T12:56:55Z',
    enabled: true,
    hardwareRevision: 'revision 1',
    firmwareVersion: '1.0.0',
    description: 'description',
  };

  const testMessage = {
    id: '6047435eadbe1d000168042e',
    source: 'lowcosttracker',
    tenant: 'Tracker-42',
    message: {
      key1: 'value1',
      key2: 'value2'
    },
    timestamp: '2020-11-12T12:56:55Z'
  };

  const testMessages = [testMessage, testMessage];

  beforeEach(() => {
    TestBed.configureTestingModule({
      imports: [HttpClientTestingModule],
      providers: [SensorDataService]
    });
    service = TestBed.inject(SensorDataService);
    httpMock = TestBed.inject(HttpTestingController);
  });

  it('should be created', () => {
    expect(service).toBeTruthy();
  });

  it('should get the correct device message', () => {
    service.getLatestDeviceMessage(testDevice.source, testDevice.tenant).subscribe((data: object) => {
      expect(data).toEqual(testMessage);
    });
    const url = `${sensorDataHistoryServiceUrl}/v1/messages/sources/${testDevice.source}/tenants/${testDevice.tenant}/latest`;
    // Act
    // workaround because of query params
    const req = httpMock
        .expectOne(req => req.method === 'GET' && req.url === url);
    req.flush(testMessage);
    // Assert
    httpMock.verify();
  });

  it('should get the correct device messages', () => {
    // Arrange

    service.getLatestDeviceMessage(testDevice.source, testDevice.tenant).subscribe((data: object[]) => {
      expect(data).toEqual(testMessages);
    });
    const url = `${sensorDataHistoryServiceUrl}/v1/messages/sources/${testDevice.source}/tenants/${testDevice.tenant}/latest`;
    // Act
    // workaround because of query params
    const req = httpMock
        .expectOne(req => req.method === 'GET' && req.url === url);
    req.flush(testMessages);
    // Assert
    httpMock.verify();
  });
});
