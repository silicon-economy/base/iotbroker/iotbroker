/**
 * @license
 * Copyright Open Logistics Foundation
 *
 * Licensed under the Open Logistics Foundation License 1.3.
 * For details on the licensing terms, see the LICENSE file.
 * SPDX-License-Identifier: OLFL-1.3
 */

import { async, ComponentFixture, TestBed } from '@angular/core/testing';
import { TranslateLoader, TranslateModule } from '@ngx-translate/core';
import { HttpClient } from '@angular/common/http';
import { HttpClientTestingModule } from '@angular/common/http/testing';
import { TranslateHttpLoader } from '@ngx-translate/http-loader';
import { LiveMessageDisplayComponent } from 'src/app/features/device-messages/pages/live-message-display/live-message-display.component';
import { of, Subject } from 'rxjs';
import { MatDialogModule } from '@angular/material/dialog';
import { SubscriptionService } from 'src/app/features/device-messages/services/subscription.service';
import { MessagesTableComponent } from 'src/app/features/device-messages/components/messages-table/messages-table.component';
import { MatTableModule } from '@angular/material/table';
import { MatPaginatorModule } from '@angular/material/paginator';
import { BrowserAnimationsModule } from '@angular/platform-browser/animations';
import { ExpandableTableComponent } from 'src/app/shared/components/expandable-table/expandable-table.component';

export function HttpLoaderFactory(http: HttpClient) {
  return new TranslateHttpLoader(http, './app/assets/lang/', '.json');
}

describe('LiveMessageDisplayComponent', () => {
  let component: LiveMessageDisplayComponent;
  let fixture: ComponentFixture<LiveMessageDisplayComponent>;

  let mockSubscriptionService;

  const spyDialog = jasmine.createSpyObj('MatDialog', ['open']);
  const spySubscriptionService = {
    ...
      jasmine.createSpyObj('SubscriptionService', ['getTopics', 'listenToTopic']),
    topicCountNotifier: new Subject(),
    messageNotifier: new Subject(),
  };

  const tableChild = jasmine.createSpyObj('MessagesTableComponent', ['showMessages']);

  beforeEach(async(() => {

    TestBed.configureTestingModule({
      imports: [
        MatDialogModule,
        HttpClientTestingModule,
        BrowserAnimationsModule,
        MatTableModule,
        MatPaginatorModule,
        TranslateModule.forRoot({
          loader: {
            provide: TranslateLoader,
            useFactory: HttpLoaderFactory,
            deps: [HttpClient]
          }
        }),],
      providers: [
        {provide: MatDialogModule, useValue: spyDialog},
        {provide: SubscriptionService, useValue: spySubscriptionService},
        {provide: MessagesTableComponent, useValue: tableChild}
      ],
      declarations: [LiveMessageDisplayComponent, MessagesTableComponent, ExpandableTableComponent]
    });

    // mock service
    mockSubscriptionService = TestBed.get(SubscriptionService);
    mockSubscriptionService.getTopics.and.returnValue(['newTopic']);
    mockSubscriptionService.listenToTopic.and.returnValue(of([]));


    TestBed.compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(LiveMessageDisplayComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });

});
