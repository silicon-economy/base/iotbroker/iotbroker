/**
 * @license
 * Copyright Open Logistics Foundation
 *
 * Licensed under the Open Logistics Foundation License 1.3.
 * For details on the licensing terms, see the LICENSE file.
 * SPDX-License-Identifier: OLFL-1.3
 */

import { AfterViewInit, Component, ElementRef, OnDestroy, ViewChild } from '@angular/core';
import { MatSnackBar } from '@angular/material/snack-bar';
import { MessagesTableComponent } from 'src/app/features/device-messages/components/messages-table/messages-table.component';
import { DeviceInstanceSelectionComponent } from 'src/app/shared/components/device-instance-selection/device-instance-selection.component';
import { SensorDataService } from 'src/app/features/device-messages/services/sensor-data.service';
import { OverlayService } from 'src/app/core/services/overlay.service';
import { SNACKBAR_ERROR_DURATION } from 'src/app/core/constants/application.configuration';

@Component({
  selector: 'app-history-message-display',
  templateUrl: './history-message-display.component.html',
  styleUrls: ['./history-message-display.component.scss'],
})
export class HistoryMessageDisplayComponent implements AfterViewInit, OnDestroy {
  @ViewChild(MessagesTableComponent) table: MessagesTableComponent;
  @ViewChild(DeviceInstanceSelectionComponent) topicSelection: DeviceInstanceSelectionComponent;
  @ViewChild('dataContainer') container: ElementRef;

  constructor(private sensorDataService: SensorDataService,
              private snackBar: MatSnackBar, private overlayService: OverlayService) {
  }

  ngAfterViewInit() {
    // Setup overlay after view init, otherwise the nativeElement Reference will not be loaded
    this.overlayService.setup(this.container.nativeElement);
  }

  ngOnDestroy(): void {
    this.overlayService.cleanup();
  }

  /**
   * gets the messages through the http service and passes them on to the table component
   */
  public showMessages(): void {
    if (this.topicSelection.elementsAreValid()) {
      this.overlayService.startLoadingAnimation();
      this.sensorDataService.getDeviceMessages(this.topicSelection.getSource().value, this.topicSelection.getTenant().value)
        .subscribe(result => {
            if (result.length > 0) {
              this.table.clearMessages();
              this.table.addMessages(result);
            } else {
              this.snackBar.open('There are no messages!', 'Close', {
                duration: SNACKBAR_ERROR_DURATION,
              });
            }
          },
          () => {
            this.snackBar.open('Could not load the Messages.', 'Close', {
              duration: SNACKBAR_ERROR_DURATION,
            });
          }, () => {
            this.overlayService.stopLoadingAnimation();
          });
    }
  }

}
