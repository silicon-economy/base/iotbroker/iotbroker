/**
 * @license
 * Copyright Open Logistics Foundation
 *
 * Licensed under the Open Logistics Foundation License 1.3.
 * For details on the licensing terms, see the LICENSE file.
 * SPDX-License-Identifier: OLFL-1.3
 */

import { ComponentFixture, TestBed, waitForAsync } from '@angular/core/testing';
import { TranslateLoader, TranslateModule } from '@ngx-translate/core';
import { HttpClient } from '@angular/common/http';
import { HttpClientTestingModule } from '@angular/common/http/testing';
import { TranslateHttpLoader } from '@ngx-translate/http-loader';
import { DeviceInstanceOverviewComponent } from 'src/app/features/devices/pages/device-instance-overview/device-instance-overview.component';
import { MatToolbarModule } from '@angular/material/toolbar';
import { MatListModule } from '@angular/material/list';
import { MatCardModule } from '@angular/material/card';
import { MatExpansionModule } from '@angular/material/expansion';
import { BrowserAnimationsModule } from '@angular/platform-browser/animations';
import { MatButtonModule } from '@angular/material/button';
import { MatIconModule } from '@angular/material/icon';
import { of } from 'rxjs';
import { MatFormFieldModule } from '@angular/material/form-field';
import { MatInputModule } from '@angular/material/input';
import { UntypedFormControl, FormsModule, ReactiveFormsModule } from '@angular/forms';
import { DeviceInstance } from 'src/app/core/models/device-instance';
import { MatDialogModule, MatDialogRef } from '@angular/material/dialog';
import { OverlayService } from 'src/app/core/services/overlay.service';
import { MatSnackBarModule } from '@angular/material/snack-bar';
import { MatPaginatorModule } from '@angular/material/paginator';
import { DeviceInstanceService } from 'src/app/features/devices/services/device-instance.service';
import { SensorDataService } from 'src/app/features/device-messages/services/sensor-data.service';
import { MatCheckboxModule } from '@angular/material/checkbox';
import { MatTableModule } from '@angular/material/table';
import { ExpandableTableComponent } from 'src/app/shared/components/expandable-table/expandable-table.component';
import { MockComponent, MockInstance } from 'ng-mocks';
import { DeviceInstanceEditFormComponent } from 'src/app/features/devices/components/device-instance-edit-form/device-instance-edit-form.component';
import { ConfirmDialogComponent } from 'src/app/shared/components/confirm-dialog/confirm-dialog.component';
import { DeviceInstanceSelectionComponent } from 'src/app/shared/components/device-instance-selection/device-instance-selection.component';
import { AutofillSelectComponent } from 'src/app/shared/components/autofill-select/autofill-select.component';

export function HttpLoaderFactory(http: HttpClient) {
  return new TranslateHttpLoader(http, './app/assets/lang/', '.json');
}

describe('DevicesOverviewComponent', () => {
  MockInstance.scope();
  let component: DeviceInstanceOverviewComponent;
  let fixture: ComponentFixture<DeviceInstanceOverviewComponent>;

  let mockSensorDataService;
  let mockDialog;
  let mockDeviceInstanceService;

  const sensorDataSpy = jasmine.createSpyObj('SensorDataService', ['getLatestDeviceMessage']);
  const spyDialog = jasmine.createSpyObj('MatDialog', ['open', 'afterClosed']);
  const spyOverlay = jasmine.createSpyObj('OverlayService', ['setup', 'startLoadingAnimation',
    'stopLoadingAnimation', 'cleanup']);
  const spyDeviceInstanceService = jasmine.createSpyObj('DeviceInstanceService', ['postDevice',
    'deleteDevice', 'updateDevice', 'getDevices']);

  // Test data
  const testDevice: DeviceInstance = {
    id: '123',
    source: 'source1',
    tenant: 'tenant1',
    deviceTypeIdentifier: 'ID123',
    registrationTime: '2020-11-12T12:56:55Z',
    lastSeen: '2020-11-12T12:56:55Z',
    enabled: true,
    hardwareRevision: 'revision 1',
    firmwareVersion: '1.0.0',
    description: 'description',
  };
  const testDevice2: DeviceInstance = {
    id: '456',
    source: 'source2',
    tenant: 'tenant1',
    deviceTypeIdentifier: 'ID321',
    registrationTime: '2020-11-12T12:56:55Z',
    lastSeen: '2020-11-12T12:56:55Z',
    enabled: true,
    hardwareRevision: 'revision 2',
    firmwareVersion: '2.0.0',
    description: 'description',
  };

  const testDevice3: DeviceInstance = {
    id: '789',
    source: 'source2',
    tenant: 'tenant2',
    deviceTypeIdentifier: 'ID456',
    registrationTime: '2020-11-12T12:56:55Z',
    lastSeen: '2020-11-12T12:56:55Z',
    enabled: true,
    hardwareRevision: 'revision 2',
    firmwareVersion: '2.0.0',
    description: 'description',
  };
  const testDevice4: DeviceInstance = {
    id: '111',
    source: 'source1',
    tenant: 'tenant2',
    deviceTypeIdentifier: 'ID654',
    registrationTime: '2020-11-12T12:56:55Z',
    lastSeen: '2020-11-12T12:56:55Z',
    enabled: true,
    hardwareRevision: 'r1',
    firmwareVersion: 'v1',
    description: 'desc1',
  };
  const testDevices: DeviceInstance[] = [testDevice, testDevice2];

  const sourceFormControl = new UntypedFormControl('');
  const tenantFormControl = new UntypedFormControl('');
  let elementsAreValid = true;

  beforeEach(waitForAsync(() => {
    TestBed.configureTestingModule({
      declarations: [DeviceInstanceOverviewComponent, MockComponent(ConfirmDialogComponent), ExpandableTableComponent,
        MockComponent(DeviceInstanceEditFormComponent), MockComponent(DeviceInstanceSelectionComponent),
        MockComponent(AutofillSelectComponent)],
      imports: [
        MatToolbarModule,
        MatListModule,
        MatTableModule,
        MatCardModule,
        MatExpansionModule,
        BrowserAnimationsModule,
        MatButtonModule,
        MatIconModule,
        MatDialogModule,
        MatFormFieldModule,
        MatInputModule,
        MatCheckboxModule,
        FormsModule,
        MatCheckboxModule,
        ReactiveFormsModule,
        MatPaginatorModule,
        MatCardModule,
        TranslateModule.forRoot({
          loader: {
            provide: TranslateLoader,
            useFactory: HttpLoaderFactory,
            deps: [HttpClient]
          }
        }),
        HttpClientTestingModule,
        MatSnackBarModule
      ],
      providers: [
        {provide: MatDialogModule, useValue: spyDialog},
        {provide: OverlayService, useValue: spyOverlay},
        {provide: SensorDataService, useValue: sensorDataSpy},
        {provide: DeviceInstanceService, useValue: spyDeviceInstanceService},
      ],
    });

    mockDialog = TestBed.inject(MatDialogModule);
    mockDialog = TestBed.inject(MatDialogModule);
    mockDialog.open.and.returnValue(testDevice4);
    mockDialog.afterClosed.and.returnValue(of(testDevice3));

    mockSensorDataService = TestBed.inject(SensorDataService);
    mockSensorDataService.getLatestDeviceMessage.and.returnValue(of([]));

    mockDeviceInstanceService = TestBed.inject(DeviceInstanceService);
    mockDeviceInstanceService.postDevice.and.returnValue(of(testDevice));
    mockDeviceInstanceService.updateDevice.and.returnValue(of(testDevice));
    mockDeviceInstanceService.deleteDevice.and.returnValue(of(testDevice));
    mockDeviceInstanceService.getDevices.and.returnValue(of([]));


    MockInstance(DeviceInstanceSelectionComponent, () => ({
      getTenant(): UntypedFormControl {
        return tenantFormControl;
      },
      getSource(): UntypedFormControl {
        return sourceFormControl;
      },
      elementsAreValid(): boolean {
        return elementsAreValid;
      }
    }));

    TestBed.compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(DeviceInstanceOverviewComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  afterEach(() => {
    fixture.destroy();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });

  it('should add the correct device', () => {
    spyOn(component, 'loadDevices');
    component.addDevice(testDevice);
    expect(spyDeviceInstanceService.postDevice).toHaveBeenCalled();
    expect(component.loadDevices).toHaveBeenCalled();
  });

  it('should delete the correct device', () => {
    spyOn(component, 'loadDevices');
    component.deviceCount = 1;
    component.table.dataSource.data = testDevices;
    component.deleteDevice(testDevice.id);
    expect(spyDeviceInstanceService.deleteDevice).toHaveBeenCalled();
    expect(component.loadDevices).toHaveBeenCalled();
  });

  it('should update the correct device', () => {
    spyOn(component, 'loadDevices');
    component.table.dataSource.data = testDevices;
    component.updateDevice(testDevice);
    expect(spyDeviceInstanceService.updateDevice).toHaveBeenCalled();
    expect(component.loadDevices).toHaveBeenCalled();
  });

  it('should open Dialog', () => {
    component.openConfirmationDialog('test');
    component.openCreateDeviceDialog();
    component.openUpdateDeviceDialog(testDevice);
    expect(component.dialogRefDevice).toBeDefined();
    expect(component.dialogRefConfirmation).toBeDefined();
  });

  it('should delete the device', () => {
    spyOn(component.dialog, 'open')
      .and
      .returnValue({
        afterClosed: () => of(true)
      } as MatDialogRef<typeof component>);

    spyOn(component, 'deleteDevice');

    component.openConfirmationDialog(testDevice.id);
    expect(component.deleteDevice).toHaveBeenCalled();
  });

  it('should load all devices', () => {
    spyOn(component.table, 'clearElements');
    sourceFormControl.setValue('*');
    tenantFormControl.setValue('*');
    component.amountDevices.setValue(10);

    component.loadDevices();
    expect(spyDeviceInstanceService.getDevices).toHaveBeenCalled();
    expect(component.table.clearElements).toHaveBeenCalled();
  });

  it('should load all devices from one source', () => {
    sourceFormControl.setValue('source');
    tenantFormControl.setValue('*');
    component.amountDevices.setValue(10);

    component.loadDevices();
    expect(spyDeviceInstanceService.getDevices).toHaveBeenCalled();
  });

  it('should load one device', () => {
    sourceFormControl.setValue('source');
    tenantFormControl.setValue('tenant');
    component.amountDevices.setValue(10);

    component.loadDevices();
    expect(spyDeviceInstanceService.getDevices).toHaveBeenCalled();
  });

  it('should not load devices when the amount of devices is not valid', () => {
    spyOn(component.table, 'clearElements');
    component.amountDevices.setValue(null);
    component.loadDevices();
    expect(component.table.clearElements).toHaveBeenCalledTimes(0);
  });

  it('should load devices on button click', () => {
    spyOn(component, 'loadDevices');
    component.onLoadClick();
    expect(component.loadDevices).toHaveBeenCalled();
  });

  it('should not load devices on button click when elements are not valid', () => {
    elementsAreValid = false;
    spyOn(component, 'loadDevices');
    component.onLoadClick();
    expect(component.loadDevices).toHaveBeenCalledTimes(0);
  });
});
