/**
 * @license
 * Copyright Open Logistics Foundation
 *
 * Licensed under the Open Logistics Foundation License 1.3.
 * For details on the licensing terms, see the LICENSE file.
 * SPDX-License-Identifier: OLFL-1.3
 */

import { AfterViewInit, Component, ElementRef, OnDestroy, ViewChild } from '@angular/core';
import { MatDialog, MatDialogRef } from '@angular/material/dialog';
import { MatSnackBar } from '@angular/material/snack-bar';
import { ConfirmationComponent } from 'src/app/shared/components/confirmation/confirmation.component';
import { DeviceInstance } from 'src/app/core/models/device-instance';
import { DeviceInstanceService } from 'src/app/features/devices/services/device-instance.service';
import { OverlayService } from 'src/app/core/services/overlay.service';
import { SNACKBAR_ERROR_DURATION } from 'src/app/core/constants/application.configuration';
import { SensorDataService } from 'src/app/features/device-messages/services/sensor-data.service';
import { MatPaginator } from '@angular/material/paginator';
import { ExpandableTableComponent } from 'src/app/shared/components/expandable-table/expandable-table.component';
import { UntypedFormControl, Validators } from '@angular/forms';
import { FormMode } from 'src/app/features/devices/constants/form-mode.enum';
import { ConfirmDialogComponent } from 'src/app/shared/components/confirm-dialog/confirm-dialog.component';
import { DeviceInstanceEditFormComponent } from 'src/app/features/devices/components/device-instance-edit-form/device-instance-edit-form.component';
import { ConfirmAction, DeviceInstanceDialogConfiguration } from 'src/app/features/devices/pages/device-instance-overview/device-instance-dialog-configuration';
import { DialogData } from 'src/app/shared/components/confirm-dialog/models/dialog-data.interface';
import { TranslateService } from '@ngx-translate/core';
import { TableColumnConfig } from 'src/app/shared/components/expandable-table/models/table-column-config';
import { DeviceInstanceSelectionComponent } from 'src/app/shared/components/device-instance-selection/device-instance-selection.component';
import { Observable } from 'rxjs';

/**
 * This component retrieves devices from the database and shows them
 * With this component devices can be created, deleted and edited
 */
@Component({
  selector: 'app-device-instance-overview',
  templateUrl: './device-instance-overview.component.html',
  styleUrls: ['./device-instance-overview.component.scss']
})
export class DeviceInstanceOverviewComponent implements AfterViewInit, OnDestroy {

  @ViewChild(MatPaginator) paginator: MatPaginator;
  @ViewChild(ExpandableTableComponent) table: ExpandableTableComponent<DeviceInstance>;
  @ViewChild('dataContainer') container: ElementRef;
  @ViewChild(DeviceInstanceSelectionComponent) selectionComponent: DeviceInstanceSelectionComponent;

  // the attributes of the table objects that are displayed
  public tableColumnConfig: TableColumnConfig[] = [
    new TableColumnConfig('source', 'deviceinstanceoverview.th_source'),
    new TableColumnConfig('tenant', 'deviceinstanceoverview.th_tenant'),
    new TableColumnConfig('registrationTime', 'deviceinstanceoverview.th_registrationTime',
      (value) => {
        return new Date(Date.parse(value)).toLocaleString();
      }),
    new TableColumnConfig('lastSeen', 'deviceinstanceoverview.th_lastSeen',
      (value) => {
        return new Date(Date.parse(value)).toLocaleString();
      }),
    new TableColumnConfig('enabled', 'deviceinstanceoverview.th_enabled'),
  ];

  dialogRefDevice: MatDialogRef<ConfirmDialogComponent<DeviceInstanceEditFormComponent>>;
  dialogRefConfirmation: MatDialogRef<ConfirmationComponent>;

  /**actions
   * the latest message from the devices from which the panel was opened
   */
  deviceMessages = new Map<string, string>();

  // Mapping for loading Animations
  deviceMessageLoadingStates = new Map<string, boolean>();

  deviceCount = 0;
  amountDevices = new UntypedFormControl('10', Validators.required);

  constructor(
    private deviceService: DeviceInstanceService,
    public dialog: MatDialog,
    private snackBar: MatSnackBar,
    private sensorDataService: SensorDataService,
    private overlayService: OverlayService,
    private translateService: TranslateService) {
  }

  public ngAfterViewInit(): void {
    // Setup overlay after view init, otherwise the nativeElement Reference will not be loaded
    this.overlayService.setup(this.container.nativeElement);
    this.table.dataSource.paginator = this.paginator;

    this.initialLoad();
  }

  public ngOnDestroy(): void {
    this.overlayService.cleanup();
  }

  private initialLoad(): void {
    const deviceObservable = this.deviceService.getDevices(null, null, 0, this.amountDevices.value);
    this.getDevices(deviceObservable);
  }

  public loadDevices(): void {
    if (this.amountDevices.invalid) {
      this.amountDevices.markAsTouched();
      return;
    }

    let deviceObservable;
    const source = this.selectionComponent.getSource().value;
    const tenant = this.selectionComponent.getTenant().value;
    if (source == '*' && tenant == '*') {
      deviceObservable = this.deviceService.getDevices(null, null,0, this.amountDevices.value);
    } else if (source != '*' && tenant == '*') {
      deviceObservable = this.deviceService.getDevices(source,null, 0, this.amountDevices.value);
    }  else if (source == '*' && tenant != '*') {
      deviceObservable = this.deviceService.getDevices(null, tenant, 0, this.amountDevices.value);
    } else {
      deviceObservable = this.deviceService.getDevices(source, tenant, null, null);
    }

    this.getDevices(deviceObservable);
  }

  private getDevices(deviceObservable: Observable<DeviceInstance[]>) {
    this.overlayService.startLoadingAnimation();
    deviceObservable.subscribe(result => {
      this.table.clearElements();
      this.table.addElements(result);
      this.deviceCount = result.length;
    }, error => {
      this.snackBar.open(error.error, 'Close', {duration: SNACKBAR_ERROR_DURATION});
    }, () => {
      this.overlayService.stopLoadingAnimation();
    });
  }

  public onLoadClick() {
    if (this.selectionComponent.elementsAreValid()) {
      this.loadDevices();
    } else {
      this.snackBar.open('Inputs are not valid', 'Close', {
        duration: SNACKBAR_ERROR_DURATION
      });
    }
  }

  /**
   * Adds a new device to the database through the http service
   * @param device New Device that is added
   */
  public addDevice(device: DeviceInstance): void {
    this.deviceService.postDevice(device).subscribe(() => {
      this.loadDevices();
    }, error => {
      this.snackBar.open(error.error, 'Close', {duration: SNACKBAR_ERROR_DURATION});
    });
  }

  /**
   * deletes a device through the http service
   * @param id Id of the device that is deleted
   */
  public deleteDevice(id: string): void {
    this.deviceService.deleteDevice(id).subscribe(() => {
      this.loadDevices();
    }, error => {
      this.snackBar.open(error.error, 'Close', {duration: SNACKBAR_ERROR_DURATION});
    });
  }

  /**
   * Updates the device through the http service
   * @param newDevice The updated device
   */
  public updateDevice(newDevice: DeviceInstance): void {
    this.deviceService.updateDevice(newDevice).subscribe(() => {
      this.loadDevices();
    }, error => {
      this.snackBar.open(error.error, 'Close', {duration: SNACKBAR_ERROR_DURATION});
    });
  }

  /**
   * retrieves the latest device message and shows it in the device panel
   * @param device Device from which the message is retrieved
   */
  public getDeviceMessage(device: DeviceInstance): void {
    this.deviceMessageLoadingStates.set(device.id, true);
    this.sensorDataService.getLatestDeviceMessage(device.source, device.tenant).subscribe(data => {
      if (data != null) {
        this.deviceMessages.set(device.id, JSON.stringify(data, undefined, 2));
      }
      this.deviceMessageLoadingStates.set(device.id, false);
    }, error => {
      this.snackBar.open(error.message, 'Close', {duration: SNACKBAR_ERROR_DURATION});
      this.deviceMessageLoadingStates.set(device.id, false);
    });
  }

  /**
   * Opens Confirmation for device's deletion
   * @param id ID from device to delete after confirmation
   */
  public openConfirmationDialog(id: string): void {
    this.dialogRefConfirmation = this.dialog.open(ConfirmationComponent, {
      data: {
        type: ConfirmationComponent.DELETE_CONFIRM
      }
    });
    this.dialogRefConfirmation.afterClosed().subscribe(result => {
      if (result) {
        this.deleteDevice(id);
      }
    });
  }

  /**
   * Opens a mat dialog to create a new device
   * forwards the result to createNewDevice-method
   */
  public openCreateDeviceDialog(): void {
    const confirmAction: ConfirmAction = (deviceInstance: DeviceInstance) => this.addDevice(deviceInstance);
    const dialogData: DialogData<DeviceInstanceEditFormComponent> = {
      title: this.translateService.instant('deviceinstanceoverview.create_device_title'),
      componentConfiguration: new DeviceInstanceDialogConfiguration(FormMode.Create, new DeviceInstance(), confirmAction)
    };

    this.dialogRefDevice = this.dialog.open<ConfirmDialogComponent<DeviceInstanceEditFormComponent>>(ConfirmDialogComponent, {
      width: '600px',
      data: dialogData,
    });
  }

  /**
   * Opens a mat dialog to edit an existing device
   * forwards the result to new updateDevice-method
   * @param device The devices that is edited
   */
  public openUpdateDeviceDialog(device: DeviceInstance): void {
    const confirmAction: ConfirmAction = (deviceInstance: DeviceInstance) => this.updateDevice(deviceInstance);
    const dialogData: DialogData<DeviceInstanceEditFormComponent> = {
      title: this.translateService.instant('deviceinstanceoverview.update_device_title'),
      componentConfiguration: new DeviceInstanceDialogConfiguration(FormMode.Edit, device, confirmAction)
    };

    this.dialogRefDevice = this.dialog.open<ConfirmDialogComponent<DeviceInstanceEditFormComponent>>(ConfirmDialogComponent, {
      width: '600px',
      data: dialogData
    });
  }

}
