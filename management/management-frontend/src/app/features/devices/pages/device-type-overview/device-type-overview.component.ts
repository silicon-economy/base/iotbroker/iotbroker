/**
 * @license
 * Copyright Open Logistics Foundation
 *
 * Licensed under the Open Logistics Foundation License 1.3.
 * For details on the licensing terms, see the LICENSE file.
 * SPDX-License-Identifier: OLFL-1.3
 */

import { AfterViewInit, Component, ElementRef, OnDestroy, ViewChild } from '@angular/core';
import { DeviceType } from 'src/app/core/models/device-type';
import { MatDialog, MatDialogRef } from '@angular/material/dialog';
import { ConfirmationComponent } from 'src/app/shared/components/confirmation/confirmation.component';
import { OverlayService } from 'src/app/core/services/overlay.service';
import { DeviceTypeService } from 'src/app/features/devices/services/device-type.service';
import { MatSnackBar } from '@angular/material/snack-bar';
import { SNACKBAR_ERROR_DURATION } from 'src/app/core/constants/application.configuration';
import { MatPaginator } from '@angular/material/paginator';
import { ExpandableTableComponent } from 'src/app/shared/components/expandable-table/expandable-table.component';
import { UntypedFormControl, Validators } from '@angular/forms';
import { FormMode } from 'src/app/features/devices/constants/form-mode.enum';
import { ConfirmDialogComponent } from 'src/app/shared/components/confirm-dialog/confirm-dialog.component';
import { DeviceTypeEditFormComponent } from 'src/app/features/devices/components/device-type-edit-form/device-type-edit-form.component';
import { ConfirmAction, DeviceTypeDialogConfiguration } from 'src/app/features/devices/pages/device-type-overview/device-type-dialog-configuration';
import { DialogData } from 'src/app/shared/components/confirm-dialog/models/dialog-data.interface';
import { TranslateService } from '@ngx-translate/core';
import { TableColumnConfig } from 'src/app/shared/components/expandable-table/models/table-column-config';
import { AutofillSelectComponent } from 'src/app/shared/components/autofill-select/autofill-select.component';
import { Observable } from 'rxjs';

/**
 * This component retrieves device types from the database and shows them
 * With this component device types can be created, deleted and edited
 */
@Component({
  selector: 'app-device-type-overview',
  templateUrl: './device-type-overview.component.html',
  styleUrls: ['./device-type-overview.component.scss']
})
export class DeviceTypeOverviewComponent implements AfterViewInit, OnDestroy {

  @ViewChild(MatPaginator) paginator: MatPaginator;
  @ViewChild(ExpandableTableComponent) table: ExpandableTableComponent<DeviceType>;
  @ViewChild('dataContainer') container: ElementRef;

  // for the filters
  @ViewChild('selectSource') selectSource: AutofillSelectComponent;
  @ViewChild('selectIdentifier') selectIdentifier: AutofillSelectComponent;

  // the attributes of the table objects that are displayed
  public tableColumnConfig: TableColumnConfig[] = [
    new TableColumnConfig('source', 'devicetypeoverview.th_source'),
    new TableColumnConfig('identifier', 'devicetypeoverview.th_identifier'),
    new TableColumnConfig('enabled', 'devicetypeoverview.th_enabled')
  ];
  dialogRefDeviceType: MatDialogRef<ConfirmDialogComponent<DeviceTypeEditFormComponent>>;
  dialogRefConfirmation: MatDialogRef<ConfirmationComponent>;

  deviceTypesCount = 0;
  amountTypes = new UntypedFormControl('10', Validators.required);

  constructor(
    public dialog: MatDialog,
    private overlayService: OverlayService,
    private deviceTypeService: DeviceTypeService,
    private snackBar: MatSnackBar,
    private translateService: TranslateService) {
  }

  public ngAfterViewInit(): void {
    // Setup overlay after view init, otherwise the nativeElement Reference will not be loaded
    this.overlayService.setup(this.container.nativeElement);
    this.table.dataSource.paginator = this.paginator;
    this.getSources();
    this.initialLoad();
  }

  getSources() {
    this.deviceTypeService.getSources().subscribe((sources: string[]) => {
      this.selectSource.initializeElements(sources);
    }, error => {
      console.error(error);
      this.snackBar.open('Could not load the sources.', 'Close', {duration: SNACKBAR_ERROR_DURATION});
    });
  }

  getIdentifiersFromOneSource() {
    this.selectIdentifier.element.setValue('');
    const identifiers = [];
    this.deviceTypeService.getDeviceTypes(this.selectSource.element.value,null, 0, 1000).subscribe(
      (types: DeviceType[]) => {
        for (const type of types) {
          identifiers.push(type.identifier);
        }
        this.selectIdentifier.initializeElements(identifiers);
      }, error => {
        console.error(error);
        this.snackBar.open('Could not load the identifiers.', 'Close', {duration: SNACKBAR_ERROR_DURATION});
      });
  }

  public ngOnDestroy(): void {
    this.overlayService.cleanup();
  }

  private initialLoad(): void {
    const deviceObservable = this.deviceTypeService.getDeviceTypes(null, null,0, this.amountTypes.value);
    this.getTypes(deviceObservable);
  }

  public loadTypes(): void {
    if (this.amountTypes.invalid) {
      this.amountTypes.markAsTouched();
      return;
    }

    let deviceObservable;
    const source = this.selectSource.element.value;
    const identifier = this.selectIdentifier.element.value;
    if (source == '*' && identifier == '*') {
      deviceObservable = this.deviceTypeService.getDeviceTypes(null, null, 0, this.amountTypes.value);
    } else if (source !== '*' && identifier == '*') {
      deviceObservable = this.deviceTypeService.getDeviceTypes(source, null, 0, this.amountTypes.value);
    } else if (source == '*' && identifier != '*') {
      deviceObservable = this.deviceTypeService.getDeviceTypes(null, identifier, 0, this.amountTypes.value);
    }
    else {
      deviceObservable = this.deviceTypeService.getDeviceTypes(source, identifier, null, null);
    }

    this.getTypes(deviceObservable);
  }

  private getTypes(typeObservable: Observable<DeviceType[]>) {
    this.overlayService.startLoadingAnimation();
    typeObservable.subscribe(result => {
      this.table.clearElements();
      this.table.addElements(result);
      this.deviceTypesCount = result.length;
    }, error => {
      this.snackBar.open(error.error, 'Close', {duration: SNACKBAR_ERROR_DURATION});
    }, () => {
      this.overlayService.stopLoadingAnimation();
    });
  }

  public onLoadClick() {
    if (this.selectSource.element.valid && this.selectIdentifier.element.valid) {
      this.loadTypes();
    } else {
      this.snackBar.open('Inputs are not valid', 'Close', {
        duration: SNACKBAR_ERROR_DURATION
      });
    }
  }

  /**
   * Adds a new device type to the database through the http service
   * @param deviceType New DeviceType that is added
   */
  public addDeviceType(deviceType: DeviceType): void {
    this.deviceTypeService.postDeviceType(deviceType).subscribe(() => {
      this.loadTypes();
    }, error => {
      this.snackBar.open(error.error, 'Close', {duration: SNACKBAR_ERROR_DURATION});
    });
  }

  /**
   * Deletes a device type through the http service
   * @param id ID of the device type that is deleted
   */
  public deleteDeviceType(id: string): void {
    this.deviceTypeService.deleteDeviceType(id).subscribe(() => {
      this.loadTypes();
    }, error => {
      this.snackBar.open(error.error, 'Close', {duration: SNACKBAR_ERROR_DURATION});
    });
  }

  /**
   * Updates the device type through the http service
   * @param newDeviceType The updated device type
   */
  public updateDeviceType(newDeviceType: DeviceType): void {
    this.deviceTypeService.updateDeviceType(newDeviceType).subscribe(() => {
      this.loadTypes();
    }, error => {
      this.snackBar.open(error.error, 'Close', {duration: SNACKBAR_ERROR_DURATION});
    });
  }

  /**
   * Opens a mat dialog to create a new device type
   * forwards the result to createNewDeviceType method
   */
  public openCreateDeviceDialog(): void {
    const confirmAction: ConfirmAction = (deviceType: DeviceType) => this.addDeviceType(deviceType);
    const dialogData: DialogData<DeviceTypeEditFormComponent> = {
      title: this.translateService.instant('devicetypeoverview.create_type_title'),
      componentConfiguration: new DeviceTypeDialogConfiguration(FormMode.Create, new DeviceType(), confirmAction)
    };

    this.dialogRefDeviceType = this.dialog.open<ConfirmDialogComponent<DeviceTypeEditFormComponent>>(ConfirmDialogComponent, {
      width: '600px',
      data: dialogData
    });
  }

  /**
   * Opens a mat dialog to update a new device type
   * forwards the result to updateDeviceType method
   */
  public openUpdateDeviceTypeDialog(deviceType: DeviceType): void {
    const confirmAction: ConfirmAction = (dt: DeviceType) => this.updateDeviceType(dt);
    const dialogData: DialogData<DeviceTypeEditFormComponent> = {
      title: this.translateService.instant('devicetypeoverview.update_type_title'),
      componentConfiguration: new DeviceTypeDialogConfiguration(FormMode.Edit, deviceType, confirmAction)
    };

    this.dialogRefDeviceType = this.dialog.open<ConfirmDialogComponent<DeviceTypeEditFormComponent>>(ConfirmDialogComponent, {
      width: '600px',
      data: dialogData
    });
  }

  /**
   * Opens Confirmation for the deletion of the device type
   * @param id ID from device type to delete after confirmation
   */
  public openConfirmationDialog(id: string): void {
    this.dialogRefConfirmation = this.dialog.open(ConfirmationComponent, {
      data: {
        type: ConfirmationComponent.DELETE_CONFIRM
      }
    });
    this.dialogRefConfirmation.afterClosed().subscribe(result => {
      if (result) {
        this.deleteDeviceType(id);
      }
    });
  }

}
