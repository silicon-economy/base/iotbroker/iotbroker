/**
 * @license
 * Copyright Open Logistics Foundation
 *
 * Licensed under the Open Logistics Foundation License 1.3.
 * For details on the licensing terms, see the LICENSE file.
 * SPDX-License-Identifier: OLFL-1.3
 */

import { ComponentFixture, TestBed, waitForAsync } from '@angular/core/testing';
import { DeviceTypeOverviewComponent } from 'src/app/features/devices/pages/device-type-overview/device-type-overview.component';
import { DeviceType } from 'src/app/core/models/device-type';
import { of, throwError } from 'rxjs';
import { MatDialogModule, MatDialogRef } from '@angular/material/dialog';
import { MatExpansionModule } from '@angular/material/expansion';
import { BrowserAnimationsModule } from '@angular/platform-browser/animations';
import { MatFormFieldModule } from '@angular/material/form-field';
import { UntypedFormControl, FormsModule, ReactiveFormsModule } from '@angular/forms';
import { TranslateLoader, TranslateModule } from '@ngx-translate/core';
import { HttpClient } from '@angular/common/http';
import { HttpClientTestingModule } from '@angular/common/http/testing';
import { HttpLoaderFactory } from 'src/app/features/devices/pages/device-instance-overview/device-instance-overview.component.spec';
import { MatPaginatorModule } from '@angular/material/paginator';
import { MatSnackBarModule } from '@angular/material/snack-bar';
import { MatCardModule } from '@angular/material/card';
import { DeviceTypeService } from 'src/app/features/devices/services/device-type.service';
import { MatInputModule } from '@angular/material/input';
import { MatCheckboxModule } from '@angular/material/checkbox';
import { MatTableModule } from '@angular/material/table';
import { MatIconModule } from '@angular/material/icon';
import { OverlayService } from 'src/app/core/services/overlay.service';
import { ExpandableTableComponent } from 'src/app/shared/components/expandable-table/expandable-table.component';
import { MockComponent, MockInstance } from 'ng-mocks';
import { DeviceTypeEditFormComponent } from 'src/app/features/devices/components/device-type-edit-form/device-type-edit-form.component';
import { ConfirmDialogComponent } from 'src/app/shared/components/confirm-dialog/confirm-dialog.component';
import { AutofillSelectComponent } from 'src/app/shared/components/autofill-select/autofill-select.component';

describe('DevicetypeOverviewComponent', () => {
  MockInstance.scope();
  let component: DeviceTypeOverviewComponent;
  let fixture: ComponentFixture<DeviceTypeOverviewComponent>;

  let mockDeviceTypeService;

  const spyDeviceTypeService = jasmine.createSpyObj('DeviceTypeService', ['getDeviceTypes',
    'getDeviceTypeCount', 'postDeviceType', 'deleteDeviceType', 'updateDeviceType', 'getSources']);
  const spyDialog = jasmine.createSpyObj('MatDialog', ['open', 'afterClosed']);
  const spyOverlay = jasmine.createSpyObj('OverlayService', ['setup', 'startLoadingAnimation',
    'stopLoadingAnimation', 'cleanup']);

  const deviceType: DeviceType = {
    id: '123',
    source: 'source1',
    identifier: 'ID123',
    providedBy: ['a', 'b'],
    description: 'None',
    enabled: true,
    autoRegisterDeviceInstances: false,
    autoEnableDeviceInstances: false
  };

  const deviceType2: DeviceType = {
    id: '111',
    source: 'source2',
    identifier: 'ID111',
    providedBy: ['c', 'd'],
    description: 'None',
    enabled: true,
    autoRegisterDeviceInstances: true,
    autoEnableDeviceInstances: true
  };

  const deviceTypes = [deviceType, deviceType2];
  const deviceTypeIdentifiers = [deviceType.identifier, deviceType2.identifier];

  const testFormControl = new UntypedFormControl('');

  beforeEach(waitForAsync(() => {
    TestBed.configureTestingModule({
      declarations: [DeviceTypeOverviewComponent, ExpandableTableComponent,
        MockComponent(DeviceTypeEditFormComponent), MockComponent(ConfirmDialogComponent),
        MockComponent(AutofillSelectComponent)],
      imports: [
        MatDialogModule,
        MatPaginatorModule,
        MatTableModule,
        BrowserAnimationsModule,
        MatExpansionModule,
        TranslateModule.forRoot({
          loader: {
            provide: TranslateLoader,
            useFactory: HttpLoaderFactory,
            deps: [HttpClient]
          }
        }),
        HttpClientTestingModule,
        MatFormFieldModule,
        MatInputModule,
        MatCheckboxModule,
        FormsModule,
        ReactiveFormsModule,
        MatSnackBarModule,
        MatPaginatorModule,
        MatCardModule,
        MatIconModule
      ],
      providers: [
        {provide: MatDialogModule, useValue: spyDialog},
        {provide: DeviceTypeService, useValue: spyDeviceTypeService},
        {provide: OverlayService, useValue: spyOverlay}
      ]
    });

    mockDeviceTypeService = TestBed.inject(DeviceTypeService);
    mockDeviceTypeService.getDeviceTypeCount.and.returnValue(of(10));
    mockDeviceTypeService.getDeviceTypes.and.returnValue(of([]));
    mockDeviceTypeService.postDeviceType.and.returnValue(of(deviceTypes));
    mockDeviceTypeService.deleteDeviceType.and.returnValue(of(deviceType));
    mockDeviceTypeService.updateDeviceType.and.returnValue(of(deviceType));
    mockDeviceTypeService.getSources.and.returnValue(of([]));

    MockInstance(AutofillSelectComponent, () => ({
      element: testFormControl
    }));

    TestBed.compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(DeviceTypeOverviewComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });


  it('should add the correct the device type', () => {
    spyOn(component, 'loadTypes');
    component.addDeviceType(deviceType);
    expect(mockDeviceTypeService.postDeviceType).toHaveBeenCalled();
    expect(component.loadTypes).toHaveBeenCalled();
  });

  it('should delete the correct the device type', () => {
    spyOn(component, 'loadTypes');
    component.deleteDeviceType(deviceType.id);
    expect(mockDeviceTypeService.deleteDeviceType).toHaveBeenCalled();
    expect(component.loadTypes).toHaveBeenCalled();
  });

  it('should update the correct the device type', () => {
    spyOn(component, 'loadTypes');
    component.table.dataSource.data = deviceTypes;
    component.updateDeviceType(deviceType);
    expect(mockDeviceTypeService.updateDeviceType).toHaveBeenCalled();
    expect(component.loadTypes).toHaveBeenCalled();
  });

  it('should open Dialog', () => {
    component.openConfirmationDialog('test');
    component.openCreateDeviceDialog();
    component.openUpdateDeviceTypeDialog(deviceType);
    expect(component.dialogRefDeviceType).toBeDefined();
    expect(component.dialogRefConfirmation).toBeDefined();
  });

  it('should delete the device type', () => {
    spyOn(component.dialog, 'open')
      .and
      .returnValue({
        afterClosed: () => of(true)
      } as MatDialogRef<typeof component>);

    spyOn(component, 'deleteDeviceType');

    component.openConfirmationDialog('1');
    expect(component.deleteDeviceType).toHaveBeenCalled();
  });

  it('should update the device type', () => {
    spyOn(component.dialog, 'open')
      .and
      .returnValue({
        afterClosed: () => of(true)
      } as MatDialogRef<typeof component>);

    spyOn(component, 'updateDeviceType');

    component.updateDeviceType(deviceType);
    expect(component.updateDeviceType).toHaveBeenCalled();
  });

  it('should not add the device type if an error occurs', () => {
    spyOn(component, 'loadTypes');
    mockDeviceTypeService.postDeviceType.and.returnValue(throwError({status: 404}));
    component.addDeviceType(deviceType);

    expect(component.loadTypes).toHaveBeenCalledTimes(0);
  });

  it('should not delete the device type if an error occurs', () => {
    spyOn(component, 'loadTypes');
    mockDeviceTypeService.deleteDeviceType.and.returnValue(throwError({status: 404}));
    component.deleteDeviceType(deviceType.id);

    expect(component.loadTypes).toHaveBeenCalledTimes(0);
  });

  it('should not update the device type if an error occurs', () => {
    spyOn(component, 'loadTypes');
    mockDeviceTypeService.updateDeviceType.and.returnValue(throwError({status: 404}));
    // the updated device
    const updatedDeviceType: DeviceType = {
      id: '123',
      source: 'source1',
      identifier: 'ID123',
      providedBy: ['a', 'b', 'c'],
      description: 'New description',
      enabled: true,
      autoRegisterDeviceInstances: true,
      autoEnableDeviceInstances: false
    };
    component.updateDeviceType(updatedDeviceType);
    expect(component.loadTypes).toHaveBeenCalledTimes(0);
  });

  it('should get all identifiers from one source', () => {
    mockDeviceTypeService.getDeviceTypes.and.returnValue(of(deviceTypes));
    spyOn(component.selectIdentifier, 'initializeElements');
    component.getIdentifiersFromOneSource();
    expect(spyDeviceTypeService.getDeviceTypes).toHaveBeenCalled();
    expect(component.selectIdentifier.initializeElements).toHaveBeenCalled();
    expect(component.selectIdentifier.initializeElements).toHaveBeenCalledWith(deviceTypeIdentifiers);
  });

  it('should load all types', () => {
    spyOn(component.table, 'clearElements');
    component.selectSource.element.setValue('*');
    component.selectIdentifier.element.setValue('*');
    component.amountTypes.setValue(10);

    component.loadTypes();
    expect(spyDeviceTypeService.getDeviceTypes).toHaveBeenCalled();
    expect(component.table.clearElements).toHaveBeenCalled();
  });

  it('should load all types from one source', () => {
    component.selectSource.element.setValue('source');
    component.selectIdentifier.element.setValue('*');
    component.amountTypes.setValue(10);

    component.loadTypes();
    expect(spyDeviceTypeService.getDeviceTypes).toHaveBeenCalled();
  });

  it('should load one type', () => {
    component.selectSource.element.setValue('source');
    component.selectIdentifier.element.setValue('identifier');
    component.amountTypes.setValue(10);

    component.loadTypes();
    expect(spyDeviceTypeService.getDeviceTypes).toHaveBeenCalled();
  });

  it('should load types on button click', () => {
    spyOn(component, 'loadTypes');
    component.onLoadClick();
    expect(component.loadTypes).toHaveBeenCalled();
  });

  it('should not load types on button click when elements are not valid', () => {
    component.selectSource.element.setErrors({});
    spyOn(component, 'loadTypes');
    component.onLoadClick();
    expect(component.loadTypes).toHaveBeenCalledTimes(0);
  });
});
