/**
 * @license
 * Copyright Open Logistics Foundation
 *
 * Licensed under the Open Logistics Foundation License 1.3.
 * For details on the licensing terms, see the LICENSE file.
 * SPDX-License-Identifier: OLFL-1.3
 */

import { HttpClientTestingModule, HttpTestingController } from '@angular/common/http/testing';
import { TestBed } from '@angular/core/testing';
import { DeviceInstance } from 'src/app/core/models/device-instance';
import { DeviceInstanceService } from 'src/app/features/devices/services/device-instance.service';


describe('DeviceService', () => {
  let deviceService: DeviceInstanceService;
  let httpMock: HttpTestingController;

  const deviceRegistryServiceUrl = 'http://localhost:8081';

  const testDevice: DeviceInstance = {
    id: '123',
    source: 'source1',
    tenant: 'tenant1',
    deviceTypeIdentifier: 'ID1234',
    registrationTime: '2020-11-12T12:56:55Z',
    lastSeen: '2020-11-12T12:56:55Z',
    enabled: true,
    hardwareRevision: 'revision 1',
    firmwareVersion: '1.0.0',
    description: 'description',
  };

  const testDevice2: DeviceInstance = {
    id: '234',
    source: 'source2',
    tenant: 'tenant2',
    deviceTypeIdentifier: 'ID2345',
    registrationTime: '2020-11-12T12:56:55Z',
    lastSeen: '2020-11-12T12:56:55Z',
    enabled: true,
    hardwareRevision: 'revision 2',
    firmwareVersion: '2.0.0',
    description: 'description',
  };

  const testDevices: DeviceInstance[] = [testDevice, testDevice2];

  beforeEach(() => {
    TestBed.configureTestingModule({
      imports: [HttpClientTestingModule],
      providers: [DeviceInstanceService]
    });
    deviceService = TestBed.inject(DeviceInstanceService);
    httpMock = TestBed.inject(HttpTestingController);
  });

  it('should be created', () => {
    expect(deviceService).toBeTruthy();
  });

  it('should get the correct device count', () => {
    const deviceCount = 10;
    deviceService.getDevicesCount().subscribe((count: number) => {
      expect(count).toEqual(deviceCount);
    });

    const request = httpMock.expectOne(`${deviceRegistryServiceUrl}/v1/deviceInstances/count`, 'call to api');
    expect(request.request.method).toBe('GET');
    request.flush(deviceCount);
    httpMock.verify();
  });

  it('should get the correct devices', () => {
    const pageIndex = 0;
    const pageSize = 10;
    deviceService.getDevices(null, null, pageIndex, pageSize).subscribe((devices: DeviceInstance[]) => {
      expect(devices).toEqual(testDevices);
    });

    const request = httpMock.expectOne(
        `${deviceRegistryServiceUrl}/v1/deviceInstances?pageIndex=${pageIndex}&pageSize=${pageSize}`,
        'call to  api');
    expect(request.request.method).toBe('GET');
    request.flush(testDevices);
    httpMock.verify();
  });

  it('should get the correct device', () => {
    const source = 'source';
    const tenant = 'tenant';
    deviceService.getDevices(source, tenant, null, null).subscribe((device: DeviceInstance[]) => {
      expect(device).toEqual(testDevices);
    });

    const request = httpMock.expectOne(
      `${deviceRegistryServiceUrl}/v1/deviceInstances?source=${source}&tenant=${tenant}`,
      'call to  api');
    expect(request.request.method).toBe('GET');
    request.flush(testDevices);
    httpMock.verify();
  });

  it('should return the posted device', () => {
    deviceService.postDevice(testDevice).subscribe((postedDevice: DeviceInstance) => {
      expect(postedDevice).toEqual(testDevice);
    });

    const request = httpMock.expectOne(`${deviceRegistryServiceUrl}/v1/deviceInstances`, 'call to api');
    expect(request.request.method).toBe('POST');
    request.flush(testDevice);
    httpMock.verify();
  });

  it('should return the updated device', () => {
    const id = '123';
    deviceService.updateDevice(testDevice).subscribe((updatedDevice: DeviceInstance) => {
      expect(updatedDevice).toEqual(testDevice);
    });

    const request = httpMock.expectOne(`${deviceRegistryServiceUrl}/v1/deviceInstances/${id}`, 'call to api');
    expect(request.request.method).toBe('PUT');
    request.flush(testDevice);
    httpMock.verify();
  });

  it('should delete the correct device', () => {
    const responseStatus = 200;
    const id = '123';
    deviceService.deleteDevice(id).subscribe((data: string) => {
      expect(JSON.parse(data).status).toEqual(responseStatus);
    });

    const request = httpMock.expectOne(`${deviceRegistryServiceUrl}/v1/deviceInstances/${id}`, 'call to api');
    expect(request.request.method).toBe('DELETE');
    request.flush(JSON.stringify({status: responseStatus}));
    httpMock.verify();
  });
});
