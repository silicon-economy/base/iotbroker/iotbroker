/**
 * @license
 * Copyright Open Logistics Foundation
 *
 * Licensed under the Open Logistics Foundation License 1.3.
 * For details on the licensing terms, see the LICENSE file.
 * SPDX-License-Identifier: OLFL-1.3
 */

import { HttpClient } from '@angular/common/http';
import { Injectable } from '@angular/core';
import { Observable } from 'rxjs';
import { environment } from 'src/app/environments/environment';
import { DeviceInstance } from 'src/app/core/models/device-instance';
import { HTTP_OPTIONS_JSON_HEADER } from 'src/app/core/constants/http.configuration';

@Injectable({
  providedIn: 'root'
})
export class DeviceInstanceService {

  private static readonly DEVICE_REGISTRY_SERVICE_URL = `${environment.deviceRegistryServiceUrl}`;

  constructor(private http: HttpClient) {
  }

  /**
   * This method retrieves all sources
   * @return The sources as an observable
   */
  getAllSources(): Observable<string[]> {
    const url = `${DeviceInstanceService.DEVICE_REGISTRY_SERVICE_URL}/v1/deviceInstances/sources`;
    return this.http.get<string[]>(url);
  }

  /**
   * This method retrieves device instances based off the given parameter
   * @param source Source of the device instance
   * @param tenant Tenant of the device instance
   * @param pageIndex The page index from which the devices are retrieved
   * @param pageSize The size of the page from which the devices are retrieved
   */
  getDevices(source?: string, tenant?: string, pageIndex?: number, pageSize?: number): Observable<DeviceInstance[]> {
    const params = Object.assign({},
      source === null ? null : {source},
      tenant === null ? null : {tenant},
      pageIndex === null ? null : {pageIndex},
      pageSize === null ? null : {pageSize}
    );
    const url = `${DeviceInstanceService.DEVICE_REGISTRY_SERVICE_URL}/v1/deviceInstances`;
    return this.http.get<DeviceInstance[]>(url, {
      params: params
    });
  }

  /**
   * Returns the number of all registered devices
   * @return The number of devices
   */
  getDevicesCount() {
    const url = `${DeviceInstanceService.DEVICE_REGISTRY_SERVICE_URL}/v1/deviceInstances/count`;
    return this.http.get<number>(url);
  }

  /**
   * This method posts a new device
   * @param device The device that is added to the database
   */
  postDevice(device: DeviceInstance): Observable<DeviceInstance> {
    const url = `${DeviceInstanceService.DEVICE_REGISTRY_SERVICE_URL}/v1/deviceInstances`;
    const newDevice = {
      source: device.source,
      tenant: device.tenant,
      deviceTypeIdentifier: device.deviceTypeIdentifier,
      lastSeen: device.lastSeen,
      enabled: device.enabled,
      description: device.description,
      hardwareRevision: device.hardwareRevision,
      firmwareVersion: device.firmwareVersion,
    };
    return this.http.post<DeviceInstance>(url, JSON.stringify(newDevice), HTTP_OPTIONS_JSON_HEADER);
  }

  /**
   * This method updates an existing device
   * @param device The device that is updated
   */
  updateDevice(device: DeviceInstance): Observable<DeviceInstance> {
    const url = `${DeviceInstanceService.DEVICE_REGISTRY_SERVICE_URL}/v1/deviceInstances/${device.id}`;
    const newDevice = {
      deviceTypeIdentifier: device.deviceTypeIdentifier,
      lastSeen: device.lastSeen,
      enabled: device.enabled,
      description: device.description,
      hardwareRevision: device.hardwareRevision,
      firmwareVersion: device.firmwareVersion,
    };
    return this.http.put<DeviceInstance>(url, newDevice, HTTP_OPTIONS_JSON_HEADER);
  }

  /**
   * This method deletes a specific device
   * @param id The id of the device
   */
  deleteDevice(id: string) {
    const url = `${DeviceInstanceService.DEVICE_REGISTRY_SERVICE_URL}/v1/deviceInstances/${id}`;
    return this.http.delete(url);
  }
}
