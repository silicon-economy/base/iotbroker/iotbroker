/**
 * @license
 * Copyright Open Logistics Foundation
 *
 * Licensed under the Open Logistics Foundation License 1.3.
 * For details on the licensing terms, see the LICENSE file.
 * SPDX-License-Identifier: OLFL-1.3
 */

import { HttpClient } from '@angular/common/http';
import { Injectable } from '@angular/core';
import { Observable } from 'rxjs';
import { environment } from 'src/app/environments/environment';
import { DeviceType } from 'src/app/core/models/device-type';
import { HTTP_OPTIONS_JSON_HEADER } from 'src/app/core/constants/http.configuration';

@Injectable({
  providedIn: 'root'
})
export class DeviceTypeService {


  private static readonly DEVICE_REGISTRY_SERVICE_URL = `${environment.deviceRegistryServiceUrl}`;

  constructor(private http: HttpClient) {
  }

  getDeviceTypes(source?: string, identifier?: string, pageIndex?: number, pageSize?: number): Observable<DeviceType[]> {
    const params = Object.assign({},
      source === null ? null : {source},
      identifier === null ? null : {identifier},
      pageIndex === null ? null : {pageIndex},
      pageSize === null ? null : {pageSize}
    );
    const url = `${DeviceTypeService.DEVICE_REGISTRY_SERVICE_URL}/v1/deviceTypes`;
    return this.http.get<DeviceType[]>(url, {
      params: params
    });
  }

  /**
   * This method retrieves the number of device types
   * @returns The number of device types as an observable
   */
  getDeviceTypeCount(): Observable<number> {
    const url = `${DeviceTypeService.DEVICE_REGISTRY_SERVICE_URL}/v1/deviceTypes/count`;
    return this.http.get<number>(url);
  }

  getSources(): Observable<string[]> {
    const url = `${DeviceTypeService.DEVICE_REGISTRY_SERVICE_URL}/v1/deviceTypes/sources`;
    return this.http.get<string[]>(url);
  }

  /**
   * Posts a new device type
   * @param deviceType The new device type
   */
  postDeviceType(deviceType: DeviceType): Observable<DeviceType> {
    const url = `${DeviceTypeService.DEVICE_REGISTRY_SERVICE_URL}/v1/deviceTypes`;
    const newType = {
      source: deviceType.source,
      identifier: deviceType.identifier,
      providedBy: deviceType.providedBy,
      description: deviceType.description,
      enabled: deviceType.enabled,
      autoRegisterDeviceInstances: deviceType.autoRegisterDeviceInstances,
      autoEnableDeviceInstances: deviceType.autoEnableDeviceInstances,
    };
    return this.http.post<DeviceType>(url, JSON.stringify(newType), HTTP_OPTIONS_JSON_HEADER);
  }

  /**
   * Updates an existing device type
   * @param deviceType The updated device type
   */
  updateDeviceType(deviceType: DeviceType): Observable<DeviceType> {
    const url = `${DeviceTypeService.DEVICE_REGISTRY_SERVICE_URL}/v1/deviceTypes/${deviceType.id}`;
    const newType = {
      source: deviceType.source,
      identifier: deviceType.identifier,
      providedBy: deviceType.providedBy,
      description: deviceType.description,
      enabled: deviceType.enabled,
      autoRegisterDeviceInstances: deviceType.autoRegisterDeviceInstances,
      autoEnableDeviceInstances: deviceType.autoEnableDeviceInstances,
    };
    return this.http.put<DeviceType>(url, JSON.stringify(newType), HTTP_OPTIONS_JSON_HEADER);
  }

  /**
   * Deletes a specific device type
   * @param id The ID from the device type that will be deleted
   */
  deleteDeviceType(id: string) {
    const url = `${DeviceTypeService.DEVICE_REGISTRY_SERVICE_URL}/v1/deviceTypes/${id}`;
    return this.http.delete(url);
  }

}
