/**
 * @license
 * Copyright Open Logistics Foundation
 *
 * Licensed under the Open Logistics Foundation License 1.3.
 * For details on the licensing terms, see the LICENSE file.
 * SPDX-License-Identifier: OLFL-1.3
 */

import { HttpClientTestingModule, HttpTestingController } from '@angular/common/http/testing';
import { TestBed } from '@angular/core/testing';
import { DeviceTypeService } from 'src/app/features/devices/services/device-type.service';
import { DeviceType } from 'src/app/core/models/device-type';


describe('DeviceTypeService', () => {
  let deviceTypeService: DeviceTypeService;
  let httpMock: HttpTestingController;

  const deviceRegistryServiceUrl = 'http://localhost:8081';

  const testDeviceType: DeviceType = {
    id: '123',
    source: 'source1',
    identifier: 'ID123',
    providedBy: ['a', 'b'],
    description: 'None',
    enabled: true,
    autoRegisterDeviceInstances: false,
    autoEnableDeviceInstances: false
  };

  const testDeviceType2: DeviceType = {
    id: '234',
    source: 'source2',
    identifier: 'ID234',
    providedBy: ['c', 'd'],
    description: 'None',
    enabled: true,
    autoRegisterDeviceInstances: false,
    autoEnableDeviceInstances: false
  };

  const testSources = ['source1', 'source2'];

  const testDeviceTypes = [testDeviceType, testDeviceType2];

  beforeEach(() => {
    TestBed.configureTestingModule({
      imports: [HttpClientTestingModule],
      providers: [DeviceTypeService]
    });
    deviceTypeService = TestBed.inject(DeviceTypeService);
    httpMock = TestBed.inject(HttpTestingController);
  });

  it('should be created', () => {
    expect(deviceTypeService).toBeTruthy();
  });

  it('should get the correct device type count', () => {
    const deviceTypeCount = 10;
    deviceTypeService.getDeviceTypeCount().subscribe((count: number) => {
      expect(count).toEqual(deviceTypeCount);
    });

    const request = httpMock.expectOne(`${deviceRegistryServiceUrl}/v1/deviceTypes/count`, 'call to api');
    expect(request.request.method).toBe('GET');
    request.flush(deviceTypeCount);
    httpMock.verify();
  });

  it('should get the correct sources', () => {
    deviceTypeService.getSources().subscribe((sources: string[]) => {
      expect(sources).toEqual(testSources);
    });

    const request = httpMock.expectOne(`${deviceRegistryServiceUrl}/v1/deviceTypes/sources`, 'call to api');
    expect(request.request.method).toBe('GET');
    request.flush(testSources);
    httpMock.verify();
  });

  it('should get the correct device types from one source', () => {
    const pageIndex = 0;
    const pageSize = 10;
    const source = 'source';
    deviceTypeService.getDeviceTypes(source, null, pageIndex, pageSize).subscribe((deviceTypes: DeviceType[]) => {
      expect(deviceTypes).toEqual(testDeviceTypes);
    });

    const request = httpMock.expectOne(
      `${deviceRegistryServiceUrl}/v1/deviceTypes?source=${source}&pageIndex=${pageIndex}&pageSize=${pageSize}`,
      'call to  api');
    expect(request.request.method).toBe('GET');
    request.flush(testDeviceTypes);
    httpMock.verify();
  });

  it('should get the correct device type', () => {
    const pageIndex = 0;
    const pageSize = 10;
    deviceTypeService.getDeviceTypes(null, null, pageIndex, pageSize).subscribe((deviceTypes: DeviceType[]) => {
      expect(deviceTypes).toEqual(testDeviceTypes);
    });

    const request = httpMock.expectOne(
      `${deviceRegistryServiceUrl}/v1/deviceTypes?pageIndex=${pageIndex}&pageSize=${pageSize}`,
      'call to  api');
    expect(request.request.method).toBe('GET');
    request.flush(testDeviceTypes);
    httpMock.verify();
  });

  it('should return the posted device type', () => {
    deviceTypeService.postDeviceType(testDeviceType).subscribe((postedDeviceType: DeviceType) => {
      expect(postedDeviceType).toEqual(testDeviceType);
    });

    const request = httpMock.expectOne(`${deviceRegistryServiceUrl}/v1/deviceTypes`, 'call to api');
    expect(request.request.method).toBe('POST');
    request.flush(testDeviceType);
    httpMock.verify();
  });

  it('should return the updated device type', () => {
    const id = '123';
    deviceTypeService.updateDeviceType(testDeviceType).subscribe((updatedDeviceType: DeviceType) => {
      expect(updatedDeviceType).toEqual(testDeviceType);
    });

    const request = httpMock.expectOne(`${deviceRegistryServiceUrl}/v1/deviceTypes/${id}`, 'call to api');
    expect(request.request.method).toBe('PUT');
    request.flush(testDeviceType);
    httpMock.verify();
  });

  it('should delete the correct device type', () => {
    const responseStatus = 200;
    const id = '123';
    deviceTypeService.deleteDeviceType(id).subscribe((data: string) => {
      expect(JSON.parse(data).status).toEqual(responseStatus);
    });

    const request = httpMock.expectOne(`${deviceRegistryServiceUrl}/v1/deviceTypes/${id}`, 'call to api');
    expect(request.request.method).toBe('DELETE');
    request.flush(JSON.stringify({status: responseStatus}));
    httpMock.verify();
  });
});
