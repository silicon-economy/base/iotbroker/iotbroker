/**
 * @license
 * Copyright Open Logistics Foundation
 *
 * Licensed under the Open Logistics Foundation License 1.3.
 * For details on the licensing terms, see the LICENSE file.
 * SPDX-License-Identifier: OLFL-1.3
 */

import { Component, Input, OnInit } from '@angular/core';
import { DeviceType } from 'src/app/core/models/device-type';
import { UntypedFormBuilder, UntypedFormGroup, Validators } from '@angular/forms';
import { isNotEffectivelyEmptyValidator } from 'src/app/core/utils/validators';
import { DeviceTypeService } from 'src/app/features/devices/services/device-type.service';
import { Subject } from 'rxjs';
import { FormMode } from 'src/app/features/devices/constants/form-mode.enum';

@Component({
  selector: 'app-device-type-edit-form',
  templateUrl: './device-type-edit-form.component.html',
  styleUrls: ['./device-type-edit-form.component.scss']
})
export class DeviceTypeEditFormComponent implements OnInit {

  @Input() type: DeviceType = new DeviceType();
  @Input() mode: FormMode = FormMode.Show;

  typeForm: UntypedFormGroup;

  constructor(private fb: UntypedFormBuilder,
              private deviceTypeService: DeviceTypeService) {
  }

  public ngOnInit() {
    this.typeForm = this.fb.group({
      source: [this.type.source],
      identifier: [this.type.identifier],
      providedBy: [this.type.providedBy],
      description: [this.type.description],
      enabled: [this.type.enabled],
      autoRegisterDeviceInstances: [this.type.autoRegisterDeviceInstances],
      autoEnableDeviceInstances: [this.type.autoEnableDeviceInstances],
    });

    this.configureForm();
  }

  /**
   * Disables form fields and sets validators depending form mode
   */
  configureForm(): void {
    switch(this.mode) {
      case FormMode.Show: {
        Object.keys(this.typeForm.controls).forEach(key => {
          this.typeForm.get(key).disable();
        });
        break;
      }
      case FormMode.Edit: {
        this.typeForm.get('source').disable();
        this.typeForm.get('identifier').disable();
        break;
      }
      case FormMode.Create: {
        this.typeForm.get('source').addValidators([Validators.required, isNotEffectivelyEmptyValidator]);
        this.typeForm.get('identifier').addValidators([Validators.required, isNotEffectivelyEmptyValidator]);
        break;
      }
    }
  }

  get source() {
    return this.typeForm.get('source');
  }

  get identifier() {
    return this.typeForm.get('identifier');
  }

  get description() {
    return this.typeForm.get('description');
  }

  get providedBy() {
    return this.typeForm.get('providedBy');
  }

  get enabled() {
    return this.typeForm.get('enabled');
  }

  get autoRegisterDeviceInstances() {
    return this.typeForm.get('autoRegisterDeviceInstances');
  }

  get autoEnableDeviceInstances() {
    return this.typeForm.get('autoEnableDeviceInstances');
  }

  // If one of the required fields is null or invalid this method returns false
  public requiredFieldsAreValid(): boolean {
    return !(this.source.value === null || this.identifier.value === null
        || this.source.invalid || this.identifier.invalid);
  }

  submit() {
    if (!this.requiredFieldsAreValid()) {
      return null;
    }

    const deviceType = new DeviceType();
    const subject = new Subject<DeviceType>();

    this.deviceTypeService.getDeviceTypes(this.source.value, this.identifier.value, null, null).subscribe(types => {
      if (this.mode !== FormMode.Edit && types.length !== 0) {
        this.typeForm.controls['identifier'].setErrors({ 'invalid': 'true' });
        subject.error({ 'invalid': 'true' });
        return null;
      }

      // create result device type
      deviceType.source = this.source.value;
      deviceType.identifier = this.identifier.value;
      deviceType.providedBy = [];
      if (this.providedBy.value != null) {
        for (const provider of this.providedBy.value.toString().split(',')) {
          deviceType.providedBy.push(provider);
        }
      }
      deviceType.description = this.description.value;
      deviceType.enabled = this.enabled.value;
      deviceType.autoRegisterDeviceInstances = this.autoRegisterDeviceInstances.value;
      deviceType.autoEnableDeviceInstances = this.autoEnableDeviceInstances.value;

      // updated devices already have an id
      if (this.mode === FormMode.Edit) {
        deviceType.id = this.type.id;
      }

      subject.next(deviceType);
    });

    return subject.hasError ? null : subject.asObservable();
  }

}
