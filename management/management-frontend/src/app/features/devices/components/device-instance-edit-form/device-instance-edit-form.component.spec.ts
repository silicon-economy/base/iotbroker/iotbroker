/**
 * @license
 * Copyright Open Logistics Foundation
 *
 * Licensed under the Open Logistics Foundation License 1.3.
 * For details on the licensing terms, see the LICENSE file.
 * SPDX-License-Identifier: OLFL-1.3
 */

import { ComponentFixture, TestBed, waitForAsync } from '@angular/core/testing';
import { DeviceInstanceEditFormComponent } from 'src/app/features/devices/components/device-instance-edit-form/device-instance-edit-form.component';
import { DeviceInstance } from 'src/app/core/models/device-instance';
import { DeviceType } from 'src/app/core/models/device-type';
import { MatFormFieldModule } from '@angular/material/form-field';
import { MatInputModule } from '@angular/material/input';
import { FormsModule, ReactiveFormsModule } from '@angular/forms';
import { BrowserAnimationsModule } from '@angular/platform-browser/animations';
import { HttpClientTestingModule } from '@angular/common/http/testing';
import { TranslateLoader, TranslateModule } from '@ngx-translate/core';
import { HttpClient } from '@angular/common/http';
import { MatCardModule } from '@angular/material/card';
import { MatCheckboxModule } from '@angular/material/checkbox';
import { DeviceTypeService } from 'src/app/features/devices/services/device-type.service';
import { of, Subject } from 'rxjs';
import { FormMode } from 'src/app/features/devices/constants/form-mode.enum';
import { FormControlPipe } from 'src/app/shared/pipes/form-control.pipe';
import { HttpLoaderFactory } from 'src/app/app.module';

describe('DeviceEditFormComponent', () => {
  let component: DeviceInstanceEditFormComponent;
  let fixture: ComponentFixture<DeviceInstanceEditFormComponent>;

  let deviceTypeServiceMock;

  const deviceTypeServiceSpy = jasmine.createSpyObj('DeviceTypeService', ['getDeviceTypes']);

  // test device
  const testDevice: DeviceInstance = {
    source: 'source',
    tenant: 'tenant',
    description: 'description',
    hardwareRevision: 'hr',
    firmwareVersion: 'fr',
    deviceTypeIdentifier: 'id',
    registrationTime: '2020-11-12T12:56:55Z',
    lastSeen: null,
    enabled: true
  };

  const testDevice2: DeviceInstance = {
    id: '1',
    source: 'source',
    tenant: 'tenant',
    description: 'description',
    hardwareRevision: 'hr',
    firmwareVersion: 'fr',
    deviceTypeIdentifier: 'id',
    registrationTime: '2020-11-12T12:56:55Z',
    lastSeen: '2020-11-12T12:56:55Z',
    enabled: true
  };

  // test device type
  const testDeviceType: DeviceType = {
    id: '1',
    source: 'source',
    identifier: 'id',
    providedBy: ['x', 'y'],
    description: 'description',
    enabled: true,
    autoRegisterDeviceInstances: true,
    autoEnableDeviceInstances: false
  };

  beforeEach(waitForAsync(() => {
    TestBed.configureTestingModule({
      declarations: [DeviceInstanceEditFormComponent, FormControlPipe],
      imports: [
        MatCardModule,
        MatCheckboxModule,
        MatFormFieldModule,
        MatInputModule,
        FormsModule,
        ReactiveFormsModule,
        BrowserAnimationsModule,
        HttpClientTestingModule,
        TranslateModule.forRoot({
          loader: {
            provide: TranslateLoader,
            useFactory: HttpLoaderFactory,
            deps: [HttpClient]
          }
        })
      ],
      providers: [
        { provide: DeviceTypeService, useValue: deviceTypeServiceSpy }
      ]
    });

    deviceTypeServiceMock = TestBed.inject(DeviceTypeService);
    deviceTypeServiceMock.getDeviceTypes.and.returnValue(of(testDeviceType));

    TestBed.compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(DeviceInstanceEditFormComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });

  it('should check the required fields', () => {
    component.mode = FormMode.Create;

    // fill required fields correctly
    component.source.setValue('source');
    component.tenant.setValue('tenant');
    component.deviceTypeIdentifier.setValue(testDevice.deviceTypeIdentifier);

    expect(component.requiredFieldsAreValid()).toBeTrue();
  });

  it('should check the invalid required fields', () => {
    component.mode = FormMode.Create;

    // fill required fields correctly
    component.source.setValue('source');
    component.deviceTypeIdentifier.setValue(testDevice.deviceTypeIdentifier);

    expect(component.requiredFieldsAreValid()).toBeFalse();
  });


  it('should return the correct new device', () => {
    component.mode = FormMode.Create;
    // fill device fields
    component.source.setValue(testDevice.source);
    component.tenant.setValue(testDevice.tenant);
    component.deviceTypeIdentifier.setValue(testDevice.deviceTypeIdentifier);
    component.lastSeen.setValue(null);
    component.enabled.setValue(testDevice.enabled);
    component.description.setValue(testDevice.description);
    component.hardwareRevision.setValue(testDevice.hardwareRevision);
    component.firmwareVersion.setValue(testDevice.firmwareVersion);

    const subject = new Subject<DeviceInstance>();
    subject.next(testDevice);

    expect(component.requiredFieldsAreValid()).toBeTrue();

    expect(component.submit()).toEqual(subject.asObservable());
    expect(deviceTypeServiceMock.getDeviceTypes).toHaveBeenCalled();
  });

  it('should return the correct updated device', () => {
    component.mode = FormMode.Edit;
    // fill device fields
    component.source.setValue(testDevice2.source);
    component.tenant.setValue(testDevice2.tenant);
    component.deviceTypeIdentifier.setValue(testDevice2.deviceTypeIdentifier);
    component.lastSeen.setValue(testDevice2.lastSeen);
    component.enabled.setValue(testDevice2.enabled);
    component.description.setValue(testDevice2.description);
    component.hardwareRevision.setValue(testDevice2.hardwareRevision);
    component.firmwareVersion.setValue(testDevice2.firmwareVersion);

    const subject = new Subject<DeviceInstance>();
    subject.next(testDevice2);

    expect(component.requiredFieldsAreValid()).toBeTrue();

    expect(component.submit()).toEqual(subject.asObservable());
    expect(deviceTypeServiceMock.getDeviceTypes).toHaveBeenCalled();
  });

  it('should not return device with wrong device type', () => {
    deviceTypeServiceMock.getDeviceTypes.and.returnValue(of([]));
    component.mode = FormMode.Create;
    // fill device fields
    component.source.setValue(testDevice.source);
    component.tenant.setValue(testDevice.tenant);
    component.deviceTypeIdentifier.setValue(testDevice.deviceTypeIdentifier);
    component.lastSeen.setValue(null);
    component.enabled.setValue(testDevice.enabled);
    component.description.setValue(testDevice.description);
    component.hardwareRevision.setValue(testDevice.hardwareRevision);
    component.firmwareVersion.setValue(testDevice.firmwareVersion);

    expect(component.submit()).toBeNull();
    expect(deviceTypeServiceMock.getDeviceTypes).toHaveBeenCalled();
  });
});
