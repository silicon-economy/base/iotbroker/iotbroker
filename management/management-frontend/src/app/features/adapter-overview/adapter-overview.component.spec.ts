/**
 * @license
 * Copyright Open Logistics Foundation
 *
 * Licensed under the Open Logistics Foundation License 1.3.
 * For details on the licensing terms, see the LICENSE file.
 * SPDX-License-Identifier: OLFL-1.3
 */

import { async, ComponentFixture, TestBed } from '@angular/core/testing';
import { TranslateLoader, TranslateModule } from '@ngx-translate/core';
import { HttpClient } from '@angular/common/http';
import { HttpClientTestingModule, HttpTestingController } from '@angular/common/http/testing';
import { Type } from '@angular/core';
import { TranslateHttpLoader } from '@ngx-translate/http-loader';
import { AdapterOverviewComponent } from 'src/app/features/adapter-overview/adapter-overview.component';
import { MatToolbarModule } from '@angular/material/toolbar';
import { MatListModule } from '@angular/material/list';
import { MatCardModule } from '@angular/material/card';
import { MatExpansionModule } from '@angular/material/expansion';
import { BrowserAnimationsModule } from '@angular/platform-browser/animations';
import { MatButtonModule } from '@angular/material/button';
import { MatIconModule } from '@angular/material/icon';
import { of } from 'rxjs';
import { HttpService } from 'src/app/core/services/http.service';
import { Adapter } from 'src/app/core/models/adapter';
import { OverlayService } from 'src/app/core/services/overlay.service';

export function HttpLoaderFactory(http: HttpClient) {
  return new TranslateHttpLoader(http, './app/assets/lang/', '.json');
}

describe('AdapterOverviewComponent', () => {
  let component: AdapterOverviewComponent;
  let fixture: ComponentFixture<AdapterOverviewComponent>;

  let httpMock: HttpTestingController;
  let mockService;
  const spy = jasmine.createSpyObj('HttpService', ['getAllAdapter']);
  const spyOverlay = jasmine.createSpyObj('OverlayService', ['setup', 'startLoadingAnimation', 'stopLoadingAnimation', 'cleanup']);


  // Test Data
  const testAdapter1: Adapter = {
    adapterType: 'Adapter 1',
    instanceCount: 2,
    instances: [{ instanceName: 'abc', timeStarted: '1.1.2000' }, { instanceName: 'xyz', timeStarted: '1.1.9999' }]
  };

  const testAdapter2: Adapter = {
    adapterType: 'Adapter 1',
    instanceCount: 2,
    instances: [{ instanceName: 'dasd', timeStarted: '1.1.2000' }, { instanceName: 'weqw', timeStarted: '1.1.9999' }]
  };

  const testAdapter: Adapter[] = [testAdapter1, testAdapter2];

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [AdapterOverviewComponent],
      imports: [
        MatToolbarModule,
        MatListModule,
        MatCardModule,
        MatExpansionModule,
        BrowserAnimationsModule,
        MatButtonModule,
        MatIconModule,
        TranslateModule.forRoot({
          loader: {
            provide: TranslateLoader,
            useFactory: HttpLoaderFactory,
            deps: [HttpClient]
          }
        }),
        HttpClientTestingModule
      ],
      providers: [
        { provide: HttpService, useValue: spy },
        { provide: OverlayService, useValue: spyOverlay }
      ]
    });

    fixture = TestBed.createComponent(AdapterOverviewComponent);
    httpMock = fixture.debugElement.injector.get<HttpTestingController>(HttpTestingController as Type<HttpTestingController>);

    // mock functions
    mockService = TestBed.get(HttpService);
    mockService.getAllAdapter.and.returnValue(of(testAdapter));
    TestBed.compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(AdapterOverviewComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });

  it('should get all adapter', () => {
    expect(mockService.getAllAdapter).toBeDefined();
    expect(mockService.getAllAdapter).toHaveBeenCalled();
    expect(component.adapterList.length).toBe(2);
  });
});
