/**
 * @license
 * Copyright Open Logistics Foundation
 *
 * Licensed under the Open Logistics Foundation License 1.3.
 * For details on the licensing terms, see the LICENSE file.
 * SPDX-License-Identifier: OLFL-1.3
 */

import { FormControlPipe } from 'src/app/shared/pipes/form-control.pipe';

describe('FormControlPipe', () => {
  it('create an instance', () => {
    const pipe = new FormControlPipe();
    expect(pipe).toBeTruthy();
  });
});
