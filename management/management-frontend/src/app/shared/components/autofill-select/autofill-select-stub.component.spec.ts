/**
 * @license
 * Copyright Open Logistics Foundation
 *
 * Licensed under the Open Logistics Foundation License 1.3.
 * For details on the licensing terms, see the LICENSE file.
 * SPDX-License-Identifier: OLFL-1.3
 */

import { Component, Input } from '@angular/core';
import { AutofillSelectComponent } from 'src/app/shared/components/autofill-select/autofill-select.component';
import { UntypedFormControl, Validators } from '@angular/forms';

@Component({
  selector: 'app-autofill-select',
  template: '',
  providers: [
    {
      provide: AutofillSelectComponent,
      useClass: AutofillSelectStubComponent
    }
  ]
})
export class AutofillSelectStubComponent {

  @Input() label = '';
  @Input() error = this.label + ' is required';
  @Input() allowWildcards: boolean;

  elementList: string[] = [];
  element = new UntypedFormControl('', [Validators.required]);



  initializeElements(list: string[]) {
    this.elementList = list;
  }
}
