/**
 * @license
 * Copyright Open Logistics Foundation
 *
 * Licensed under the Open Logistics Foundation License 1.3.
 * For details on the licensing terms, see the LICENSE file.
 * SPDX-License-Identifier: OLFL-1.3
 */

import { ConfirmDialogConfiguration } from 'src/app/shared/components/confirm-dialog/dialog-configuration/confirm-dialog-configuration.interface';

export interface DialogData<C> {
  title: string,
  componentConfiguration: ConfirmDialogConfiguration<C>;
}
