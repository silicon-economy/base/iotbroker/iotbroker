/**
 * @license
 * Copyright Open Logistics Foundation
 *
 * Licensed under the Open Logistics Foundation License 1.3.
 * For details on the licensing terms, see the LICENSE file.
 * SPDX-License-Identifier: OLFL-1.3
 */

import { Type } from '@angular/core';
import { Subject } from 'rxjs';

export interface ConfirmDialogConfiguration<C> {
  contentComponentType: Type<C>;

  configureContentComponent(contentComponent: C): void;

  dialogConfirmAction(contentComponent: C): Subject<boolean>;
}
