/**
 * @license
 * Copyright Open Logistics Foundation
 *
 * Licensed under the Open Logistics Foundation License 1.3.
 * For details on the licensing terms, see the LICENSE file.
 * SPDX-License-Identifier: OLFL-1.3
 */

import { DialogContentDirective } from 'src/app/shared/components/confirm-dialog/directives/dialog-content.directive';
import { Component, ViewChild } from '@angular/core';
import { ComponentFixture, TestBed } from '@angular/core/testing';

@Component({
  template: '<ng-template appDialogContent></ng-template>',
})
class TestHostComponent {
  @ViewChild(DialogContentDirective, { static: true }) directive: DialogContentDirective;
}

describe('DialogContentDirective', () => {

  let fixture: ComponentFixture<TestHostComponent>;
  beforeEach(async () => {
    await TestBed.configureTestingModule({
      declarations: [DialogContentDirective, TestHostComponent],
    }).compileComponents();
    fixture = TestBed.createComponent(TestHostComponent);
  });

  it('should create an instance', () => {
    const directive = fixture.componentInstance.directive;
    expect(directive).toBeTruthy();
  });
});
