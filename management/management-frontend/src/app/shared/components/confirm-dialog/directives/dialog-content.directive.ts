/**
 * @license
 * Copyright Open Logistics Foundation
 *
 * Licensed under the Open Logistics Foundation License 1.3.
 * For details on the licensing terms, see the LICENSE file.
 * SPDX-License-Identifier: OLFL-1.3
 */

import { Directive, ViewContainerRef } from '@angular/core';

@Directive({
  selector: '[appDialogContent]'
})
export class DialogContentDirective {

  constructor(public viewContainerRef: ViewContainerRef) {
  }
}
