/**
 * @license
 * Copyright Open Logistics Foundation
 *
 * Licensed under the Open Logistics Foundation License 1.3.
 * For details on the licensing terms, see the LICENSE file.
 * SPDX-License-Identifier: OLFL-1.3
 */

import {ServiceLocator} from 'src/app/core/services/locator.service';
import { TranslateService } from '@ngx-translate/core';

export class TableColumnConfig {
  public translateService;
  attributeName: string;
  i18nKey: string;
  format: FormatFunction;

  /**
   * @param attributeName Name of the value
   * @param i18nKey Key for translation
   * @param format Optional formatting method for the value
   */
  constructor(attributeName: string, i18nKey: string, format: FormatFunction = standardStringFormatter ) {
    this.attributeName = attributeName;
    this.format = format;
    this.i18nKey = i18nKey;
    this.translateService = ServiceLocator.injector.get(TranslateService);
  }

  getHeader(){
    return this.translateService.instant(this.i18nKey);
  }

}

export type FormatFunction = (value) => string;
export const standardStringFormatter: FormatFunction = (value) => {
  return value.toString();
};

