/**
 * @license
 * Copyright Open Logistics Foundation
 *
 * Licensed under the Open Logistics Foundation License 1.3.
 * For details on the licensing terms, see the LICENSE file.
 * SPDX-License-Identifier: OLFL-1.3
 */

import { AfterContentInit, AfterViewInit, Component, Input, OnInit, TemplateRef, ViewChild } from '@angular/core';
import { animate, state, style, transition, trigger } from '@angular/animations';
import { MatSort } from '@angular/material/sort';
import { MatTableDataSource } from '@angular/material/table';
import { TableColumnConfig } from 'src/app/shared/components/expandable-table/models/table-column-config';

@Component({
  selector: 'app-expandable-table',
  templateUrl: './expandable-table.component.html',
  styleUrls: ['./expandable-table.component.scss'],
  animations: [
    trigger('detailExpand', [
      state('collapsed', style({height: '0px', minHeight: '0'})),
      state('expanded', style({height: '*'})),
      transition('expanded <=> collapsed', animate('225ms cubic-bezier(0.4, 0.0, 0.2, 1)')),
    ]),
  ]
})
export class ExpandableTableComponent<T> implements AfterContentInit, AfterViewInit, OnInit {

  @ViewChild(MatSort) sort: MatSort;

  @Input() itemTemplate: TemplateRef<object>;
  @Input() description: string;
  @Input() tableColumnConfiguration: TableColumnConfig[] = [];

  // Variables for the table
  public dataSource = new MatTableDataSource<T>();
  public expandedElement: T | null;

  public actionColumn: TableColumnConfig;

  public ngOnInit() {
    this.actionColumn = new TableColumnConfig('actions', 'expandabletable.actions', null);
  }

  public ngAfterContentInit(): void {
    this.dataSource.data = [];
  }

  public ngAfterViewInit(): void {
    this.dataSource.sort = this.sort;
  }

  public getAttributes(): string[] {
    const attributes = this.tableColumnConfiguration.map((element) => element.attributeName);
    attributes.push(this.actionColumn.attributeName);
    return attributes;
  }

  public addElement(element: T): void {
    this.dataSource.data.push(element);
    this.dataSource._updateChangeSubscription();
  }

  public addElementToBeginning(element: T): void {
    this.dataSource.data.unshift(element);
    this.dataSource._updateChangeSubscription();
  }

  public addElements(elements: T[]): void {
    for (const element of elements) {
      this.dataSource.data.push(element);
    }
    this.dataSource._updateChangeSubscription();
  }

  public clearElements(): void {
    this.dataSource.data = [];
    this.dataSource._updateChangeSubscription();
  }

}
