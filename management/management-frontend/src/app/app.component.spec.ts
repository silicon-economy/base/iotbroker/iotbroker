/**
 * @license
 * Copyright Open Logistics Foundation
 *
 * Licensed under the Open Logistics Foundation License 1.3.
 * For details on the licensing terms, see the LICENSE file.
 * SPDX-License-Identifier: OLFL-1.3
 */

import { async, ComponentFixture, TestBed } from '@angular/core/testing';
import { AppComponent } from 'src/app/app.component';
import { TranslateModule, TranslateLoader } from '@ngx-translate/core';
import { HttpClient } from '@angular/common/http';
import { HttpClientTestingModule } from '@angular/common/http/testing';
import { TranslateHttpLoader } from '@ngx-translate/http-loader';
import { AppRoutingModule } from 'src/app/app-routing.module';
import { AdapterOverviewComponent } from 'src/app/features/adapter-overview/adapter-overview.component';
import { LoadPageComponent } from 'src/app/features/load-page/load-page.component';
import { WelcomePageComponent } from 'src/app/features/welcome-page/welcome-page.component';
import { DeviceInstanceOverviewComponent } from 'src/app/features/devices/pages/device-instance-overview/device-instance-overview.component';
import { MatToolbarModule } from '@angular/material/toolbar';
import { MatListModule } from '@angular/material/list';
import { MatCardModule } from '@angular/material/card';
import { MatExpansionModule } from '@angular/material/expansion';
import { BrowserAnimationsModule } from '@angular/platform-browser/animations';
import { MatButtonModule } from '@angular/material/button';
import { MatIconModule } from '@angular/material/icon';
import { NgApexchartsModule } from 'ng-apexcharts';
import { MatSelectModule } from '@angular/material/select';
import { FormsModule, ReactiveFormsModule } from '@angular/forms';
import { BrowserModule } from '@angular/platform-browser';
import { MatRadioModule } from '@angular/material/radio';
import { LiveMessageDisplayComponent } from 'src/app/features/device-messages/pages/live-message-display/live-message-display.component';
import { MatTableModule } from '@angular/material/table';
import { of } from 'rxjs';
import { DeviceInstance } from 'src/app/core/models/device-instance';
import { MatOptionModule } from '@angular/material/core';
import { MatFormFieldModule } from '@angular/material/form-field';
import { HttpService } from 'src/app/core/services/http.service';
import { MatTreeModule } from '@angular/material/tree';
import { MatDialogModule, MatDialogRef, MAT_DIALOG_DATA } from '@angular/material/dialog';
import { MatSidenavModule } from '@angular/material/sidenav';

export function HttpLoaderFactory(http: HttpClient) {
  return new TranslateHttpLoader(http, '../app/assets/lang/', '.json');
}

describe('AppComponent', () => {
  let component: AppComponent;
  let fixture: ComponentFixture<AppComponent>;

  let mockDeviceinfo: DeviceInstance;
  let mockService;
  const spy = jasmine.createSpyObj('HttpService', ['getAllDevices']);

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [
        AppComponent,
        AdapterOverviewComponent,
        LoadPageComponent,
        WelcomePageComponent,
        DeviceInstanceOverviewComponent,
        LiveMessageDisplayComponent],
      imports: [
        AppRoutingModule,
        MatToolbarModule,
        MatTableModule,
        MatListModule,
        MatCardModule,
        MatExpansionModule,
        BrowserAnimationsModule,
        MatButtonModule,
        MatIconModule,
        MatSelectModule,
        FormsModule,
        ReactiveFormsModule,
        MatRadioModule,
        MatOptionModule,
        NgApexchartsModule,
        MatListModule,
        BrowserModule,
        MatTreeModule,
        MatSidenavModule,
        TranslateModule.forRoot({
          loader: {
            provide: TranslateLoader,
            useFactory: HttpLoaderFactory,
            deps: [HttpClient]
          }
        }),
        HttpClientTestingModule,
        MatFormFieldModule,
        MatDialogModule
      ],
      providers: [{ provide: HttpService, useValue: spy }, { provide: MatDialogRef, useValue: {} },
        { provide: MAT_DIALOG_DATA, useValue: {} }]
    });

    fixture = TestBed.createComponent(AppComponent);
    mockDeviceinfo = {
      id: 'id',
      source: 'source1',
      tenant: 'tenant1',
      deviceTypeIdentifier: 'ID123',
      registrationTime: 'test',
      lastSeen: 'test',
      enabled: false,
      hardwareRevision: 'revision 1',
      firmwareVersion: 'version 1',
      description: 'description',
    };
    mockService = TestBed.get(HttpService);
    mockService.getAllDevices.and.returnValue(of([mockDeviceinfo]));

    TestBed.compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(AppComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });

  it('should change the language', () => {
    spyOn(component.translate, 'use');
    component.changeLang('de');
    expect(component.translate.use).toHaveBeenCalledWith('de');
  });

  it('should toggle side nave only if breakpointStateLtMd is false', () => {
    spyOn(component.sidenavService, 'toggleSidenavState');
    component.breakpointStateLtMd = true;
    component.sidenavStateToggle();
    expect(component.sidenavService.toggleSidenavState).toHaveBeenCalledTimes(0);

    component.breakpointStateLtMd = false;
    component.sidenavStateToggle();
    expect(component.sidenavService.toggleSidenavState).toHaveBeenCalled();

  });

  it('should cycle the themes', () => {
    const firstTheme = document.body.classList[0];
    component.CycleTheme();
    const secondTheme = document.body.classList[0];
    expect(firstTheme === secondTheme).toBeFalse();
  });




});
