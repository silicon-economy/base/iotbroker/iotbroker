/**
 * Template to replace environment variables during runtime.
 * Needs to be done in assets folder since environment.ts is barely accessible after build.
 * Attention: New environment variables also need to be added to assets/env.js.
 * Detailed Information: https://pumpingco.de/blog/environment-variables-angular-docker/
 */

(function(window){
    window["env"]=window["env"]||{}
    window['env']['production'] = "${production}";
    window["env"]["MANAGEMENT_BACKEND_URL"] = "${MANAGEMENT_BACKEND_URL}";
    window["env"]["MANAGEMENT_BACKEND_HOST"] = "${MANAGEMENT_BACKEND_HOST}";
    window["env"]["MANAGEMENT_BACKEND_PORT"] = "${MANAGEMENT_BACKEND_PORT}";
    window["env"]["DEVICE_REGISTRY_SERVICE_URL"] = "${DEVICE_REGISTRY_SERVICE_URL}";
    window["env"]["SENSOR_DATA_HISTORY_SERVICE_URL"] = "${SENSOR_DATA_HISTORY_SERVICE_URL}";
    window["env"]["SENSOR_DATA_SUBSCRIPTION_SERVICE_WEBSOCKET_URL"] = "${SENSOR_DATA_SUBSCRIPTION_SERVICE_WEBSOCKET_URL}";
}(this))
