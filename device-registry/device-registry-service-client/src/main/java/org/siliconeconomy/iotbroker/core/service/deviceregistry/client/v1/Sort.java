/*
 * Copyright Open Logistics Foundation
 *
 * Licensed under the Open Logistics Foundation License 1.3.
 * For details on the licensing terms, see the LICENSE file.
 * SPDX-License-Identifier: OLFL-1.3
 */
package org.siliconeconomy.iotbroker.core.service.deviceregistry.client.v1;

/**
 * Defines the possible sort directions for an enumeration of elements.
 *
 * @author M. Grzenia
 */
public enum Sort {

  /**
   * Ascending sort direction.
   */
  ASC,
  /**
   * Descending sort direction.
   */
  DESC;
}
