/*
 * Copyright Open Logistics Foundation
 *
 * Licensed under the Open Logistics Foundation License 1.3.
 * For details on the licensing terms, see the LICENSE file.
 * SPDX-License-Identifier: OLFL-1.3
 */
package org.siliconeconomy.iotbroker.core.service.deviceregistry.client.v1.caching;

import org.siliconeconomy.iotbroker.model.device.DeviceInstance;

import java.util.Objects;
import java.util.function.Predicate;

import static java.util.Objects.requireNonNull;

/**
 * A filter for {@link DeviceInstance}s to be cached.
 * <p>
 * Filters device instances that are not associated with a specific device type.
 *
 * @author M. Grzenia
 */
public class DeviceInstanceFilter
        implements Predicate<DeviceInstance> {

    private final String deviceSource;
    private final String deviceTypeIdentifier;

    /**
     * Creates a new instance.
     *
     * @param deviceSource         Device source that is being used as a filter criterion.
     * @param deviceTypeIdentifier Device type that is being used as a filter criterion.
     */
    public DeviceInstanceFilter(String deviceSource, String deviceTypeIdentifier) {
        this.deviceSource = requireNonNull(deviceSource, "deviceSource");
        this.deviceTypeIdentifier = requireNonNull(deviceTypeIdentifier, "deviceTypeIdentifier");
    }

    @Override
    public boolean test(DeviceInstance deviceInstance) {
        return Objects.equals(deviceInstance.getSource(), deviceSource)
            && Objects.equals(deviceInstance.getDeviceTypeIdentifier(), deviceTypeIdentifier);
    }
}
