/*
 * Copyright Open Logistics Foundation
 *
 * Licensed under the Open Logistics Foundation License 1.3.
 * For details on the licensing terms, see the LICENSE file.
 * SPDX-License-Identifier: OLFL-1.3
 */
package org.siliconeconomy.iotbroker.core.service.deviceregistry.client.v1.exceptions;

/**
 * Indicates that an HTTP request resulted in a response with a status code 409.
 *
 * @author M. Grzenia
 */
public class ConflictException
    extends ClientErrorException {

  public ConflictException(String message) {
    super(message);
  }
}
