/*
 * Copyright Open Logistics Foundation
 *
 * Licensed under the Open Logistics Foundation License 1.3.
 * For details on the licensing terms, see the LICENSE file.
 * SPDX-License-Identifier: OLFL-1.3
 */
package org.siliconeconomy.iotbroker.core.service.deviceregistry.client.v1.caching;

import org.checkerframework.checker.nullness.qual.NonNull;
import org.checkerframework.checker.nullness.qual.Nullable;
import org.siliconeconomy.iotbroker.experimental.adapter.deviceupdate.DeviceTypeUpdateFilter;
import org.siliconeconomy.iotbroker.model.device.DeviceInstance;
import org.siliconeconomy.iotbroker.model.device.DeviceType;

import java.util.*;
import java.util.function.BiFunction;
import java.util.function.BinaryOperator;

import static java.util.Objects.requireNonNull;

/**
 * A cache for information about registered {@link DeviceType}s and {@link DeviceInstance}s.
 * <p>
 * This can include information retrieved directly from the IoT Broker's Device Registry Service
 * (via {@link CachingDeviceRegistryServiceClient} as well as asynchronously received updates
 * regarding registered {@link DeviceType}s and {@link DeviceInstance}s (via
 * {@link DeviceRegistryCacheUpdater}).
 * <p>
 * Implementation remarks:
 * <ul>
 *   <li>During runtime, the cache may be accessed by multiple threads, which is why its methods are
 *   explicitly synchronized.</li>
 *   <li>This is a rather simple implementation of a cache without a concept for cache invalidation.
 *   </li>
 * </ul>
 *
 * @author M. Grzenia
 */
public class DeviceRegistryCache {

  /**
   * The map containing the cached {@link DeviceType}s.
   */
  private final Map<String, DeviceType> deviceTypes = new HashMap<>();
  private final DeviceTypeFilter deviceTypeFilter;
  /**
   * The function that determines the key for a {@link DeviceType} to be cached.
   */
  private final BiFunction<String, String, String> typeKeyFunction = new DeviceTypeKeyFunction();
  /**
   * The map containing the cached {@link DeviceInstance}s.
   */
  private final Map<String, DeviceInstance> deviceInstances = new HashMap<>();
  private final DeviceInstanceFilter deviceInstanceFilter;
  /**
   * The function that determines the key for a {@link DeviceInstance} to be cached.
   */
  private final BiFunction<String, String, String> instanceKeyFunction
      = new DeviceInstanceKeyFunction();

  /**
   * Creates a new instance.
   *
   * @param deviceTypeFilter     A filter for {@link DeviceType}s to be cached.
   * @param deviceInstanceFilter A filter for {@link DeviceInstance}s to be cached.
   */
  public DeviceRegistryCache(DeviceTypeFilter deviceTypeFilter,
                             DeviceInstanceFilter deviceInstanceFilter) {
    this.deviceTypeFilter = requireNonNull(deviceTypeFilter, "deviceTypeFilter");
    this.deviceInstanceFilter = requireNonNull(deviceInstanceFilter, "deviceInstanceFilter");
  }

  /**
   * Checks whether a {@link DeviceType} with the given {@code source} and {@code identifier} is
   * contained in the cache.
   *
   * @param source     The device type source.
   * @param identifier The device type identifier.
   * @return {@code true}, if a corresponding device type is contained in the cache, otherwise
   * {@code false}.
   */
  public synchronized boolean containsDeviceType(@Nullable String source,
                                                 @Nullable String identifier) {
    if (source == null || identifier == null) {
      return false;
    }

    return deviceTypes.containsKey(typeKeyFunction.apply(source, identifier));
  }

  /**
   * Checks whether a {@link DeviceType} with the given {@code id} is contained in the cache.
   *
   * @param id The device type ID.
   * @return {@code true}, if a corresponding device type is contained in the cache, otherwise
   * {@code false}.
   */
  public synchronized boolean containsDeviceType(@NonNull String id) {
    return deviceTypes.values().stream()
        .anyMatch(deviceType -> Objects.equals(id, deviceType.getId()));
  }

  /**
   * Returns the {@link DeviceType} with the given {@code source} and {@code identifier}.
   *
   * @param source     The device type source.
   * @param identifier The device type identifier.
   * @return A device type.
   * @throws NoSuchElementException If a device type with the given {@code source} and
   *                                {@code identifier} is not contained in the cache.
   */
  public synchronized DeviceType getDeviceType(@Nullable String source,
                                               @Nullable String identifier) {
    DeviceType deviceType = deviceTypes.get(typeKeyFunction.apply(source, identifier));
    if (deviceType == null) {
      throw new NoSuchElementException(
          String.format("DeviceType - source: %s, identifier: %s.", source, identifier)
      );
    }

    return deviceType;
  }

  /**
   * Returns the {@link DeviceType} with the given {@code id}.
   *
   * @param id The device type ID.
   * @return A device type.
   * @throws NoSuchElementException If a device type with the given {@code id} is not contained in
   *                                the cache.
   */
  public synchronized DeviceType getDeviceType(@NonNull String id) {
    return deviceTypes.values().stream()
        .filter(deviceType -> Objects.equals(id, deviceType.getId()))
        .findFirst()
        .orElseThrow(() -> new NoSuchElementException(
            String.format("DeviceType - id: %s.", id)
        ));
  }

  /**
   * Adds the given {@link DeviceType} to the cache but only if it passes the cache's
   * {@link DeviceTypeUpdateFilter}.
   *
   * @param deviceType The device type.
   */
  public synchronized void addDeviceType(@NonNull DeviceType deviceType) {
    requireNonNull(deviceType, "deviceType");

    if (!deviceTypeFilter.test(deviceType)) {
      return;
    }

    deviceTypes.put(typeKeyFunction.apply(deviceType.getSource(), deviceType.getIdentifier()),
                    deviceType);
  }

  /**
   * Removes the {@link DeviceType} with the given {@code id} from the cache.
   *
   * @param id The device type ID.
   */
  public synchronized void removeDeviceType(String id) {
    Optional<DeviceType> deviceTypeOpt = deviceTypes.values().stream()
        .filter(deviceType -> Objects.equals(id, deviceType.getId()))
        .findFirst();

    if (deviceTypeOpt.isEmpty()) {
      return;
    }

    deviceTypes.remove(typeKeyFunction.apply(deviceTypeOpt.get().getSource(),
                                             deviceTypeOpt.get().getIdentifier()));
  }

  /**
   * Checks whether a {@link DeviceInstance} with the given {@code source} and {@code tenant} is
   * contained in the cache.
   *
   * @param source The device instance source.
   * @param tenant The device instance tenant.
   * @return {@code true}, if a corresponding device instance is contained in the cache, otherwise
   * {@code false}.
   */
  public synchronized boolean containsDeviceInstance(@Nullable String source,
                                                     @Nullable String tenant) {
    if (source == null || tenant == null) {
      return false;
    }

    return deviceInstances.containsKey(instanceKeyFunction.apply(source, tenant));
  }

  /**
   * Checks whether a {@link DeviceInstance} with the given {@code id} is contained in the cache.
   *
   * @param id The device instance ID.
   * @return {@code true}, if a corresponding device type is contained in the cache, otherwise
   * {@code false}.
   */
  public synchronized boolean containsDeviceInstance(@NonNull String id) {
    return deviceInstances.values().stream()
        .anyMatch(deviceInstance -> Objects.equals(id, deviceInstance.getId()));
  }

  /**
   * Returns the {@link DeviceInstance} with the given {@code source} and {@code tenant}.
   *
   * @param source The device instance source.
   * @param tenant The device instance tenant.
   * @return A device instance.
   * @throws NoSuchElementException If a device instance with the given {@code source} and
   *                                {@code tenant} is not contained in the cache.
   */
  public synchronized DeviceInstance getDeviceInstance(@Nullable String source,
                                                       @Nullable String tenant) {
    DeviceInstance deviceInstance = deviceInstances.get(instanceKeyFunction.apply(source, tenant));
    if (deviceInstance == null) {
      throw new NoSuchElementException(
          String.format("DeviceInstance - source: %s, tenant: %s.", source, tenant)
      );
    }

    return deviceInstance;
  }

  /**
   * Returns the {@link DeviceInstance} with the given {@code id}.
   *
   * @param id The device instance ID.
   * @return A device instance.
   * @throws NoSuchElementException If a device instance with the given {@code id} is not contained
   *                                in the cache.
   */
  public synchronized DeviceInstance getDeviceInstance(String id) {
    return deviceInstances.values().stream()
        .filter(deviceInstance -> Objects.equals(id, deviceInstance.getId()))
        .findFirst()
        .orElseThrow(() -> new NoSuchElementException(
            String.format("DeviceInstance - id: %s.", id)
        ));
  }

  /**
   * Adds the given {@link DeviceInstance} to the cache but only if it passes the cache's
   * {@link DeviceInstanceFilter}.
   *
   * @param deviceInstance The device instance.
   */
  public synchronized void addDeviceInstance(DeviceInstance deviceInstance) {
    if (!deviceInstanceFilter.test(deviceInstance)) {
      return;
    }

    deviceInstances.put(instanceKeyFunction.apply(deviceInstance.getSource(),
                                                  deviceInstance.getTenant()),
                        deviceInstance);
  }

  /**
   * Removes the {@link DeviceInstance} with the given {@code id} from the cache.
   *
   * @param id The device instance ID.
   */
  public synchronized void removeDeviceInstance(String id) {
    Optional<DeviceInstance> deviceInstanceOpt = deviceInstances.values().stream()
        .filter(deviceInstance -> Objects.equals(id, deviceInstance.getId()))
        .findFirst();

    if (deviceInstanceOpt.isEmpty()) {
      return;
    }

    deviceInstances.remove(instanceKeyFunction.apply(deviceInstanceOpt.get().getSource(),
                                                     deviceInstanceOpt.get().getTenant()));
  }

  /**
   * Defines the function that determines the key for a {@link DeviceType} to be cached.
   */
  private static class DeviceTypeKeyFunction
      implements BinaryOperator<String> {

    @Override
    public String apply(String source, String identifier) {
      return source + "-" + identifier;
    }
  }

  /**
   * Defines the function that determines the key for a {@link DeviceInstance} to be cached.
   */
  private static class DeviceInstanceKeyFunction
      implements BinaryOperator<String> {

    @Override
    public String apply(String source, String tenant) {
      return source + "-" + tenant;
    }
  }
}
