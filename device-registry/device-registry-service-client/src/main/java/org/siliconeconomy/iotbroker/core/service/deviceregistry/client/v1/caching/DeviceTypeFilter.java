/*
 * Copyright Open Logistics Foundation
 *
 * Licensed under the Open Logistics Foundation License 1.3.
 * For details on the licensing terms, see the LICENSE file.
 * SPDX-License-Identifier: OLFL-1.3
 */
package org.siliconeconomy.iotbroker.core.service.deviceregistry.client.v1.caching;

import org.siliconeconomy.iotbroker.model.device.DeviceType;

import java.util.Objects;
import java.util.function.Predicate;

import static java.util.Objects.requireNonNull;

/**
 * A filter for {@link DeviceType}s to be cached.
 *
 * @author M. Grzenia
 */
public class DeviceTypeFilter
    implements Predicate<DeviceType> {

  private final String deviceSource;
  private final String deviceTypeIdentifier;

  /**
   * Creates a new instance.
   *
   * @param deviceSource         Device source that is used as a filter criterion.
   * @param deviceTypeIdentifier Device type identifier that is used as a filter criterion.
   */
  public DeviceTypeFilter(String deviceSource, String deviceTypeIdentifier) {
    this.deviceSource = requireNonNull(deviceSource, "deviceSource");
    this.deviceTypeIdentifier = requireNonNull(deviceTypeIdentifier, "deviceTypeIdentifier");
  }

  @Override
  public boolean test(DeviceType deviceType) {
    return Objects.equals(deviceType.getSource(), deviceSource)
        && Objects.equals(deviceType.getIdentifier(), deviceTypeIdentifier);
  }
}
