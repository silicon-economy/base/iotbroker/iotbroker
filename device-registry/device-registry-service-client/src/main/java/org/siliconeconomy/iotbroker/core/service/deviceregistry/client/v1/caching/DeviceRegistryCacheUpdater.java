/*
 * Copyright Open Logistics Foundation
 *
 * Licensed under the Open Logistics Foundation License 1.3.
 * For details on the licensing terms, see the LICENSE file.
 * SPDX-License-Identifier: OLFL-1.3
 */
package org.siliconeconomy.iotbroker.core.service.deviceregistry.client.v1.caching;

import org.checkerframework.checker.nullness.qual.NonNull;
import org.siliconeconomy.iotbroker.experimental.adapter.deviceupdate.DeviceInstanceUpdateHandler;
import org.siliconeconomy.iotbroker.experimental.adapter.deviceupdate.DeviceTypeUpdateHandler;
import org.siliconeconomy.iotbroker.model.device.DeviceInstance;
import org.siliconeconomy.iotbroker.model.device.DeviceType;

import static java.util.Objects.requireNonNull;

/**
 * Handles updates in the context of the registration of {@link DeviceType}s and
 * {@link DeviceInstance}s and ensures that information stored in the {@link DeviceRegistryCache} is
 * updated accordingly.
 *
 * @author M. Grzenia
 */
public class DeviceRegistryCacheUpdater
        implements DeviceInstanceUpdateHandler,
                   DeviceTypeUpdateHandler {

  private final DeviceRegistryCache cache;

  /**
   * Creates a new instance.
   *
   * @param cache The cache containing information about registered {@link DeviceType}s and
   *              {@link DeviceInstance}s.
   */
  public DeviceRegistryCacheUpdater(DeviceRegistryCache cache) {
    this.cache = cache;
  }

  @Override
  public void onDeviceTypeCreated(@NonNull DeviceType newDeviceType) {
    // Newly created device types are explicitly not added to the cache here. Instead, this is
    // done lazily when a specific device type is fetched via the
    // CachingDeviceRegistryServiceClient.
  }

  @Override
  public void onDeviceTypeModified(@NonNull DeviceType oldDeviceType,
                                   @NonNull DeviceType newDeviceType) {
    requireNonNull(oldDeviceType, "oldDeviceType");
    requireNonNull(newDeviceType, "newDeviceType");

    synchronized (cache) {
      if (cache.containsDeviceType(oldDeviceType.getSource(), oldDeviceType.getIdentifier())) {
        // Perform some pseudo cache invalidation by removing the modified device type from the
        // cache. With the modifications it may no longer pass the cache's filter. If the device
        // type is still eligible to be cached, this will be done (again lazily) the next time this
        // specific device type is fetched.
        cache.removeDeviceType(oldDeviceType.getId());
      }
    }
  }

  @Override
  public void onDeviceTypeDeleted(@NonNull DeviceType oldDeviceType) {
    requireNonNull(oldDeviceType, "oldDeviceType");

    synchronized (cache) {
      cache.removeDeviceType(oldDeviceType.getId());
    }
  }

  @Override
  public void onDeviceInstanceCreated(@NonNull DeviceInstance newDeviceInstance) {
    // Newly created device instances are explicitly not added to the cache here. Instead, this is
    // done lazily when a specific device instance is fetched via the
    // CachingDeviceRegistryServiceClient.
  }

  @Override
  public void onDeviceInstanceModified(@NonNull DeviceInstance oldDeviceInstance,
                                       @NonNull DeviceInstance newDeviceInstance) {
    requireNonNull(oldDeviceInstance, "oldDeviceInstance");
    requireNonNull(newDeviceInstance, "newDeviceInstance");

    synchronized (cache) {
      if (cache.containsDeviceInstance(oldDeviceInstance.getSource(),
                                       oldDeviceInstance.getTenant())) {
        // Perform some pseudo cache invalidation by removing the modified device instance from the
        // cache. With the modifications it may no longer pass the cache's filter. If the device
        // instance is still eligible to be cached, this will be done (again lazily) the next time
        // this specific device instance is fetched.
        cache.removeDeviceInstance(oldDeviceInstance.getId());
      }
    }
  }

  @Override
  public void onDeviceInstanceDeleted(@NonNull DeviceInstance oldDeviceInstance) {
    requireNonNull(oldDeviceInstance, "oldDeviceInstance");

    synchronized (cache) {
      cache.removeDeviceInstance(oldDeviceInstance.getId());
    }
  }
}
