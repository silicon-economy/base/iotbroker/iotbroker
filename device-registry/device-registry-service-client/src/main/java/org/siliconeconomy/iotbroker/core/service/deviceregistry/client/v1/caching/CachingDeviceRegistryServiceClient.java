/*
 * Copyright Open Logistics Foundation
 *
 * Licensed under the Open Logistics Foundation License 1.3.
 * For details on the licensing terms, see the LICENSE file.
 * SPDX-License-Identifier: OLFL-1.3
 */
package org.siliconeconomy.iotbroker.core.service.deviceregistry.client.v1.caching;

import org.checkerframework.checker.nullness.qual.NonNull;
import org.checkerframework.checker.nullness.qual.Nullable;
import org.siliconeconomy.iotbroker.core.service.deviceregistry.client.v1.DeviceRegistryServiceClient;
import org.siliconeconomy.iotbroker.core.service.deviceregistry.client.v1.Sort;
import org.siliconeconomy.iotbroker.core.service.deviceregistry.common.webapi.v1.model.deviceinstance.DeviceInstanceCreateInputTO;
import org.siliconeconomy.iotbroker.core.service.deviceregistry.common.webapi.v1.model.deviceinstance.DeviceInstanceUpdateInputTO;
import org.siliconeconomy.iotbroker.core.service.deviceregistry.common.webapi.v1.model.devicetype.DeviceTypeCreateInputTO;
import org.siliconeconomy.iotbroker.core.service.deviceregistry.common.webapi.v1.model.devicetype.DeviceTypeUpdateInputTO;
import org.siliconeconomy.iotbroker.model.device.DeviceInstance;
import org.siliconeconomy.iotbroker.model.device.DeviceType;

import java.util.List;

import static java.util.Objects.requireNonNull;

/**
 * A {@link DeviceRegistryServiceClient} implementation with caching capabilities.
 * <p>
 * To reduce the number of requests to the Device Registry Service, this implementation <em>can</em>
 * cache responses regarding {@link DeviceType}s and {@link DeviceInstance}s for which requests were
 * made. Whether a {@link DeviceType} or {@link DeviceInstance} is <em>actually</em> added to the
 * cache depends on cache-specific rules (see {@link DeviceRegistryCache}).
 * <p>
 * Requests regarding {@link DeviceType}s and {@link DeviceInstance}s that are not (yet) contained
 * in the cache are delegated to the {@link DeviceRegistryServiceClient} provided to this class.
 *
 * @author M. Grzenia
 */
public class CachingDeviceRegistryServiceClient
    implements DeviceRegistryServiceClient {

  private final DeviceRegistryServiceClient delegate;
  private final DeviceRegistryCache cache;

  /**
   * Creates a new instance.
   *
   * @param delegate The {@link DeviceRegistryServiceClient} to delegate actual requests to the
   *                 Device Registry Service.
   * @param cache    The cache containing information about registered {@link DeviceType}s and
   *                 {@link DeviceInstance}s.
   */
  public CachingDeviceRegistryServiceClient(DeviceRegistryServiceClient delegate,
                                            DeviceRegistryCache cache) {
    this.delegate = requireNonNull(delegate, "delegate");
    this.cache = requireNonNull(cache, "cache");
  }

  @Override
  public @NonNull List<DeviceInstance> fetchDeviceInstances(@Nullable String source,
                                                            @Nullable String tenant,
                                                            @Nullable Integer pageIndex,
                                                            @Nullable Integer pageSize,
                                                            @Nullable Sort sortBySource,
                                                            @Nullable Sort sortByTenant) {
    synchronized (cache) {
      if (cache.containsDeviceInstance(source, tenant)) {
        return List.of(cache.getDeviceInstance(source, tenant));
      }

      List<DeviceInstance> deviceInstances = delegate.fetchDeviceInstances(source,
                                                                           tenant,
                                                                           pageIndex,
                                                                           pageSize,
                                                                           sortBySource,
                                                                           sortByTenant);
      for (DeviceInstance deviceInstance : deviceInstances) {
        cache.addDeviceInstance(deviceInstance);
      }

      return deviceInstances;
    }
  }

  @Override
  public @NonNull DeviceInstance createDeviceInstance(@NonNull DeviceInstanceCreateInputTO input) {
    // Newly created device instances are explicitly not added to the cache here. Instead, this is
    // done lazily when a specific device instance is fetched.
    return delegate.createDeviceInstance(input);
  }

  @Override
  public long fetchDeviceInstancesCount() {
    return delegate.fetchDeviceInstancesCount();
  }

  @Override
  public @NonNull List<String> fetchDeviceInstancesSources() {
    return delegate.fetchDeviceInstancesSources();
  }

  @Override
  public @NonNull DeviceInstance fetchDeviceInstance(@NonNull String id) {
    synchronized (cache) {
      if (cache.containsDeviceInstance(id)) {
        return cache.getDeviceInstance(id);
      }

      DeviceInstance deviceInstance = delegate.fetchDeviceInstance(id);
      cache.addDeviceInstance(deviceInstance);

      return deviceInstance;
    }
  }

  @Override
  public @NonNull DeviceInstance updateDeviceInstance(@NonNull String id,
                                                      @NonNull DeviceInstanceUpdateInputTO input) {
    DeviceInstance deviceInstance = delegate.updateDeviceInstance(id, input);
    // Perform some pseudo cache invalidation by removing the modified device instance from the
    // cache. With the modifications it may no longer pass the cache's filter. If the device
    // instance is still eligible to be cached, this will be done (again lazily) the next time this
    // specific device instance is fetched.
    cache.removeDeviceInstance(id);
    return deviceInstance;
  }

  @Override
  public void deleteDeviceInstance(@NonNull String id) {
    delegate.deleteDeviceInstance(id);
    cache.removeDeviceInstance(id);
  }

  @Override
  public @NonNull List<DeviceType> fetchDeviceTypes(@Nullable String source,
                                                    @Nullable String identifier,
                                                    @Nullable Integer pageIndex,
                                                    @Nullable Integer pageSize,
                                                    @Nullable Sort sortBySource,
                                                    @Nullable Sort sortByIdentifier) {
    synchronized (cache) {
      if (cache.containsDeviceType(source, identifier)) {
        return List.of(cache.getDeviceType(source, identifier));
      }

      List<DeviceType> deviceTypes = delegate.fetchDeviceTypes(source,
                                                               identifier,
                                                               pageIndex,
                                                               pageSize,
                                                               sortBySource,
                                                               sortByIdentifier);

      for (DeviceType deviceType : deviceTypes) {
        cache.addDeviceType(deviceType);
      }

      return deviceTypes;
    }
  }

  @Override
  public @NonNull DeviceType createDeviceType(@NonNull DeviceTypeCreateInputTO input) {
    // Newly created device types are explicitly not added to the cache here. Instead, this is
    // done lazily when a specific device type is fetched.
    return delegate.createDeviceType(input);
  }

  @Override
  public long fetchDeviceTypesCount() {
    return delegate.fetchDeviceTypesCount();
  }

  @Override
  public @NonNull List<String> fetchDeviceTypesSources() {
    return delegate.fetchDeviceTypesSources();
  }

  @Override
  public @NonNull DeviceType fetchDeviceType(@NonNull String id) {
    synchronized (cache) {
      if (cache.containsDeviceType(id)) {
        return cache.getDeviceType(id);
      }

      DeviceType deviceType = delegate.fetchDeviceType(id);
      cache.addDeviceType(deviceType);

      return deviceType;
    }
  }

  @Override
  public @NonNull DeviceType updateDeviceType(@NonNull String id,
                                              @NonNull DeviceTypeUpdateInputTO input) {
    DeviceType deviceType = delegate.updateDeviceType(id, input);
    // Perform some pseudo cache invalidation by removing the modified device type from the
    // cache. With the modifications it may no longer pass the cache's filter. If the device type is
    // still eligible to be cached, this will be done (again lazily) the next time this specific
    // device type is fetched.
    cache.removeDeviceType(id);
    return deviceType;
  }

  @Override
  public void deleteDeviceType(@NonNull String id) {
    delegate.deleteDeviceType(id);
    cache.removeDeviceType(id);
  }
}
