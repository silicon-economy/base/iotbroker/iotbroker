/*
 * Copyright Open Logistics Foundation
 *
 * Licensed under the Open Logistics Foundation License 1.3.
 * For details on the licensing terms, see the LICENSE file.
 * SPDX-License-Identifier: OLFL-1.3
 */
package org.siliconeconomy.iotbroker.core.service.deviceregistry.client.v1.exceptions;

/**
 * Base class for exceptions indicating that an HTTP request resulted in a response with a status
 * code 5XX.
 *
 * @author M. Grzenia
 */
public class ServerErrorException
    extends HttpException {

  public ServerErrorException(String message) {
    super(message);
  }
}
