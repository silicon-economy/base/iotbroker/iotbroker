/*
 * Copyright 2023 Open Logistics Foundation
 *
 * Licensed under the Open Logistics License 1.0.
 * For details on the licensing terms, see the LICENSE file.
 */
package org.siliconeconomy.iotbroker.core.service.deviceregistry.client.v1.caching;

import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.siliconeconomy.iotbroker.model.device.DeviceInstance;

import java.time.Instant;

import static org.assertj.core.api.Assertions.assertThat;

/**
 * Test cases for {@link DeviceInstanceFilter}.
 *
 * @author M. Grzenia
 */
class DeviceInstanceFilterTest {
  private static String DEVICE_SOURCE = "sensingpuck";
  private static String DEVICE_TYPE_IDENTIFIER = "sensingpuck-v1";

  /**
   * Class under test.
   */
  private DeviceInstanceFilter filter;

  @BeforeEach
  void setUp() {
    filter = new DeviceInstanceFilter(DEVICE_SOURCE, DEVICE_TYPE_IDENTIFIER);
  }

  @Test
  void test_whenDeviceInstanceAssociatedWithDeviceType_thenReturnsTrue() {
    DeviceInstance deviceInstance = dummyDeviceInstance()
        .withSource(DEVICE_SOURCE)
        .withTenant("1")
        .withDeviceTypeIdentifier(DEVICE_TYPE_IDENTIFIER);

    boolean result = filter.test(deviceInstance);

    assertThat(result).isTrue();
  }

  @Test
  void test_whenDeviceInstanceNotAssociatedWithDeviceType_thenReturnsFalse() {
    // Test 1 - Only source matches.
    DeviceInstance deviceInstance = dummyDeviceInstance()
        .withSource(DEVICE_SOURCE)
        .withTenant("1")
        .withDeviceTypeIdentifier("some-identifier");

    boolean result = filter.test(deviceInstance);

    assertThat(result).isFalse();

    // Test 2 - Only device type identifier matches.
    deviceInstance = dummyDeviceInstance()
        .withSource("some-source")
        .withTenant("1")
        .withDeviceTypeIdentifier(DEVICE_TYPE_IDENTIFIER);

    result = filter.test(deviceInstance);

    assertThat(result).isFalse();

    // Test 3 - Nothing matches.
    deviceInstance = dummyDeviceInstance()
        .withSource("some-source")
        .withTenant("1")
        .withDeviceTypeIdentifier("some-identifier");

    result = filter.test(deviceInstance);

    assertThat(result).isFalse();
  }

  private DeviceInstance dummyDeviceInstance() {
    return new DeviceInstance("",
                              "",
                              "",
                              "",
                              Instant.EPOCH,
                              Instant.EPOCH,
                              false,
                              "",
                              "",
                              "");
  }
}
