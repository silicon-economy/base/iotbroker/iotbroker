/*
 * Copyright 2023 Open Logistics Foundation
 *
 * Licensed under the Open Logistics License 1.0.
 * For details on the licensing terms, see the LICENSE file.
 */
package org.siliconeconomy.iotbroker.core.service.deviceregistry.client.v1.caching;

import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.siliconeconomy.iotbroker.model.device.DeviceInstance;
import org.siliconeconomy.iotbroker.model.device.DeviceType;

import java.time.Instant;
import java.util.Set;

import static org.mockito.ArgumentMatchers.anyString;
import static org.mockito.Mockito.*;

/**
 * Test cases for {@link DeviceRegistryCacheUpdater}.
 *
 * @author M. Grzenia
 */
class DeviceRegistryCacheUpdaterTest {

  /**
   * Class under test.
   */
  private DeviceRegistryCacheUpdater cacheUpdater;
  /**
   * Test dependencies.
   */
  private DeviceRegistryCache cache;

  @BeforeEach
  void setUp() {
    cache = mock(DeviceRegistryCache.class);
    cacheUpdater = new DeviceRegistryCacheUpdater(cache);
  }

  @Test
  void onDeviceTypeModified_whenOldDeviceTypeInCache_thenRemovesDeviceTypeFromCache() {
    DeviceType oldDeviceType = dummyDeviceType().withId("some-id").withEnabled(false);
    DeviceType newDeviceType = dummyDeviceType().withId("some-id").withEnabled(true);
    when(cache.containsDeviceType(anyString(), anyString())).thenReturn(true);

    cacheUpdater.onDeviceTypeModified(oldDeviceType, newDeviceType);

    verify(cache).removeDeviceType("some-id");
  }

  @Test
  void onDeviceTypeModified_whenOldDeviceTypeNotInCache_thenDoesNotAlterCache() {
    DeviceType oldDeviceType = dummyDeviceType().withId("some-id").withEnabled(false);
    DeviceType newDeviceType = dummyDeviceType().withId("some-id").withEnabled(true);
    when(cache.containsDeviceType(anyString(), anyString())).thenReturn(false);

    cacheUpdater.onDeviceTypeModified(oldDeviceType, newDeviceType);

    verify(cache).containsDeviceType(oldDeviceType.getSource(), oldDeviceType.getIdentifier());
    verifyNoMoreInteractions(cache);
  }

  @Test
  void onDeviceTypeDeleted_removesDeviceTypeFromCache() {
    DeviceType oldDeviceType = dummyDeviceType().withId("some-id");

    cacheUpdater.onDeviceTypeDeleted(oldDeviceType);

    verify(cache).removeDeviceType("some-id");
  }

  @Test
  void onDeviceInstanceModified_whenOldDeviceInstanceInCache_thenRemovesDeviceInstanceFromCache() {
    DeviceInstance oldDeviceInstance = dummyDeviceInstance().withId("some-id").withEnabled(false);
    DeviceInstance newDeviceInstance = dummyDeviceInstance().withId("some-id").withEnabled(true);
    when(cache.containsDeviceInstance(anyString(), anyString())).thenReturn(true);

    cacheUpdater.onDeviceInstanceModified(oldDeviceInstance, newDeviceInstance);

    verify(cache).removeDeviceInstance("some-id");
  }

  @Test
  void onDeviceInstanceModified_whenOldDeviceInstanceNotInCache_thenDoesNotAlterCache() {
    DeviceInstance oldDeviceInstance = dummyDeviceInstance().withId("some-id").withEnabled(false);
    DeviceInstance newDeviceInstance = dummyDeviceInstance().withId("some-id").withEnabled(true);
    when(cache.containsDeviceInstance(anyString(), anyString())).thenReturn(false);

    cacheUpdater.onDeviceInstanceModified(oldDeviceInstance, newDeviceInstance);

    verify(cache).containsDeviceInstance(oldDeviceInstance.getSource(),
                                         oldDeviceInstance.getTenant());
    verifyNoMoreInteractions(cache);
  }

  @Test
  void onDeviceInstanceDeleted_removesDeviceInstanceFromCache() {
    DeviceInstance oldDeviceInstance = dummyDeviceInstance().withId("some-id");

    cacheUpdater.onDeviceInstanceDeleted(oldDeviceInstance);

    verify(cache).removeDeviceInstance("some-id");
  }

  private DeviceType dummyDeviceType() {
    return new DeviceType("",
                          "",
                          "",
                          Set.of(),
                          "",
                          false,
                          false,
                          false);
  }

  private DeviceInstance dummyDeviceInstance() {
    return new DeviceInstance("",
                              "",
                              "",
                              "",
                              Instant.EPOCH,
                              Instant.EPOCH,
                              false,
                              "",
                              "",
                              "");
  }
}
