/*
 * Copyright 2023 Open Logistics Foundation
 *
 * Licensed under the Open Logistics License 1.0.
 * For details on the licensing terms, see the LICENSE file.
 */
package org.siliconeconomy.iotbroker.core.service.deviceregistry.client.v1.caching;

import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.siliconeconomy.iotbroker.model.device.DeviceType;

import java.util.Set;

import static org.assertj.core.api.Assertions.assertThat;

/**
 * Test cases for {@link DeviceTypeFilter}.
 *
 * @author M. Grzenia
 */
class DeviceTypeFilterTest {
  private static final String DEVICE_SOURCE = "sensingpuck-adapter";
  private static final String DEVICE_TYPE_IDENTIFIER = "sensingpuck-v1";

  /**
   * Class under test.
   */
  private DeviceTypeFilter filter;

  @BeforeEach
  void setUp() {
    filter = new DeviceTypeFilter(DEVICE_SOURCE, DEVICE_TYPE_IDENTIFIER);
  }

  @Test
  void test_whenDeviceTypeMatches_thenReturnsTrue() {
    DeviceType deviceType = dummyDeviceType()
        .withSource(DEVICE_SOURCE)
        .withIdentifier(DEVICE_TYPE_IDENTIFIER);

    boolean result = filter.test(deviceType);

    assertThat(result).isTrue();
  }

  @Test
  void test_whenDeviceTypeDoesNotMatch_thenReturnsFalse() {
    // Test 1 - Only source matches.
    DeviceType deviceType = dummyDeviceType()
        .withSource(DEVICE_SOURCE)
        .withIdentifier("some-identifier");

    boolean result = filter.test(deviceType);

    assertThat(result).isFalse();

    // Test 2 - Only identifier matches.
    deviceType = dummyDeviceType()
        .withSource("some-source")
        .withIdentifier(DEVICE_TYPE_IDENTIFIER);

    result = filter.test(deviceType);

    assertThat(result).isFalse();

    // Test 3 - Nothing matches.
    deviceType = dummyDeviceType()
        .withSource("some-source")
        .withIdentifier("some-identifier");

    result = filter.test(deviceType);

    assertThat(result).isFalse();
  }

  private DeviceType dummyDeviceType() {
    return new DeviceType("",
                          "",
                          "",
                          Set.of(),
                          "",
                          false,
                          false,
                          false);
  }
}
