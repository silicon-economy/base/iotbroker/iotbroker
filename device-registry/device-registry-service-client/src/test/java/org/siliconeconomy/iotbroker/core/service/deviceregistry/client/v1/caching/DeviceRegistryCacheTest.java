/*
 * Copyright 2023 Open Logistics Foundation
 *
 * Licensed under the Open Logistics License 1.0.
 * For details on the licensing terms, see the LICENSE file.
 */
package org.siliconeconomy.iotbroker.core.service.deviceregistry.client.v1.caching;

import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.siliconeconomy.iotbroker.model.device.DeviceInstance;
import org.siliconeconomy.iotbroker.model.device.DeviceType;

import java.time.Instant;
import java.util.NoSuchElementException;
import java.util.Set;

import static org.assertj.core.api.Assertions.assertThat;
import static org.assertj.core.api.Assertions.assertThatThrownBy;
import static org.mockito.Mockito.mock;
import static org.mockito.Mockito.when;

/**
 * Test cases for {@link DeviceRegistryCache}.
 *
 * @author M. Grzenia
 */
class DeviceRegistryCacheTest {

  /**
   * Class under test.
   */
  private DeviceRegistryCache cache;
  /**
   * Test dependencies.
   */
  private DeviceTypeFilter deviceTypeFilter;
  private DeviceInstanceFilter deviceInstanceFilter;

  @BeforeEach
  void setUp() {
    deviceTypeFilter = mock(DeviceTypeFilter.class);
    deviceInstanceFilter = mock(DeviceInstanceFilter.class);
    cache = new DeviceRegistryCache(deviceTypeFilter, deviceInstanceFilter);
  }

  @Test
  void shouldAddDeviceTypeToCacheThatIsPassingFilter() {
    DeviceType deviceType = dummyDeviceType()
        .withId("some-id")
        .withSource("some-source")
        .withIdentifier("some-identifier");
    when(deviceTypeFilter.test(deviceType)).thenReturn(true);

    // Ensure the device type is not already cached.
    assertThat(cache.containsDeviceType("some-id")).isFalse();
    assertThat(cache.containsDeviceType("some-source", "some-identifier")).isFalse();

    cache.addDeviceType(deviceType);

    // Ensure the device type is now cached.
    assertThat(cache.containsDeviceType("some-id")).isTrue();
    assertThat(cache.containsDeviceType("some-source", "some-identifier")).isTrue();

    // Ensure the correct device type is returned.
    assertThat(cache.getDeviceType("some-id")).isEqualTo(deviceType);
    assertThat(cache.getDeviceType("some-source", "some-identifier")).isEqualTo(deviceType);
  }

  @Test
  void shouldNotAddDeviceTypeToCacheThatIsNotPassingFilter() {
    DeviceType deviceType = dummyDeviceType()
        .withId("some-id")
        .withSource("some-source")
        .withIdentifier("some-identifier");
    when(deviceTypeFilter.test(deviceType)).thenReturn(false);

    // Ensure the device type is not already cached.
    assertThat(cache.containsDeviceType("some-id")).isFalse();

    cache.addDeviceType(deviceType);

    // Ensure the device type is still not cached.
    assertThat(cache.containsDeviceType("some-id")).isFalse();
  }

  @Test
  void shouldRemoveCachedDeviceType() {
    DeviceType deviceType = dummyDeviceType()
        .withId("some-id")
        .withSource("some-source")
        .withIdentifier("some-identifier");
    when(deviceTypeFilter.test(deviceType)).thenReturn(true);

    // Ensure the device type is not already cached.
    assertThat(cache.containsDeviceType("some-id")).isFalse();

    // Add a device type to the cache.
    cache.addDeviceType(deviceType);
    assertThat(cache.containsDeviceType("some-id")).isTrue();

    // Remove the device type from the cache.
    cache.removeDeviceType("some-id");
    assertThat(cache.containsDeviceType("some-id")).isFalse();
  }

  @Test
  void shouldAddDeviceInstanceToCacheThatIsPassingFilter() {
    DeviceInstance deviceInstance = dummyDeviceInstance()
        .withId("some-id")
        .withSource("some-source")
        .withTenant("some-tenant")
        .withDeviceTypeIdentifier("some-identifier");
    when(deviceInstanceFilter.test(deviceInstance)).thenReturn(true);

    // Ensure the device instance is not already cached.
    assertThat(cache.containsDeviceInstance("some-id")).isFalse();
    assertThat(cache.containsDeviceInstance("some-source", "some-tenant")).isFalse();

    cache.addDeviceInstance(deviceInstance);

    // Ensure the device instance is now cached.
    assertThat(cache.containsDeviceInstance("some-id")).isTrue();
    assertThat(cache.containsDeviceInstance("some-source", "some-tenant")).isTrue();

    // Ensure the correct instance type is returned.
    assertThat(cache.getDeviceInstance("some-id")).isEqualTo(deviceInstance);
    assertThat(cache.getDeviceInstance("some-source", "some-tenant")).isEqualTo(deviceInstance);
  }

  @Test
  void shouldNotAddDeviceInstanceToCacheThatIsNotPassingFilter() {
    DeviceInstance deviceInstance = dummyDeviceInstance()
        .withId("some-id")
        .withSource("some-source")
        .withTenant("some-tenant")
        .withDeviceTypeIdentifier("some-identifier");
    when(deviceInstanceFilter.test(deviceInstance)).thenReturn(false);

    // Ensure the device instance is not already cached.
    assertThat(cache.containsDeviceInstance("some-id")).isFalse();

    cache.addDeviceInstance(deviceInstance);

    // Ensure the device instance is still not cached.
    assertThat(cache.containsDeviceInstance("some-id")).isFalse();
  }

  @Test
  void shouldRemoveCachedDeviceInstance() {
    DeviceInstance deviceInstance = dummyDeviceInstance()
        .withId("some-id")
        .withSource("some-source")
        .withTenant("some-tenant")
        .withDeviceTypeIdentifier("some-identifier");
    when(deviceInstanceFilter.test(deviceInstance)).thenReturn(true);

    // Ensure the device instance is not already cached.
    assertThat(cache.containsDeviceInstance("some-id")).isFalse();

    // Add a device instance to the cache.
    cache.addDeviceInstance(deviceInstance);
    assertThat(cache.containsDeviceInstance("some-id")).isTrue();

    // Remove the device instance from the cache.
    cache.removeDeviceInstance("some-id");
    assertThat(cache.containsDeviceInstance("some-id")).isFalse();
  }

  @Test
  void shouldThrowExceptionWhenRetrievingDeviceTypeNotInCache() {
    // Case 1: Retrieve by source and identifier.
    assertThat(cache.containsDeviceType("some-source", "some-identifier")).isFalse();
    assertThatThrownBy(() -> cache.getDeviceType("some-source", "some-identifier"))
        .isInstanceOf(NoSuchElementException.class);

    // Case 2: Retrieve by id.
    assertThat(cache.containsDeviceType("some-id")).isFalse();
    assertThatThrownBy(() -> cache.getDeviceType("some-id"))
        .isInstanceOf(NoSuchElementException.class);
  }

  @Test
  void shouldThrowExceptionWhenRetrievingDeviceInstanceNotInCache() {
    // Case 1: Retrieve by source and tenant.
    assertThat(cache.containsDeviceInstance("some-source", "some-tenant")).isFalse();
    assertThatThrownBy(() -> cache.getDeviceInstance("some-source", "some-tenant"))
        .isInstanceOf(NoSuchElementException.class);

    // Case 2: Retrieve by id.
    assertThat(cache.containsDeviceInstance("some-id")).isFalse();
    assertThatThrownBy(() -> cache.getDeviceInstance("some-id"))
        .isInstanceOf(NoSuchElementException.class);
  }

  private DeviceType dummyDeviceType() {
    return new DeviceType("",
                          "",
                          "",
                          Set.of(),
                          "",
                          false,
                          false,
                          false);
  }

  private DeviceInstance dummyDeviceInstance() {
    return new DeviceInstance("",
                              "",
                              "",
                              "",
                              Instant.EPOCH,
                              Instant.EPOCH,
                              false,
                              "",
                              "",
                              "");
  }
}
