/*
 * Copyright 2023 Open Logistics Foundation
 *
 * Licensed under the Open Logistics License 1.0.
 * For details on the licensing terms, see the LICENSE file.
 */
package org.siliconeconomy.iotbroker.core.service.deviceregistry.client.v1.caching;

import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.siliconeconomy.iotbroker.core.service.deviceregistry.client.v1.DeviceRegistryServiceClient;
import org.siliconeconomy.iotbroker.core.service.deviceregistry.common.webapi.v1.model.deviceinstance.DeviceInstanceCreateInputTO;
import org.siliconeconomy.iotbroker.core.service.deviceregistry.common.webapi.v1.model.deviceinstance.DeviceInstanceUpdateInputTO;
import org.siliconeconomy.iotbroker.core.service.deviceregistry.common.webapi.v1.model.devicetype.DeviceTypeCreateInputTO;
import org.siliconeconomy.iotbroker.core.service.deviceregistry.common.webapi.v1.model.devicetype.DeviceTypeUpdateInputTO;
import org.siliconeconomy.iotbroker.model.device.DeviceInstance;
import org.siliconeconomy.iotbroker.model.device.DeviceType;

import java.time.Instant;
import java.util.List;
import java.util.Set;

import static org.assertj.core.api.Assertions.assertThat;
import static org.mockito.ArgumentMatchers.anyString;
import static org.mockito.Mockito.*;

/**
 * Test cases for {@link CachingDeviceRegistryServiceClient}.
 *
 * @author M. Grzenia
 */
class CachingDeviceRegistryServiceClientTest {

  /**
   * Class under test.
   */
  private CachingDeviceRegistryServiceClient client;
  /**
   * Test dependencies.
   */
  private DeviceRegistryServiceClient delegateClient;
  private DeviceRegistryCache cache;

  @BeforeEach
  void setUp() {
    delegateClient = mock(DeviceRegistryServiceClient.class);
    cache = mock(DeviceRegistryCache.class);
    client = new CachingDeviceRegistryServiceClient(delegateClient, cache);
  }

  @Test
  void fetchDeviceInstances_whenDeviceInstanceInCache_thenReturnsDeviceInstanceFromCache() {
    DeviceInstance deviceInstance = dummyDeviceInstance();
    when(cache.containsDeviceInstance(anyString(), anyString())).thenReturn(true);
    when(cache.getDeviceInstance(anyString(), anyString())).thenReturn(deviceInstance);

    List<DeviceInstance> result
        = client.fetchDeviceInstances("some-source", "some-tenant", null, null, null, null);

    assertThat(result)
        .hasSize(1)
        .containsExactly(deviceInstance);
    verifyNoInteractions(delegateClient);
  }

  @Test
  void fetchDeviceInstances_whenDeviceInstanceNotInCache_thenReturnsDeviceInstanceFromDelegateClient() {
    DeviceInstance deviceInstance = dummyDeviceInstance();
    when(cache.containsDeviceInstance(anyString(), anyString())).thenReturn(false);
    when(delegateClient.fetchDeviceInstances(anyString(), anyString(), any(), any(), any(), any()))
        .thenReturn(List.of(deviceInstance));

    List<DeviceInstance> result
        = client.fetchDeviceInstances("some-source", "some-tenant", null, null, null, null);

    assertThat(result)
        .hasSize(1)
        .containsExactly(deviceInstance);
    // Verify that the delegate is called and the device instance is added to the cache.
    verify(delegateClient).fetchDeviceInstances(eq("some-source"),
                                                eq("some-tenant"),
                                                isNull(),
                                                isNull(),
                                                isNull(),
                                                isNull());
    verify(cache).addDeviceInstance(deviceInstance);
  }

  @Test
  void createDeviceInstance_delegatesMethodCall() {
    DeviceInstanceCreateInputTO deviceInstanceInput = new DeviceInstanceCreateInputTO();
    DeviceInstance deviceInstance = dummyDeviceInstance();
    when(delegateClient.createDeviceInstance(deviceInstanceInput)).thenReturn(deviceInstance);

    DeviceInstance result = client.createDeviceInstance(deviceInstanceInput);

    assertThat(result).isEqualTo(deviceInstance);
    verify(delegateClient).createDeviceInstance(deviceInstanceInput);
  }

  @Test
  void fetchDeviceInstancesCount_delegatesMethodCall() {
    when(delegateClient.fetchDeviceInstancesCount()).thenReturn(13L);

    long result = client.fetchDeviceInstancesCount();

    assertThat(result).isEqualTo(13L);
    verify(delegateClient).fetchDeviceInstancesCount();
  }

  @Test
  void fetchDeviceInstancesSources_delegatesMethodCall() {
    when(delegateClient.fetchDeviceInstancesSources())
        .thenReturn(List.of("some-source", "some-other-source"));

    List<String> result = client.fetchDeviceInstancesSources();

    assertThat(result)
        .hasSize(2)
        .containsExactly("some-source", "some-other-source");
    verify(delegateClient).fetchDeviceInstancesSources();
  }

  @Test
  void fetchDeviceInstance_whenDeviceInstanceInCache_thenReturnsDeviceInstanceFromCache() {
    DeviceInstance deviceInstance = dummyDeviceInstance();
    when(cache.containsDeviceInstance(anyString())).thenReturn(true);
    when(cache.getDeviceInstance(anyString())).thenReturn(deviceInstance);

    DeviceInstance result = client.fetchDeviceInstance("some-id");

    assertThat(result).isEqualTo(deviceInstance);
    verifyNoInteractions(delegateClient);
  }

  @Test
  void fetchDeviceInstance_whenDeviceInstanceNotInCache_thenReturnsDeviceInstanceFromDelegateClient() {
    DeviceInstance deviceInstance = dummyDeviceInstance();
    when(cache.containsDeviceInstance(anyString())).thenReturn(false);
    when(delegateClient.fetchDeviceInstance(anyString())).thenReturn(deviceInstance);

    DeviceInstance result = client.fetchDeviceInstance("some-id");

    assertThat(result).isEqualTo(deviceInstance);
    // Verify that the delegate is called and the device instance is added to the cache.
    verify(delegateClient).fetchDeviceInstance("some-id");
    verify(cache).addDeviceInstance(deviceInstance);
  }

  @Test
  void updateDeviceInstance_delegatesMethodCall() {
    DeviceInstanceUpdateInputTO deviceInstanceInput = new DeviceInstanceUpdateInputTO();
    DeviceInstance deviceInstance = dummyDeviceInstance();
    when(delegateClient.updateDeviceInstance(anyString(), eq(deviceInstanceInput)))
        .thenReturn(deviceInstance);

    DeviceInstance result = client.updateDeviceInstance("some-id", deviceInstanceInput);

    assertThat(result).isEqualTo(deviceInstance);
    // Verify that the delegate is called and the device instance is removed from the cache.
    verify(delegateClient).updateDeviceInstance("some-id", deviceInstanceInput);
    verify(cache).removeDeviceInstance("some-id");
  }

  @Test
  void deleteDeviceInstance_delegatesMethodCallAndRemovesDeviceInstanceFromCache() {
    client.deleteDeviceInstance("some-id");

    verify(delegateClient).deleteDeviceInstance("some-id");
    verify(cache).removeDeviceInstance("some-id");
  }

  @Test
  void fetchDeviceTypes_whenDeviceTypeInCache_thenReturnsDeviceTypeFromCache() {
    DeviceType deviceType = dummyDeviceType();
    when(cache.containsDeviceType(anyString(), anyString())).thenReturn(true);
    when(cache.getDeviceType(anyString(), anyString())).thenReturn(deviceType);

    List<DeviceType> result
        = client.fetchDeviceTypes("some-source", "some-identifier", null, null, null, null);

    assertThat(result)
        .hasSize(1)
        .containsExactly(deviceType);
    verifyNoInteractions(delegateClient);
  }

  @Test
  void fetchDeviceTypes_whenDeviceTypeNotInCache_thenReturnsDeviceTypeFromDelegateClient() {
    DeviceType deviceType = dummyDeviceType();
    when(cache.containsDeviceType(anyString(), anyString())).thenReturn(false);
    when(delegateClient.fetchDeviceTypes(anyString(), anyString(), any(), any(), any(), any()))
        .thenReturn(List.of(deviceType));

    List<DeviceType> result
        = client.fetchDeviceTypes("some-source", "some-identifier", null, null, null, null);

    assertThat(result)
        .hasSize(1)
        .containsExactly(deviceType);
    // Verify that the delegate is called and the device type is added to the cache.
    verify(delegateClient).fetchDeviceTypes(eq("some-source"),
                                            eq("some-identifier"),
                                            isNull(),
                                            isNull(),
                                            isNull(),
                                            isNull());
    verify(cache).addDeviceType(deviceType);
  }

  @Test
  void createDeviceType_delegatesMethodCall() {
    DeviceTypeCreateInputTO deviceTypeInput = new DeviceTypeCreateInputTO();
    DeviceType deviceType = dummyDeviceType();
    when(delegateClient.createDeviceType(deviceTypeInput)).thenReturn(deviceType);

    DeviceType result = client.createDeviceType(deviceTypeInput);

    assertThat(result).isEqualTo(deviceType);
    verify(delegateClient).createDeviceType(deviceTypeInput);
  }

  @Test
  void fetchDeviceTypesCount_delegatesMethodCall() {
    when(delegateClient.fetchDeviceTypesCount()).thenReturn(17L);

    long result = client.fetchDeviceTypesCount();

    assertThat(result).isEqualTo(17L);
    verify(delegateClient).fetchDeviceTypesCount();
  }

  @Test
  void fetchDeviceTypesSources_delegatesMethodCall() {
    when(delegateClient.fetchDeviceTypesSources())
        .thenReturn(List.of("some-source", "some-other-source"));

    List<String> result = client.fetchDeviceTypesSources();

    assertThat(result)
        .hasSize(2)
        .containsExactly("some-source", "some-other-source");
    verify(delegateClient).fetchDeviceTypesSources();
  }

  @Test
  void fetchDeviceType_whenDeviceTypeInCache_thenReturnsDeviceTypeFromCache() {
    DeviceType deviceType = dummyDeviceType();
    when(cache.containsDeviceType(anyString())).thenReturn(true);
    when(cache.getDeviceType(anyString())).thenReturn(deviceType);

    DeviceType result = client.fetchDeviceType("some-id");

    assertThat(result).isEqualTo(deviceType);
    verifyNoInteractions(delegateClient);
  }

  @Test
  void fetchDeviceType_whenDeviceTypeNotInCache_thenReturnsDeviceTypeFromDelegateClient() {
    DeviceType deviceType = dummyDeviceType();
    when(cache.containsDeviceType(anyString())).thenReturn(false);
    when(delegateClient.fetchDeviceType(anyString())).thenReturn(deviceType);

    DeviceType result = client.fetchDeviceType("some-id");

    assertThat(result).isEqualTo(deviceType);
    // Verify that the delegate is called and the device type is added to the cache.
    verify(delegateClient).fetchDeviceType("some-id");
    verify(cache).addDeviceType(deviceType);
  }

  @Test
  void updateDeviceType_delegatesMethodCall() {
    DeviceTypeUpdateInputTO deviceTypeInput = new DeviceTypeUpdateInputTO();
    DeviceType deviceType = dummyDeviceType();
    when(delegateClient.updateDeviceType(anyString(), eq(deviceTypeInput)))
        .thenReturn(deviceType);

    DeviceType result = client.updateDeviceType("some-id", deviceTypeInput);

    assertThat(result).isEqualTo(deviceType);
    // Verify that the delegate is called and the device type is removed from the cache.
    verify(delegateClient).updateDeviceType("some-id", deviceTypeInput);
    verify(cache).removeDeviceType("some-id");
  }

  @Test
  void deleteDeviceType_delegatesMethodCallAndRemovesDeviceTypeFromCache() {
    client.deleteDeviceType("some-id");

    verify(delegateClient).deleteDeviceType("some-id");
    verify(cache).removeDeviceType("some-id");
  }

  private DeviceType dummyDeviceType() {
    return new DeviceType("",
                          "",
                          "",
                          Set.of(),
                          "",
                          false,
                          false,
                          false);
  }

  private DeviceInstance dummyDeviceInstance() {
    return new DeviceInstance("",
                              "",
                              "",
                              "",
                              Instant.EPOCH,
                              Instant.EPOCH,
                              false,
                              "",
                              "",
                              "");
  }
}
