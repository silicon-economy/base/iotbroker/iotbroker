/*
 * Copyright Open Logistics Foundation
 *
 * Licensed under the Open Logistics Foundation License 1.3.
 * For details on the licensing terms, see the LICENSE file.
 * SPDX-License-Identifier: OLFL-1.3
 */
package org.siliconeconomy.iotbroker.core.service.deviceregistry.common.webapi.v1.model.devicetype;

import lombok.EqualsAndHashCode;
import lombok.Getter;
import lombok.Setter;
import lombok.ToString;
import lombok.experimental.Accessors;
import org.siliconeconomy.iotbroker.model.device.DeviceType;

/**
 * This transfer object is used for updating existing {@link DeviceType}s.
 * <p>
 * Where possible, documentation of private fields is omitted, since in most cases it is identical
 * to the corresponding documentation in {@link DeviceType}.
 *
 * @author M. Grzenia
 */
@Setter
@Getter
@Accessors(chain = true)
@EqualsAndHashCode(callSuper = true)
@ToString(callSuper = true)
public class DeviceTypeUpdateInputTO
    extends DeviceTypeCommonInputTO {
}
