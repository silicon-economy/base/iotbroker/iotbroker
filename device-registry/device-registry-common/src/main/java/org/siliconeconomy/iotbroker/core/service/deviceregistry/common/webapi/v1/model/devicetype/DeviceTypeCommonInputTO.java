/*
 * Copyright Open Logistics Foundation
 *
 * Licensed under the Open Logistics Foundation License 1.3.
 * For details on the licensing terms, see the LICENSE file.
 * SPDX-License-Identifier: OLFL-1.3
 */
package org.siliconeconomy.iotbroker.core.service.deviceregistry.common.webapi.v1.model.devicetype;

import lombok.EqualsAndHashCode;
import lombok.Getter;
import lombok.Setter;
import lombok.ToString;
import lombok.experimental.Accessors;
import org.siliconeconomy.iotbroker.model.device.DeviceType;

import java.util.Set;

/**
 * This transfer object defines the base information for registering new or updating existing
 * {@link DeviceType}s that is shared between various other transfer objects.
 * <p>
 * Where possible, documentation of private fields is omitted, since in most cases it is identical
 * to the corresponding documentation in {@link DeviceType}.
 * <p>
 * No primitive data types are used to allow {@code null} values for all attributes.
 *
 * @author M. Grzenia
 */
@Setter
@Getter
@Accessors(chain = true)
@EqualsAndHashCode
@ToString
public class DeviceTypeCommonInputTO {

  private Set<String> providedBy;
  private String description;
  private Boolean enabled;
  private Boolean autoRegisterDeviceInstances;
  private Boolean autoEnableDeviceInstances;
}
