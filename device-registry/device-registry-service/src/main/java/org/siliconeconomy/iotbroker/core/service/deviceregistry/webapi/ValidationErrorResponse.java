/*
 * Copyright Open Logistics Foundation
 *
 * Licensed under the Open Logistics Foundation License 1.3.
 * For details on the licensing terms, see the LICENSE file.
 * SPDX-License-Identifier: OLFL-1.3
 */
package org.siliconeconomy.iotbroker.core.service.deviceregistry.webapi;

import lombok.*;

import java.util.ArrayList;
import java.util.List;

/**
 * A response for reporting validation errors to clients of the Device Registry Service's REST API.
 *
 * @author M. Grzenia
 */
@AllArgsConstructor
@NoArgsConstructor
@Getter
@Setter
@EqualsAndHashCode
public class ValidationErrorResponse {

  /**
   * A list of all validation errors that occurred during a request.
   */
  private List<ValidationError> validationErrors = new ArrayList<>();
}
