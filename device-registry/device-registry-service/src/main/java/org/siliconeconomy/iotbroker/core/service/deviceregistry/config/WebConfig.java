/*
 * Copyright Open Logistics Foundation
 *
 * Licensed under the Open Logistics Foundation License 1.3.
 * For details on the licensing terms, see the LICENSE file.
 * SPDX-License-Identifier: OLFL-1.3
 */
package org.siliconeconomy.iotbroker.core.service.deviceregistry.config;

import org.springframework.context.annotation.Configuration;
import org.springframework.web.servlet.config.annotation.CorsRegistry;
import org.springframework.web.servlet.config.annotation.WebMvcConfigurer;

import static java.util.Objects.requireNonNull;

/**
 * Configuration of Spring MVC.
 * <p>
 * Enables CORS for the endpoints provided by the service and adds a configurable set of URL
 * patterns to the list of allowed origins.
 *
 * @author M. Grzenia
 */
@Configuration
public class WebConfig
    implements WebMvcConfigurer {

  private final DeviceRegistryServiceProperties deviceRegistryServiceProperties;

  /**
   * Creates a new instance.
   *
   * @param deviceRegistryServiceProperties The service's configuration properties.
   */
  public WebConfig(DeviceRegistryServiceProperties deviceRegistryServiceProperties) {
    this.deviceRegistryServiceProperties = requireNonNull(deviceRegistryServiceProperties,
                                                          "deviceRegistryServiceProperties");
  }

  @Override
  public void addCorsMappings(CorsRegistry registry) {
    registry.addMapping("/**")
        .allowedOriginPatterns(
            deviceRegistryServiceProperties.getCorsAllowedOriginPatterns().toArray(new String[]{})
        )
        .allowedMethods("*");
  }
}
