/*
 * Copyright Open Logistics Foundation
 *
 * Licensed under the Open Logistics Foundation License 1.3.
 * For details on the licensing terms, see the LICENSE file.
 * SPDX-License-Identifier: OLFL-1.3
 */
package org.siliconeconomy.iotbroker.core.service.deviceregistry;

import org.siliconeconomy.iotbroker.core.service.deviceregistry.config.DeviceRegistryServiceProperties;
import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.boot.context.properties.EnableConfigurationProperties;

/**
 * The application starting point.
 *
 * @author M. Grzenia
 */
@SpringBootApplication
@EnableConfigurationProperties({DeviceRegistryServiceProperties.class})
public class DeviceRegistryServiceApplication {

  public static void main(String[] args) {
    SpringApplication.run(DeviceRegistryServiceApplication.class, args);
  }
}
