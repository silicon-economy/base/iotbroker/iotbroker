/*
 * Copyright Open Logistics Foundation
 *
 * Licensed under the Open Logistics Foundation License 1.3.
 * For details on the licensing terms, see the LICENSE file.
 * SPDX-License-Identifier: OLFL-1.3
 */
package org.siliconeconomy.iotbroker.core.service.deviceregistry.config;

import org.modelmapper.ModelMapper;
import org.siliconeconomy.iotbroker.core.service.deviceregistry.deviceinstance.DeviceInstanceEntityConverter;
import org.siliconeconomy.iotbroker.core.service.deviceregistry.devicetype.DeviceTypeEntityConverter;
import org.siliconeconomy.iotbroker.core.service.deviceregistry.webapi.v1.DeviceInstanceCreateInputConverter;
import org.siliconeconomy.iotbroker.core.service.deviceregistry.webapi.v1.DeviceTypeCreateInputConverter;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;

/**
 * Configuration of the {@link ModelMapper} bean to use.
 *
 * @author M. Grzenia
 */
@Configuration
public class ModelMapperConfig {

  @Bean
  public ModelMapper modelMapper() {
    var modelMapper = new ModelMapper();
    modelMapper.addConverter(new DeviceInstanceCreateInputConverter());
    modelMapper.addConverter(new DeviceInstanceEntityConverter());
    modelMapper.addConverter(new DeviceTypeCreateInputConverter());
    modelMapper.addConverter(new DeviceTypeEntityConverter());
    return modelMapper;
  }
}
