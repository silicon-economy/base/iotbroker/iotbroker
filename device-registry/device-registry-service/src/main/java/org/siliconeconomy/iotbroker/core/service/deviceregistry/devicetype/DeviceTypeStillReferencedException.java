/*
 * Copyright Open Logistics Foundation
 *
 * Licensed under the Open Logistics Foundation License 1.3.
 * For details on the licensing terms, see the LICENSE file.
 * SPDX-License-Identifier: OLFL-1.3
 */
package org.siliconeconomy.iotbroker.core.service.deviceregistry.devicetype;

/**
 * Thrown to indicate that a device type is still referenced by one or more device instances.
 *
 * @author M. Grzenia
 */
public class DeviceTypeStillReferencedException
    extends RuntimeException {

  public DeviceTypeStillReferencedException(String source, String identifier) {
    super(String.format(
        "The device type with source '%s' and identifier '%s' is still referenced by one or more " +
            "device instances.",
        source,
        identifier
    ));
  }
}
