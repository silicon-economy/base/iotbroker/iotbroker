/*
 * Copyright Open Logistics Foundation
 *
 * Licensed under the Open Logistics Foundation License 1.3.
 * For details on the licensing terms, see the LICENSE file.
 * SPDX-License-Identifier: OLFL-1.3
 */
package org.siliconeconomy.iotbroker.core.service.deviceregistry.webapi;

import lombok.*;

/**
 * Represents a validation error for a single property (e.g. a field or parameter).
 *
 * @author M. Grzenia
 */
@AllArgsConstructor
@NoArgsConstructor
@Getter
@Setter
@EqualsAndHashCode
public class ValidationError {

  /**
   * The name of the affected property.
   */
  private String property;
  /**
   * The message describing the validation error.
   */
  private String message;
}
