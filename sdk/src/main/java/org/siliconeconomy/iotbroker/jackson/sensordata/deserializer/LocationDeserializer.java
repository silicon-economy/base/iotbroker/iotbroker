/*
 * Copyright Open Logistics Foundation
 *
 * Licensed under the Open Logistics Foundation License 1.3.
 * For details on the licensing terms, see the LICENSE file.
 * SPDX-License-Identifier: OLFL-1.3
 */
package org.siliconeconomy.iotbroker.jackson.sensordata.deserializer;

import com.fasterxml.jackson.core.JsonParser;
import com.fasterxml.jackson.databind.DeserializationContext;
import com.fasterxml.jackson.databind.ObjectMapper;
import com.fasterxml.jackson.databind.deser.std.StdDeserializer;
import com.fasterxml.jackson.databind.node.ObjectNode;
import org.siliconeconomy.iotbroker.model.sensordata.Location;
import org.siliconeconomy.iotbroker.model.sensordata.LocationDetails;
import org.siliconeconomy.iotbroker.model.sensordata.location.LocationEncodingType;

import java.io.IOException;

/**
 * Deserializer for instances of {@link Location}.
 *
 * @author M. Grzenia
 */
public class LocationDeserializer
    extends StdDeserializer<Location<? extends LocationDetails<?>>> {

  public LocationDeserializer() {
    super(Location.class);
  }

  @Override
  @SuppressWarnings("unchecked")
  public Location<? extends LocationDetails<?>> deserialize(
      JsonParser jsonParser,
      DeserializationContext context)
      throws IOException {
    ObjectMapper mapper = (ObjectMapper) jsonParser.getCodec();
    ObjectNode objectNode = mapper.readTree(jsonParser);

    LocationEncodingType locationEncodingType
        = mapper.treeToValue(objectNode.get("encodingType"), LocationEncodingType.class);

    return new Location(
        mapper.treeToValue(objectNode.get("name"), String.class),
        mapper.treeToValue(objectNode.get("description"), String.class),
        locationEncodingType.getLocationDetailsClass(),
        mapper.treeToValue(
            objectNode.get("details"),
            locationEncodingType.getLocationDetailsClass()
        )
    );
  }
}
