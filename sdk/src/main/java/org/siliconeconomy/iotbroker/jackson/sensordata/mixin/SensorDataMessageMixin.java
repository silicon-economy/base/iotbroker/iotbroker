/*
 * Copyright Open Logistics Foundation
 *
 * Licensed under the Open Logistics Foundation License 1.3.
 * For details on the licensing terms, see the LICENSE file.
 * SPDX-License-Identifier: OLFL-1.3
 */
package org.siliconeconomy.iotbroker.jackson.sensordata.mixin;

import com.fasterxml.jackson.annotation.JsonCreator;
import com.fasterxml.jackson.annotation.JsonProperty;
import org.siliconeconomy.iotbroker.model.sensordata.*;

import java.time.Instant;
import java.util.Collection;

/**
 * A Jackson Mixin for {@link SensorDataMessage} instances.
 * <p>
 * Allows {@link SensorDataMessage} instances to be deserialized which otherwise would not be
 * possible due to a missing default constructor and missing setters.
 */
public abstract class SensorDataMessageMixin {

  @JsonCreator
  SensorDataMessageMixin(
      @JsonProperty("id") String id,
      @JsonProperty("originId") String originId,
      @JsonProperty("source") String source,
      @JsonProperty("tenant") String tenant,
      @JsonProperty("timestamp") Instant timestamp,
      @JsonProperty("location") Location<? extends LocationDetails<?>> location,
      @JsonProperty("datastreams") Collection<Datastream<? extends ObservationResult<?>>> datastreams) {
    // This constructor won't be called
  }
}
