/*
 * Copyright Open Logistics Foundation
 *
 * Licensed under the Open Logistics Foundation License 1.3.
 * For details on the licensing terms, see the LICENSE file.
 * SPDX-License-Identifier: OLFL-1.3
 */
package org.siliconeconomy.iotbroker.jackson.device;

import com.fasterxml.jackson.databind.Module;
import com.fasterxml.jackson.databind.ObjectMapper;
import com.fasterxml.jackson.databind.SerializationFeature;
import com.fasterxml.jackson.databind.module.SimpleModule;
import com.fasterxml.jackson.datatype.jsr310.JavaTimeModule;
import org.siliconeconomy.iotbroker.jackson.device.mixin.DeviceInstanceMixin;
import org.siliconeconomy.iotbroker.jackson.device.mixin.DeviceInstanceUpdateMixin;
import org.siliconeconomy.iotbroker.jackson.device.mixin.DeviceTypeMixin;
import org.siliconeconomy.iotbroker.jackson.device.mixin.DeviceTypeUpdateMixin;
import org.siliconeconomy.iotbroker.model.device.DeviceInstance;
import org.siliconeconomy.iotbroker.model.device.DeviceInstanceUpdate;
import org.siliconeconomy.iotbroker.model.device.DeviceType;
import org.siliconeconomy.iotbroker.model.device.DeviceTypeUpdate;

/**
 * A {@link Module} with already registered serializers, deserializers and mixins needed for
 * serialization/deserialization of instances of:
 * <ul>
 *   <li>{@link DeviceType}</li>
 *   <li>{@link DeviceTypeUpdate}</li>
 *   <li>{@link DeviceInstance}</li>
 *   <li>{@link DeviceInstanceUpdate}</li>
 * </ul>
 * <p>
 * Note: In addition to registering this module, an {@link ObjectMapper} instance must be configured
 * with the {@link SerializationFeature#WRITE_DATES_AS_TIMESTAMPS} disabled and the
 * {@link JavaTimeModule} registered for the serialization/deserialization to work as intended.
 *
 * @author M. Grzenia
 */
public class DeviceModule
    extends SimpleModule {

  public DeviceModule() {
    configureMixins();
  }

  private void configureMixins() {
    setMixInAnnotation(DeviceType.class, DeviceTypeMixin.class);
    setMixInAnnotation(DeviceTypeUpdate.class, DeviceTypeUpdateMixin.class);
    setMixInAnnotation(DeviceInstance.class, DeviceInstanceMixin.class);
    setMixInAnnotation(DeviceInstanceUpdate.class, DeviceInstanceUpdateMixin.class);
  }
}
