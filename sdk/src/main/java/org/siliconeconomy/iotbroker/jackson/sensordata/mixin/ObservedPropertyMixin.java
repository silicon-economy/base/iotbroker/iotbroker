/*
 * Copyright Open Logistics Foundation
 *
 * Licensed under the Open Logistics Foundation License 1.3.
 * For details on the licensing terms, see the LICENSE file.
 * SPDX-License-Identifier: OLFL-1.3
 */
package org.siliconeconomy.iotbroker.jackson.sensordata.mixin;

import com.fasterxml.jackson.annotation.JsonCreator;
import com.fasterxml.jackson.annotation.JsonProperty;
import org.siliconeconomy.iotbroker.model.sensordata.ObservedProperty;

/**
 * A Jackson Mixin for {@link ObservedProperty} instances.
 * <p>
 * Allows {@link ObservedProperty} instances to be deserialized which otherwise would not be
 * possible due to a missing default constructor and missing setters.
 */
public abstract class ObservedPropertyMixin {

  @JsonCreator
  ObservedPropertyMixin(@JsonProperty("name") String name,
                        @JsonProperty("description") String description) {
    // This constructor won't be called
  }
}
