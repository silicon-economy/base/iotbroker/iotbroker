/*
 * Copyright Open Logistics Foundation
 *
 * Licensed under the Open Logistics Foundation License 1.3.
 * For details on the licensing terms, see the LICENSE file.
 * SPDX-License-Identifier: OLFL-1.3
 */
package org.siliconeconomy.iotbroker.jackson.sensordata.serializer;

import com.fasterxml.jackson.core.JsonGenerator;
import com.fasterxml.jackson.databind.SerializerProvider;
import com.fasterxml.jackson.databind.ser.std.StdSerializer;
import org.siliconeconomy.iotbroker.model.sensordata.LocationDetails;

import java.io.IOException;

/**
 * Serializer for instances of {@link LocationDetails}.
 * This serializer merely enables a flat serialization hierarchy.
 *
 * @author M. Grzenia
 */
public class LocationDetailsSerializer
    extends StdSerializer<LocationDetails> {

  public LocationDetailsSerializer() {
    super(LocationDetails.class, false);
  }

  @Override
  public void serialize(LocationDetails value,
                        JsonGenerator generator,
                        SerializerProvider provider)
      throws IOException {
    generator.writeObject(value.getDetails());
  }
}
