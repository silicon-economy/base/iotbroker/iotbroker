/*
 * Copyright Open Logistics Foundation
 *
 * Licensed under the Open Logistics Foundation License 1.3.
 * For details on the licensing terms, see the LICENSE file.
 * SPDX-License-Identifier: OLFL-1.3
 */
package org.siliconeconomy.iotbroker.jackson.sensordata.deserializer;

import com.fasterxml.jackson.core.JsonParser;
import com.fasterxml.jackson.databind.DeserializationContext;
import com.fasterxml.jackson.databind.ObjectMapper;
import com.fasterxml.jackson.databind.deser.std.StdDeserializer;
import com.fasterxml.jackson.databind.node.TextNode;
import org.siliconeconomy.iotbroker.jackson.sensordata.serializer.ClassSerializer;
import org.siliconeconomy.iotbroker.model.sensordata.location.LocationEncodingType;

import java.io.IOException;

/**
 * Deserializer for instances of {@link LocationEncodingType}.
 *
 * @author M. Grzenia
 * @see ClassSerializer
 */
public class LocationEncodingTypeDeserializer
    extends StdDeserializer<LocationEncodingType> {

  public LocationEncodingTypeDeserializer() {
    super(LocationEncodingType.class);
  }

  @Override
  public LocationEncodingType deserialize(JsonParser jsonParser,
                                          DeserializationContext context)
      throws IOException {
    ObjectMapper mapper = (ObjectMapper) jsonParser.getCodec();
    TextNode objectNode = mapper.readTree(jsonParser);
    String stringToDeserialize = mapper.treeToValue(objectNode, String.class);

    return LocationEncodingType.valueOf(stringToDeserialize);
  }
}
