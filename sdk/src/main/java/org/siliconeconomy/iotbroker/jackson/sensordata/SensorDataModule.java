/*
 * Copyright Open Logistics Foundation
 *
 * Licensed under the Open Logistics Foundation License 1.3.
 * For details on the licensing terms, see the LICENSE file.
 * SPDX-License-Identifier: OLFL-1.3
 */
package org.siliconeconomy.iotbroker.jackson.sensordata;

import com.fasterxml.jackson.databind.Module;
import com.fasterxml.jackson.databind.ObjectMapper;
import com.fasterxml.jackson.databind.SerializationFeature;
import com.fasterxml.jackson.databind.module.SimpleModule;
import com.fasterxml.jackson.datatype.jsr310.JavaTimeModule;
import org.siliconeconomy.iotbroker.jackson.sensordata.deserializer.*;
import org.siliconeconomy.iotbroker.jackson.sensordata.mixin.ObservationMixin;
import org.siliconeconomy.iotbroker.jackson.sensordata.mixin.ObservedPropertyMixin;
import org.siliconeconomy.iotbroker.jackson.sensordata.mixin.SensorDataMessageMixin;
import org.siliconeconomy.iotbroker.jackson.sensordata.mixin.UnitOfMeasurementMixin;
import org.siliconeconomy.iotbroker.jackson.sensordata.serializer.ClassSerializer;
import org.siliconeconomy.iotbroker.jackson.sensordata.serializer.LocationDetailsSerializer;
import org.siliconeconomy.iotbroker.jackson.sensordata.serializer.ObservationResultSerializer;
import org.siliconeconomy.iotbroker.model.sensordata.*;
import org.siliconeconomy.iotbroker.model.sensordata.location.LatitudeLongitudeDetails;
import org.siliconeconomy.iotbroker.model.sensordata.location.LocationEncodingType;
import org.siliconeconomy.iotbroker.model.sensordata.location.MapcodeDetails;
import org.siliconeconomy.iotbroker.model.sensordata.location.SimpleDetails;
import org.siliconeconomy.iotbroker.model.sensordata.observation.ObservationType;

/**
 * A {@link Module} with already registered serializers, deserializers and mixins needed for
 * serialization/deserialization of {@link SensorDataMessage} instances.
 * <p>
 * Note: In addition to registering this module, an {@link ObjectMapper} instance must be configured
 * with the {@link SerializationFeature#WRITE_DATES_AS_TIMESTAMPS} disabled and the
 * {@link JavaTimeModule} registered for the serialization/deserialization to work as intended.
 *
 * @author M. Grzenia
 */
public class SensorDataModule
    extends SimpleModule {

  public SensorDataModule() {
    configureSerializers();
    configureDeserializers();
    configureMixins();
  }

  private void configureSerializers() {
    addSerializer(Class.class, new ClassSerializer());
    addSerializer(LocationDetails.class, new LocationDetailsSerializer());
    addSerializer(ObservationResult.class, new ObservationResultSerializer());
  }

  private void configureDeserializers() {
    addDeserializer(Class.class, new ClassDeserializer());
    addDeserializer(Datastream.class, new DatastreamDeserializer());
    addDeserializer(ObservationType.class, new ObservationTypeDeserializer());
    addDeserializer(Location.class, new LocationDeserializer());
    addDeserializer(LocationEncodingType.class, new LocationEncodingTypeDeserializer());

    // Deserializers for specific location details types of non-primitive type
    addDeserializer(LatitudeLongitudeDetails.class, new LongitudeLatitudeDetailsDeserializer());
    addDeserializer(MapcodeDetails.class, new MapcodeDetailsDeserializer());
    addDeserializer(SimpleDetails.class, new SimpleDetailsDeserializer());

    // Deserializers for specific observation result types of non-primitive type
    // No deserializers yet.
  }

  private void configureMixins() {
    setMixInAnnotation(SensorDataMessage.class, SensorDataMessageMixin.class);
    setMixInAnnotation(ObservedProperty.class, ObservedPropertyMixin.class);
    setMixInAnnotation(UnitOfMeasurement.class, UnitOfMeasurementMixin.class);
    setMixInAnnotation(Observation.class, ObservationMixin.class);
  }
}
