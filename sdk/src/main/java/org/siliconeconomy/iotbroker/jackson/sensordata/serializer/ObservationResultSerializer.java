/*
 * Copyright Open Logistics Foundation
 *
 * Licensed under the Open Logistics Foundation License 1.3.
 * For details on the licensing terms, see the LICENSE file.
 * SPDX-License-Identifier: OLFL-1.3
 */
package org.siliconeconomy.iotbroker.jackson.sensordata.serializer;

import com.fasterxml.jackson.core.JsonGenerator;
import com.fasterxml.jackson.databind.SerializerProvider;
import com.fasterxml.jackson.databind.ser.std.StdSerializer;
import org.siliconeconomy.iotbroker.model.sensordata.ObservationResult;

import java.io.IOException;

/**
 * Serializer for instances of {@link ObservationResult}.
 * This serializer merely enables a flat serialization hierarchy.
 *
 * @author M. Grzenia
 */
public class ObservationResultSerializer
    extends StdSerializer<ObservationResult> {

  public ObservationResultSerializer() {
    super(ObservationResult.class, false);
  }

  @Override
  public void serialize(ObservationResult value,
                        JsonGenerator generator,
                        SerializerProvider provider)
      throws IOException {
    generator.writeObject(value.getResult());
  }
}
