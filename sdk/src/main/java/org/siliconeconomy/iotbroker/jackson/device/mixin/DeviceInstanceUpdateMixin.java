/*
 * Copyright Open Logistics Foundation
 *
 * Licensed under the Open Logistics Foundation License 1.3.
 * For details on the licensing terms, see the LICENSE file.
 * SPDX-License-Identifier: OLFL-1.3
 */
package org.siliconeconomy.iotbroker.jackson.device.mixin;

import com.fasterxml.jackson.annotation.JsonCreator;
import com.fasterxml.jackson.annotation.JsonProperty;
import org.siliconeconomy.iotbroker.model.device.DeviceInstance;
import org.siliconeconomy.iotbroker.model.device.DeviceInstanceUpdate;

/**
 * A Jackson Mixin for {@link DeviceInstanceUpdate} instances.
 * <p>
 * Allows {@link DeviceInstanceUpdate} instances to be deserialized which otherwise would not be
 * possible due to a missing default constructor and missing setters.
 *
 * @author M. Grzenia
 */
public abstract class DeviceInstanceUpdateMixin {

  @JsonCreator
  DeviceInstanceUpdateMixin(@JsonProperty("newState") DeviceInstance newState,
                            @JsonProperty("oldState") DeviceInstance oldState,
                            @JsonProperty("type") DeviceInstanceUpdate.Type type) {
    // This constructor won't be called
  }
}
