/*
 * Copyright Open Logistics Foundation
 *
 * Licensed under the Open Logistics Foundation License 1.3.
 * For details on the licensing terms, see the LICENSE file.
 * SPDX-License-Identifier: OLFL-1.3
 */
package org.siliconeconomy.iotbroker.jackson.device.mixin;

import com.fasterxml.jackson.annotation.JsonCreator;
import com.fasterxml.jackson.annotation.JsonProperty;
import org.siliconeconomy.iotbroker.model.device.DeviceType;
import org.siliconeconomy.iotbroker.model.device.DeviceTypeUpdate;

/**
 * A Jackson Mixin for {@link DeviceTypeUpdate} instances.
 * <p>
 * Allows {@link DeviceTypeUpdate} instances to be deserialized which otherwise would not be
 * possible due to a missing default constructor and missing setters.
 *
 * @author M. Grzenia
 */
public abstract class DeviceTypeUpdateMixin {

  @JsonCreator
  DeviceTypeUpdateMixin(@JsonProperty("newState") DeviceType newState,
                        @JsonProperty("oldState") DeviceType oldState,
                        @JsonProperty("type") DeviceTypeUpdate.Type type) {
    // This constructor won't be called
  }
}
