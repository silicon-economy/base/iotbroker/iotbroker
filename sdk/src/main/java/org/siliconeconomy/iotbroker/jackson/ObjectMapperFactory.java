/*
 * Copyright Open Logistics Foundation
 *
 * Licensed under the Open Logistics Foundation License 1.3.
 * For details on the licensing terms, see the LICENSE file.
 * SPDX-License-Identifier: OLFL-1.3
 */
package org.siliconeconomy.iotbroker.jackson;

import com.fasterxml.jackson.databind.ObjectMapper;
import com.fasterxml.jackson.databind.SerializationFeature;
import com.fasterxml.jackson.datatype.jsr310.JavaTimeModule;
import org.siliconeconomy.iotbroker.jackson.device.DeviceModule;
import org.siliconeconomy.iotbroker.jackson.sensordata.SensorDataModule;
import org.siliconeconomy.iotbroker.model.device.DeviceInstance;
import org.siliconeconomy.iotbroker.model.device.DeviceInstanceUpdate;
import org.siliconeconomy.iotbroker.model.device.DeviceType;
import org.siliconeconomy.iotbroker.model.device.DeviceTypeUpdate;
import org.siliconeconomy.iotbroker.model.sensordata.SensorDataMessage;

/**
 * A factory for creating {@link ObjectMapper} instances capable of serializing and deserializing
 * IoT Broker data structures.
 *
 * @author M. Grzenia
 */
public class ObjectMapperFactory {

  private ObjectMapperFactory() {
  }

  /**
   * Creates an {@link ObjectMapper} instance that is configured to serialize and deserialize
   * instances of:
   * <ul>
   *   <li>{@link DeviceType}</li>
   *   <li>{@link DeviceTypeUpdate}</li>
   *   <li>{@link DeviceInstance}</li>
   *   <li>{@link DeviceInstanceUpdate}</li>
   *   <li>{@link SensorDataMessage}</li>
   * </ul>
   *
   * @return An {@link ObjectMapper} instance.
   */
  public static ObjectMapper createObjectMapper() {
    return new ObjectMapper()
        .registerModule(new JavaTimeModule())
        .configure(SerializationFeature.WRITE_DATES_AS_TIMESTAMPS, false)
        .registerModule(new DeviceModule())
        .registerModule(new SensorDataModule());
  }
}
