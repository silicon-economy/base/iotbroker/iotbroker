/*
 * Copyright Open Logistics Foundation
 *
 * Licensed under the Open Logistics Foundation License 1.3.
 * For details on the licensing terms, see the LICENSE file.
 * SPDX-License-Identifier: OLFL-1.3
 */
package org.siliconeconomy.iotbroker.jackson.sensordata.mixin;

import com.fasterxml.jackson.annotation.JsonCreator;
import com.fasterxml.jackson.annotation.JsonProperty;
import org.siliconeconomy.iotbroker.model.sensordata.Location;
import org.siliconeconomy.iotbroker.model.sensordata.LocationDetails;
import org.siliconeconomy.iotbroker.model.sensordata.Observation;
import org.siliconeconomy.iotbroker.model.sensordata.ObservationResult;

import java.time.Instant;
import java.util.Map;

/**
 * A Jackson Mixin for {@link Observation} instances.
 * <p>
 * Allows {@link Observation} instances to be deserialized which otherwise would not be possible
 * due to a missing default constructor and missing setters.
 *
 * @param <T> The type of the observation result.
 */
public abstract class ObservationMixin<T extends ObservationResult<?>> {

  @JsonCreator
  ObservationMixin(@JsonProperty("timestamp") Instant timestamp,
                   @JsonProperty("result") T result,
                   @JsonProperty("location") Location<? extends LocationDetails<?>> location,
                   @JsonProperty("parameters") Map<String, String> parameters) {
    // This constructor won't be called
  }
}
