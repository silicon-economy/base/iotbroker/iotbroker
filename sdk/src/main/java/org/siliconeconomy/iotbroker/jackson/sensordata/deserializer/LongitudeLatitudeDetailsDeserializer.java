/*
 * Copyright Open Logistics Foundation
 *
 * Licensed under the Open Logistics Foundation License 1.3.
 * For details on the licensing terms, see the LICENSE file.
 * SPDX-License-Identifier: OLFL-1.3
 */
package org.siliconeconomy.iotbroker.jackson.sensordata.deserializer;

import com.fasterxml.jackson.core.JsonParser;
import com.fasterxml.jackson.databind.DeserializationContext;
import com.fasterxml.jackson.databind.ObjectMapper;
import com.fasterxml.jackson.databind.deser.std.StdDeserializer;
import com.fasterxml.jackson.databind.node.ObjectNode;
import org.siliconeconomy.iotbroker.model.sensordata.location.LatitudeLongitudeDetails;
import org.siliconeconomy.iotbroker.model.sensordata.location.datatype.LatLong;

import java.io.IOException;

/**
 * Deserializer for instances of {@link LatitudeLongitudeDetails}.
 *
 * @author M. Grzenia
 */
public class LongitudeLatitudeDetailsDeserializer
    extends StdDeserializer<LatitudeLongitudeDetails> {

  public LongitudeLatitudeDetailsDeserializer() {
    super(LatitudeLongitudeDetails.class);
  }

  @Override
  public LatitudeLongitudeDetails deserialize(JsonParser jsonParser,
                                              DeserializationContext context)
      throws IOException {
    ObjectMapper mapper = (ObjectMapper) jsonParser.getCodec();
    ObjectNode objectNode = mapper.readTree(jsonParser);

    return new LatitudeLongitudeDetails(
        new LatLong(
            mapper.treeToValue(objectNode.get("latitude"), Double.class),
            mapper.treeToValue(objectNode.get("longitude"), Double.class)
        )
    );
  }
}
