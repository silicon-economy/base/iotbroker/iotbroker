/*
 * Copyright Open Logistics Foundation
 *
 * Licensed under the Open Logistics Foundation License 1.3.
 * For details on the licensing terms, see the LICENSE file.
 * SPDX-License-Identifier: OLFL-1.3
 */
package org.siliconeconomy.iotbroker.amqp;

/**
 * Defines the configuration of the AMQP exchange where (raw) device messages can be published to by
 * device adapters.
 * <p>
 * Since the format and content of device messages can differ significantly across different device
 * types but also within the same device type, there is no predefined structure or format for
 * messages published via this exchange. It is up to the respective device adapters how they publish
 * device messages.
 * <p>
 * Components interested in (raw) device messages of one or more device types are expected to know
 * how to process these messages. Developers of device adapters that publish such messages are
 * therefore encouraged to document how their respective device adapter publishes messages to this
 * exchange (e.g. with regard to AMQP message properties like the encoding type and content type).
 *
 * @author M. Grzenia
 */
public class DeviceMessageExchangeConfiguration {

  /**
   * The name of the exchange.
   */
  public static final String NAME = "device.message";
  /**
   * The exchange type.
   * <p>
   * Corresponds to the AMQP <em>topic</em> exchange type.
   */
  public static final String TYPE = "topic";
  /**
   * The exchange durability.
   */
  public static final boolean DURABLE = true;
  /**
   * The format of the routing key used to publish (raw) device messages to the exchange.
   * <p>
   * The format is based on the following patter:
   * {@literal <deviceTypeIdentifier>.<source>.<tenant>}
   *
   * @see #formatDeviceMessageRoutingKey(String, String, String)
   */
  public static final String ROUTING_KEY_FORMAT = "%s.%s.%s";

  private DeviceMessageExchangeConfiguration() {
  }

  /**
   * Convenience method for formatting the {@link #ROUTING_KEY_FORMAT}
   *
   * @param deviceTypeIdentifier The device type identifier to use.
   * @param source               The source to use.
   * @param tenant               The tenant to use.
   * @return The formatted string.
   */
  public static String formatDeviceMessageRoutingKey(String deviceTypeIdentifier,
                                                     String source,
                                                     String tenant) {
    return String.format(ROUTING_KEY_FORMAT, deviceTypeIdentifier, source, tenant);
  }
}
