/*
 * Copyright Open Logistics Foundation
 *
 * Licensed under the Open Logistics Foundation License 1.3.
 * For details on the licensing terms, see the LICENSE file.
 * SPDX-License-Identifier: OLFL-1.3
 */
package org.siliconeconomy.iotbroker.amqp;

import org.siliconeconomy.iotbroker.model.sensordata.SensorDataMessage;

/**
 * Defines the configuration of the AMQP exchange where {@link SensorDataMessage}s are published to.
 *
 * @author M. Grzenia
 */
public class SensorDataExchangeConfiguration {

  /**
   * The name of the exchange.
   */
  public static final String NAME = "sensor.data";
  /**
   * The exchange type.
   * <p>
   * Corresponds to the AMQP <em>topic</em> exchange type.
   */
  public static final String TYPE = "topic";
  /**
   * The exchange durability.
   */
  public static final boolean DURABLE = true;
  /**
   * The routing key used to publish {@link SensorDataMessage}s to the exchange.
   */
  public static final String ROUTING_KEY = "sensor.data.message";

  private SensorDataExchangeConfiguration() {
  }
}
