/*
 * Copyright Open Logistics Foundation
 *
 * Licensed under the Open Logistics Foundation License 1.3.
 * For details on the licensing terms, see the LICENSE file.
 * SPDX-License-Identifier: OLFL-1.3
 */
package org.siliconeconomy.iotbroker.util;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

/**
 * A {@link Thread.UncaughtExceptionHandler} that provides logging for when a thread abruptly
 * terminates due to an uncaught exception.
 *
 * @author M. Grzenia
 */
public class UncaughtExceptionLogger
    implements Thread.UncaughtExceptionHandler {

  /**
   * This class's Logger.
   */
  private static final Logger LOG = LoggerFactory.getLogger(UncaughtExceptionLogger.class);

  @Override
  public void uncaughtException(Thread t, Throwable e) {
    LOG.error("Unhandled exception", e);
  }
}
