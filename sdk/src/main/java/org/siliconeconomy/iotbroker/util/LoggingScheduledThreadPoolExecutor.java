/*
 * Copyright Open Logistics Foundation
 *
 * Licensed under the Open Logistics Foundation License 1.3.
 * For details on the licensing terms, see the LICENSE file.
 * SPDX-License-Identifier: OLFL-1.3
 */
package org.siliconeconomy.iotbroker.util;

import org.checkerframework.checker.nullness.qual.NonNull;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import java.util.concurrent.*;

import static java.util.Objects.requireNonNull;

/**
 * A custom thread pool that reuses a fixed number of threads operating off a shared unbounded
 * queue.
 * <p>
 * Instances of this class behave similarly to a thread pool created with
 * {@link Executors#newScheduledThreadPool(int)}. The only difference is that exceptions thrown by
 * submitted tasks do get logged, which is not the case for {@link ExecutorService}s created with the
 * corresponding methods provided by {@link Executors}, which can make debugging more difficult.
 *
 * @author M. Grzenia
 */
public class LoggingScheduledThreadPoolExecutor
    extends ScheduledThreadPoolExecutor {

  /**
   * The logger instance that should be used by default for all instances of this class.
   */
  private static final Logger DEFAULT_LOGGER
      = LoggerFactory.getLogger(LoggingScheduledThreadPoolExecutor.class);

  private final Logger log;

  /**
   * Creates a new instance.
   *
   * @param numberOfThreads The number of threads to keep in the pool, even if they are idle.
   * @throws IllegalArgumentException If {@code numberOfThreads <= 0}
   */
  public LoggingScheduledThreadPoolExecutor(int numberOfThreads) {
    this(numberOfThreads, DEFAULT_LOGGER);
  }

  /**
   * Creates a new instance.
   * <p>
   * This constructor is intentionally package private since it is intended to be used only in
   * tests. The purpose of this class is to allow uncaught exceptions in thread pool executor
   * threads to be logged. Instead of relying on some third-party library to be able to test if
   * the logging actually works, having this constructor and allowing to use a simple mock in
   * tests seems to be an acceptable compromise.
   *
   * @param numberOfThreads The number of threads to keep in the pool, even if they are idle.
   * @param log             The logger instance to be utilized for logging messages.
   * @throws IllegalArgumentException If {@code numberOfThreads <= 0}
   */
  LoggingScheduledThreadPoolExecutor(int numberOfThreads, @NonNull Logger log) {
    super(
        numberOfThreads,
        runnable -> {
          var thread = Executors.defaultThreadFactory().newThread(runnable);
          thread.setUncaughtExceptionHandler(new UncaughtExceptionLogger());
          return thread;
        }
    );
    this.log = requireNonNull(log, "log");
  }

  @Override
  protected void afterExecute(Runnable r, Throwable t) {
    super.afterExecute(r, t);
    if (t == null && r instanceof Future<?>) {
      try {
        Future<?> future = (Future<?>) r;
        if (future.isDone()) {
          future.get();
        } else {
          log.debug("Future was not done: {}", future);
        }
      } catch (ExecutionException ee) {
        log.warn("Unhandled exception in executed task", ee.getCause());
      } catch (CancellationException ce) {
        log.debug("Task was cancelled", ce);
      } catch (InterruptedException ie) {
        log.debug("Interrupted during Future.get()", ie);
        // Ignore/Reset
        Thread.currentThread().interrupt();
      }
    }
    if (t != null) {
      log.error("Abrupt termination", t);
    }
  }
}
