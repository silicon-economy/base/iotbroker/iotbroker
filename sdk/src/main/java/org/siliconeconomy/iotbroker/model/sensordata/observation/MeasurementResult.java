/*
 * Copyright Open Logistics Foundation
 *
 * Licensed under the Open Logistics Foundation License 1.3.
 * For details on the licensing terms, see the LICENSE file.
 * SPDX-License-Identifier: OLFL-1.3
 */
package org.siliconeconomy.iotbroker.model.sensordata.observation;

import org.siliconeconomy.iotbroker.model.sensordata.ObservationResult;

import java.util.Objects;

/**
 * Describes an observation result using a floating-point number.
 * <p>
 * This is the implementation for the {@link ObservationType#MEASUREMENT}.
 *
 * @author C. Hoppe
 * @author M. Grzenia
 */
public class MeasurementResult
    implements ObservationResult<Double> {

  private final Double measurement;

  /**
   * Creates a new instance.
   *
   * @param measurement The result as a floating-point number. Can be {@code null}, if the result
   *                    could not be determined.
   */
  public MeasurementResult(Double measurement) {
    this.measurement = measurement;
  }

  @Override
  public Double getResult() {
    return measurement;
  }

  @Override
  public boolean equals(Object o) {
    if (!(o instanceof MeasurementResult)) {
      return false;
    }

    MeasurementResult other = (MeasurementResult) o;
    return Objects.equals(measurement, other.measurement);
  }

  @Override
  public int hashCode() {
    return Objects.hash(measurement);
  }
}
