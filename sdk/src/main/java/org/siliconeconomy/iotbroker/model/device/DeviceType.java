/*
 * Copyright Open Logistics Foundation
 *
 * Licensed under the Open Logistics Foundation License 1.3.
 * For details on the licensing terms, see the LICENSE file.
 * SPDX-License-Identifier: OLFL-1.3
 */
package org.siliconeconomy.iotbroker.model.device;

import org.checkerframework.checker.nullness.qual.NonNull;

import java.util.Objects;
import java.util.Set;

import static java.util.Objects.requireNonNull;

/**
 * A device type contains information concerning multiple devices of the same type.
 * <p>
 * A device type can be considered as a <em>logical</em> group of devices that are similar with
 * respect to one or more distinctive characteristics. Examples of such characteristics can be:
 * <ul>
 *     <li>The name of the device model.</li>
 *     <li>A revision or version of the device's hardware or software.</li>
 *     <li>Other device properties.</li>
 * </ul>
 * These characteristics, or even a combination of them, can be used to describe a particular type
 * of device. For example, if the revision of a device's hardware changes, the device could be
 * considered a different type than it was at a previous revision.
 * <p>
 * In any case, a device type is uniquely identifiable via its {@link #source} and
 * {@link #identifier}. Multiple device types with the same source and identifier are therefore not
 * allowed.
 * <p>
 * This class provides {@code with*()} methods, which create a copy of an object, differing in the
 * value provided to the method.
 *
 * @author M. Grzenia
 * @author D. Ronnenberg
 * @see DeviceInstance
 */
public class DeviceType {

  @NonNull
  private final String id;
  @NonNull
  private final String source;
  @NonNull
  private final String identifier;
  @NonNull
  private final Set<String> providedBy;
  @NonNull
  private final String description;
  private final boolean enabled;
  private final boolean autoRegisterDeviceInstances;
  private final boolean autoEnableDeviceInstances;

  /**
   * Creates a new instance.
   *
   * @param id                          The unique identifier for device type.
   * @param source                      The source that the device type is associated with.
   * @param identifier                  The identifier for the device type.
   * @param providedBy                  The list of adapter identifiers.
   * @param description                 The description for the device type.
   * @param enabled                     Whether the device type is enabled or not.
   * @param autoRegisterDeviceInstances Whether new {@link DeviceInstance}s associated with this
   *                                    device type should be automatically registered with the
   *                                    IoT Broker.
   * @param autoEnableDeviceInstances   Whether newly registered {@link DeviceInstance}s associated
   *                                    with this device type should be enabled automatically.
   */
  public DeviceType(@NonNull String id,
                    @NonNull String source,
                    @NonNull String identifier,
                    @NonNull Set<String> providedBy,
                    @NonNull String description,
                    boolean enabled,
                    boolean autoRegisterDeviceInstances,
                    boolean autoEnableDeviceInstances) {
    this.id = requireNonNull(id, "id");
    this.source = requireNonNull(source, "source");
    this.identifier = requireNonNull(identifier, "identifier");
    this.providedBy = requireNonNull(providedBy, "providedBy");
    this.description = requireNonNull(description, "description");
    this.enabled = enabled;
    this.autoRegisterDeviceInstances = autoRegisterDeviceInstances;
    this.autoEnableDeviceInstances = autoEnableDeviceInstances;
  }

  /**
   * Returns the unique identifier for device type.
   * <p>
   * This field is not to be confused with the {@link #getIdentifier() identifier} field. While the
   * ID is used to uniquely identify a device type across <em>all</em> device types, regardless of
   * any other device type properties, the identifier can only be used to uniquely identify a device
   * type in the context of the {@link #getSource() source} it is associated with.
   * <p>
   * Some APIs may support using this attribute as an alternative shortcut for accessing device
   * types.
   *
   * @return The unique identifier for device type.
   */
  @NonNull
  public String getId() {
    return id;
  }

  public DeviceType withId(@NonNull String id) {
    return new DeviceType(id,
                          source,
                          identifier,
                          providedBy,
                          description,
                          enabled,
                          autoRegisterDeviceInstances,
                          autoEnableDeviceInstances);
  }

  /**
   * Returns the source that the device type is associated with. (E.g. as a source of data.)
   *
   * @return The source that the device type is associated with.
   */
  @NonNull
  public String getSource() {
    return source;
  }

  public DeviceType withSource(@NonNull String source) {
    return new DeviceType(id,
                          source,
                          identifier,
                          providedBy,
                          description,
                          enabled,
                          autoRegisterDeviceInstances,
                          autoEnableDeviceInstances);
  }

  /**
   * Returns the identifier for the device type (which is unique in the context of the device type's
   * {@link #getSource() source}).
   *
   * @return The identifier for the device type.
   */
  @NonNull
  public String getIdentifier() {
    return identifier;
  }

  public DeviceType withIdentifier(@NonNull String identifier) {
    return new DeviceType(id,
                          source,
                          identifier,
                          providedBy,
                          description,
                          enabled,
                          autoRegisterDeviceInstances,
                          autoEnableDeviceInstances);
  }

  /**
   * Returns the list of adapter identifiers via which devices of this type are provided within the
   * IoT Broker.
   *
   * @return The list of adapter identifiers.
   */
  @NonNull
  public Set<String> getProvidedBy() {
    return providedBy;
  }

  public DeviceType withProvidedBy(@NonNull Set<String> providedBy) {
    return new DeviceType(id,
                          source,
                          identifier,
                          providedBy,
                          description,
                          enabled,
                          autoRegisterDeviceInstances,
                          autoEnableDeviceInstances);
  }

  /**
   * Returns the description for the device type.
   * <p>
   * Optional: May be an empty string.
   *
   * @return The description for the device type.
   */
  @NonNull
  public String getDescription() {
    return description;
  }

  public DeviceType withDescription(@NonNull String description) {
    return new DeviceType(id,
                          source,
                          identifier,
                          providedBy,
                          description,
                          enabled,
                          autoRegisterDeviceInstances,
                          autoEnableDeviceInstances);
  }

  /**
   * Returns whether the device type is enabled or not.
   * <p>
   * For devices that are associated with a disabled device type, incoming data will not be
   * processed by their associated adapter.
   *
   * @return {@code true}, if the device type is enabled, otherwise {@code false}.
   */
  public boolean isEnabled() {
    return enabled;
  }

  public DeviceType withEnabled(boolean enabled) {
    return new DeviceType(id,
                          source,
                          identifier,
                          providedBy,
                          description,
                          enabled,
                          autoRegisterDeviceInstances,
                          autoEnableDeviceInstances);
  }

  /**
   * Returns whether new (as yet unknown) {@link DeviceInstance}s associated with this device type
   * should be automatically registered with the IoT Broker.
   *
   * @return {@code true}, if new {@link DeviceInstance}s should be automatically registered with
   * the IoT Broker, otherwise {@code false}.
   */
  public boolean isAutoRegisterDeviceInstances() {
    return autoRegisterDeviceInstances;
  }

  public DeviceType withAutoRegisterDeviceInstances(boolean autoRegisterDeviceInstances) {
    return new DeviceType(id,
                          source,
                          identifier,
                          providedBy,
                          description,
                          enabled,
                          autoRegisterDeviceInstances,
                          autoEnableDeviceInstances);
  }

  /**
   * Returns whether newly registered {@link DeviceInstance}s associated with this device type
   * should be enabled automatically.
   *
   * @return {@code true}, if newly registered {@link DeviceInstance}s should be enabled
   * automatically enabled, otherwise {@code false}.
   */
  public boolean isAutoEnableDeviceInstances() {
    return autoEnableDeviceInstances;
  }

  public DeviceType withAutoEnableDeviceInstances(boolean autoEnableDeviceInstances) {
    return new DeviceType(id,
                          source,
                          identifier,
                          providedBy,
                          description,
                          enabled,
                          autoRegisterDeviceInstances,
                          autoEnableDeviceInstances);
  }

  @Override
  public boolean equals(Object o) {
    if (!(o instanceof DeviceType)) {
      return false;
    }

    DeviceType other = (DeviceType) o;
    return enabled == other.enabled
        && autoRegisterDeviceInstances == other.autoRegisterDeviceInstances
        && autoEnableDeviceInstances == other.autoEnableDeviceInstances
        && Objects.equals(id, other.id)
        && Objects.equals(source, other.source)
        && Objects.equals(identifier, other.identifier)
        && Objects.equals(providedBy, other.providedBy)
        && Objects.equals(description, other.description);
  }

  @Override
  public int hashCode() {
    return Objects.hash(id, source, identifier, providedBy, description, enabled,
                        autoRegisterDeviceInstances, autoEnableDeviceInstances);
  }

  @Override
  public String toString() {
    return "DeviceType{" +
        "id=" + id +
        ", source=" + source +
        ", identifier=" + identifier +
        ", providedBy=" + providedBy +
        ", description=" + description +
        ", enabled=" + enabled +
        ", autoRegisterDeviceInstances=" + autoRegisterDeviceInstances +
        ", autoEnableDeviceInstances=" + autoEnableDeviceInstances +
        '}';
  }
}
