/*
 * Copyright Open Logistics Foundation
 *
 * Licensed under the Open Logistics Foundation License 1.3.
 * For details on the licensing terms, see the LICENSE file.
 * SPDX-License-Identifier: OLFL-1.3
 */
package org.siliconeconomy.iotbroker.model.sensordata.location;

import org.checkerframework.checker.nullness.qual.NonNull;
import org.siliconeconomy.iotbroker.model.sensordata.LocationDetails;

import java.util.Objects;

import static java.util.Objects.requireNonNull;

/**
 * Describes a location in the simplest and most generic way using a string.
 * <p>
 * Can be used to describe a logical position. This is the implementation for the
 * {@link LocationEncodingType#SIMPLE}.
 *
 * @author C. Hoppe
 * @author M. Grzenia
 */
public class SimpleDetails
    implements LocationDetails<String> {

  @NonNull
  private final String location;

  /**
   * Creates a new instance.
   *
   * @param location The string describing the location.
   */
  public SimpleDetails(@NonNull String location) {
    this.location = requireNonNull(location, "location");
  }

  @Override
  @NonNull
  public String getDetails() {
    return location;
  }

  @Override
  public boolean equals(Object o) {
    if (!(o instanceof SimpleDetails)) {
      return false;
    }

    SimpleDetails other = (SimpleDetails) o;
    return Objects.equals(location, other.location);
  }

  @Override
  public int hashCode() {
    return Objects.hash(location);
  }
}
