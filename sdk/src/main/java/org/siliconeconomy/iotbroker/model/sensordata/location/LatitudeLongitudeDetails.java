/*
 * Copyright Open Logistics Foundation
 *
 * Licensed under the Open Logistics Foundation License 1.3.
 * For details on the licensing terms, see the LICENSE file.
 * SPDX-License-Identifier: OLFL-1.3
 */
package org.siliconeconomy.iotbroker.model.sensordata.location;

import org.checkerframework.checker.nullness.qual.NonNull;
import org.siliconeconomy.iotbroker.model.sensordata.LocationDetails;
import org.siliconeconomy.iotbroker.model.sensordata.location.datatype.LatLong;

import java.util.Objects;

import static java.util.Objects.requireNonNull;

/**
 * Describes a location as geographic coordinates using latitude and longitude coordinates.
 * <p>
 * This is the implementation for the {@link LocationEncodingType#LATLONG}.
 *
 * @author C. Hoppe
 * @author M. Grzenia
 */
public class LatitudeLongitudeDetails
    implements LocationDetails<LatLong> {

  @NonNull
  private final LatLong latlong;

  /**
   * Creates a new instance.
   *
   * @param latlong The latitude and longitude coordinates.
   */
  public LatitudeLongitudeDetails(@NonNull LatLong latlong) {
    this.latlong = requireNonNull(latlong, "latlong");
  }

  /**
   * Returns the latitude and longitude coordinates.
   *
   * @return The latitude and longitude coordinates.
   * @deprecated Use {@link #getDetails()} instead.
   */
  @Deprecated(forRemoval = true)
  @NonNull
  public LatLong getLatlong() {
    return latlong;
  }

  @Override
  @NonNull
  public LatLong getDetails() {
    return latlong;
  }

  @Override
  public boolean equals(Object o) {
    if (!(o instanceof LatitudeLongitudeDetails)) {
      return false;
    }

    LatitudeLongitudeDetails other = (LatitudeLongitudeDetails) o;
    return Objects.equals(latlong, other.latlong);
  }

  @Override
  public int hashCode() {
    return Objects.hash(latlong);
  }
}
