/*
 * Copyright Open Logistics Foundation
 *
 * Licensed under the Open Logistics Foundation License 1.3.
 * For details on the licensing terms, see the LICENSE file.
 * SPDX-License-Identifier: OLFL-1.3
 */
package org.siliconeconomy.iotbroker.model.sensordata.observation;

import org.siliconeconomy.iotbroker.model.sensordata.ObservationResult;

import java.util.Objects;

/**
 * Describes an observation result using an integer.
 * <p>
 * This is the implementation for the {@link ObservationType#COUNT}.
 *
 * @author C. Hoppe
 * @author M. Grzenia
 */
public class CountResult
    implements ObservationResult<Long> {

  private final Long count;

  /**
   * Creates a new instance.
   *
   * @param count The result as an integer. Can be {@code null}, if the result could not be
   *              determined.
   */
  public CountResult(Long count) {
    this.count = count;
  }

  @Override
  public Long getResult() {
    return count;
  }

  @Override
  public boolean equals(Object o) {
    if (!(o instanceof CountResult)) {
      return false;
    }

    CountResult other = (CountResult) o;
    return Objects.equals(count, other.count);
  }

  @Override
  public int hashCode() {
    return Objects.hash(count);
  }
}
