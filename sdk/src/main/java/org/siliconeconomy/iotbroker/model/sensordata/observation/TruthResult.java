/*
 * Copyright Open Logistics Foundation
 *
 * Licensed under the Open Logistics Foundation License 1.3.
 * For details on the licensing terms, see the LICENSE file.
 * SPDX-License-Identifier: OLFL-1.3
 */
package org.siliconeconomy.iotbroker.model.sensordata.observation;

import org.siliconeconomy.iotbroker.model.sensordata.ObservationResult;

import java.util.Objects;

/**
 * Describes an observation result using a truth value.
 * <p>
 * This is the implementation for the {@link ObservationType#TRUTH}.
 *
 * @author C. Hoppe
 * @author M. Grzenia
 */
public class TruthResult
    implements ObservationResult<Boolean> {

  /**
   * The result as a truth value.
   */
  private final Boolean truth;

  /**
   * Creates a new instance.
   *
   * @param truth The result as a truth value. Can be {@code null}, if the result could not be
   *              determined.
   */
  public TruthResult(Boolean truth) {
    this.truth = truth;
  }

  @Override
  public Boolean getResult() {
    return truth;
  }

  @Override
  public boolean equals(Object o) {
    if (!(o instanceof TruthResult)) {
      return false;
    }

    TruthResult other = (TruthResult) o;
    return Objects.equals(truth, other.truth);
  }

  @Override
  public int hashCode() {
    return Objects.hash(truth);
  }
}
