/*
 * Copyright Open Logistics Foundation
 *
 * Licensed under the Open Logistics Foundation License 1.3.
 * For details on the licensing terms, see the LICENSE file.
 * SPDX-License-Identifier: OLFL-1.3
 */
package org.siliconeconomy.iotbroker.model.sensordata;

import org.checkerframework.checker.nullness.qual.NonNull;
import org.siliconeconomy.iotbroker.model.sensordata.observation.ObservationType;

import java.util.Objects;

import static java.util.Objects.requireNonNull;

/**
 * This class describes the unit of a measurement for {@link ObservationResult}s in a
 * {@link Datastream}'s {@link Observation}s.
 * <p>
 * Although a unit of measurement is usually provided for datastreams with an
 * {@link ObservationType#MEASUREMENT}, it is not necessarily limited to these types.
 * In general, a unit of measurement can be used to further specify the result of an observation.
 * In cases where the name or the symbol of a unit of measurement cannot be meaningfully expressed,
 * the corresponding values should be set to an empty string.
 *
 * @author C. Hoppe
 * @author M. Grzenia
 */
public class UnitOfMeasurement {

  @NonNull
  private final String name;
  @NonNull
  private final String symbol;

  /**
   * Creates a new instance.
   *
   * @param name   The full name of the unit of measurement.
   * @param symbol The textual form of the unit symbol.
   */
  public UnitOfMeasurement(@NonNull String name, @NonNull String symbol) {
    this.name = requireNonNull(name, "name");
    this.symbol = requireNonNull(symbol, "symbol");
  }

  /**
   * Returns the full name of the unit of measurement.
   * <p>
   * If there is no unit name, the value is an empty string.
   *
   * @return The full name of the unit of measurement.
   */
  @NonNull
  public String getName() {
    return name;
  }

  /**
   * Returns the textual form of the unit symbol.
   * <p>
   * If there is no unit symbol, the value is an empty string.
   *
   * @return The textual form of the unit symbol.
   */
  @NonNull
  public String getSymbol() {
    return symbol;
  }

  @Override
  public boolean equals(Object o) {
    if (!(o instanceof UnitOfMeasurement)) {
      return false;
    }

    UnitOfMeasurement other = (UnitOfMeasurement) o;
    return Objects.equals(name, other.name) && Objects.equals(symbol, other.symbol);
  }

  @Override
  public int hashCode() {
    return Objects.hash(name, symbol);
  }
}
