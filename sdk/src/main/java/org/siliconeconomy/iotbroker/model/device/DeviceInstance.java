/*
 * Copyright Open Logistics Foundation
 *
 * Licensed under the Open Logistics Foundation License 1.3.
 * For details on the licensing terms, see the LICENSE file.
 * SPDX-License-Identifier: OLFL-1.3
 */
package org.siliconeconomy.iotbroker.model.device;

import org.checkerframework.checker.nullness.qual.NonNull;

import java.time.Instant;
import java.util.Objects;

import static java.util.Objects.requireNonNull;

/**
 * A representation of a device that is known to the IoT Broker.
 * <p>
 * A device is defined by the following properties:
 * <ul>
 *     <li>It is uniquely identifiable via its {@link #source} and {@link #tenant}. Multiple devices
 *     with the same source and tenant are therefore not allowed.</li>
 *     <li>It is always associated with a specific {@link DeviceType}.</li>
 * </ul>
 * In addition to the properties listed above, this class contains information that is important in
 * the context of the IoT Broker (e.g. the registration time or whether the device is enabled) and
 * information describing details about the device itself (e.g. the device's firmware version).
 * <p>
 * For fields containing optional information, most, if not all, are required not to be
 * {@code null}. This is done merely to avoid checks for {@code null}.
 * <p>
 * This class provides {@code with*()} methods, which create a copy of an object, differing in the
 * value provided to the method.
 *
 * @author M. Grzenia
 * @author D. Ronnenberg
 */
public class DeviceInstance {

  @NonNull
  private final String id;
  @NonNull
  private final String source;
  @NonNull
  private final String tenant;
  @NonNull
  private final String deviceTypeIdentifier;
  @NonNull
  private final Instant registrationTime;
  @NonNull
  private final Instant lastSeen;
  private final boolean enabled;
  @NonNull
  private final String description;
  @NonNull
  private final String hardwareRevision;
  @NonNull
  private final String firmwareVersion;

  /**
   * Creates a new instance.
   *
   * @param id                   The unique identifier for the device.
   * @param source               The source that the device is associated with.
   * @param tenant               The tenant for the device.
   * @param deviceTypeIdentifier The identifier of the associated {@link DeviceType}.
   * @param registrationTime     The date and time when the device was registered.
   * @param lastSeen             The date and time when the device was last seen.
   * @param enabled              Whether the device is enabled or not.
   * @param description          The description for the device.
   * @param hardwareRevision     The device's hardware revision.
   * @param firmwareVersion      The device's firmware version.
   */
  public DeviceInstance(@NonNull String id,
                        @NonNull String source,
                        @NonNull String tenant,
                        @NonNull String deviceTypeIdentifier,
                        @NonNull Instant registrationTime,
                        @NonNull Instant lastSeen,
                        boolean enabled,
                        @NonNull String description,
                        @NonNull String hardwareRevision,
                        @NonNull String firmwareVersion) {
    this.id = requireNonNull(id, "id");
    this.source = requireNonNull(source, "source");
    this.tenant = requireNonNull(tenant, "tenant");
    this.deviceTypeIdentifier = requireNonNull(deviceTypeIdentifier, "deviceTypeIdentifier");
    this.registrationTime = requireNonNull(registrationTime, "registrationTime");
    this.lastSeen = requireNonNull(lastSeen, "lastSeen");
    this.enabled = enabled;
    this.description = requireNonNull(description, "description");
    this.hardwareRevision = requireNonNull(hardwareRevision, "hardwareRevision");
    this.firmwareVersion = requireNonNull(firmwareVersion, "firmwareVersion");
  }

  /**
   * Returns the unique identifier for the device.
   * <p>
   * This field is not to be confused with the {@link #getTenant() tenant} field. While the ID is
   * used to uniquely identify a device instance across <em>all</em> device instances, regardless of
   * any other device instance properties, the tenant can only be used to uniquely identify a device
   * instance in the context of the {@link #getSource() source} it is associated with.
   * <p>
   * Some APIs may support using this attribute as an alternative shortcut for accessing device
   * instances.
   *
   * @return The unique identifier for the device.
   */
  @NonNull
  public String getId() {
    return id;
  }

  public DeviceInstance withId(@NonNull String id) {
    requireNonNull(id, "id");
    return new DeviceInstance(id,
                              source,
                              tenant,
                              deviceTypeIdentifier,
                              registrationTime,
                              lastSeen,
                              enabled,
                              description,
                              hardwareRevision,
                              firmwareVersion);
  }

  /**
   * Returns the source that the device is associated with. (E.g. as a source of data.)
   *
   * @return The source that the device is associated with.
   */
  @NonNull
  public String getSource() {
    return source;
  }

  public DeviceInstance withSource(@NonNull String source) {
    requireNonNull(source, "source");
    return new DeviceInstance(id,
                              source,
                              tenant,
                              deviceTypeIdentifier,
                              registrationTime,
                              lastSeen,
                              enabled,
                              description,
                              hardwareRevision,
                              firmwareVersion);
  }

  /**
   * Returns the tenant for the device (which is unique in the context of the device's
   * {@link #getSource() source}). (E.g. the device ID.)
   *
   * @return The tenant for the device.
   */
  @NonNull
  public String getTenant() {
    return tenant;
  }

  public DeviceInstance withTenant(@NonNull String tenant) {
    requireNonNull(tenant, "tenant");
    return new DeviceInstance(id,
                              source,
                              tenant,
                              deviceTypeIdentifier,
                              registrationTime,
                              lastSeen,
                              enabled,
                              description,
                              hardwareRevision,
                              firmwareVersion);
  }

  /**
   * Returns the identifier of the associated {@link DeviceType}.
   *
   * @return The identifier of the associated {@link DeviceType}.
   */
  @NonNull
  public String getDeviceTypeIdentifier() {
    return deviceTypeIdentifier;
  }

  public DeviceInstance withDeviceTypeIdentifier(@NonNull String deviceTypeIdentifier) {
    requireNonNull(deviceTypeIdentifier, "deviceTypeIdentifier");
    return new DeviceInstance(id,
                              source,
                              tenant,
                              deviceTypeIdentifier,
                              registrationTime,
                              lastSeen,
                              enabled,
                              description,
                              hardwareRevision,
                              firmwareVersion);
  }

  /**
   * Returns the date and time when the device was registered.
   *
   * @return The date and time when the device was registered.
   */
  @NonNull
  public Instant getRegistrationTime() {
    return registrationTime;
  }

  public DeviceInstance withRegistrationTime(@NonNull Instant registrationTime) {
    requireNonNull(registrationTime, "registrationTime");
    return new DeviceInstance(id,
                              source,
                              tenant,
                              deviceTypeIdentifier,
                              registrationTime,
                              lastSeen,
                              enabled,
                              description,
                              hardwareRevision,
                              firmwareVersion);
  }

  /**
   * Returns the date and time when the device was last seen.
   * <p>
   * If there has been no communication with the device so far, the value is {@link Instant#EPOCH}.
   *
   * @return The date and time when the device was last seen.
   */
  @NonNull
  public Instant getLastSeen() {
    return lastSeen;
  }

  public DeviceInstance withLastSeen(@NonNull Instant lastSeen) {
    requireNonNull(lastSeen, "lastSeen");
    return new DeviceInstance(id,
                              source,
                              tenant,
                              deviceTypeIdentifier,
                              registrationTime,
                              lastSeen,
                              enabled,
                              description,
                              hardwareRevision,
                              firmwareVersion);
  }

  /**
   * Returns whether the device is enabled or not.
   * <p>
   * For disabled devices, incoming data will not be processed by their associated adapter.
   *
   * @return {@code true}, if the device is enabled, otherwise {@code false}.
   */
  public boolean isEnabled() {
    return enabled;
  }

  public DeviceInstance withEnabled(boolean enabled) {
    return new DeviceInstance(id,
                              source,
                              tenant,
                              deviceTypeIdentifier,
                              registrationTime,
                              lastSeen,
                              enabled,
                              description,
                              hardwareRevision,
                              firmwareVersion);
  }

  /**
   * Returns the description for the device.
   * <p>
   * Optional: May be an empty string.
   *
   * @return The description for the device.
   */
  @NonNull
  public String getDescription() {
    return description;
  }

  public DeviceInstance withDescription(@NonNull String description) {
    requireNonNull(description, "description");
    return new DeviceInstance(id,
                              source,
                              tenant,
                              deviceTypeIdentifier,
                              registrationTime,
                              lastSeen,
                              enabled,
                              description,
                              hardwareRevision,
                              firmwareVersion);
  }

  /**
   * Returns the device's hardware revision.
   * <p>
   * Optional: May be an empty string.
   *
   * @return The device's hardware revision.
   */
  @NonNull
  public String getHardwareRevision() {
    return hardwareRevision;
  }

  public DeviceInstance withHardwareRevision(@NonNull String hardwareRevision) {
    requireNonNull(hardwareRevision, "hardwareRevision");
    return new DeviceInstance(id,
                              source,
                              tenant,
                              deviceTypeIdentifier,
                              registrationTime,
                              lastSeen,
                              enabled,
                              description,
                              hardwareRevision,
                              firmwareVersion);
  }

  /**
   * Returns the device's firmware version.
   * <p>
   * Optional: May be an empty string.
   *
   * @return The device's firmware version.
   */
  @NonNull
  public String getFirmwareVersion() {
    return firmwareVersion;
  }

  public DeviceInstance withFirmwareVersion(@NonNull String firmwareVersion) {
    requireNonNull(firmwareVersion, "firmwareVersion");
    return new DeviceInstance(id,
                              source,
                              tenant,
                              deviceTypeIdentifier,
                              registrationTime,
                              lastSeen,
                              enabled,
                              description,
                              hardwareRevision,
                              firmwareVersion);
  }

  @Override
  public boolean equals(Object o) {
    if (!(o instanceof DeviceInstance)) {
      return false;
    }

    DeviceInstance other = (DeviceInstance) o;
    return enabled == other.enabled
        && Objects.equals(id, other.id)
        && Objects.equals(source, other.source)
        && Objects.equals(tenant, other.tenant)
        && Objects.equals(deviceTypeIdentifier, other.deviceTypeIdentifier)
        && Objects.equals(registrationTime, other.registrationTime)
        && Objects.equals(lastSeen, other.lastSeen)
        && Objects.equals(description, other.description)
        && Objects.equals(hardwareRevision, other.hardwareRevision)
        && Objects.equals(firmwareVersion, other.firmwareVersion);
  }

  @Override
  public int hashCode() {
    return Objects.hash(id, source, tenant, deviceTypeIdentifier, registrationTime, lastSeen,
                        enabled, description, hardwareRevision, firmwareVersion);
  }

  @Override
  public String toString() {
    return "DeviceInstance{" +
        "id=" + id +
        ", source=" + source +
        ", tenant=" + tenant +
        ", deviceTypeIdentifier=" + deviceTypeIdentifier +
        ", registrationTime=" + registrationTime +
        ", lastSeen=" + lastSeen +
        ", enabled=" + enabled +
        ", description=" + description +
        ", hardwareRevision=" + hardwareRevision +
        ", firmwareVersion=" + firmwareVersion +
        '}';
  }
}
