/*
 * Copyright Open Logistics Foundation
 *
 * Licensed under the Open Logistics Foundation License 1.3.
 * For details on the licensing terms, see the LICENSE file.
 * SPDX-License-Identifier: OLFL-1.3
 */
package org.siliconeconomy.iotbroker.model.sensordata.observation;

import org.siliconeconomy.iotbroker.model.sensordata.ObservationResult;

import java.util.Objects;

/**
 * Describes an observation result in the simplest and most generic way using a string.
 * <p>
 * This is the implementation for the {@link ObservationType#SIMPLE}.
 *
 * @author M. Grzenia
 */
public class SimpleResult
    implements ObservationResult<String> {

  /**
   * The result as a string.
   */
  private final String result;

  /**
   * Creates a new instance.
   *
   * @param result The result as a string. Can be {@code null}, if the result could not be
   *               determined.
   */
  public SimpleResult(String result) {
    this.result = result;
  }

  @Override
  public String getResult() {
    return result;
  }

  @Override
  public boolean equals(Object o) {
    if (!(o instanceof SimpleResult)) {
      return false;
    }

    SimpleResult other = (SimpleResult) o;
    return Objects.equals(result, other.result);
  }

  @Override
  public int hashCode() {
    return Objects.hash(result);
  }
}
