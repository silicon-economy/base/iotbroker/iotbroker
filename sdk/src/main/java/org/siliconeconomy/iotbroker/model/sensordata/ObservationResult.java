/*
 * Copyright Open Logistics Foundation
 *
 * Licensed under the Open Logistics Foundation License 1.3.
 * For details on the licensing terms, see the LICENSE file.
 * SPDX-License-Identifier: OLFL-1.3
 */
package org.siliconeconomy.iotbroker.model.sensordata;

import org.checkerframework.checker.nullness.qual.Nullable;
import org.siliconeconomy.iotbroker.model.sensordata.observation.ObservationType;

/**
 * Represents the <em>actual result</em> of an {@link Observation} for a specific
 * {@link ObservationType}.
 *
 * @param <T> The type of the <em>actual result</em>.
 * @author C. Hoppe
 * @author M. Grzenia
 */
public interface ObservationResult<T> {

  /**
   * Returns the <em>actual result</em>.
   * Can be {@code null}, if the <em>actual result</em> of an {@link Observation} could not be
   * determined.
   *
   * @return The <em>actual result</em>.
   */
  @Nullable
  T getResult();
}
