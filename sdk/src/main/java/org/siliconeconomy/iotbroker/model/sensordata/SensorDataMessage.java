/*
 * Copyright Open Logistics Foundation
 *
 * Licensed under the Open Logistics Foundation License 1.3.
 * For details on the licensing terms, see the LICENSE file.
 * SPDX-License-Identifier: OLFL-1.3
 */
package org.siliconeconomy.iotbroker.model.sensordata;

import org.checkerframework.checker.nullness.qual.NonNull;

import java.time.Instant;
import java.util.*;

import static java.util.Objects.requireNonNull;

/**
 * This class models (sensor) data sent by a device connected to the IoT Broker.
 * <p>
 * For every type of data (e.g. ambient temperature, ambient humidity, CPU temperature or battery
 * voltage) this class contains a {@link Datastream} which groups a collection of
 * {@link Observation}s measuring the same {@link ObservedProperty} which corresponds to a specific
 * type of data.
 * In addition, this class may contain information about the {@link Location} of the device when it
 * sent the data.
 *
 * @author C. Hoppe
 * @author M. Grzenia
 */
public class SensorDataMessage {

  @NonNull
  private final String id;
  @NonNull
  private final String originId;
  @NonNull
  private final String source;
  @NonNull
  private final String tenant;
  @NonNull
  private final Instant timestamp;
  private final Location<? extends LocationDetails<?>> location;
  @NonNull
  private final List<Datastream<? extends ObservationResult<?>>> datastreams;

  /**
   * Creates a new instance.
   * <p>
   * This constructor is especially required for deserialization of {@link SensorDataMessage}s
   * from JSON. With the Jackson framework, this can usually be achieved simply by using
   * <i>traditional</i> setter methods. However, in order to preserve the immutability of
   * instances of this class, this is not possible here.
   *
   * @param id          A unique identifier for the message.
   * @param originId    Reference to the message that originally contained the sensor data. May
   *                    be an empty string if no reference back to the original device message is
   *                    necessary or exists.
   * @param source      The source that the device represents.
   * @param tenant      A unique identifier for the device.
   * @param timestamp   The time the device sent the data.
   * @param location    The (optional) location of the device.
   * @param datastreams The list of datastreams.
   */
  public SensorDataMessage(
      @NonNull String id,
      @NonNull String originId,
      @NonNull String source,
      @NonNull String tenant,
      @NonNull Instant timestamp,
      Location<? extends LocationDetails<?>> location,
      @NonNull Collection<Datastream<? extends ObservationResult<?>>> datastreams) {
    this.id = requireNonNull(id, "id");
    this.originId = requireNonNull(originId, "originId");
    this.source = requireNonNull(source, "source");
    this.tenant = requireNonNull(tenant, "tenant");
    this.timestamp = requireNonNull(timestamp, "timestamp");
    this.location = location;
    this.datastreams = new ArrayList<>(requireNonNull(datastreams, "datastreams"));
  }

  /**
   * Returns the unique identifier of the message.
   *
   * @return The unique identifier of the message.
   */
  @NonNull
  public String getId() {
    return id;
  }

  /**
   * Returns the reference to the (device) message that originally contained the sensor data.
   * <p>
   * Optional: May be an empty string if no reference back to the original device message is
   * necessary or exists.
   *
   * @return The reference to the message that originally contained the sensor data.
   */
  @NonNull
  public String getOriginId() {
    return originId;
  }

  /**
   * Returns the source that the device represents. (E.g. as a source of messages that contain
   * data.)
   *
   * @return The source that the device represents.
   */
  @NonNull
  public String getSource() {
    return source;
  }

  /**
   * Returns the tenant of the device. (E.g. the device ID.)
   * <p>
   * The tenant is only unique in the context of the device's source.
   *
   * @return The tenant of the device.
   */
  @NonNull
  public String getTenant() {
    return tenant;
  }

  /**
   * Returns the date and time the device sent the data.
   * <p>
   * This time may be identical to the time of one or multiple {@link Observation}s in one or
   * multiple {@link Datastream}s. However, this may not be the case for observations made by the
   * device before this time.
   *
   * @return The date and time the device sent the data.
   */
  @NonNull
  public Instant getTimestamp() {
    return timestamp;
  }

  /**
   * Returns the location of the device.
   * <p>
   * Optional: May be {@code null}, if the device cannot determine its location.
   *
   * @return The location of the device.
   */
  public Location<? extends LocationDetails<?>> getLocation() {
    return location;
  }

  /**
   * Returns the list of datastreams, each grouping a collection of {@link Observation}s measuring
   * the same {@link ObservedProperty}.
   * <p>
   * This method returns an unmodifiable view of the datastreams.
   *
   * @return The list of datastreams.
   */
  @NonNull
  public List<Datastream<? extends ObservationResult<?>>> getDatastreams() {
    return Collections.unmodifiableList(datastreams);
  }

  @Override
  public boolean equals(Object o) {
    if (!(o instanceof SensorDataMessage)) {
      return false;
    }

    SensorDataMessage other = (SensorDataMessage) o;
    return Objects.equals(id, other.id)
        && Objects.equals(originId, other.originId)
        && Objects.equals(source, other.source)
        && Objects.equals(tenant, other.tenant)
        && Objects.equals(timestamp, other.timestamp)
        && Objects.equals(location, other.location)
        && Objects.equals(datastreams, other.datastreams);
  }

  @Override
  public int hashCode() {
    return Objects.hash(id, originId, source, tenant, timestamp, location, datastreams);
  }

  @Override
  public String toString() {
    return "SensorDataMessage{" +
        "id=" + id +
        ", originId=" + originId +
        ", source=" + source +
        ", tenant=" + tenant +
        ", timestamp=" + timestamp +
        ", location=" + location +
        ", datastreams=" + datastreams +
        '}';
  }
}
