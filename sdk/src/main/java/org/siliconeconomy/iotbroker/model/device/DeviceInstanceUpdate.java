/*
 * Copyright Open Logistics Foundation
 *
 * Licensed under the Open Logistics Foundation License 1.3.
 * For details on the licensing terms, see the LICENSE file.
 * SPDX-License-Identifier: OLFL-1.3
 */
package org.siliconeconomy.iotbroker.model.device;

import org.checkerframework.checker.nullness.qual.NonNull;

import java.util.Objects;

import static java.util.Objects.requireNonNull;

/**
 * Represents an update event emitted for {@link DeviceInstance}s.
 *
 * @author M. Grzenia
 * @author D. Ronnenberg
 */
public class DeviceInstanceUpdate {

  private final DeviceInstance newState;
  private final DeviceInstance oldState;
  @NonNull
  private final Type type;

  /**
   * Creates a new instance.
   *
   * @param newState The new state of the {@link DeviceInstance}.
   * @param oldState The old state of the {@link DeviceInstance}.
   * @param type     The type of the update event.
   * @throws IllegalArgumentException If either {@code newState} or {@code oldState} is
   *                                  {@code null} without {@code type} having the appropriate
   *                                  value.
   */
  public DeviceInstanceUpdate(DeviceInstance newState,
                              DeviceInstance oldState,
                              @NonNull Type type) {
    this.type = requireNonNull(type, "type");
    if (newState == null && type != Type.DELETED) {
      throw new IllegalArgumentException("newState is null but object has not been deleted.");
    }
    this.newState = newState;
    if (oldState == null && type != Type.CREATED) {
      throw new IllegalArgumentException("oldState is null but object has not been created.");
    }
    this.oldState = oldState;
  }

  /**
   * Returns the new state of the {@link DeviceInstance} for which this update event was created.
   *
   * @return The new state.
   */
  public DeviceInstance getNewState() {
    return newState;
  }

  /**
   * Returns the old state of the {@link DeviceInstance} for which this update event was created.
   *
   * @return The old state.
   */
  public DeviceInstance getOldState() {
    return oldState;
  }

  /**
   * Returns the update event's type.
   *
   * @return The update event's type.
   */
  @NonNull
  public Type getType() {
    return type;
  }

  @Override
  public boolean equals(Object o) {
    if (!(o instanceof DeviceInstanceUpdate)) {
      return false;
    }

    DeviceInstanceUpdate other = (DeviceInstanceUpdate) o;
    return Objects.equals(newState, other.newState)
        && Objects.equals(oldState, other.oldState)
        && type == other.type;
  }

  @Override
  public int hashCode() {
    return Objects.hash(newState, oldState, type);
  }

  @Override
  public String toString() {
    return "DeviceInstanceUpdate{" +
        "newState=" + newState +
        ", oldState=" + oldState +
        ", type=" + type +
        '}';
  }

  /**
   * Indicates the type of the update event.
   */
  public enum Type {
    /**
     * Indicates that a {@link DeviceInstance} has been created.
     */
    CREATED,
    /**
     * Indicates that a {@link DeviceInstance} has been modified.
     */
    MODIFIED,
    /**
     * Indicates that a {@link DeviceInstance} has been deleted.
     */
    DELETED;
  }
}
