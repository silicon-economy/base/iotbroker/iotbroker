/*
 * Copyright Open Logistics Foundation
 *
 * Licensed under the Open Logistics Foundation License 1.3.
 * For details on the licensing terms, see the LICENSE file.
 * SPDX-License-Identifier: OLFL-1.3
 */
package org.siliconeconomy.iotbroker.model.sensordata;

import org.checkerframework.checker.nullness.qual.NonNull;
import org.siliconeconomy.iotbroker.model.sensordata.location.LocationEncodingType;

/**
 * Represents the details of a {@link Location} for a specific {@link LocationEncodingType}.
 *
 * @param <T> The type of the details.
 * @author C. Hoppe
 * @author M. Grzenia
 */
public interface LocationDetails<T> {

  /**
   * Returns the location details.
   *
   * @return The location details.
   */
  @NonNull
  T getDetails();
}
