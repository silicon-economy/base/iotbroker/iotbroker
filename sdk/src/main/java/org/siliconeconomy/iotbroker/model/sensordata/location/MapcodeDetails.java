/*
 * Copyright Open Logistics Foundation
 *
 * Licensed under the Open Logistics Foundation License 1.3.
 * For details on the licensing terms, see the LICENSE file.
 * SPDX-License-Identifier: OLFL-1.3
 */
package org.siliconeconomy.iotbroker.model.sensordata.location;

import org.checkerframework.checker.nullness.qual.NonNull;
import org.siliconeconomy.iotbroker.model.sensordata.LocationDetails;
import org.siliconeconomy.iotbroker.model.sensordata.location.datatype.Mapcode;

import java.util.Objects;

import static java.util.Objects.requireNonNull;

/**
 * Describes a location using a mapcode.
 * <p>
 * This is the implementation for the {@link LocationEncodingType#MAPCODE}.
 *
 * @author C. Hoppe
 * @author M. Grzenia
 */
public class MapcodeDetails
    implements LocationDetails<Mapcode> {

  @NonNull
  private final Mapcode mapcode;

  /**
   * Creates a new instance.
   *
   * @param mapcode The mapcode.
   */
  public MapcodeDetails(@NonNull Mapcode mapcode) {
    this.mapcode = requireNonNull(mapcode, "mapcode");
  }

  /**
   * Returns the mapcode.
   *
   * @return The mapcode
   * @deprecated Use {@link #getDetails()} instead.
   */
  @Deprecated(forRemoval = true)
  @NonNull
  public Mapcode getMapcode() {
    return mapcode;
  }

  @Override
  @NonNull
  public Mapcode getDetails() {
    return mapcode;
  }

  @Override
  public boolean equals(Object o) {
    if (!(o instanceof MapcodeDetails)) {
      return false;
    }

    MapcodeDetails other = (MapcodeDetails) o;
    return Objects.equals(mapcode, other.mapcode);
  }

  @Override
  public int hashCode() {
    return Objects.hash(mapcode);
  }
}
