/*
 * Copyright Open Logistics Foundation
 *
 * Licensed under the Open Logistics Foundation License 1.3.
 * For details on the licensing terms, see the LICENSE file.
 * SPDX-License-Identifier: OLFL-1.3
 */
package org.siliconeconomy.iotbroker.model.sensordata.location.datatype;

import org.checkerframework.checker.nullness.qual.NonNull;

import java.util.Objects;

import static java.util.Objects.requireNonNull;

/**
 * Represents a mapcode.
 *
 * @author C. Hoppe
 * @author M. Grzenia
 */
public class Mapcode {

  @NonNull
  private final String territory;
  @NonNull
  private final String code;

  /**
   * Creates a new instance.
   *
   * @param territory The territory information.
   * @param code      The code.
   */
  public Mapcode(@NonNull String territory, @NonNull String code) {
    this.territory = requireNonNull(territory, "territory");
    this.code = requireNonNull(code, "code");
  }

  /**
   * Returns the territory information.
   *
   * @return The territory information.
   */
  @NonNull
  public String getTerritory() {
    return territory;
  }

  /**
   * Returns the code.
   *
   * @return The code.
   */
  @NonNull
  public String getCode() {
    return code;
  }

  @Override
  public boolean equals(Object o) {
    if (!(o instanceof Mapcode)) {
      return false;
    }

    Mapcode other = (Mapcode) o;
    return Objects.equals(territory, other.territory) && Objects.equals(code, other.code);
  }

  @Override
  public int hashCode() {
    return Objects.hash(territory, code);
  }

  @Override
  public String toString() {
    return territory + " " + code;
  }
}
