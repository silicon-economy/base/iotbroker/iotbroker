/*
 * Copyright Open Logistics Foundation
 *
 * Licensed under the Open Logistics Foundation License 1.3.
 * For details on the licensing terms, see the LICENSE file.
 * SPDX-License-Identifier: OLFL-1.3
 */
package org.siliconeconomy.iotbroker.model.sensordata;

import org.checkerframework.checker.nullness.qual.NonNull;

import java.util.ArrayList;
import java.util.Collections;
import java.util.List;
import java.util.Objects;

import static java.util.Objects.requireNonNull;

/**
 * A datastream groups a collection of {@link Observation}s measuring the same
 * {@link ObservedProperty} which corresponds to a specific type of data (e.g. ambient temperature,
 * ambient humidity, CPU temperature or battery voltage).
 * <p>
 * It references the {@link ObservationResult} class that is used for the collection of observations
 * which allows the actual result of an observation to be evaluated during serialization and
 * deserialization. In addition, a datastream defines a {@link UnitOfMeasurement} to specify the
 * measured quantity.
 *
 * @param <T> The type of observations grouped by the datastream.
 * @author C. Hoppe
 * @author M. Grzenia
 */
public class Datastream<T extends ObservationResult<?>> {

  @NonNull
  private final ObservedProperty observedProperty;
  @NonNull
  private final Class<T> observationType;
  @NonNull
  private final UnitOfMeasurement unitOfMeasurement;
  @NonNull
  private final List<Observation<T>> observations;

  /**
   * Creates a new instance.
   *
   * @param observedProperty  The property that was observed.
   * @param observationType   The type of observations grouped by the datastream.
   * @param unitOfMeasurement The unit of measurement.
   * @param observations      The list of observations.
   */
  public Datastream(@NonNull ObservedProperty observedProperty,
                    @NonNull Class<T> observationType,
                    @NonNull UnitOfMeasurement unitOfMeasurement,
                    @NonNull List<Observation<T>> observations) {
    this.observedProperty = requireNonNull(observedProperty, "observedProperty");
    this.unitOfMeasurement = requireNonNull(unitOfMeasurement, "unitOfMeasurement");
    this.observationType = requireNonNull(observationType, "observationType");
    this.observations = new ArrayList<>(requireNonNull(observations, "observations"));
  }

  /**
   * Returns the property that was observed.
   *
   * @return The property that was observed.
   */
  @NonNull
  public ObservedProperty getObservedProperty() {
    return observedProperty;
  }

  /**
   * Returns the type of {@link Observation}s grouped by the datastream.
   *
   * @return The type of {@link Observation}s grouped by the datastream.
   */
  @NonNull
  public Class<T> getObservationType() {
    return observationType;
  }

  /**
   * Returns the unit of measurement.
   * <p>
   * The unit of measurement applies to the result of an {@link Observation}.
   *
   * @return The unit of measurement.
   */
  @NonNull
  public UnitOfMeasurement getUnitOfMeasurement() {
    return unitOfMeasurement;
  }

  /**
   * Returns the list of observations, each describing the act of measuring or otherwise determining
   * the value of an {@link ObservedProperty}.
   * <p>
   * This method returns an unmodifiable view of the observations.
   *
   * @return The list of observations.
   */
  @NonNull
  public List<Observation<T>> getObservations() {
    return Collections.unmodifiableList(observations);
  }

  @Override
  public boolean equals(Object o) {
    if (!(o instanceof Datastream)) {
      return false;
    }

    Datastream<?> other = (Datastream<?>) o;
    return Objects.equals(observedProperty, other.observedProperty)
        && Objects.equals(observationType, other.observationType)
        && Objects.equals(unitOfMeasurement, other.unitOfMeasurement)
        && Objects.equals(observations, other.observations);
  }

  @Override
  public int hashCode() {
    return Objects.hash(observedProperty, observationType, unitOfMeasurement, observations);
  }
}
