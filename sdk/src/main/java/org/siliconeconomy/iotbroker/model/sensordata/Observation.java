/*
 * Copyright Open Logistics Foundation
 *
 * Licensed under the Open Logistics Foundation License 1.3.
 * For details on the licensing terms, see the LICENSE file.
 * SPDX-License-Identifier: OLFL-1.3
 */
package org.siliconeconomy.iotbroker.model.sensordata;

import org.checkerframework.checker.nullness.qual.NonNull;

import java.time.Instant;
import java.util.Collections;
import java.util.HashMap;
import java.util.Map;
import java.util.Objects;

import static java.util.Objects.requireNonNull;

/**
 * An observation describes the act of measuring or otherwise determining the value of an
 * {@link ObservedProperty}.
 * <p>
 * It defines the time the observation happened, the {@link ObservationResult} that contains the
 * actual result of the observation and a set of generic parameters that can be used to attach more
 * information to the observation. In addition, an observation may contain information about the
 * {@link Location} where the observation happened.
 * <p>
 * In case the <em>actual result</em> of the observation could not be determined and the
 * {@link ObservationResult} contains a {@code null} value, additional information can be included
 * in the observation's parameters to describe this circumstance in more detail.
 *
 * @param <T> The type of the observation result.
 * @author C. Hoppe
 * @author M. Grzenia
 */
public class Observation<T extends ObservationResult<?>> {

  @NonNull
  private final Instant timestamp;
  @NonNull
  private final T result;
  private final Location<? extends LocationDetails<?>> location;
  @NonNull
  private final Map<String, String> parameters;

  /**
   * Creates a new instance.
   *
   * @param timestamp  The time the observation happened.
   * @param result     The result of the observation.
   * @param location   The (optional) location where the observation happened.
   * @param parameters The map of generic parameters.
   */
  public Observation(@NonNull Instant timestamp,
                     @NonNull T result,
                     Location<? extends LocationDetails<?>> location,
                     @NonNull Map<String, String> parameters) {
    this.timestamp = requireNonNull(timestamp, "timestamp");
    this.result = requireNonNull(result, "result");
    this.location = location;
    this.parameters = new HashMap<>(requireNonNull(parameters, "parameters"));
  }

  /**
   * Returns the time the observation happened.
   *
   * @return The time the observation happened.
   */
  @NonNull
  public Instant getTimestamp() {
    return timestamp;
  }

  /**
   * Returns the result of the observation.
   * <p>
   * Although the result object in an {@link Observation} can not be {@code null}, the <em>actual
   * result</em> contained in the {@link ObservationResult} can be {@code null}. This might be the
   * case, if the <em>actual result</em> could not be determined.
   *
   * @return The result of the observation.
   */
  @NonNull
  public T getResult() {
    return result;
  }

  /**
   * Returns the (optional) location where the observation happened.
   * <p>
   * The location of an observation may be identical to the location of the device that made the
   * observation. If the device that made the observation cannot determine the location of the
   * observation, the value is {@code null}.
   *
   * @return The location where the observation happened.
   */
  public Location<? extends LocationDetails<?>> getLocation() {
    return location;
  }

  /**
   * Returns the observation's (generic) parameters.
   * <p>
   * These parameters can contain further information about the observation.
   * <p>
   * This method returns an unmodifiable view of the observation's parameters.
   *
   * @return The observation's parameters.
   */
  @NonNull
  public Map<String, String> getParameters() {
    return Collections.unmodifiableMap(parameters);
  }

  /**
   * Returns the value of the parameter with the given key.
   *
   * @param key The key of the parameter.
   * @return The value of the parameter.
   */
  public String getParameter(String key) {
    return parameters.get(key);
  }

  @Override
  public boolean equals(Object o) {
    if (!(o instanceof Observation)) {
      return false;
    }

    Observation<?> other = (Observation<?>) o;
    return Objects.equals(timestamp, other.timestamp)
        && Objects.equals(result, other.result)
        && Objects.equals(location, other.location)
        && Objects.equals(parameters, other.parameters);
  }

  @Override
  public int hashCode() {
    return Objects.hash(timestamp, result, location, parameters);
  }
}
