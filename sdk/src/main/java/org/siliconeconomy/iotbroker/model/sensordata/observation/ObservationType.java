/*
 * Copyright Open Logistics Foundation
 *
 * Licensed under the Open Logistics Foundation License 1.3.
 * For details on the licensing terms, see the LICENSE file.
 * SPDX-License-Identifier: OLFL-1.3
 */
package org.siliconeconomy.iotbroker.model.sensordata.observation;

import org.siliconeconomy.iotbroker.model.sensordata.Observation;
import org.siliconeconomy.iotbroker.model.sensordata.ObservationResult;

/**
 * Defines the different types of {@link Observation} and links them to the
 * {@link ObservationResult} sub types they are associated with.
 * <p>
 * During serialization and deserialization, any references to the different observation result sub
 * type classes are mapped to the names of the enum elements.
 *
 * @author C. Hoppe
 * @author M. Grzenia
 */
public enum ObservationType {

  /**
   * An observation whose result is an integer.
   */
  COUNT(CountResult.class),
  /**
   * An observation whose result is a floating-point number.
   */
  MEASUREMENT(MeasurementResult.class),
  /**
   * An observation whose result is a string.
   */
  SIMPLE(SimpleResult.class),
  /**
   * An observation whose result is a truth value.
   */
  TRUTH(TruthResult.class);

  private final Class<? extends ObservationResult<?>> observationResultClass;

  ObservationType(Class<? extends ObservationResult<?>> observationResultClass) {
    this.observationResultClass = observationResultClass;
  }

  /**
   * Returns the result type (of an observation) the observation type is associated with.
   *
   * @return The result type (of an observation) the observation type is associated with.
   */
  public Class<? extends ObservationResult<?>> getObservationResultClass() {
    return observationResultClass;
  }
}
