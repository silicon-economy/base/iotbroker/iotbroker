/*
 * Copyright Open Logistics Foundation
 *
 * Licensed under the Open Logistics Foundation License 1.3.
 * For details on the licensing terms, see the LICENSE file.
 * SPDX-License-Identifier: OLFL-1.3
 */
package org.siliconeconomy.iotbroker.model.sensordata;

import org.checkerframework.checker.nullness.qual.NonNull;

import java.util.Objects;

import static java.util.Objects.requireNonNull;

/**
 * An observed property describes the property observed in the {@link Observation}s of a
 * {@link Datastream}.
 *
 * @author C. Hoppe
 * @author M. Grzenia
 */
public class ObservedProperty {

  @NonNull
  private final String name;
  @NonNull
  private final String description;

  /**
   * Creates a new instance.
   *
   * @param name        A label for the observed property, commonly a descriptive name.
   * @param description A description about the observed property.
   */
  public ObservedProperty(@NonNull String name, @NonNull String description) {
    this.name = requireNonNull(name, "name");
    this.description = requireNonNull(description, "description");
  }

  /**
   * Returns the label for the observed property, commonly a descriptive name.
   *
   * @return The label for the observed property.
   */
  @NonNull
  public String getName() {
    return name;
  }

  /**
   * Returns the observed property's description.
   *
   * @return The observed property's description
   */
  @NonNull
  public String getDescription() {
    return description;
  }

  @Override
  public boolean equals(Object o) {
    if (!(o instanceof ObservedProperty)) {
      return false;
    }
    ObservedProperty other = (ObservedProperty) o;
    return Objects.equals(name, other.name) && Objects.equals(description, other.description);
  }

  @Override
  public int hashCode() {
    return Objects.hash(name, description);
  }
}
