/*
 * Copyright Open Logistics Foundation
 *
 * Licensed under the Open Logistics Foundation License 1.3.
 * For details on the licensing terms, see the LICENSE file.
 * SPDX-License-Identifier: OLFL-1.3
 */
package org.siliconeconomy.iotbroker.model.sensordata.location.datatype;

import java.util.Objects;

/**
 * Represents a tuple of latitude and longitude coordinates.
 *
 * @author C. Hoppe
 * @author M. Grzenia
 */
public class LatLong {

  private final double latitude;
  private final double longitude;

  /**
   * Creates a new instance.
   *
   * @param latitude  The latitude coordinate.
   * @param longitude The longitude coordinate.
   */
  public LatLong(double latitude, double longitude) {
    this.latitude = latitude;
    this.longitude = longitude;
  }

  /**
   * Returns the latitude coordinate.
   *
   * @return The latitude coordinate.
   */
  public double getLatitude() {
    return latitude;
  }

  /**
   * Returns the longitude coordinate.
   *
   * @return The longitude coordinate.
   */
  public double getLongitude() {
    return longitude;
  }

  @Override
  public boolean equals(Object o) {
    if (!(o instanceof LatLong)) {
      return false;
    }

    LatLong other = (LatLong) o;
    return Double.compare(other.latitude, latitude) == 0
        && Double.compare(other.longitude, longitude) == 0;
  }

  @Override
  public int hashCode() {
    return Objects.hash(latitude, longitude);
  }

  @Override
  public String toString() {
    return latitude + "," + longitude;
  }
}
