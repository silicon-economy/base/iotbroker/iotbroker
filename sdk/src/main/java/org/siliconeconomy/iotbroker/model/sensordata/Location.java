/*
 * Copyright Open Logistics Foundation
 *
 * Licensed under the Open Logistics Foundation License 1.3.
 * For details on the licensing terms, see the LICENSE file.
 * SPDX-License-Identifier: OLFL-1.3
 */
package org.siliconeconomy.iotbroker.model.sensordata;

import org.checkerframework.checker.nullness.qual.NonNull;

import java.util.Objects;

import static java.util.Objects.requireNonNull;

/**
 * A location describes the location of a device (see {@link SensorDataMessage}) or the location
 * where an {@link Observation} happened.
 * <p>
 * The location of an observation may be identical to the location of the device that made the
 * observation. However, this is not always the case (e.g. in the case of remote sensing) and these
 * two locations can therefore be different.
 * <p>
 * A location references the {@link LocationDetails} class that is used to describe the details of
 * the actual location, which allows the location details to be evaluated during serialization and
 * deserialization.
 *
 * @param <T> The (encoding) type of the location details.
 * @author C. Hoppe
 * @author M. Grzenia
 */
public class Location<T extends LocationDetails<?>> {

  @NonNull
  private final String name;
  @NonNull
  private final String description;
  @NonNull
  private final Class<T> encodingType;
  @NonNull
  private final T details;

  /**
   * Creates a new instance.
   *
   * @param name         A label for the location, commonly a descriptive name.
   * @param description  A description about the location.
   * @param encodingType The (encoding) type of the location details.
   * @param details      The details of the actual location.
   */
  public Location(@NonNull String name,
                  @NonNull String description,
                  @NonNull Class<T> encodingType,
                  @NonNull T details) {
    this.name = requireNonNull(name, "name");
    this.description = requireNonNull(description, "description");
    this.encodingType = requireNonNull(encodingType, "encodingType");
    this.details = requireNonNull(details, "details");
  }

  /**
   * Returns the label for the location, commonly a descriptive name.
   * <p>
   * May be an empty string.
   *
   * @return The label for the location.
   */
  public @NonNull String getName() {
    return name;
  }

  /**
   * Returns the description of the location.
   * <p>
   * May be an empty string.
   *
   * @return The description of the location.
   */
  public @NonNull String getDescription() {
    return description;
  }

  /**
   * Returns the (encoding) type of the location details.
   *
   * @return The type of the location details.
   */
  public @NonNull Class<T> getEncodingType() {
    return encodingType;
  }

  /**
   * Returns the details of the actual location.
   *
   * @return The details of the actual location.
   */
  public @NonNull T getDetails() {
    return details;
  }

  @Override
  public boolean equals(Object o) {
    if (!(o instanceof Location)) {
      return false;
    }

    Location<?> other = (Location<?>) o;
    return Objects.equals(name, other.name)
        && Objects.equals(description, other.description)
        && Objects.equals(encodingType, other.encodingType)
        && Objects.equals(details, other.details);
  }

  @Override
  public int hashCode() {
    return Objects.hash(name, description, encodingType, details);
  }
}
