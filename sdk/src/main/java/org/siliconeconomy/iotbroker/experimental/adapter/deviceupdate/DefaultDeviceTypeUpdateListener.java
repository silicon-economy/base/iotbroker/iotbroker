/*
 * Copyright Open Logistics Foundation
 *
 * Licensed under the Open Logistics Foundation License 1.3.
 * For details on the licensing terms, see the LICENSE file.
 * SPDX-License-Identifier: OLFL-1.3
 */
package org.siliconeconomy.iotbroker.experimental.adapter.deviceupdate;

import org.checkerframework.checker.nullness.qual.NonNull;
import org.siliconeconomy.iotbroker.model.device.DeviceTypeUpdate;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import java.util.ArrayList;
import java.util.List;

import static java.util.Objects.requireNonNull;

/**
 * An implementation of {@link DeviceTypeUpdateListener} that only handles {@link DeviceTypeUpdate}s
 * that pass a {@link DeviceTypeUpdateFilter}.
 *
 * @author M. Grzenia
 */
public class DefaultDeviceTypeUpdateListener
    implements DeviceTypeUpdateListener {

  private static final Logger LOG = LoggerFactory.getLogger(DefaultDeviceTypeUpdateListener.class);
  /**
   * The handlers to be notified when {@link DeviceTypeUpdate}s are received.
   * <p>
   * Use a list to provide control over the order in which the handlers are notified.
   */
  private final List<DeviceTypeUpdateHandler> updateHandlers = new ArrayList<>();
  private final DeviceTypeUpdateFilter deviceTypeUpdateFilter;

  /**
   * Creates a new instance.
   *
   * @param deviceTypeUpdateFilter The filter to use.
   */
  public DefaultDeviceTypeUpdateListener(@NonNull DeviceTypeUpdateFilter deviceTypeUpdateFilter) {
    this.deviceTypeUpdateFilter = requireNonNull(deviceTypeUpdateFilter, "deviceTypeUpdateFilter");
  }

  @Override
  public void addDeviceTypeUpdateHandler(
      @NonNull DeviceTypeUpdateHandler deviceTypeUpdateHandler) {
    requireNonNull(deviceTypeUpdateHandler, "deviceTypeUpdateHandler");

    if (updateHandlers.contains(deviceTypeUpdateHandler)) {
      // Avoid adding the same handler more than once.
      return;
    }

    updateHandlers.add(deviceTypeUpdateHandler);
  }

  @Override
  public void removeDeviceTypeUpdateHandler(
      @NonNull DeviceTypeUpdateHandler deviceTypeUpdateHandler) {
    requireNonNull(deviceTypeUpdateHandler, "deviceTypeUpdateHandler");

    updateHandlers.remove(deviceTypeUpdateHandler);
  }

  @Override
  public void onDeviceTypeUpdate(@NonNull DeviceTypeUpdate deviceTypeUpdate) {
    requireNonNull(deviceTypeUpdate, "deviceTypeUpdate");

    LOG.debug("Received device type update: {}", deviceTypeUpdate);
    if (!deviceTypeUpdateFilter.test(deviceTypeUpdate)) {
      LOG.debug("Ignoring filtered update...");
      return;
    }

    switch (deviceTypeUpdate.getType()) {
      case CREATED:
        updateHandlers.forEach(
            handler -> handler.onDeviceTypeCreated(deviceTypeUpdate.getNewState())
        );
        break;
      case MODIFIED:
        updateHandlers.forEach(
            handler -> handler.onDeviceTypeModified(deviceTypeUpdate.getOldState(),
                                                    deviceTypeUpdate.getNewState())
        );
        break;
      case DELETED:
        updateHandlers.forEach(
            handler -> handler.onDeviceTypeDeleted(deviceTypeUpdate.getOldState())
        );
        break;
      default:
        throw new IllegalArgumentException(String.format("Unhandled update type: %s",
                                                         deviceTypeUpdate.getType()));
    }
  }
}
