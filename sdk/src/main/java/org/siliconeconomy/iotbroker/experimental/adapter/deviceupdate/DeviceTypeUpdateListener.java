/*
 * Copyright Open Logistics Foundation
 *
 * Licensed under the Open Logistics Foundation License 1.3.
 * For details on the licensing terms, see the LICENSE file.
 * SPDX-License-Identifier: OLFL-1.3
 */
package org.siliconeconomy.iotbroker.experimental.adapter.deviceupdate;

import org.checkerframework.checker.nullness.qual.NonNull;
import org.siliconeconomy.iotbroker.amqp.DeviceManagementExchangeConfiguration;
import org.siliconeconomy.iotbroker.model.device.DeviceTypeUpdate;

/**
 * Defines methods that a component listening for {@link DeviceTypeUpdate}s must implement.
 * <p>
 * {@link DeviceTypeUpdate} events are published via the IoT Broker's AMQP broker. For information
 * on the configuration of the corresponding AMQP exchange, refer to
 * {@link DeviceManagementExchangeConfiguration}.
 *
 * @author M. Grzenia
 */
public interface DeviceTypeUpdateListener {

  /**
   * Adds/registers a handler for {@link DeviceTypeUpdate}s.
   * <p>
   * {@link DeviceTypeUpdate}s received by the component implementing this interface should be
   * forwarded to registered {@link DeviceTypeUpdateHandler}s.
   *
   * @param deviceTypeUpdateHandler The handler to add/register.
   * @see #onDeviceTypeUpdate(DeviceTypeUpdate)
   */
  void addDeviceTypeUpdateHandler(@NonNull DeviceTypeUpdateHandler deviceTypeUpdateHandler);

  /**
   * Removes the handler for {@link DeviceTypeUpdate}s.
   *
   * @param deviceTypeUpdateHandler The handler to remove.
   */
  void removeDeviceTypeUpdateHandler(@NonNull DeviceTypeUpdateHandler deviceTypeUpdateHandler);

  /**
   * Invoked when a new {@link DeviceTypeUpdate} has been received.
   * <p>
   * Depending on the {@link DeviceTypeUpdate#getType() type} of the received event, the registered
   * {@link DeviceTypeUpdateHandler}s should be notified accordingly.
   *
   * @param deviceTypeUpdate The received {@link DeviceTypeUpdate}.
   */
  void onDeviceTypeUpdate(@NonNull DeviceTypeUpdate deviceTypeUpdate);
}
