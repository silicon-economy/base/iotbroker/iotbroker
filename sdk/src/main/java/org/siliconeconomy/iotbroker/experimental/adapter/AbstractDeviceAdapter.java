/*
 * Copyright Open Logistics Foundation
 *
 * Licensed under the Open Logistics Foundation License 1.3.
 * For details on the licensing terms, see the LICENSE file.
 * SPDX-License-Identifier: OLFL-1.3
 */
package org.siliconeconomy.iotbroker.experimental.adapter;

import org.checkerframework.checker.nullness.qual.NonNull;
import org.checkerframework.checker.nullness.qual.Nullable;
import org.siliconeconomy.iotbroker.experimental.adapter.deviceupdate.DeviceInstanceUpdateHandler;
import org.siliconeconomy.iotbroker.experimental.adapter.deviceupdate.DeviceInstanceUpdateListener;
import org.siliconeconomy.iotbroker.experimental.adapter.deviceupdate.DeviceTypeUpdateHandler;
import org.siliconeconomy.iotbroker.experimental.adapter.deviceupdate.DeviceTypeUpdateListener;
import org.siliconeconomy.iotbroker.model.device.DeviceInstance;
import org.siliconeconomy.iotbroker.model.device.DeviceInstanceUpdate;
import org.siliconeconomy.iotbroker.model.device.DeviceType;
import org.siliconeconomy.iotbroker.model.device.DeviceTypeUpdate;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import java.util.Objects;
import java.util.Optional;
import java.util.Set;

import static java.util.Objects.requireNonNull;

/**
 * A base class for {@link DeviceAdapter}s.
 * <p>
 * This class provides some additional methods that help with development of custom
 * {@link DeviceAdapter}s. It defines some fundamental processes that, in the context of the IoT
 * Broker, are expected to be handled by {@link DeviceAdapter} implementations. These include:
 * <ul>
 *   <li>Registration of a {@link DeviceType} upon adapter initialization.</li>
 *   <li>Opening a {@link DeviceType} specific communication channel to allow communication with
 *   the corresponding devices.</li>
 *   <li>Handling updates to the adapter's {@link DeviceType} and corresponding
 *   {@link DeviceInstance}s that are published via the IoT Broker's AMQP broker through
 *   {@link DeviceTypeUpdate} and {@link DeviceInstanceUpdate} messages. Especially updates to the
 *   {@link DeviceType#isEnabled() device type's 'enabled' state} and the
 *   {@link DeviceInstance#isEnabled() 'enabled' state of device instances} are expected to be
 *   respected by a {@link DeviceAdapter}.<br>
 *   <b>Implementation remark:</b> For this, this class implements the
 *   {@link DeviceTypeUpdateHandler} and {@link DeviceInstanceUpdateHandler} interfaces. For a
 *   {@link DeviceAdapter} the callback methods defined by these interfaces are expected to be
 *   invoked only on relevant updates. In the case of a {@link DeviceAdapter}, an update is
 *   considered relevant if it corresponds to the adapter's {@link DeviceType} (i.e. regarding its
 *   {@code source} and {@code deviceTypeIdentifier}).
 *   </li>
 * </ul>
 *
 * @author M. Grzenia
 */
public abstract class AbstractDeviceAdapter
    implements DeviceAdapter,
               DeviceTypeUpdateHandler,
               DeviceInstanceUpdateHandler {

  private static final Logger LOG = LoggerFactory.getLogger(AbstractDeviceAdapter.class);
  private final AdapterAttributes adapterAttributes;
  private final DeviceTypeUpdateListener deviceTypeUpdateListener;
  private final DeviceInstanceUpdateListener deviceInstanceUpdateListener;
  /**
   * Indicates whether the adapter is initialized.
   */
  private boolean initialized = false;
  /**
   * Indicates whether the adapter is enabled.
   */
  private boolean enabled = false;

  /**
   * Creates a new instance.
   *
   * @param adapterAttributes            Provides information about the adapter's attributes.
   * @param deviceTypeUpdateListener     The listener for {@link DeviceTypeUpdate}s to register
   *                                     with.
   * @param deviceInstanceUpdateListener The listener for {@link DeviceInstanceUpdate}s to register
   *                                     with.
   */
  protected AbstractDeviceAdapter(
      @NonNull AdapterAttributes adapterAttributes,
      @NonNull DeviceTypeUpdateListener deviceTypeUpdateListener,
      @NonNull DeviceInstanceUpdateListener deviceInstanceUpdateListener) {
    this.adapterAttributes = requireNonNull(adapterAttributes, "adapterAttributes");
    this.deviceTypeUpdateListener = requireNonNull(deviceTypeUpdateListener,
                                                   "deviceTypeUpdateListener");
    this.deviceInstanceUpdateListener = requireNonNull(deviceInstanceUpdateListener,
                                                       "deviceInstanceUpdateListener");
  }

  @Override
  public void initialize() {
    if (isInitialized()) {
      return;
    }

    LOG.info("Initializing adapter...");
    if (!isDeviceTypeRegistered()) {
      LOG.info("Device type is not yet registered. Registering device type...");
      registerDeviceType();
    } else {
      // An appropriate device type seems to be already registered. Check whether it is provided by
      // (i.e. already associated with) this adapter.
      if (!isDeviceTypeProvidedByAdapter()) {
        // If a device type is registered but not yet provided by this adapter, then the device type
        // was probably registered by another adapter. Update the device type registration to also
        // regard this adapter.
        LOG.info("Associating adapter with already existing device type...");
        updateDeviceTypeProvidedByAdapter();
      }
    }
    LOG.info("Device type registration complete.");

    // The adapter's 'enabled' state corresponds to the 'enabled' state of its device type. For
    // enabled device types, enable the adapter right away.
    if (isDeviceTypeEnabled()) {
      LOG.info("Device type is already enabled. Enabling adapter...");
      enable();
    }

    deviceTypeUpdateListener.addDeviceTypeUpdateHandler(this);
    deviceInstanceUpdateListener.addDeviceInstanceUpdateHandler(this);

    initialized = true;
  }

  @Override
  public void terminate() {
    if (!isInitialized()) {
      return;
    }

    LOG.info("Terminating adapter...");
    deviceInstanceUpdateListener.removeDeviceInstanceUpdateHandler(this);
    deviceTypeUpdateListener.removeDeviceTypeUpdateHandler(this);

    disable();

    initialized = false;
  }

  @Override
  public boolean isInitialized() {
    return initialized;
  }

  @Override
  public synchronized void enable() {
    if (isEnabled()) {
      return;
    }

    if (!isDeviceTypeEnabled()) {
      LOG.debug("Device type is not enabled. Not enabling adapter.");
      return;
    }

    LOG.info("Enabling adapter...");
    openCommunicationChannel();

    enabled = true;
  }

  @Override
  public synchronized void disable() {
    if (!isEnabled()) {
      return;
    }

    if (isDeviceTypeEnabled()) {
      LOG.debug("Device type is not disabled. Not disabling adapter.");
      return;
    }

    LOG.info("Disabling adapter...");
    closeCommunicationChannel();

    enabled = false;
  }

  @Override
  public synchronized boolean isEnabled() {
    return enabled;
  }

  @Override
  @NonNull
  public AdapterAttributes getAdapterAttributes() {
    return adapterAttributes;
  }

  @Override
  public boolean isDeviceTypeRegistered() {
    return fetchAdapterDeviceType().isPresent();
  }

  @Override
  public boolean isDeviceTypeProvidedByAdapter() {
    return fetchAdapterDeviceType()
        .map(DeviceType::getProvidedBy)
        .filter(this::containsAdapterIdentifier)
        .isPresent();
  }

  @Override
  public boolean isDeviceTypeEnabled() {
    return fetchAdapterDeviceType()
        .filter(DeviceType::isEnabled)
        .isPresent();
  }

  @Override
  public boolean isDeviceInstanceRegistered(@NonNull String tenant) {
    requireNonNull(tenant, "tenant");

    return fetchAdapterDeviceInstance(tenant).isPresent();
  }

  @Override
  public boolean isDeviceInstanceAssociatedWithAdapterDeviceType(@NonNull String tenant) {
    requireNonNull(tenant, "tenant");

    return fetchAdapterDeviceInstance(tenant)
        .filter(this::isAssociatedWithAdapterDeviceType)
        .isPresent();
  }

  @Override
  public boolean isDeviceInstanceEnabled(@NonNull String tenant) {
    requireNonNull(tenant, "tenant");

    return fetchAdapterDeviceInstance(tenant)
        .filter(DeviceInstance::isEnabled)
        .isPresent();
  }

  @Override
  public boolean isDeviceInstanceAutoRegistrationEnabled() {
    return fetchAdapterDeviceType()
        .filter(DeviceType::isAutoRegisterDeviceInstances)
        .isPresent();
  }

  @Override
  public boolean attemptDeviceInstanceAutoRegistration(@NonNull String tenant,
                                                       @Nullable Object registrationContext) {
    requireNonNull(tenant, "tenant");

    if (isDeviceInstanceRegistered(tenant)) {
      if (!isDeviceInstanceAssociatedWithAdapterDeviceType(tenant)) {
        LOG.info("Device instance '{}' is already registered but it is not associated with the " +
                     "adapter's device type. Considering auto-registration unsuccessful.", tenant);
        return false;
      }
      LOG.debug("Device instance '{}' is already registered. Doing nothing.", tenant);
      return true;
    }

    if (isDeviceInstanceAutoRegistrationEnabled()) {
      LOG.debug("Auto-registration for device instances is enabled." +
                    " Registering device instance '{}'.", tenant);
      registerDeviceInstance(tenant, registrationContext);
      return true;
    }

    LOG.debug("Auto-registration for device instance '{}' was not successful.", tenant);
    return false;
  }

  /**
   * {@inheritDoc}
   * <p>
   * When this method is invoked, the adapter's device type has been registered. (This includes
   * the registration the adapter performs during its initialization as well as a manually
   * registered device type.) This implementation ensures that the adapter is enabled if its (now
   * registered) device type is enabled. This is particularly relevant in the case of a manually
   * registered device type, as it implies that the adapter's device type was previously manually
   * deleted.
   * <p>
   * <b>Overriding methods are expected to call this implementation, too.</b>
   *
   * @see #onDeviceTypeDeleted(DeviceType)
   */
  @Override
  public void onDeviceTypeCreated(@NonNull DeviceType newDeviceType) {
    requireNonNull(newDeviceType, "newDeviceType");

    // As a precaution, ensure that the adapter's device type is actually registered now.
    if (isDeviceTypeRegistered() && isDeviceTypeEnabled()) {
      enable();
    }
  }

  /**
   * {@inheritDoc}
   * <p>
   * This implementation ensures that the adapter is enabled/disabled if it's device type has been
   * enabled/disabled.
   * <p>
   * <b>Overriding methods are expected to call this implementation, too.</b>
   */
  @Override
  public void onDeviceTypeModified(@NonNull DeviceType oldDeviceType,
                                   @NonNull DeviceType newDeviceType) {
    requireNonNull(oldDeviceType, "oldDeviceType");
    requireNonNull(newDeviceType, "newDeviceType");

    // Consider updates to the 'enabled' state of the adapter's device type.
    if (oldDeviceType.isEnabled() != newDeviceType.isEnabled()) {
      if (isDeviceTypeEnabled()) {
        LOG.debug("Device type has been enabled. Enabling adapter...");
        enable();
      } else {
        LOG.debug("Device type has been disabled. Disabling adapter...");
        disable();
      }
    }
  }

  /**
   * {@inheritDoc}
   * <p>
   * When this method is invoked, the adapter's device type has been deleted. Without an appropriate
   * device type the adapter cannot function properly. Instead of terminating the adapter (which
   * might be an option in some situations), this implementation just disables it. This way the
   * adapter is still listening for {@link DeviceTypeUpdate} messages and a newly registered (and
   * appropriately configured) {@link DeviceType} would allow the adapter to be enabled again
   * without having to restart it.
   * <p>
   * <b>Overriding methods are expected to call this implementation, too.</b>
   *
   * @see #onDeviceTypeCreated(DeviceType)
   */
  @Override
  public void onDeviceTypeDeleted(@NonNull DeviceType oldDeviceType) {
    requireNonNull(oldDeviceType, "oldDeviceType");

    // As a precaution, ensure that the adapter's device type is actually no longer registered.
    if (!isDeviceTypeRegistered()) {
      LOG.warn("The adapter's device type has been deleted. The adapter will be disabled. " +
                   "To allow the adapter to be enabled again, register an appropriately " +
                   "configured device type or restart the adapter.");
      disable();
    }
  }

  /**
   * {@inheritDoc}
   * <p>
   * <b>Overriding methods are expected to call this implementation, too.</b>
   */
  @Override
  public void onDeviceInstanceCreated(@NonNull DeviceInstance newDeviceInstance) {
    // This method does not yet contain an implementation. Including it here (anyway) allows custom
    // adapters to omit the implementation of this method if it is not strictly necessary.
  }

  /**
   * {@inheritDoc}
   * <p>
   * <b>Overriding methods are expected to call this implementation, too.</b>
   */
  @Override
  public void onDeviceInstanceModified(@NonNull DeviceInstance oldDeviceInstance,
                                       @NonNull DeviceInstance newDeviceInstance) {
    // This method does not yet contain an implementation. Including it here (anyway) allows custom
    // adapters to omit the implementation of this method if it is not strictly necessary.
  }

  /**
   * {@inheritDoc}
   * <p>
   * <b>Overriding methods are expected to call this implementation, too.</b>
   */
  @Override
  public void onDeviceInstanceDeleted(@NonNull DeviceInstance oldDeviceInstance) {
    // This method does not yet contain an implementation. Including it here (anyway) allows custom
    // adapters to omit the implementation of this method if it is not strictly necessary.
  }

  /**
   * Opens the communication channel for devices to connect to/with the adapter.
   * <p>
   * Details about the communication, e.g. which protocol is used to exchange data with the devices,
   * or whether the adapter acts as a server or as a client, are specific to the adapter's device
   * type and thus specific to the implementation of an adapter.
   */
  protected abstract void openCommunicationChannel();

  /**
   * Closes the communication channel.
   */
  protected abstract void closeCommunicationChannel();

  /**
   * Returns the adapter's {@link DeviceType} as it is registered with the IoT Broker.
   * <p>
   * Information provided by {@link #getAdapterAttributes()} should be used to query for the
   * corresponding {@link DeviceType}.
   *
   * @return An {@link Optional} containing the adapter's {@link DeviceType} or
   * {@link Optional#empty()}, if it is not registered yet.
   */
  @NonNull
  protected abstract Optional<DeviceType> fetchAdapterDeviceType();

  /**
   * Returns the {@link DeviceInstance} with the given {@code tenant} (that is associated with the
   * adapter's {@link DeviceType}) as it is registered with the IoT Broker.
   * <p>
   * Information provided by {@link #getAdapterAttributes()} should be used to query for the
   * corresponding {@link DeviceInstance}. (This mainly concerns the {@code source} of the
   * {@link DeviceInstance} as it is not provided to the method and expected to be implicitly
   * derived from the adapter's {@link DeviceType}.)
   *
   * @param tenant The tenant.
   * @return An {@link Optional} containing the {@link DeviceInstance} or {@link Optional#empty()},
   * if it is not registered yet.
   */
  @NonNull
  protected abstract Optional<DeviceInstance> fetchAdapterDeviceInstance(String tenant);

  private boolean isAssociatedWithAdapterDeviceType(DeviceInstance deviceInstance) {
    return Objects.equals(deviceInstance.getSource(),
                          getAdapterAttributes().getAdapterDeviceSource())
        && Objects.equals(deviceInstance.getDeviceTypeIdentifier(),
                          getAdapterAttributes().getAdapterDeviceTypeIdentifier());
  }

  private boolean containsAdapterIdentifier(Set<String> adapterIdentifiers) {
    return adapterIdentifiers.contains(getAdapterAttributes().getAdapterIdentifier());
  }
}
