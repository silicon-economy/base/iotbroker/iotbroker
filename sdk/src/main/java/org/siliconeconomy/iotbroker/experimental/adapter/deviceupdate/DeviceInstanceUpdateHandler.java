/*
 * Copyright Open Logistics Foundation
 *
 * Licensed under the Open Logistics Foundation License 1.3.
 * For details on the licensing terms, see the LICENSE file.
 * SPDX-License-Identifier: OLFL-1.3
 */
package org.siliconeconomy.iotbroker.experimental.adapter.deviceupdate;

import org.checkerframework.checker.nullness.qual.NonNull;
import org.siliconeconomy.iotbroker.model.device.DeviceInstance;
import org.siliconeconomy.iotbroker.model.device.DeviceInstanceUpdate;

/**
 * Defines methods that a component interested in updates in the context of the registration of
 * {@link DeviceInstance}s must implement.
 *
 * @author M. Grzenia
 */
public interface DeviceInstanceUpdateHandler {

  /**
   * Invoked when a {@link DeviceInstance} has been created.
   * <p>
   * Corresponds to {@link DeviceInstanceUpdate.Type#CREATED}.
   *
   * @param newDeviceInstance The new {@link DeviceInstance} state.
   */
  void onDeviceInstanceCreated(@NonNull DeviceInstance newDeviceInstance);

  /**
   * Invoked when a {@link DeviceInstance} has been modified.
   * <p>
   * Corresponds to {@link DeviceInstanceUpdate.Type#MODIFIED}.
   *
   * @param oldDeviceInstance The old {@link DeviceInstance} state.
   * @param newDeviceInstance The new {@link DeviceInstance} state.
   */
  void onDeviceInstanceModified(@NonNull DeviceInstance oldDeviceInstance,
                                @NonNull DeviceInstance newDeviceInstance);

  /**
   * Invoked when a {@link DeviceInstance} has been deleted.
   * <p>
   * Corresponds to {@link DeviceInstanceUpdate.Type#DELETED}.
   *
   * @param oldDeviceInstance The old {@link DeviceInstance} state.
   */
  void onDeviceInstanceDeleted(@NonNull DeviceInstance oldDeviceInstance);
}
