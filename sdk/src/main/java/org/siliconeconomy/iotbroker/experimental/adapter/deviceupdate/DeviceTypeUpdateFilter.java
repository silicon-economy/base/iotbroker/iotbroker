/*
 * Copyright Open Logistics Foundation
 *
 * Licensed under the Open Logistics Foundation License 1.3.
 * For details on the licensing terms, see the LICENSE file.
 * SPDX-License-Identifier: OLFL-1.3
 */
package org.siliconeconomy.iotbroker.experimental.adapter.deviceupdate;

import org.siliconeconomy.iotbroker.model.device.DeviceTypeUpdate;

import java.util.function.Predicate;

/**
 * A filter for {@link DeviceTypeUpdate}s.
 *
 * @author M. Grzenia
 */
public interface DeviceTypeUpdateFilter
    extends Predicate<DeviceTypeUpdate> {
}
