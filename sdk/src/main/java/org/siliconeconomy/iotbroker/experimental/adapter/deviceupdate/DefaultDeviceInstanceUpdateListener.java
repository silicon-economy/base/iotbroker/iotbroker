/*
 * Copyright Open Logistics Foundation
 *
 * Licensed under the Open Logistics Foundation License 1.3.
 * For details on the licensing terms, see the LICENSE file.
 * SPDX-License-Identifier: OLFL-1.3
 */
package org.siliconeconomy.iotbroker.experimental.adapter.deviceupdate;

import org.checkerframework.checker.nullness.qual.NonNull;
import org.siliconeconomy.iotbroker.model.device.DeviceInstanceUpdate;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import java.util.ArrayList;
import java.util.List;

import static java.util.Objects.requireNonNull;

/**
 * An implementation of {@link DeviceInstanceUpdateListener} that only handles
 * {@link DeviceInstanceUpdate}s that pass a {@link DeviceInstanceUpdateFilter}.
 *
 * @author M. Grzenia
 */
public class DefaultDeviceInstanceUpdateListener
    implements DeviceInstanceUpdateListener {

  private static final Logger LOG
      = LoggerFactory.getLogger(DefaultDeviceInstanceUpdateListener.class);
  /**
   * The handlers to be notified when {@link DeviceInstanceUpdate}s are received.
   * <p>
   * Use a list to provide control over the order in which the handlers are notified.
   */
  private final List<DeviceInstanceUpdateHandler> updateHandlers = new ArrayList<>();

  private final DeviceInstanceUpdateFilter deviceInstanceUpdateFilter;

  /**
   * Creates a new instance.
   *
   * @param deviceInstanceUpdateFilter The filter to use.
   */
  public DefaultDeviceInstanceUpdateListener(
      @NonNull DeviceInstanceUpdateFilter deviceInstanceUpdateFilter) {
    this.deviceInstanceUpdateFilter = requireNonNull(deviceInstanceUpdateFilter,
                                                     "deviceInstanceUpdateFilter");
  }

  @Override
  public void addDeviceInstanceUpdateHandler(
      @NonNull DeviceInstanceUpdateHandler deviceInstanceUpdateHandler) {
    requireNonNull(deviceInstanceUpdateHandler, "deviceInstanceUpdateHandler");

    if (updateHandlers.contains(deviceInstanceUpdateHandler)) {
      // Avoid adding the same handler more than once.
      return;
    }

    updateHandlers.add(deviceInstanceUpdateHandler);
  }

  @Override
  public void removeDeviceInstanceUpdateHandler(
      @NonNull DeviceInstanceUpdateHandler deviceInstanceUpdateHandler) {
    requireNonNull(deviceInstanceUpdateHandler, "deviceInstanceUpdateHandler");

    updateHandlers.remove(deviceInstanceUpdateHandler);
  }

  @Override
  public void onDeviceInstanceUpdate(@NonNull DeviceInstanceUpdate deviceInstanceUpdate) {
    requireNonNull(deviceInstanceUpdate, "deviceInstanceUpdate");

    LOG.debug("Received device instance update: {}", deviceInstanceUpdate);
    if (!deviceInstanceUpdateFilter.test(deviceInstanceUpdate)) {
      LOG.debug("Ignoring filtered update...");
      return;
    }

    switch (deviceInstanceUpdate.getType()) {
      case CREATED:
        updateHandlers.forEach(
            handler -> handler.onDeviceInstanceCreated(deviceInstanceUpdate.getNewState())
        );
        break;
      case MODIFIED:
        updateHandlers.forEach(
            handler -> handler.onDeviceInstanceModified(deviceInstanceUpdate.getOldState(),
                                                        deviceInstanceUpdate.getNewState()));
        break;
      case DELETED:
        updateHandlers.forEach(
            handler -> handler.onDeviceInstanceDeleted(deviceInstanceUpdate.getOldState())
        );
        break;
      default:
        throw new IllegalArgumentException(String.format("Unhandled update type: %s",
                                                         deviceInstanceUpdate.getType()));
    }
  }
}
