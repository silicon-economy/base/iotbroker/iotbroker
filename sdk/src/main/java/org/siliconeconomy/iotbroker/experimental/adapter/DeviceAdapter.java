/*
 * Copyright Open Logistics Foundation
 *
 * Licensed under the Open Logistics Foundation License 1.3.
 * For details on the licensing terms, see the LICENSE file.
 * SPDX-License-Identifier: OLFL-1.3
 */
package org.siliconeconomy.iotbroker.experimental.adapter;

import org.checkerframework.checker.nullness.qual.NonNull;
import org.checkerframework.checker.nullness.qual.Nullable;
import org.siliconeconomy.iotbroker.amqp.SensorDataExchangeConfiguration;
import org.siliconeconomy.iotbroker.model.device.DeviceInstance;
import org.siliconeconomy.iotbroker.model.device.DeviceInstanceUpdate;
import org.siliconeconomy.iotbroker.model.device.DeviceType;
import org.siliconeconomy.iotbroker.model.device.DeviceTypeUpdate;
import org.siliconeconomy.iotbroker.model.sensordata.SensorDataMessage;

/**
 * Defines methods that an adapter integrating a specific type of (IoT) device into the IoT Broker
 * must implement.
 * <p>
 * The following aspects are part of the scope of an adapter:
 * <ul>
 *   <li><b>Device communication</b> - Providing a way to communicate with the devices the adapter
 *   provides an IoT Broker integration for. Details about the communication (e.g., which protocol
 *   is used to exchange data with the devices) are specific to the type of device and therefore
 *   also specific to the implementation of an adapter.</li>
 *   <li><b>{@link DeviceType} and {@link DeviceInstance} registration</b> - Ensuring that an
 *   appropriate {@link DeviceType} is registered that corresponds to type of device that the
 *   adapter provides an IoT Broker integration for. Additionally, ensuring that an appropriate
 *   {@link DeviceInstance} is registered for every device that connects to the adapter (depending
 *   on the configuration of the {@link DeviceType}. Only if an appropriate {@link DeviceType} and
 *   {@link DeviceInstance} is registered for a particular device, the data received form that
 *   device is expected to be processed by an adapter. Note that an adapter is expected to provide
 *   an integration for (i.e. be associated with) <em>a single</em> {@link DeviceType}.
 *   <li><b>{@link SensorDataMessage} handling</b> - Where applicable, extracting information from
 *   data received from devices and mapping it to {@link SensorDataMessage} instances. These
 *   messages are then expected to be published via AMQP to a dedicated AMQP exchange (see
 *   {@link SensorDataExchangeConfiguration}).</li>
 * </ul>
 * For the sake of simplicity, this documentation refers to "the {@link DeviceType} the adapter
 * provides an IoT Broker integration for" or "the {@link DeviceType} the adapter is associated
 * with" as "the adapter's {@link DeviceType}".
 * <p>
 * Since an adapter is expected to provide an integration for <em>a single</em> {@link DeviceType},
 * methods defined in this interface that refer to {@link DeviceInstance}s (e.g.
 * {@link DeviceAdapter#isDeviceInstanceRegistered(String)}) are expected to implicitly derive the
 * corresponding {@link DeviceInstance}'s {@code source} from the adapter's {@link DeviceType}.
 * Therefore, no {@code source} needs to be provided to these methods.
 *
 * @author M. Grzenia
 */
public interface DeviceAdapter {

  /**
   * Initializes the adapter.
   * <p>
   * Upon initialization, the adapter is expected to
   * <ul>
   *   <li>register the {@link DeviceType} it provides an IoT Broker integration for.</li>
   *   <li>be enabled in case its {@link DeviceType} is enabled.</li>
   * </ul>
   * With the initialization process complete, the adapter should be configured to receive
   * {@link DeviceTypeUpdate} and {@link DeviceInstanceUpdate} messages so that it can react to
   * changes to its {@link DeviceType} and corresponding {@link DeviceInstance}s accordingly.
   */
  void initialize();

  /**
   * Terminates the adapter.
   */
  void terminate();

  /**
   * Checks whether the adapter is initialized or not.
   *
   * @return {@code true}, if the adapter is initialized, otherwise {@code false}.
   */
  boolean isInitialized();

  /**
   * Enables the adapter.
   * <p>
   * An enabled adapter may start communication with actual devices.
   */
  void enable();

  /**
   * Disables the adapter.
   * <p>
   * A disabled adapter may stop communication with any devices.
   */
  void disable();

  /**
   * Checks whether the adapter is enabled or not.
   * <p>
   * The adapter's {@code enabled} state corresponds to the {@code enabled} state of its
   * {@link DeviceType}. Thus, for an enabled {@link DeviceType}, the adapter is expected to be
   * enabled as well. Likewise, for a disabled {@link DeviceType}, the adapter is expected to be
   * disabled.
   *
   * @return {@code true}, if the adapter is enabled, otherwise {@code false}.
   */
  boolean isEnabled();

  /**
   * Returns information about the adapter's attributes.
   *
   * @return Information about the adapter's attributes.
   */
  @NonNull
  AdapterAttributes getAdapterAttributes();

  /**
   * Checks whether the adapter's {@link DeviceType} is registered with the IoT Broker.
   *
   * @return {@code true}, if the {@link DeviceType} is registered, otherwise {@code false}.
   */
  boolean isDeviceTypeRegistered();

  /**
   * Registers the adapter's {@link DeviceType} with the IoT Broker.
   */
  void registerDeviceType();

  /**
   * Checks whether adapter's {@link DeviceType} is provided by the adapter itself. (Or in other
   * words, checks whether the adapter is associated with its {@link DeviceType}.)
   * <p>
   * For example, an adapter might not (yet) be associated with its {@link DeviceType}, if a
   * different adapter has registered it.
   * <p>
   * Implementations of this method should consider {@code DeviceType#getProvidedBy()}.
   *
   * @return {@code true}, if the {@link DeviceType} is provided by the adapter, otherwise
   * {@code false}.
   */
  boolean isDeviceTypeProvidedByAdapter();

  /**
   * Updates the adapter's {@link DeviceType}, so that after calling this method, the
   * {@link DeviceType} <em>is</em> provided by the adapter.
   * <p>
   * Implementations of this method should consider {@code DeviceType#getProvidedBy()}.
   * Additionally, adapter implementations <b>should not</b> modify any entries other than their
   * own.
   */
  void updateDeviceTypeProvidedByAdapter();

  /**
   * Checks whether the adapter's {@link DeviceType} is enabled.
   * <p>
   * Implementations of this method should consider {@code DeviceType#isEnabled()}.
   *
   * @return {@code true}, if the {@link DeviceType} is enabled, otherwise {@code false}.
   */
  boolean isDeviceTypeEnabled();

  /**
   * Checks whether a {@link DeviceInstance} with the given {@code tenant} is registered with the
   * IoT Broker.
   *
   * @param tenant The tenant to check.
   * @return {@code true}, if a {@link DeviceInstance} with the given {@code tenant} is registered,
   * otherwise {@code false}.
   */
  boolean isDeviceInstanceRegistered(@NonNull String tenant);

  /**
   * Checks whether the {@link DeviceInstance} with the given {@code tenant} is associated with
   * the adapter's {@link DeviceType} considering the
   * {@code DeviceInstance#getDeviceTypeIdentifier() device instance's device type identifier}.
   *
   * @param tenant The tenant to check.
   * @return {@code true}, if a {@link DeviceInstance} with the given {@code tenant} is associated
   * with the adapter's {@link DeviceType}, otherwise {@code false}.
   */
  boolean isDeviceInstanceAssociatedWithAdapterDeviceType(@NonNull String tenant);

  /**
   * Registers a {@link DeviceInstance} for the given {@code tenant} with the IoT Broker.
   * <p>
   * When registering a {@link DeviceInstance}, this method can be provided with an optional
   * {@code registrationContext} object that contains contextual information about the
   * {@link DeviceInstance} to be registered. Such contextual information can be used, for example,
   * to populate the attributes of a {@link DeviceInstance}.
   *
   * @param tenant              The tenant to register a {@link DeviceInstance} for.
   * @param registrationContext An object that contains contextual information about the
   *                            {@link DeviceInstance} to be registered.
   */
  void registerDeviceInstance(@NonNull String tenant, @Nullable Object registrationContext);

  /**
   * Checks whether the {@link DeviceInstance} with the given {@code tenant} is enabled.
   * <p>
   * Implementations of this method should consider {@code DeviceInstance#isEnabled()}.
   *
   * @param tenant The tenant to check.
   * @return {@code true}, if the {@link DeviceInstance} is enabled, otherwise {@code false}.
   */
  boolean isDeviceInstanceEnabled(@NonNull String tenant);

  /**
   * Checks whether the auto-registration for {@link DeviceInstance}s of the adapter's
   * {@link DeviceType} is enabled.
   * <p>
   * Implementations of this method should consider
   * {@code DeviceType#isAutoRegisterDeviceInstances()}.
   *
   * @return {@code true}, if the auto-registration is enabled, otherwise {@code false}.
   */
  boolean isDeviceInstanceAutoRegistrationEnabled();

  /**
   * Attempts to auto-register a {@link DeviceInstance} with the given {@code tenant}.
   * <p>
   * Implementations of this method should consider
   * {@code DeviceType#isAutoRegisterDeviceInstances()}.
   *
   * @param tenant              The tenant to register a {@link DeviceInstance} for.
   * @param registrationContext An object that contains contextual information about the
   *                            {@link DeviceInstance} to be registered.
   * @return {@code true}, if the auto-registration was successful, otherwise {@code false}.
   * @see #registerDeviceInstance(String, Object)
   */
  boolean attemptDeviceInstanceAutoRegistration(@NonNull String tenant,
                                                @Nullable Object registrationContext);
}
