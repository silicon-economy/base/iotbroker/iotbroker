/*
 * Copyright Open Logistics Foundation
 *
 * Licensed under the Open Logistics Foundation License 1.3.
 * For details on the licensing terms, see the LICENSE file.
 * SPDX-License-Identifier: OLFL-1.3
 */
package org.siliconeconomy.iotbroker.experimental.adapter.deviceupdate;

import org.checkerframework.checker.nullness.qual.NonNull;
import org.siliconeconomy.iotbroker.model.device.DeviceType;
import org.siliconeconomy.iotbroker.model.device.DeviceTypeUpdate;

/**
 * Defines methods that a component interested in updates in the context of the registration of
 * {@link DeviceType}s must implement.
 *
 * @author M. Grzenia
 */
public interface DeviceTypeUpdateHandler {

  /**
   * Invoked when a {@link DeviceType} has been created.
   * <p>
   * Corresponds to {@link DeviceTypeUpdate.Type#CREATED}.
   *
   * @param newDeviceType The new {@link DeviceType} state.
   */
  void onDeviceTypeCreated(@NonNull DeviceType newDeviceType);

  /**
   * Invoked when a {@link DeviceType} has been modified.
   * <p>
   * Corresponds to {@link DeviceTypeUpdate.Type#MODIFIED}.
   *
   * @param oldDeviceType The old {@link DeviceType} state.
   * @param newDeviceType The new {@link DeviceType} state.
   */
  void onDeviceTypeModified(@NonNull DeviceType oldDeviceType, @NonNull DeviceType newDeviceType);

  /**
   * Invoked when a {@link DeviceType} has been deleted.
   * <p>
   * Corresponds to {@link DeviceTypeUpdate.Type#DELETED}.
   *
   * @param oldDeviceType The old {@link DeviceType} state.
   */
  void onDeviceTypeDeleted(@NonNull DeviceType oldDeviceType);
}
