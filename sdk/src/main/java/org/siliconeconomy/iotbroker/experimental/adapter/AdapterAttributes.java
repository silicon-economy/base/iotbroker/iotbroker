/*
 * Copyright Open Logistics Foundation
 *
 * Licensed under the Open Logistics Foundation License 1.3.
 * For details on the licensing terms, see the LICENSE file.
 * SPDX-License-Identifier: OLFL-1.3
 */
package org.siliconeconomy.iotbroker.experimental.adapter;

import org.checkerframework.checker.nullness.qual.NonNull;
import org.siliconeconomy.iotbroker.model.device.DeviceInstance;
import org.siliconeconomy.iotbroker.model.device.DeviceType;

/**
 * Defines methods that provide information about a device adapter's attributes.
 *
 * @author M. Grzenia
 */
public interface AdapterAttributes {

  /**
   * Returns the identifier used by the adapter.
   * <p>
   * The adapter identifier is used primarily in the context of {@link DeviceType} registration.
   * With the {@link DeviceType#getProvidedBy()} method it indicates whether a {@link DeviceAdapter}
   * provides an IoT Broker integration for a specific {@link DeviceType}.
   *
   * @return The adapter identifier.
   */
  @NonNull
  String getAdapterIdentifier();

  /**
   * Returns the device source (that {@link DeviceType}s and {@link DeviceInstance}s are associated
   * with) for which the adapter provides an IoT Broker integration.
   *
   * @return The device source.
   */
  @NonNull
  String getAdapterDeviceSource();

  /**
   * Returns the {@link DeviceType} identifier for which the adapter provides an IoT Broker
   * integration.
   *
   * @return The {@link DeviceType} identifier.
   */
  @NonNull
  String getAdapterDeviceTypeIdentifier();
}
