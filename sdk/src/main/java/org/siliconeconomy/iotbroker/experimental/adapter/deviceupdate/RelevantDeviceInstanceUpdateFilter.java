/*
 * Copyright Open Logistics Foundation
 *
 * Licensed under the Open Logistics Foundation License 1.3.
 * For details on the licensing terms, see the LICENSE file.
 * SPDX-License-Identifier: OLFL-1.3
 */
package org.siliconeconomy.iotbroker.experimental.adapter.deviceupdate;

import org.checkerframework.checker.nullness.qual.NonNull;
import org.siliconeconomy.iotbroker.experimental.adapter.DeviceAdapter;
import org.siliconeconomy.iotbroker.model.device.DeviceInstance;
import org.siliconeconomy.iotbroker.model.device.DeviceInstanceUpdate;
import org.siliconeconomy.iotbroker.model.device.DeviceType;

import java.util.Objects;

import static java.util.Objects.requireNonNull;

/**
 * A {@link DeviceInstanceUpdateFilter} which only filters events relevant to a
 * {@link DeviceAdapter}.
 * <p>
 * A {@link DeviceInstanceUpdate} is considered relevant to a {@link DeviceAdapter} if the event is
 * related to a {@link DeviceInstance} that corresponds to the adapter's {@link DeviceType} (i.e.
 * regarding its {@code source} and {@code deviceTypeIdentifier}).
 *
 * @author M. Grzenia
 */
public class RelevantDeviceInstanceUpdateFilter
    implements DeviceInstanceUpdateFilter {

  private final String adapterDeviceSource;
  private final String adapterDeviceTypeIdentifier;

  /**
   * Creates a new instance.
   *
   * @param adapterDeviceSource         The {@link DeviceAdapter}'s device {@code source}.
   * @param adapterDeviceTypeIdentifier The {@link DeviceAdapter}'s device
   *                                    {@code deviceTypeIdentifier}.
   */
  public RelevantDeviceInstanceUpdateFilter(@NonNull String adapterDeviceSource,
                                            @NonNull String adapterDeviceTypeIdentifier) {
    this.adapterDeviceSource = requireNonNull(adapterDeviceSource, "adapterDeviceSource");
    this.adapterDeviceTypeIdentifier = requireNonNull(adapterDeviceTypeIdentifier,
                                                      "adapterDeviceTypeIdentifier");
  }

  @Override
  public boolean test(@NonNull DeviceInstanceUpdate deviceInstanceUpdate) {
    requireNonNull(deviceInstanceUpdate, "deviceInstanceUpdate");
    return isRelevantUpdate(deviceInstanceUpdate);
  }

  private boolean isRelevantUpdate(DeviceInstanceUpdate deviceInstanceUpdate) {
    return isRelevantSource(deviceInstanceUpdate)
        && isRelevantDeviceTypeIdentifierUpdate(deviceInstanceUpdate);
  }

  private boolean isRelevantSource(DeviceInstanceUpdate deviceInstanceUpdate) {
    String deviceInstanceSource = deviceInstanceUpdate.getNewState() != null
        ? deviceInstanceUpdate.getNewState().getSource()
        : deviceInstanceUpdate.getOldState().getSource();
    return Objects.equals(deviceInstanceSource, adapterDeviceSource);
  }

  private boolean isRelevantDeviceTypeIdentifierUpdate(DeviceInstanceUpdate deviceInstanceUpdate) {
    switch (deviceInstanceUpdate.getType()) {
      case CREATED:
        return Objects.equals(deviceInstanceUpdate.getNewState().getDeviceTypeIdentifier(),
                              adapterDeviceTypeIdentifier);
      case MODIFIED:
        // If the 'deviceTypeIdentifier' changes and the device instance is now associated or no
        // longer associated with the adapter's device type, the adapter might want to know this in
        // order to consider or disregard the corresponding device instance in the future.
        return Objects.equals(deviceInstanceUpdate.getOldState().getDeviceTypeIdentifier(),
                              adapterDeviceTypeIdentifier)
            || Objects.equals(deviceInstanceUpdate.getNewState().getDeviceTypeIdentifier(),
                              adapterDeviceTypeIdentifier);
      case DELETED:
        return Objects.equals(deviceInstanceUpdate.getOldState().getDeviceTypeIdentifier(),
                              adapterDeviceTypeIdentifier);
      default:
        throw new IllegalArgumentException(String.format("Unhandled update type: %s",
                                                         deviceInstanceUpdate.getType()));
    }
  }
}
