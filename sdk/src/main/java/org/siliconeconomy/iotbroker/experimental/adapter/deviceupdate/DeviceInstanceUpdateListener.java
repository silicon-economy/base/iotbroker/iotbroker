/*
 * Copyright Open Logistics Foundation
 *
 * Licensed under the Open Logistics Foundation License 1.3.
 * For details on the licensing terms, see the LICENSE file.
 * SPDX-License-Identifier: OLFL-1.3
 */
package org.siliconeconomy.iotbroker.experimental.adapter.deviceupdate;

import org.checkerframework.checker.nullness.qual.NonNull;
import org.siliconeconomy.iotbroker.amqp.DeviceManagementExchangeConfiguration;
import org.siliconeconomy.iotbroker.model.device.DeviceInstanceUpdate;

/**
 * Defines methods that a component listening for {@link DeviceInstanceUpdate}s must implement.
 * <p>
 * {@link DeviceInstanceUpdate} events are published via the IoT Broker's AMQP broker. For
 * information on the configuration of the corresponding AMQP exchange, refer to
 * {@link DeviceManagementExchangeConfiguration}.
 *
 * @author M. Grzenia
 */
public interface DeviceInstanceUpdateListener {

  /**
   * Adds/registers a handler for {@link DeviceInstanceUpdate}s.
   * <p>
   * {@link DeviceInstanceUpdate}s received by the component implementing this interface should be
   * forwarded to registered {@link DeviceInstanceUpdateHandler}s.
   *
   * @param deviceInstanceUpdateHandler The handler to add/register.
   * @see #onDeviceInstanceUpdate(DeviceInstanceUpdate)
   */
  void addDeviceInstanceUpdateHandler(
      @NonNull DeviceInstanceUpdateHandler deviceInstanceUpdateHandler);

  /**
   * Removes the handler for {@link DeviceInstanceUpdate}s.
   *
   * @param deviceInstanceUpdateHandler The handler to remove.
   */
  void removeDeviceInstanceUpdateHandler(
      @NonNull DeviceInstanceUpdateHandler deviceInstanceUpdateHandler);

  /**
   * Invoked when a new {@link DeviceInstanceUpdate} has been received.
   * <p>
   * Depending on the {@link DeviceInstanceUpdate#getType() type} of the received event, the
   * registered {@link DeviceInstanceUpdateHandler}s should be notified accordingly.
   *
   * @param deviceInstanceUpdate The received {@link DeviceInstanceUpdate}.
   */
  void onDeviceInstanceUpdate(@NonNull DeviceInstanceUpdate deviceInstanceUpdate);
}
