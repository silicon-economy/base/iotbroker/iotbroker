/*
 * Copyright Open Logistics Foundation
 *
 * Licensed under the Open Logistics Foundation License 1.3.
 * For details on the licensing terms, see the LICENSE file.
 * SPDX-License-Identifier: OLFL-1.3
 */
package org.siliconeconomy.iotbroker.experimental.adapter.deviceupdate;

import org.checkerframework.checker.nullness.qual.NonNull;
import org.siliconeconomy.iotbroker.experimental.adapter.DeviceAdapter;
import org.siliconeconomy.iotbroker.model.device.DeviceType;
import org.siliconeconomy.iotbroker.model.device.DeviceTypeUpdate;

import java.util.Objects;

import static java.util.Objects.requireNonNull;

/**
 * A {@link DeviceTypeUpdateFilter} which only filters events relevant to a {@link DeviceAdapter}.
 * <p>
 * A {@link DeviceTypeUpdate} is considered relevant to a {@link DeviceAdapter} if the event is
 * related to a {@link DeviceType} that corresponds to the adapter's {@link DeviceType} (i.e.
 * regarding its {@code source} and {@code deviceTypeIdentifier}).
 *
 * @author M. Grzenia
 */
public class RelevantDeviceTypeUpdateFilter
    implements DeviceTypeUpdateFilter {

  private final String adapterDeviceSource;
  private final String adapterDeviceTypeIdentifier;

  /**
   * Creates a new instance.
   *
   * @param adapterDeviceSource         The {@link DeviceAdapter}'s device {@code source}.
   * @param adapterDeviceTypeIdentifier The {@link DeviceAdapter}'s device
   *                                    {@code deviceTypeIdentifier}.
   */
  public RelevantDeviceTypeUpdateFilter(@NonNull String adapterDeviceSource,
                                        @NonNull String adapterDeviceTypeIdentifier) {
    this.adapterDeviceSource = requireNonNull(adapterDeviceSource, "adapterDeviceSource");
    this.adapterDeviceTypeIdentifier = requireNonNull(adapterDeviceTypeIdentifier,
                                                      "adapterDeviceTypeIdentifier");
  }

  @Override
  public boolean test(@NonNull DeviceTypeUpdate deviceTypeUpdate) {
    requireNonNull(deviceTypeUpdate, "deviceTypeUpdate");
    return isRelevantUpdate(deviceTypeUpdate);
  }

  private boolean isRelevantUpdate(DeviceTypeUpdate deviceTypeUpdate) {
    return isRelevantSource(deviceTypeUpdate) && isRelevantIdentifier(deviceTypeUpdate);
  }

  private boolean isRelevantSource(DeviceTypeUpdate deviceTypeUpdate) {
    String deviceTypeSource = deviceTypeUpdate.getNewState() != null
        ? deviceTypeUpdate.getNewState().getSource()
        : deviceTypeUpdate.getOldState().getSource();
    return Objects.equals(deviceTypeSource, adapterDeviceSource);
  }

  private boolean isRelevantIdentifier(DeviceTypeUpdate deviceTypeUpdate) {
    String deviceTypeIdentifier = deviceTypeUpdate.getNewState() != null
        ? deviceTypeUpdate.getNewState().getIdentifier()
        : deviceTypeUpdate.getOldState().getIdentifier();
    return Objects.equals(deviceTypeIdentifier, adapterDeviceTypeIdentifier);
  }
}
