/*
 * Copyright Open Logistics Foundation
 *
 * Licensed under the Open Logistics Foundation License 1.3.
 * For details on the licensing terms, see the LICENSE file.
 * SPDX-License-Identifier: OLFL-1.3
 */
package org.siliconeconomy.iotbroker.couchdb;

import org.ektorp.impl.ObjectMapperFactory;
import org.ektorp.impl.StdCouchDbConnector;
import org.ektorp.impl.StdObjectMapperFactory;

/**
 * The standard implementation of {@link PartitionedCouchDbConnector}.
 * <p>
 * This implementation merely extends {@link StdCouchDbConnector} without providing any additional
 * functionality. The only difference is that an instance of {@link PartitionedCouchDbInstance} is
 * required.
 *
 * @author M. Grzenia
 */
public class StdPartitionedCouchDbConnector
    extends StdCouchDbConnector
    implements PartitionedCouchDbConnector {

  public StdPartitionedCouchDbConnector(String databaseName,
                                        PartitionedCouchDbInstance dbInstance) {
    this(databaseName, dbInstance, new StdObjectMapperFactory());
  }

  public StdPartitionedCouchDbConnector(String databaseName,
                                        PartitionedCouchDbInstance dbi,
                                        ObjectMapperFactory om) {
    super(databaseName, dbi, om);
  }
}
