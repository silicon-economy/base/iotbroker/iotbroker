/*
 * Copyright Open Logistics Foundation
 *
 * Licensed under the Open Logistics Foundation License 1.3.
 * For details on the licensing terms, see the LICENSE file.
 * SPDX-License-Identifier: OLFL-1.3
 */
package org.siliconeconomy.iotbroker.couchdb;

import org.ektorp.CouchDbInstance;

/**
 * Marker interface for a {@link CouchDbInstance} with support for partitioned databases.
 *
 * @author M. Grzenia
 */
public interface PartitionedCouchDbInstance
    extends CouchDbInstance {
}
