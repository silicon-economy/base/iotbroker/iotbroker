/*
 * Copyright Open Logistics Foundation
 *
 * Licensed under the Open Logistics Foundation License 1.3.
 * For details on the licensing terms, see the LICENSE file.
 * SPDX-License-Identifier: OLFL-1.3
 */
package org.siliconeconomy.iotbroker.couchdb;

import org.ektorp.support.DesignDocument;
import org.ektorp.support.StdDesignDocumentFactory;

import java.util.HashMap;
import java.util.Map;

/**
 * Factory to create global design documents for {@link GlobalRepositorySupport}.
 * <p>
 * This factory will create global design documents.
 *
 * @author D. Ronnenberg
 */
class StdGlobalDesignDocumentFactory
    extends StdDesignDocumentFactory
    implements PartitionedDesignDocumentFactory {
  public final GlobalViewGenerator globalViewGenerator;

  public StdGlobalDesignDocumentFactory() {
    this.globalViewGenerator = new GlobalViewGenerator();
  }

  public DesignDocument generateGlobalFrom(Object metaDataSource) {
    var ddoc = newDesignDocumentInstance();

    // Create views.
    Map<String, DesignDocument.View> views = globalViewGenerator.generateGlobalViews(metaDataSource);
    views.forEach(ddoc::addView);

    // we set "partitioned" in options for this ddoc to false
    var options = new HashMap<String, Object>();
    options.put("partitioned", false);
    ddoc.setAnonymous("options", options);

    return ddoc;
  }
}
