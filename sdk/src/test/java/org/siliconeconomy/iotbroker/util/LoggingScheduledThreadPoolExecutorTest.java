/*
 * Copyright Open Logistics Foundation
 *
 * Licensed under the Open Logistics Foundation License 1.3.
 * For details on the licensing terms, see the LICENSE file.
 * SPDX-License-Identifier: OLFL-1.3
 */
package org.siliconeconomy.iotbroker.util;

import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.slf4j.Logger;

import java.util.concurrent.CancellationException;
import java.util.concurrent.Future;
import java.util.concurrent.TimeUnit;

import static org.mockito.ArgumentMatchers.any;
import static org.mockito.ArgumentMatchers.eq;
import static org.mockito.Mockito.*;

/**
 * Tests cases for {@link LoggingScheduledThreadPoolExecutor}.
 *
 * @author M. Grzenia
 */
class LoggingScheduledThreadPoolExecutorTest {

  /**
   * Class under test.
   */
  private LoggingScheduledThreadPoolExecutor executor;
  /**
   * Test dependencies.
   */
  private Logger log;

  @BeforeEach
  void setUp() {
    log = mock(Logger.class);
    executor = new LoggingScheduledThreadPoolExecutor(1, log);
  }

  @Test
  void shouldLogExecutionThrownInTask()
      throws InterruptedException {
    // Act
    executor.submit(taskThatThrowsRuntimeException());

    // Wait for execution of the task.
    executor.shutdown();
    executor.awaitTermination(5, TimeUnit.SECONDS);

    // Verify
    verify(log).warn(eq("Unhandled exception in executed task"), any(RuntimeException.class));
    verifyNoMoreInteractions(log);
  }

  @Test
  void shouldLogCancellationException()
      throws InterruptedException {
    // Act
    Future<?> future = executor.submit(longRunningTask());

    // Wait for execution of the task, but forcibly cancel it "halfway".
    executor.shutdown();
    future.cancel(true);
    executor.awaitTermination(5, TimeUnit.SECONDS);

    // Verify
    verify(log).debug(eq("Task was cancelled"), any(CancellationException.class));
    verifyNoMoreInteractions(log);
  }

  private Runnable taskThatThrowsRuntimeException() {
    return () -> {
      throw new NullPointerException();
    };
  }

  private Runnable longRunningTask() {
    return () -> {
      try {
        Thread.currentThread().wait(10000);
      } catch (InterruptedException e) {
        // In the case of this test, there is nothing to do here.
      }
    };
  }
}

