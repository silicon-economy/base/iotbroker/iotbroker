/*
 * Copyright Open Logistics Foundation
 *
 * Licensed under the Open Logistics Foundation License 1.3.
 * For details on the licensing terms, see the LICENSE file.
 * SPDX-License-Identifier: OLFL-1.3
 */
package org.siliconeconomy.iotbroker.couchdb;

import com.fasterxml.jackson.annotation.JsonInclude;
import com.fasterxml.jackson.annotation.JsonProperty;

import java.util.Objects;

/**
 * Entity for testing.
 */
@JsonInclude(JsonInclude.Include.NON_NULL)
class TestEntity {
  /**
   * The unique ID of the entity in the database.
   */
  @JsonProperty("_id")
  private String id;
  /**
   * The revision of the entity in the database.
   */
  @JsonProperty("_rev")
  private String revision;

  private String payload;

  public TestEntity(String id, String revision, String payload) {
    this.id = id;
    this.revision = revision;
    this.payload = payload;
  }

  public TestEntity() {
  }

  public String getId() {
    return this.id;
  }

  public String getRevision() {
    return this.revision;
  }

  public String getPayload() {
    return this.payload;
  }

  public void setId(String id) {
    this.id = id;
  }

  public void setRevision(String revision) {
    this.revision = revision;
  }

  public void setPayload(String payload) {
    this.payload = payload;
  }

  @Override
  public boolean equals(Object o) {
    if (!(o instanceof TestEntity)) {
      return false;
    }

    TestEntity other = (TestEntity) o;
    return Objects.equals(id, other.id)
        && Objects.equals(revision, other.revision)
        && Objects.equals(payload, other.payload);
  }

  @Override
  public int hashCode() {
    return Objects.hash(id, revision, payload);
  }

  @Override
  public String toString() {
    return "TestEntity{" +
        "id=" + id +
        ", revision=" + revision +
        ", payload=" + payload +
        '}';
  }
}
