/*
 * Copyright Open Logistics Foundation
 *
 * Licensed under the Open Logistics Foundation License 1.3.
 * For details on the licensing terms, see the LICENSE file.
 * SPDX-License-Identifier: OLFL-1.3
 */
package org.siliconeconomy.iotbroker.amqp;

import org.junit.jupiter.api.Test;

import static org.junit.jupiter.api.Assertions.assertEquals;

/**
 * Test cases for {@link DeviceMessageExchangeConfiguration}.
 *
 * @author M. Grzenia
 */
class DeviceMessageExchangeConfigurationTest {

  @Test
  void formatDeviceMessageRoutingKey() {
    assertEquals("deviceTypeIdentifier.source.tenant",
                 DeviceMessageExchangeConfiguration
                     .formatDeviceMessageRoutingKey("deviceTypeIdentifier",
                                                    "source",
                                                    "tenant")
    );
  }
}
