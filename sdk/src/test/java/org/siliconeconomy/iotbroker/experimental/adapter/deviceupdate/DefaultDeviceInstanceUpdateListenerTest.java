/*
 * Copyright Open Logistics Foundation
 *
 * Licensed under the Open Logistics Foundation License 1.3.
 * For details on the licensing terms, see the LICENSE file.
 * SPDX-License-Identifier: OLFL-1.3
 */
package org.siliconeconomy.iotbroker.experimental.adapter.deviceupdate;

import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.siliconeconomy.iotbroker.model.device.DeviceInstance;
import org.siliconeconomy.iotbroker.model.device.DeviceInstanceUpdate;

import java.time.Instant;

import static org.mockito.Mockito.*;
import static org.siliconeconomy.iotbroker.model.device.DeviceInstanceUpdate.Type.*;

/**
 * Tests for {@link DefaultDeviceInstanceUpdateListener}.
 *
 * @author M. Grzenia
 */
class DefaultDeviceInstanceUpdateListenerTest {

  /**
   * Class under test.
   */
  private DeviceInstanceUpdateListener listener;
  /**
   * Test dependencies.
   */
  private DeviceInstanceUpdateHandler handler;

  @BeforeEach
  void setUp() {
    handler = mock(DeviceInstanceUpdateHandler.class);
  }

  @Test
  void onDeviceInstanceUpdate_ignoresFilteredUpdates() {
    // Arrange
    // Configure the listener to ignore all updates.
    listener = new DefaultDeviceInstanceUpdateListener(update -> false);
    listener.addDeviceInstanceUpdateHandler(handler);
    DeviceInstanceUpdate deviceInstanceUpdate = new DeviceInstanceUpdate(dummyDeviceInstance(),
                                                                         null,
                                                                         CREATED);
    // Act
    listener.onDeviceInstanceUpdate(deviceInstanceUpdate);

    // Verify
    verifyNoMoreInteractions(handler);
  }

  @Test
  void onDeviceInstanceUpdate_whenCreatedUpdate_thenNotifiesRegisteredHandlers() {
    // Arrange
    listener = new DefaultDeviceInstanceUpdateListener(update -> true);
    listener.addDeviceInstanceUpdateHandler(handler);
    DeviceInstance newDeviceInstance = dummyDeviceInstance();
    DeviceInstanceUpdate deviceInstanceUpdate = new DeviceInstanceUpdate(newDeviceInstance,
                                                                         null,
                                                                         CREATED);

    // Act
    listener.onDeviceInstanceUpdate(deviceInstanceUpdate);

    // Verify
    verify(handler).onDeviceInstanceCreated(newDeviceInstance);
  }

  @Test
  void onDeviceInstanceUpdate_whenModifiedUpdate_thenNotifiesRegisteredHandlers() {
    // Arrange
    listener = new DefaultDeviceInstanceUpdateListener(update -> true);
    listener.addDeviceInstanceUpdateHandler(handler);
    DeviceInstance newDeviceInstance = dummyDeviceInstance();
    DeviceInstance oldDeviceInstance = dummyDeviceInstance();
    DeviceInstanceUpdate deviceInstanceUpdate = new DeviceInstanceUpdate(newDeviceInstance,
                                                                         oldDeviceInstance,
                                                                         MODIFIED);
    // Act
    listener.onDeviceInstanceUpdate(deviceInstanceUpdate);

    // Verify
    verify(handler).onDeviceInstanceModified(oldDeviceInstance, newDeviceInstance);
  }

  @Test
  void onDeviceInstanceUpdate_whenDeletedUpdate_thenNotifiesRegisteredHandlers() {
    // Arrange
    listener = new DefaultDeviceInstanceUpdateListener(update -> true);
    listener.addDeviceInstanceUpdateHandler(handler);
    DeviceInstance oldDeviceInstance = dummyDeviceInstance();
    DeviceInstanceUpdate deviceInstanceUpdate = new DeviceInstanceUpdate(null,
                                                                         oldDeviceInstance,
                                                                         DELETED);

    // Act
    listener.onDeviceInstanceUpdate(deviceInstanceUpdate);

    // Verify
    verify(handler).onDeviceInstanceDeleted(oldDeviceInstance);
  }

  private DeviceInstance dummyDeviceInstance() {
    return new DeviceInstance("", "", "", "", Instant.EPOCH, Instant.EPOCH, false, "", "", "");
  }
}
