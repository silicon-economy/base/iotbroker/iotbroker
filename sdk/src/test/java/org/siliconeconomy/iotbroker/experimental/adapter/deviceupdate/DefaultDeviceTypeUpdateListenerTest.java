/*
 * Copyright Open Logistics Foundation
 *
 * Licensed under the Open Logistics Foundation License 1.3.
 * For details on the licensing terms, see the LICENSE file.
 * SPDX-License-Identifier: OLFL-1.3
 */
package org.siliconeconomy.iotbroker.experimental.adapter.deviceupdate;

import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.siliconeconomy.iotbroker.model.device.DeviceType;
import org.siliconeconomy.iotbroker.model.device.DeviceTypeUpdate;

import java.util.Set;

import static org.mockito.Mockito.*;
import static org.siliconeconomy.iotbroker.model.device.DeviceTypeUpdate.Type.*;

/**
 * Tests for {@link DefaultDeviceTypeUpdateListener}.
 *
 * @author M. Grzenia
 */
class DefaultDeviceTypeUpdateListenerTest {

  /**
   * Class under test.
   */
  private DeviceTypeUpdateListener listener;
  /**
   * Test dependencies.
   */
  private DeviceTypeUpdateHandler handler;

  @BeforeEach
  void setUp() {
    handler = mock(DeviceTypeUpdateHandler.class);
  }

  @Test
  void onDeviceTypeUpdate_ignoresFilteredUpdates() {
    // Arrange
    // Configure the listener to ignore all updates.
    listener = new DefaultDeviceTypeUpdateListener(update -> false);
    listener.addDeviceTypeUpdateHandler(handler);
    DeviceTypeUpdate deviceTypeUpdate = new DeviceTypeUpdate(dummyDeviceType(),
                                                             null,
                                                             CREATED);
    // Act
    listener.onDeviceTypeUpdate(deviceTypeUpdate);

    // Verify
    verifyNoMoreInteractions(handler);
  }

  @Test
  void onDeviceTypeUpdate_whenCreatedUpdate_thenNotifiesRegisteredHandlers() {
    // Arrange
    listener = new DefaultDeviceTypeUpdateListener(update -> true);
    listener.addDeviceTypeUpdateHandler(handler);
    DeviceType newDeviceType = dummyDeviceType();
    DeviceTypeUpdate deviceTypeUpdate = new DeviceTypeUpdate(newDeviceType,
                                                             null,
                                                             CREATED);

    // Act
    listener.onDeviceTypeUpdate(deviceTypeUpdate);

    // Verify
    verify(handler).onDeviceTypeCreated(newDeviceType);
  }

  @Test
  void onDeviceTypeUpdate_whenModifiedUpdate_thenNotifiesRegisteredHandlers() {
    // Arrange
    listener = new DefaultDeviceTypeUpdateListener(update -> true);
    listener.addDeviceTypeUpdateHandler(handler);
    DeviceType newDeviceType = dummyDeviceType();
    DeviceType oldDeviceType = dummyDeviceType();
    DeviceTypeUpdate deviceTypeUpdate = new DeviceTypeUpdate(newDeviceType,
                                                             oldDeviceType,
                                                             MODIFIED);
    // Act
    listener.onDeviceTypeUpdate(deviceTypeUpdate);

    // Verify
    verify(handler).onDeviceTypeModified(oldDeviceType, newDeviceType);
  }

  @Test
  void onDeviceTypeUpdate_whenDeletedUpdate_thenNotifiesRegisteredHandlers() {
    // Arrange
    listener = new DefaultDeviceTypeUpdateListener(update -> true);
    listener.addDeviceTypeUpdateHandler(handler);
    DeviceType oldDeviceType = dummyDeviceType();
    DeviceTypeUpdate deviceTypeUpdate = new DeviceTypeUpdate(null,
                                                             oldDeviceType,
                                                             DELETED);

    // Act
    listener.onDeviceTypeUpdate(deviceTypeUpdate);

    // Verify
    verify(handler).onDeviceTypeDeleted(oldDeviceType);
  }

  private DeviceType dummyDeviceType() {
    return new DeviceType("", "", "", Set.of(), "", false, false, false);
  }
}
