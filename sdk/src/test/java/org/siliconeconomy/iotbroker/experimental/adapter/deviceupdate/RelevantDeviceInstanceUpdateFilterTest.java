/*
 * Copyright Open Logistics Foundation
 *
 * Licensed under the Open Logistics Foundation License 1.3.
 * For details on the licensing terms, see the LICENSE file.
 * SPDX-License-Identifier: OLFL-1.3
 */
package org.siliconeconomy.iotbroker.experimental.adapter.deviceupdate;

import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.siliconeconomy.iotbroker.model.device.DeviceInstance;
import org.siliconeconomy.iotbroker.model.device.DeviceInstanceUpdate;

import java.time.Instant;

import static org.assertj.core.api.Assertions.assertThat;
import static org.siliconeconomy.iotbroker.model.device.DeviceInstanceUpdate.Type.MODIFIED;

/**
 * Tests for {@link RelevantDeviceInstanceUpdateFilter}.
 *
 * @author M. Grzenia
 */
class RelevantDeviceInstanceUpdateFilterTest {

  /**
   * Class under test.
   */
  private RelevantDeviceInstanceUpdateFilter filter;
  private static final String ADAPTER_SOURCE = "relevant-source";
  private static final String ADAPTER_DEVICE_TYPE = "relevant-type";

  @BeforeEach
  void setUp() {
    filter = new RelevantDeviceInstanceUpdateFilter(ADAPTER_SOURCE, ADAPTER_DEVICE_TYPE);
  }

  @Test
  void evaluatesMismatchingDeviceSourceAsFalse() {
    // Arrange
    DeviceInstance newDeviceInstance = dummyDeviceInstance()
        .withSource("irrelevant-source")
        .withDeviceTypeIdentifier(ADAPTER_DEVICE_TYPE);
    DeviceInstance oldDeviceInstance = dummyDeviceInstance()
        .withSource("irrelevant-source")
        .withDeviceTypeIdentifier(ADAPTER_DEVICE_TYPE);
    DeviceInstanceUpdate deviceInstanceUpdate = new DeviceInstanceUpdate(newDeviceInstance,
                                                                         oldDeviceInstance,
                                                                         MODIFIED);
    // Act & Assert
    assertThat(filter.test(deviceInstanceUpdate)).isFalse();
  }

  @Test
  void evaluatesMismatchingDeviceTypeAsFalse() {
    // Arrange
    DeviceInstance newDeviceInstance = dummyDeviceInstance()
        .withSource(ADAPTER_SOURCE)
        .withDeviceTypeIdentifier("irrelevant-device-type");
    DeviceInstance oldDeviceInstance = dummyDeviceInstance()
        .withSource(ADAPTER_SOURCE)
        .withDeviceTypeIdentifier("irrelevant-device-type");
    DeviceInstanceUpdate deviceInstanceUpdate = new DeviceInstanceUpdate(newDeviceInstance,
                                                                         oldDeviceInstance,
                                                                         MODIFIED);
    // Act & Assert
    assertThat(filter.test(deviceInstanceUpdate)).isFalse();
  }

  @Test
  void evaluatesMatchingDeviceSourceAndDeviceTypeAsTrue() {
    // Arrange
    DeviceInstance newDeviceInstance = dummyDeviceInstance()
        .withSource(ADAPTER_SOURCE)
        .withDeviceTypeIdentifier(ADAPTER_DEVICE_TYPE);
    DeviceInstance oldDeviceInstance = dummyDeviceInstance()
        .withSource(ADAPTER_SOURCE)
        .withDeviceTypeIdentifier(ADAPTER_DEVICE_TYPE);
    DeviceInstanceUpdate deviceInstanceUpdate = new DeviceInstanceUpdate(newDeviceInstance,
                                                                         oldDeviceInstance,
                                                                         MODIFIED);

    // Act & Assert
    assertThat(filter.test(deviceInstanceUpdate)).isTrue();
  }

  @Test
  void evaluatesChangeFromMismatchingDeviceTypeToMatchingDeviceTypeAsTrue() {
    // Arrange
    DeviceInstance newDeviceInstance = dummyDeviceInstance()
        .withSource(ADAPTER_SOURCE)
        .withDeviceTypeIdentifier("irrelevant-device-type");
    DeviceInstance oldDeviceInstance = dummyDeviceInstance()
        .withSource(ADAPTER_SOURCE)
        .withDeviceTypeIdentifier(ADAPTER_DEVICE_TYPE);
    DeviceInstanceUpdate deviceInstanceUpdate = new DeviceInstanceUpdate(newDeviceInstance,
                                                                         oldDeviceInstance,
                                                                         MODIFIED);

    // Act & Assert
    assertThat(filter.test(deviceInstanceUpdate)).isTrue();
  }

  @Test
  void evaluatesChangeFromMatchingDeviceTypeToMismatchingDeviceTypeAsTrue() {
    // Arrange
    DeviceInstance newDeviceInstance = dummyDeviceInstance()
        .withSource(ADAPTER_SOURCE)
        .withDeviceTypeIdentifier(ADAPTER_DEVICE_TYPE);
    DeviceInstance oldDeviceInstance = dummyDeviceInstance()
        .withSource(ADAPTER_SOURCE)
        .withDeviceTypeIdentifier("irrelevant-device-type");
    DeviceInstanceUpdate deviceInstanceUpdate = new DeviceInstanceUpdate(newDeviceInstance,
                                                                         oldDeviceInstance,
                                                                         MODIFIED);

    // Act & Assert
    assertThat(filter.test(deviceInstanceUpdate)).isTrue();
  }

  private DeviceInstance dummyDeviceInstance() {
    return new DeviceInstance("", "", "", "", Instant.EPOCH, Instant.EPOCH, false, "", "", "");
  }
}
