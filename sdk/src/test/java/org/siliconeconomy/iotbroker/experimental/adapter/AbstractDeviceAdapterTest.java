/*
 * Copyright Open Logistics Foundation
 *
 * Licensed under the Open Logistics Foundation License 1.3.
 * For details on the licensing terms, see the LICENSE file.
 * SPDX-License-Identifier: OLFL-1.3
 */
package org.siliconeconomy.iotbroker.experimental.adapter;

import org.checkerframework.checker.nullness.qual.NonNull;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.siliconeconomy.iotbroker.experimental.adapter.deviceupdate.DeviceInstanceUpdateListener;
import org.siliconeconomy.iotbroker.experimental.adapter.deviceupdate.DeviceTypeUpdateListener;
import org.siliconeconomy.iotbroker.model.device.DeviceInstance;
import org.siliconeconomy.iotbroker.model.device.DeviceType;

import java.time.Instant;
import java.util.Optional;
import java.util.Set;

import static org.assertj.core.api.Assertions.assertThat;
import static org.mockito.ArgumentMatchers.any;
import static org.mockito.Mockito.*;

/**
 * Test cases for {@link AbstractDeviceAdapter}.
 *
 * @author M. Grzenia
 */
class AbstractDeviceAdapterTest {

  /**
   * Class under test.
   */
  private AbstractDeviceAdapter adapter;
  /**
   * Test dependencies.
   */
  private DeviceTypeUpdateListener deviceTypeUpdateListener;
  private DeviceInstanceUpdateListener deviceInstanceUpdateListener;

  @BeforeEach
  void setUp() {
    deviceInstanceUpdateListener = mock(DeviceInstanceUpdateListener.class);
    deviceTypeUpdateListener = mock(DeviceTypeUpdateListener.class);
    adapter = mock(AbstractDeviceAdapter.class,
                   withSettings()
                       .useConstructor(new TestAdapterAttributes(),
                                       deviceTypeUpdateListener,
                                       deviceInstanceUpdateListener)
                       .defaultAnswer(CALLS_REAL_METHODS)
    );
  }

  @Test
  void initialize_whenDeviceTypeNotRegistered_thenRegistersDeviceType() {
    // Arrange
    doReturn(false).when(adapter).isDeviceTypeRegistered();

    // Act
    adapter.initialize();

    // Verify
    verify(adapter).registerDeviceType();
  }

  @Test
  void initialize_whenDeviceTypeRegistered_thenEnsuresThatDeviceTypeIsProvidedByAdapter() {
    // Arrange
    doReturn(true).when(adapter).isDeviceTypeRegistered();
    doReturn(false).when(adapter).isDeviceTypeProvidedByAdapter();

    // Act
    adapter.initialize();

    // Verify
    verify(adapter).updateDeviceTypeProvidedByAdapter();
  }

  @Test
  void initialize_whenDeviceTypeEnabled_thenEnablesAdapter() {
    // Arrange
    doReturn(true).when(adapter).isDeviceTypeEnabled();

    // Act
    adapter.initialize();

    // Verify
    verify(adapter).enable();
  }

  @Test
  void initialize_whenDeviceTypeNotEnabled_thenDoesNotEnableAdapter() {
    // Arrange
    doReturn(true).when(adapter).isDeviceTypeRegistered();
    doReturn(false).when(adapter).isDeviceTypeEnabled();

    // Act
    adapter.initialize();

    // Verify
    verify(adapter, never()).enable();
  }

  @Test
  void initialize_addsUpdateHandlers() {
    // Act
    adapter.initialize();

    // Verify
    verify(deviceTypeUpdateListener).addDeviceTypeUpdateHandler(adapter);
    verify(deviceInstanceUpdateListener).addDeviceInstanceUpdateHandler(adapter);
  }

  @Test
  void terminate_removesUpdateHandlersAndDisablesAdapters() {
    // Arrange
    doReturn(true).when(adapter).isInitialized();

    // Act
    adapter.terminate();

    // Verify
    verify(deviceInstanceUpdateListener).removeDeviceInstanceUpdateHandler(adapter);
    verify(deviceTypeUpdateListener).removeDeviceTypeUpdateHandler(adapter);
    verify(adapter).disable();
  }

  @Test
  void enable_opensCommunicationChannel() {
    // Arrange
    doReturn(true).when(adapter).isDeviceTypeEnabled();

    // Act
    adapter.enable();

    // Verify
    verify(adapter).openCommunicationChannel();
  }

  @Test
  void disable_closesCommunicationChannel() {
    // Arrange
    doReturn(true).when(adapter).isDeviceTypeEnabled();
    adapter.enable();
    doReturn(false).when(adapter).isDeviceTypeEnabled();

    // Act
    adapter.disable();

    // Verify
    verify(adapter).closeCommunicationChannel();
  }

  @Test
  void isDeviceTypeRegistered_whenDeviceTypeNotRegistered_thenReturnsFalse() {
    // Arrange
    doReturn(Optional.empty()).when(adapter).fetchAdapterDeviceType();

    // Act
    boolean result = adapter.isDeviceTypeRegistered();

    // Assert
    assertThat(result).isFalse();
    verify(adapter).fetchAdapterDeviceType();
  }

  @Test
  void isDeviceTypeRegistered_whenDeviceTypeRegistered_thenReturnsTrue() {
    // Arrange
    doReturn(Optional.of(dummyDeviceType())).when(adapter).fetchAdapterDeviceType();

    // Act
    boolean result = adapter.isDeviceTypeRegistered();

    // Assert & Verify
    assertThat(result).isTrue();
    verify(adapter).fetchAdapterDeviceType();
  }

  @Test
  void isDeviceTypeProvidedByAdapter_whenDeviceTypeNotRegistered_thenReturnsFalse() {
    // Arrange
    doReturn(Optional.empty()).when(adapter).fetchAdapterDeviceType();

    // Act
    boolean result = adapter.isDeviceTypeProvidedByAdapter();

    // Assert & Verify
    assertThat(result).isFalse();
    verify(adapter).fetchAdapterDeviceType();
  }

  @Test
  void isDeviceTypeProvidedByAdapter_whenDeviceTypeIsProvidedByAdapter_thenReturnsTrue() {
    // Arrange
    DeviceType deviceType = dummyDeviceType().withProvidedBy(Set.of("test-adapter"));
    doReturn(Optional.of(deviceType)).when(adapter).fetchAdapterDeviceType();

    // Act
    boolean result = adapter.isDeviceTypeProvidedByAdapter();

    // Assert & Verify
    assertThat(result).isTrue();
    verify(adapter).fetchAdapterDeviceType();
  }

  @Test
  void isDeviceTypeProvidedByAdapter_whenDeviceTypeNotProvidedByAdapter_thenReturnsFalse() {
    // Arrange
    DeviceType deviceType = dummyDeviceType().withProvidedBy(Set.of("some-other-adapter"));
    doReturn(Optional.of(deviceType)).when(adapter).fetchAdapterDeviceType();

    // Act
    boolean result = adapter.isDeviceTypeProvidedByAdapter();

    // Assert & Verify
    assertThat(result).isFalse();
    verify(adapter).fetchAdapterDeviceType();
  }

  @Test
  void isDeviceTypeEnabled_whenDeviceTypeRegisteredAndEnabled_thenReturnsTrue() {
    // Arrange
    DeviceType deviceType = dummyDeviceType().withEnabled(true);
    doReturn(Optional.of(deviceType)).when(adapter).fetchAdapterDeviceType();

    // Act
    boolean result = adapter.isDeviceTypeEnabled();

    // Assert & Verify
    assertThat(result).isTrue();
    verify(adapter).fetchAdapterDeviceType();
  }

  @Test
  void isDeviceTypeEnabled_whenDeviceTypeNotRegistered_thenReturnsFalse() {
    // Arrange
    doReturn(Optional.empty()).when(adapter).fetchAdapterDeviceType();

    // Act
    boolean result = adapter.isDeviceTypeEnabled();

    // Assert & Verify
    assertThat(result).isFalse();
    verify(adapter).fetchAdapterDeviceType();
  }

  @Test
  void isDeviceInstanceRegistered_whenDeviceInstanceNotRegistered_thenReturnsFalse() {
    // Arrange
    doReturn(Optional.empty()).when(adapter).fetchAdapterDeviceInstance("some-tenant");

    // Act
    boolean result = adapter.isDeviceInstanceRegistered("some-tenant");

    // Assert & Verify
    assertThat(result).isFalse();
    verify(adapter).fetchAdapterDeviceInstance("some-tenant");
  }

  @Test
  void isDeviceInstanceRegistered_whenDeviceInstanceRegistered_thenReturnsTrue() {
    // Arrange
    DeviceInstance deviceInstance = dummyDeviceInstance();
    doReturn(Optional.of(deviceInstance)).when(adapter).fetchAdapterDeviceInstance("some-tenant");

    // Act
    boolean result = adapter.isDeviceInstanceRegistered("some-tenant");

    // Assert & Verify
    assertThat(result).isTrue();
    verify(adapter).fetchAdapterDeviceInstance("some-tenant");
  }

  @Test
  void isDeviceInstanceAssociatedWithAdapterDeviceType_whenDeviceInstanceRegisteredAndAssociatedWithDeviceType_thenReturnsTrue() {
    // Arrange
    DeviceInstance deviceInstance = dummyDeviceInstance()
        .withSource("test-source")
        .withDeviceTypeIdentifier("test-device-type");
    doReturn(Optional.of(deviceInstance)).when(adapter).fetchAdapterDeviceInstance("some-tenant");

    // Act
    boolean result = adapter.isDeviceInstanceAssociatedWithAdapterDeviceType("some-tenant");

    // Assert & Verify
    assertThat(result).isTrue();
    verify(adapter).fetchAdapterDeviceInstance("some-tenant");
  }

  @Test
  void isDeviceInstanceAssociatedWithAdapterDeviceType_whenDeviceInstanceNotRegistered_thenReturnsFalse() {
    // Arrange
    doReturn(Optional.empty()).when(adapter).fetchAdapterDeviceInstance("some-tenant");

    // Act
    boolean result = adapter.isDeviceInstanceAssociatedWithAdapterDeviceType("some-tenant");

    // Assert
    assertThat(result).isFalse();
    verify(adapter).fetchAdapterDeviceInstance("some-tenant");
  }

  @Test
  void isDeviceInstanceAssociatedWithAdapterDeviceType_whenDeviceInstanceRegisteredButNotAssociatedWithDeviceType_thenReturnsFalse() {
    // Arrange
    DeviceInstance deviceInstance = dummyDeviceInstance()
        .withSource("test-source")
        .withDeviceTypeIdentifier("some-other-device-type");
    doReturn(Optional.of(deviceInstance)).when(adapter).fetchAdapterDeviceInstance("some-tenant");

    // Act
    boolean result = adapter.isDeviceInstanceAssociatedWithAdapterDeviceType("some-tenant");

    // Assert & Verify
    assertThat(result).isFalse();
    verify(adapter).fetchAdapterDeviceInstance("some-tenant");
  }

  @Test
  void isDeviceInstanceEnabled_whenDeviceInstanceRegisteredAndEnabled_thenReturnsTrue() {
    // Arrange
    DeviceInstance deviceInstance = dummyDeviceInstance()
        .withEnabled(true);
    doReturn(Optional.of(deviceInstance)).when(adapter).fetchAdapterDeviceInstance("some-tenant");

    // Act
    boolean result = adapter.isDeviceInstanceEnabled("some-tenant");

    // Assert & Verify
    assertThat(result).isTrue();
    verify(adapter).fetchAdapterDeviceInstance("some-tenant");
  }

  @Test
  void isDeviceInstanceEnabled_whenDeviceInstanceNotRegistered_thenReturnsFalse() {
    // Arrange
    doReturn(Optional.empty()).when(adapter).fetchAdapterDeviceInstance("some-tenant");

    // Act
    boolean result = adapter.isDeviceInstanceEnabled("some-tenant");

    // Assert & Verify
    assertThat(result).isFalse();
    verify(adapter).fetchAdapterDeviceInstance("some-tenant");
  }

  @Test
  void isDeviceInstanceAutoRegistrationEnabled_whenDeviceTypeRegisteredAndAutoRegistrationEnabled_thenReturnsTrue() {
    // Arrange
    DeviceType deviceType = dummyDeviceType().withAutoRegisterDeviceInstances(true);
    doReturn(Optional.of(deviceType)).when(adapter).fetchAdapterDeviceType();

    // Act
    boolean result = adapter.isDeviceInstanceAutoRegistrationEnabled();

    // Assert & Verify
    assertThat(result).isTrue();
    verify(adapter).fetchAdapterDeviceType();
  }

  @Test
  void isDeviceInstanceAutoRegistrationEnabled_whenDeviceTypeNotRegistered_thenReturnsFalse() {
    // Arrange
    doReturn(Optional.empty()).when(adapter).fetchAdapterDeviceType();

    // Act
    boolean result = adapter.isDeviceInstanceAutoRegistrationEnabled();

    // Assert & Verify
    assertThat(result).isFalse();
    verify(adapter).fetchAdapterDeviceType();
  }

  @Test
  void attemptDeviceInstanceAutoRegistration_whenAutoRegistrationEnabled_thenRegistersDeviceInstance() {
    // Arrange
    doReturn(false).when(adapter).isDeviceInstanceRegistered(any());
    doReturn(true).when(adapter).isDeviceInstanceAutoRegistrationEnabled();
    Object registrationContext = new Object();

    // Act
    boolean result = adapter.attemptDeviceInstanceAutoRegistration("some-tenant", registrationContext);

    // Assert & Verify
    assertThat(result).isTrue();
    verify(adapter).registerDeviceInstance("some-tenant", registrationContext);
  }

  @Test
  void attemptDeviceInstanceAutoRegistration_whenDeviceInstanceAlreadyRegistered_thenDoesNothing() {
    // Arrange
    doReturn(true).when(adapter).isDeviceInstanceRegistered(any());
    doReturn(true).when(adapter).isDeviceInstanceAssociatedWithAdapterDeviceType(any());
    Object registrationContext = new Object();

    // Act
    boolean result = adapter.attemptDeviceInstanceAutoRegistration("some-tenant", registrationContext);

    // Assert & Verify
    assertThat(result).isTrue();
    verify(adapter, never()).registerDeviceInstance("some-tenant", registrationContext);
  }

  @Test
  void attemptDeviceInstanceAutoRegistration_whenAutoRegistrationDisabled_thenDoesNotRegisterDeviceInstance() {
    // Arrange
    doReturn(false).when(adapter).isDeviceInstanceRegistered(any());
    doReturn(false).when(adapter).isDeviceInstanceAutoRegistrationEnabled();
    Object registrationContext = new Object();

    // Act
    boolean result = adapter.attemptDeviceInstanceAutoRegistration("some-tenant", registrationContext);

    // Assert & Verify
    assertThat(result).isFalse();
    verify(adapter, never()).registerDeviceInstance(any(), any());
  }

  @Test
  void attemptDeviceInstanceAutoRegistration_whenDeviceInstanceNotAssociatedWithAdapterDeviceType_thenDoesNotRegisterDeviceInstance() {
    // Arrange
    doReturn(true).when(adapter).isDeviceInstanceRegistered(any());
    doReturn(false).when(adapter).isDeviceInstanceAssociatedWithAdapterDeviceType(any());
    doReturn(true).when(adapter).isDeviceInstanceAutoRegistrationEnabled();
    Object registrationContext = new Object();

    // Act
    boolean result = adapter.attemptDeviceInstanceAutoRegistration("some-tenant", registrationContext);

    // Assert & Verify
    assertThat(result).isFalse();
    verify(adapter, never()).registerDeviceInstance(any(), any());
  }

  @Test
  void onDeviceTypeCreated_whenDeviceTypeRegisteredAndEnabled_thenEnablesAdapter() {
    // Arrange
    doReturn(true).when(adapter).isDeviceTypeRegistered();
    doReturn(true).when(adapter).isDeviceTypeEnabled();

    // Act
    adapter.onDeviceTypeCreated(dummyDeviceType());

    // Verify
    verify(adapter).enable();
  }

  @Test
  void onDeviceTypeModified_whenDeviceTypeHasBeenEnabled_thenEnablesAdapter() {
    // Arrange
    doReturn(true).when(adapter).isDeviceTypeEnabled();
    DeviceType oldDeviceType = dummyDeviceType().withEnabled(false);
    DeviceType newDeviceType = dummyDeviceType().withEnabled(true);

    // Act
    adapter.onDeviceTypeModified(oldDeviceType, newDeviceType);

    // Verify
    verify(adapter).enable();
  }

  @Test
  void onDeviceTypeModified_whenDeviceTypeHasBeenDisabled_thenDisablesAdapter() {
    // Arrange
    doReturn(false).when(adapter).isDeviceTypeEnabled();
    DeviceType oldDeviceType = dummyDeviceType().withEnabled(true);
    DeviceType newDeviceType = dummyDeviceType().withEnabled(false);

    // Act
    adapter.onDeviceTypeModified(oldDeviceType, newDeviceType);

    // Verify
    verify(adapter).disable();
  }

  @Test
  void onDeviceTypeDeleted_whenDeviceTypeNotRegistered_thenDisablesAdapter() {
    // Arrange
    doReturn(false).when(adapter).isDeviceTypeRegistered();

    // Act
    adapter.onDeviceTypeDeleted(dummyDeviceType());

    // Verify
    verify(adapter).disable();
  }

  private DeviceType dummyDeviceType() {
    return new DeviceType("", "", "", Set.of(), "", false, false, false);
  }

  private DeviceInstance dummyDeviceInstance() {
    return new DeviceInstance("", "", "", "", Instant.EPOCH, Instant.EPOCH, false, "", "", "");
  }

  private static class TestAdapterAttributes
      implements AdapterAttributes {

    @Override
    public @NonNull String getAdapterIdentifier() {
      return "test-adapter";
    }

    @Override
    public @NonNull String getAdapterDeviceSource() {
      return "test-source";
    }

    @Override
    public @NonNull String getAdapterDeviceTypeIdentifier() {
      return "test-device-type";
    }
  }
}
