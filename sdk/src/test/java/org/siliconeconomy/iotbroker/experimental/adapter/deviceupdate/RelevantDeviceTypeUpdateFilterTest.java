/*
 * Copyright Open Logistics Foundation
 *
 * Licensed under the Open Logistics Foundation License 1.3.
 * For details on the licensing terms, see the LICENSE file.
 * SPDX-License-Identifier: OLFL-1.3
 */
package org.siliconeconomy.iotbroker.experimental.adapter.deviceupdate;

import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.siliconeconomy.iotbroker.model.device.DeviceType;
import org.siliconeconomy.iotbroker.model.device.DeviceTypeUpdate;

import java.util.Set;

import static org.assertj.core.api.Assertions.assertThat;
import static org.siliconeconomy.iotbroker.model.device.DeviceTypeUpdate.Type.MODIFIED;

/**
 * Tests for {@link RelevantDeviceTypeUpdateFilter}.
 *
 * @author M. Grzenia
 */
class RelevantDeviceTypeUpdateFilterTest {

  /**
   * Class under test.
   */
  private RelevantDeviceTypeUpdateFilter filter;
  private static final String ADAPTER_SOURCE = "relevant-source";
  private static final String ADAPTER_DEVICE_TYPE = "relevant-type";

  @BeforeEach
  void setUp() {
    filter = new RelevantDeviceTypeUpdateFilter(ADAPTER_SOURCE, ADAPTER_DEVICE_TYPE);
  }

  @Test
  void evaluatesMismatchingDeviceSourceAsFalse() {
    // Arrange
    DeviceType newDeviceType = dummyDeviceType()
        .withSource("irrelevant-source")
        .withIdentifier(ADAPTER_DEVICE_TYPE);
    DeviceType oldDeviceType = dummyDeviceType()
        .withSource("irrelevant-source")
        .withIdentifier(ADAPTER_DEVICE_TYPE);
    DeviceTypeUpdate deviceTypeUpdate = new DeviceTypeUpdate(newDeviceType,
                                                             oldDeviceType,
                                                             MODIFIED);
    // Act & Assert
    assertThat(filter.test(deviceTypeUpdate)).isFalse();
  }

  @Test
  void evaluatesMismatchingDeviceTypeAsFalse() {
    // Arrange
    DeviceType newDeviceType = dummyDeviceType()
        .withSource(ADAPTER_SOURCE)
        .withIdentifier("irrelevant-device-type");
    DeviceType oldDeviceType = dummyDeviceType()
        .withSource(ADAPTER_SOURCE)
        .withIdentifier("irrelevant-device-type");
    DeviceTypeUpdate deviceTypeUpdate = new DeviceTypeUpdate(newDeviceType,
                                                             oldDeviceType,
                                                             MODIFIED);
    // Act & Assert
    assertThat(filter.test(deviceTypeUpdate)).isFalse();
  }

  @Test
  void evaluatesMatchingDeviceSourceAndDeviceTypeAsTrue() {
    // Arrange

    DeviceType newDeviceType = dummyDeviceType()
        .withSource(ADAPTER_SOURCE)
        .withIdentifier(ADAPTER_DEVICE_TYPE);
    DeviceType oldDeviceType = dummyDeviceType()
        .withSource(ADAPTER_SOURCE)
        .withIdentifier(ADAPTER_DEVICE_TYPE);
    DeviceTypeUpdate deviceTypeUpdate = new DeviceTypeUpdate(newDeviceType,
                                                             oldDeviceType,
                                                             MODIFIED);

    // Act & Assert
    assertThat(filter.test(deviceTypeUpdate)).isTrue();
  }

  private DeviceType dummyDeviceType() {
    return new DeviceType("", "", "", Set.of(), "", false, false, false);
  }
}
