/*
 * Copyright Open Logistics Foundation
 *
 * Licensed under the Open Logistics Foundation License 1.3.
 * For details on the licensing terms, see the LICENSE file.
 * SPDX-License-Identifier: OLFL-1.3
 */
package org.siliconeconomy.iotbroker.jackson.sensordata.deserializer;

import com.fasterxml.jackson.databind.ObjectMapper;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.params.ParameterizedTest;
import org.junit.jupiter.params.provider.EnumSource;
import org.siliconeconomy.iotbroker.jackson.sensordata.deserializer.LocationEncodingTypeDeserializer;
import org.siliconeconomy.iotbroker.model.sensordata.location.LocationEncodingType;
import org.siliconeconomy.iotbroker.jackson.sensordata.SensorDataModule;

import static org.assertj.core.api.Assertions.assertThat;

/**
 * Test cases for {@link LocationEncodingTypeDeserializer}.
 *
 * @author M. Grzenia
 */
class LocationEncodingTypeDeserializerTest {

  private ObjectMapper mapper;

  @BeforeEach
  void setUp() {
    mapper = new ObjectMapper();
    mapper.registerModule(new SensorDataModule());
  }

  @ParameterizedTest
  @EnumSource(LocationEncodingType.class)
  void shouldDeserializeEnumNamesToLocationDetailsSubTypes(
      LocationEncodingType locationEncodingType) {
    assertThat(mapper.convertValue(locationEncodingType.name(), LocationEncodingType.class))
        .isEqualTo(locationEncodingType);
  }
}
