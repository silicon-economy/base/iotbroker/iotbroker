/*
 * Copyright Open Logistics Foundation
 *
 * Licensed under the Open Logistics Foundation License 1.3.
 * For details on the licensing terms, see the LICENSE file.
 * SPDX-License-Identifier: OLFL-1.3
 */
package org.siliconeconomy.iotbroker.jackson.device;

import org.siliconeconomy.iotbroker.model.device.DeviceTypeUpdate;

/**
 * Sample data for testing purposes.
 *
 * @author M. Grzenia
 */
public class ExampleDeviceTypeUpdate
    extends DeviceTypeUpdate {

  public ExampleDeviceTypeUpdate() {
    super(new ExampleDeviceType(), null, Type.CREATED);
  }
}
