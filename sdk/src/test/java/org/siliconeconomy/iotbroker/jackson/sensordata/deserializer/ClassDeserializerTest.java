/*
 * Copyright Open Logistics Foundation
 *
 * Licensed under the Open Logistics Foundation License 1.3.
 * For details on the licensing terms, see the LICENSE file.
 * SPDX-License-Identifier: OLFL-1.3
 */
package org.siliconeconomy.iotbroker.jackson.sensordata.deserializer;

import com.fasterxml.jackson.databind.ObjectMapper;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.siliconeconomy.iotbroker.jackson.sensordata.deserializer.ClassDeserializer;
import org.siliconeconomy.iotbroker.model.sensordata.Datastream;
import org.siliconeconomy.iotbroker.jackson.sensordata.SensorDataModule;

import static org.assertj.core.api.Assertions.assertThat;
import static org.assertj.core.api.Assertions.assertThatThrownBy;

/**
 * Test cases for {@link ClassDeserializer}.
 *
 * @author M. Grzenia
 */
class ClassDeserializerTest {

  private ObjectMapper mapper;

  @BeforeEach
  void setUp() {
    mapper = new ObjectMapper();
    mapper.registerModule(new SensorDataModule());
  }

  @Test
  void shouldFailForUnknownCanonicalName() {
    assertThatThrownBy(
        () -> mapper.convertValue("some.unknown.package.SomeUnknownClass", Class.class)
    )
        .isInstanceOf(IllegalArgumentException.class);
  }

  @Test
  void shouldDeserializeOtherClassesUsingCanonicalName() {
    assertThat(mapper.convertValue(String.class.getCanonicalName(), Class.class))
        .isEqualTo(String.class);
    assertThat(mapper.convertValue(Integer.class.getCanonicalName(), Class.class))
        .isEqualTo(Integer.class);
    assertThat(mapper.convertValue(Datastream.class.getCanonicalName(), Class.class))
        .isEqualTo(Datastream.class);
  }
}
