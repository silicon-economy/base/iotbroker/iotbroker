/*
 * Copyright Open Logistics Foundation
 *
 * Licensed under the Open Logistics Foundation License 1.3.
 * For details on the licensing terms, see the LICENSE file.
 * SPDX-License-Identifier: OLFL-1.3
 */
package org.siliconeconomy.iotbroker.jackson.sensordata.deserializer;

import com.fasterxml.jackson.databind.ObjectMapper;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.params.ParameterizedTest;
import org.junit.jupiter.params.provider.EnumSource;
import org.siliconeconomy.iotbroker.jackson.sensordata.deserializer.ObservationTypeDeserializer;
import org.siliconeconomy.iotbroker.model.sensordata.observation.ObservationType;
import org.siliconeconomy.iotbroker.jackson.sensordata.SensorDataModule;

import static org.assertj.core.api.Assertions.assertThat;

/**
 * Test cases for {@link ObservationTypeDeserializer}.
 *
 * @author M. Grzenia
 */
class ObservationTypeDeserializerTest {

  private ObjectMapper mapper;

  @BeforeEach
  void setUp() {
    mapper = new ObjectMapper();
    mapper.registerModule(new SensorDataModule());
  }

  @ParameterizedTest
  @EnumSource(ObservationType.class)
  void shouldDeserializeEnumNamesToObservationResultSubTypes(ObservationType observationType) {
    assertThat(mapper.convertValue(observationType.name(), ObservationType.class))
        .isEqualTo(observationType);
  }
}
