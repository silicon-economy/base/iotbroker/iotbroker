/*
 * Copyright Open Logistics Foundation
 *
 * Licensed under the Open Logistics Foundation License 1.3.
 * For details on the licensing terms, see the LICENSE file.
 * SPDX-License-Identifier: OLFL-1.3
 */
package org.siliconeconomy.iotbroker.jackson.device;

import org.siliconeconomy.iotbroker.model.device.DeviceInstance;

import java.time.Instant;

/**
 * Sample data for testing purposes.
 *
 * @author M. Grzenia
 */
public class ExampleDeviceInstance
    extends DeviceInstance {

  public ExampleDeviceInstance() {
    super("device-instance-id",
          "source",
          "tenant",
          "device-type-identifier",
          Instant.ofEpochMilli(1692616406504L),
          Instant.EPOCH,
          true,
          "description",
          "hardware-revision",
          "firmware-version");
  }
}
