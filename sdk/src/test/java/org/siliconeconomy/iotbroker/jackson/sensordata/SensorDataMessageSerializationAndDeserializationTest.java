/*
 * Copyright Open Logistics Foundation
 *
 * Licensed under the Open Logistics Foundation License 1.3.
 * For details on the licensing terms, see the LICENSE file.
 * SPDX-License-Identifier: OLFL-1.3
 */
package org.siliconeconomy.iotbroker.jackson.sensordata;

import com.fasterxml.jackson.databind.JsonNode;
import com.fasterxml.jackson.databind.ObjectMapper;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.siliconeconomy.iotbroker.jackson.ObjectMapperFactory;
import org.siliconeconomy.iotbroker.model.sensordata.SensorDataMessage;

import java.io.File;
import java.io.IOException;

import static org.assertj.core.api.Assertions.assertThat;

/**
 * A test for serialization and deserialization of {@link SensorDataMessage} instances.
 *
 * @author M. Grzenia
 */
class SensorDataMessageSerializationAndDeserializationTest {

  private ObjectMapper mapper;
  /**
   * A separate mapper solely for JSON parsing (to rule out any potential side effects regarding the
   * main mapper's configuration).
   */
  private ObjectMapper unconfiguredMapper;

  @BeforeEach
  void setUp() {
    mapper = ObjectMapperFactory.createObjectMapper();
    unconfiguredMapper = new ObjectMapper();
  }

  @Test
  void serialize()
      throws IOException {
    // Arrange
    String path = "src/test/resources/org/siliconeconomy/iotbroker/jackson/sensordata/sensor-data-message.json";
    JsonNode expectedNode = unconfiguredMapper.readTree(new File(path));
    SensorDataMessage message = new ExampleSensorDataMessage();

    // Act
    String serializationResult = mapper.writeValueAsString(message);

    // Assert
    JsonNode resultNode = unconfiguredMapper.readTree(serializationResult);
    assertThat(resultNode).isEqualTo(expectedNode);
  }

  @Test
  void deserialize()
      throws IOException {
    // Arrange
    String path = "src/test/resources/org/siliconeconomy/iotbroker/jackson/sensordata/sensor-data-message.json";
    String jsonString = unconfiguredMapper.readTree(new File(path)).toString();
    SensorDataMessage expectedResult = new ExampleSensorDataMessage();

    // Act
    SensorDataMessage deserializationResult = mapper.readValue(jsonString, SensorDataMessage.class);

    // Assert
    assertThat(deserializationResult).isEqualTo(expectedResult);
  }
}
