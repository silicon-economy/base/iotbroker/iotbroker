/*
 * Copyright Open Logistics Foundation
 *
 * Licensed under the Open Logistics Foundation License 1.3.
 * For details on the licensing terms, see the LICENSE file.
 * SPDX-License-Identifier: OLFL-1.3
 */
package org.siliconeconomy.iotbroker.jackson.device;

import org.siliconeconomy.iotbroker.model.device.DeviceInstanceUpdate;

/**
 * Sample data for testing purposes.
 *
 * @author M. Grzenia
 */
public class ExampleDeviceInstanceUpdate
    extends DeviceInstanceUpdate {

  public ExampleDeviceInstanceUpdate() {
    super(new ExampleDeviceInstance(), null, Type.CREATED);
  }
}
