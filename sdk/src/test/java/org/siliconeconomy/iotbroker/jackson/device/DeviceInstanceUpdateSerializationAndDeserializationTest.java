/*
 * Copyright Open Logistics Foundation
 *
 * Licensed under the Open Logistics Foundation License 1.3.
 * For details on the licensing terms, see the LICENSE file.
 * SPDX-License-Identifier: OLFL-1.3
 */
package org.siliconeconomy.iotbroker.jackson.device;

import com.fasterxml.jackson.databind.JsonNode;
import com.fasterxml.jackson.databind.ObjectMapper;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.siliconeconomy.iotbroker.jackson.ObjectMapperFactory;
import org.siliconeconomy.iotbroker.model.device.DeviceInstanceUpdate;

import java.io.File;
import java.io.IOException;

import static org.assertj.core.api.Assertions.assertThat;

/**
 * A test for serialization and deserialization of {@link DeviceInstanceUpdate} instances.
 *
 * @author M. Grzenia
 */
class DeviceInstanceUpdateSerializationAndDeserializationTest {

  private ObjectMapper mapper;
  /**
   * A separate mapper solely for JSON parsing (to rule out any potential side effects regarding the
   * main mapper's configuration).
   */
  private ObjectMapper unconfiguredMapper;

  @BeforeEach
  void setUp() {
    mapper = ObjectMapperFactory.createObjectMapper();
    unconfiguredMapper = new ObjectMapper();
  }

  @Test
  void serialize()
      throws IOException {
    // Arrange
    String path = "src/test/resources/org/siliconeconomy/iotbroker/jackson/device/device-instance-update.json";
    JsonNode expectedNode = unconfiguredMapper.readTree(new File(path));
    DeviceInstanceUpdate deviceInstanceUpdate = new ExampleDeviceInstanceUpdate();

    // Act
    String serializationResult = mapper.writeValueAsString(deviceInstanceUpdate);

    // Assert
    JsonNode resultNode = unconfiguredMapper.readTree(serializationResult);
    assertThat(resultNode).isEqualTo(expectedNode);
  }

  @Test
  void deserialize()
      throws IOException {
    // Arrange
    String path = "src/test/resources/org/siliconeconomy/iotbroker/jackson/device/device-instance-update.json";
    String jsonString = unconfiguredMapper.readTree(new File(path)).toString();
    DeviceInstanceUpdate expectedResult = new ExampleDeviceInstanceUpdate();

    // Act
    DeviceInstanceUpdate deserializationResult = mapper.readValue(jsonString,
                                                                  DeviceInstanceUpdate.class);

    // Assert
    assertThat(deserializationResult).isEqualTo(expectedResult);
  }
}
