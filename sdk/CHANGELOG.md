# Changelog
All notable changes to this project will be documented in this file.

The format is based on [Keep a Changelog](https://keepachangelog.com/en/1.1.0/), and this project adheres to [Semantic Versioning](https://semver.org/spec/v2.0.0.html).

## [7.0.1] - 2023-09-26
### Deprecated
- Deprecate `LatitudeLongitudeDetails.getLatLong()` and `MapcodeDetails.getMapcode()`.
  These methods are basically duplicates of the respective implementations of `LocationDetails.getDetails()`.

## [7.0.0] - 2023-08-22
### Added
- Add `org.siliconeconomy.iotbroker.jackson.ObjectMapperFactory` that can be used to create `ObjectMapper` instances capable of serializing and deserializing IoT Broker data structures to and from their JSON representation.

### Changed
- Remove any Jackson-related annotations from IoT Broker data structures and, as a replacement and where necessary, introduce Jackson mix-ins that can be used instead.
- Jackson-related classes are now organized in the `org.siliconeconomy.iotbroker.jackson` package.

## [6.1.0] - 2023-07-18
### Changed
- Extend `AbstractDeviceAdapter` by providing more implementations of methods from `DeviceAdapter` and adding interfaces and classes for handling updates to device types and device instances.

## [6.0.0] - 2023-07-05
### Changed
- Move package `org.siliconeconomy.iotbroker.utils.amqp` to `org.siliconeconomy.iotbroker.amqp`.
- Move package `org.siliconeconomy.iotbroker.utils.couchdb` to `org.siliconeconomy.iotbroker.couchdb`.
- Move package `org.siliconeconomy.iotbroker.utils.sensordata` to `org.siliconeconomy.iotbroker.sensordata`.

### Removed
- Remove deprecated classes.
- Remove the dependencies `com.rabbitmq:amqp-client:5.17.0` and `org.eclipse.paho:org.eclipse.paho.client.mqttv3:1.2.5`.

## [5.9.0] - 2023-06-16
###  Changed
- Remove obsolete test class `DistributionHelperTest`.
- Mark obsolete classes as deprecated (and for removal with the next major version).
- Remove sonarqube-specific warning suppressions and instead use `@SuppressWarnings("unchecked")` where it seems reasonable due to existing test cases.

## [5.8.1] - 2023-06-15
### Changed
- Update Jackson to 2.15.2.
- Update Checker Qual to 3.35.0
- Update JUnit Jupiter to 5.9.3.
- Update AssertJ Core to 3.24.2.
- Update Mockito Core to 5.3.1.
- Update RabbitMQ Java Client to 5.17.0.
- Update Maven Surefire Plugin to 3.1.2
- Update JaCoCo Maven Plugin to 0.8.10.
- Update License Maven Plugin to 2.1.0.
- Update Apache Maven Source Plugin to 3.3.0.
- Update Apache Maven Javadoc Plugin to 3.5.0.

## [5.8.0] - 2023-06-15
### Added
- Add a (for now experimental) `DeviceAdapter` interface that defines methods that an adapter integrating a specific type of (IoT) device into the IoT Broker must implement.
  Additionally, add an abstract implementation that provides some additional methods and defines fundamental processes that are expected to be handled by device adapter implementations.

## [5.7.0] - 2023-06-13
### Added
- Add `LoggingScheduledThreadPoolExecutor`, a custom thread pool that reuses a fixed number of threads operating off a shared unbounded queue.

## [5.6.1] - 2023-04-17
### Changed
- Relicense under the OLFL-1.3.

## [5.6.0] - 2023-01-30
### Added
- Add a configuration for the _Device Message Exchange_ where _raw_ device messages can be published to by device adapters.

### Deprecated
- Deprecate `org.siliconeconomy.iotbroker.utils.amqpclient.AmqpClient` without any replacement.
  Providing an AMQP client is out of scope for the IoT Broker SDK.
  Components that require such functionality should implement it themselves or use one of the widely available AMQP client implementations.

## [5.5.0] - 2022-03-30
### Added
- Introduce messages intended to be sent via AMQP that represent events emitted for changes to `org.siliconeconomy.iotbroker.model.device.DeviceInstance` and `org.siliconeconomy.iotbroker.model.device.DeviceType` instances.
- Add a configuration for the AMQP exchange where messages related to the new device registration process are published to.

## [5.4.1] - 2022-02-25
### Fixed
- Fix typo on an attribute of `DeviceInstance`.
  It is supposed to be `deviceTypeIdentifier`, not `deviceTypIdentifier`.

## [5.4.0] - 2022-02-22
### Added
- Data structures to be used with a new, more sophisticated device registration process.
  This process revolves around device instances and device types.

### Deprecated
- Deprecate `org.siliconeconomy.iotbroker.model.device.Device` in favor of `org.siliconeconomy.iotbroker.model.device.DeviceInstance` and `org.siliconeconomy.iotbroker.model.device.DeviceType`.

## [5.3.0] - 2022-02-15
### Added
- Add support for global queries in a partitioned CouchDB repository.
  For this purpose a new annotation `@Global` is added which can be used in conjunction with the `@View` annotation to mark a view as a global view.

## [5.2.1] - 2021-11-17
### Changed
- Adjust the style of license headers.
  Use the slash-star style to avoid dangling Javadoc comments.
  Using this style also prevents the license headers from being affected when reformatting code via the IntelliJ IDEA IDE.

## [5.2.0] - 2021-10-29
### Added
- This CHANGELOG file to keep track of the changes in this project.
- Extend the SensorData model by a SimpleResult type that can be used to describe generic observation results.

### Changed
- Introduce dedicated deserializers for LocationEncodingType and ObservationType instead of handling deserialization of these types in ClassDeserializer.
  Since both LocationEncodingType and ObservationType now have a _SIMPLE_ type, "generic" deserialization via the ClassDeserializer is no longer possible.
  Furthermore, having dedicated deserializers makes the deserialization process more explicit.
- Improve Javadoc documentation a bit and describe an observation's result more explicitly.

## [5.1.0] - 2021-10-14
This is the first public release.

The IoT Broker SDK (IoT Broker Software Development Kit) facilitates development of components that are intended to be deployed and run in the IoT Broker environment.

This release corresponds to IoT Broker v0.9.
