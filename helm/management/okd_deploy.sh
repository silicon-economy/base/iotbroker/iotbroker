#!/bin/bash
# Use Namespace already provided or the default given here
NAMESPACE="${NAMESPACE:=iot-broker}"
FULLNAME="management"

# Upgrade or install
helm --set-string="fullnameOverride=management" upgrade -n "$NAMESPACE" -i management . || exit 1
# Ensure image stream picks up the new docker image right away
oc -n "$NAMESPACE" import-image "${FULLNAME}-api-gateway"
oc -n "$NAMESPACE" import-image "${FULLNAME}-backend"
oc -n "$NAMESPACE" import-image "${FULLNAME}-frontend"
