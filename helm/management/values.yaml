## This is a YAML-formatted file.
nameOverride:
# Set a sane and simple name here
fullnameOverride:

api_gateway:
  replicaCount: 2

  image:
    repo: "nexus.apps.sele.iml.fraunhofer.de:443/sele/iotbroker/management-api-gateway"
    tag: "latest"
    # Set this to true to let openshift regularly poll for new images and update pods
    scheduledUpdate: false

  ## Configure resource requests and limits
  ## ref: http://kubernetes.io/docs/user-guide/compute-resources/
  resources:
    requests:
      memory: 512Mi
      cpu: 0.1
    limits:
      memory: 1Gi
      cpu: 0.5

  ## Node labels for pod assignment
  ## ref: https://kubernetes.io/docs/user-guide/node-selection/
  nodeSelector: {}

  livenessProbe:
    enabled: false

  readinessProbe:
    enabled: false

  service:
    port:
      http:
        target: 8080
        exposed: 8080
    annotations: {}

  ingress:
    enabled: true
    # Please make sure you choose a unique name here and don't clash with other ingresses
    # Only a single hostname is allowed, i.e. example.rest would not work here, separate with '-' instead
    host: iotbroker-management-api-gateway
    # The container port as configured above
    targetPort: http
    # Choose how to handle insecure traffic (e.g. http://), possible values, Allow, Disable, Redirect
    # This route utilizes edge termination (and this is intentionally not configurable here) so there is no reason
    # not to redirect insecure traffic to https://
    insecureEdgeTerminationPolicy: Redirect

backend:
  replicaCount: 2

  image:
    repo: "nexus.apps.sele.iml.fraunhofer.de:443/sele/iotbroker/management-backend"
    tag: "latest"
    # Set this to true to let openshift regularly poll for new images and update pods
    scheduledUpdate: false

  ## Configure resource requests and limits
  ## ref: http://kubernetes.io/docs/user-guide/compute-resources/
  resources:
    requests:
      memory: 512Mi
      cpu: 0.1
    limits:
      memory: 1Gi
      cpu: 0.5

  ## Node labels for pod assignment
  ## ref: https://kubernetes.io/docs/user-guide/node-selection/
  nodeSelector: {}

  livenessProbe:
    enabled: true
    initialDelaySeconds: 60
    periodSeconds: 5
    failureThreshold: 1

  readinessProbe:
    enabled: true
    initialDelaySeconds: 60
    periodSeconds: 5
    failureThreshold: 1

  service:
    port:
      http:
        target: 8080
        exposed: 8080
    annotations: {}

  ingress:
    enabled: true
    # Please make sure you choose a unique name here and don't clash with other ingresses
    # Only a single hostname is allowed, i.e. example.rest would not work here, separate with '-' instead
    host: iotbroker-management-rest
    # The container port as configured above
    targetPort: http
    # Choose how to handle insecure traffic (e.g. http://), possible values, Allow, Disable, Redirect
    # This route utilizes edge termination (and this is intentionally not configurable here) so there is no reason
    # not to redirect insecure traffic to https://
    insecureEdgeTerminationPolicy: Redirect

frontend:
  replicaCount: 1

  image:
    repo: "nexus.apps.sele.iml.fraunhofer.de:443/sele/iotbroker/management-frontend"
    tag: "latest"
    # Set this to true to let openshift regularly poll for new images and update pods
    scheduledUpdate: false

  ## Configure resource requests and limits
  ## ref: http://kubernetes.io/docs/user-guide/compute-resources/
  resources:
    requests:
      memory: 128Mi
      cpu: 0.1
    limits:
      memory: 256Mi
      cpu: 0.5

  ## Node labels for pod assignment
  ## ref: https://kubernetes.io/docs/user-guide/node-selection/
  nodeSelector: {}

  livenessProbe:
    enabled: false

  readinessProbe:
    enabled: false

  service:
    port:
      http:
        target: 8080
        exposed: 8080
    annotations: {}

  ingress:
    enabled: true
    # Please make sure you choose a unique name here and don't clash with other ingresses
    # Only a single hostname is allowed, i.e. example.rest would not work here, separate with '-' instead
    host: iotbroker-management
    # The container port as configured above
    targetPort: http
    # Choose how to handle insecure traffic (e.g. http://), possible values, Allow, Disable, Redirect
    # This route utilizes edge termination (and this is intentionally not configurable here) so there is no reason
    # not to redirect insecure traffic to https://
    insecureEdgeTerminationPolicy: Redirect

config:
  application:
    apiGateway:
      deviceRegistryServiceUrl: "http://device-registry-service:8080"
      sensorDataHistoryServiceUrl: "http://sensor-data-history-service:8080"
      sensorDataSubscriptionServiceWebsocketUrl: "ws://sensor-data-subscription-service:8080"
    backend:
      prometheus:
        api:
          # For calls to the Prometheus HTTP API the backend uses a HTTP client that doesn't validate
          # any certificates. This value should therefore never be set to a public URL!
          url: "https://prometheus-k8s.openshift-monitoring.svc:9091"
          endpoint:
            query: "/api/v1/query"
            queryrange: "/api/v1/query_range"
          secretName: iotbroker-serviceaccount-secret
      monitoring:
        namespace: "iot-broker"
        podLabelComponent: "label_sele_service"
        podLabelInfrastructure: "label_app_kubernetes_io_name"
      rabbitmq:
        host: "rabbitmq-ha"
        port: "5672"
        username: "admin"
        secretName: rabbitmq-ha
