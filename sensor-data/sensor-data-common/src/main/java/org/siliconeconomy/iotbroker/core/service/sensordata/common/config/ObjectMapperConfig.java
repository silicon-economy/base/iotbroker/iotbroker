/*
 * Copyright Open Logistics Foundation
 *
 * Licensed under the Open Logistics Foundation License 1.3.
 * For details on the licensing terms, see the LICENSE file.
 * SPDX-License-Identifier: OLFL-1.3
 */
package org.siliconeconomy.iotbroker.core.service.sensordata.common.config;

import com.fasterxml.jackson.databind.ObjectMapper;
import org.siliconeconomy.iotbroker.jackson.ObjectMapperFactory;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;

/**
 * Configuration for the {@link ObjectMapper} bean to be used.
 *
 * @author D. Ronnenberg
 */
@Configuration
public class ObjectMapperConfig {

  @Bean
  public ObjectMapper objectMapper() {
    return ObjectMapperFactory.createObjectMapper();
  }
}
