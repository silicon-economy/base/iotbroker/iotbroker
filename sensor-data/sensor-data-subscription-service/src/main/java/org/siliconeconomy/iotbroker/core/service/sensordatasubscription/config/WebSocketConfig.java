/*
 * Copyright Open Logistics Foundation
 *
 * Licensed under the Open Logistics Foundation License 1.3.
 * For details on the licensing terms, see the LICENSE file.
 * SPDX-License-Identifier: OLFL-1.3
 */
package org.siliconeconomy.iotbroker.core.service.sensordatasubscription.config;

import org.siliconeconomy.iotbroker.model.sensordata.SensorDataMessage;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.context.annotation.Configuration;
import org.springframework.messaging.simp.config.MessageBrokerRegistry;
import org.springframework.web.socket.config.annotation.EnableWebSocketMessageBroker;
import org.springframework.web.socket.config.annotation.StompEndpointRegistry;
import org.springframework.web.socket.config.annotation.WebSocketMessageBrokerConfigurer;

import static java.util.Objects.requireNonNull;

/**
 * Configuration of the WebSocket that clients can use to subscribe to this service.
 *
 * @author L. Gebel
 * @author M. Grzenia
 */
@Configuration
@EnableWebSocketMessageBroker
public class WebSocketConfig
    implements WebSocketMessageBrokerConfigurer {

  /**
   * The destination prefix that the STOMP broker uses for broadcasting {@link SensorDataMessage}s.
   */
  public static final String BROKER_PREFIX_SENSOR_DATA_MESSAGES = "/v1/messages";
  private final SensorDataSubscriptionServiceProperties sensorDataSubscriptionServiceProperties;

  /**
   * Creates a new instance.
   *
   * @param sensorDataSubscriptionServiceProperties The service's configuration properties.
   */
  public WebSocketConfig(
      SensorDataSubscriptionServiceProperties sensorDataSubscriptionServiceProperties) {
    this.sensorDataSubscriptionServiceProperties
        = requireNonNull(sensorDataSubscriptionServiceProperties,
                         "sensorDataSubscriptionServiceProperties");
  }

  @Override
  public void registerStompEndpoints(StompEndpointRegistry registry) {
    // Configure the endpoint(s) to which a WebSocket client needs to connect for the WebSocket
    // handshake.
    registry.addEndpoint("/websocket")
        .setAllowedOriginPatterns(
            sensorDataSubscriptionServiceProperties.getCorsAllowedOriginPatterns()
                .toArray(new String[]{})
        );
  }

  @Override
  public void configureMessageBroker(MessageBrokerRegistry config) {
    // Use the built-in message broker for subscriptions and broadcasting and route messages whose
    // destination header begins with one of the specified prefixes to the broker.
    config.enableSimpleBroker(BROKER_PREFIX_SENSOR_DATA_MESSAGES);
    // Configure prefixes to filter destinations targeting application annotated methods. This
    // results in STOMP messages whose destination header begins with the specified prefixes to be
    // routed to @MessageMapping methods in @Controller classes.
    config.setApplicationDestinationPrefixes("/app");
  }
}
