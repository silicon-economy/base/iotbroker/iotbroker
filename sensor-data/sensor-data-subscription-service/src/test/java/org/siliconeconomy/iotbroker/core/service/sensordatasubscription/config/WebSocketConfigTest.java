/*
 * Copyright Open Logistics Foundation
 *
 * Licensed under the Open Logistics Foundation License 1.3.
 * For details on the licensing terms, see the LICENSE file.
 * SPDX-License-Identifier: OLFL-1.3
 */
package org.siliconeconomy.iotbroker.core.service.sensordatasubscription.config;

import org.junit.jupiter.api.Test;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.context.properties.EnableConfigurationProperties;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.context.ApplicationContext;
import org.springframework.test.context.TestPropertySource;

import static org.assertj.core.api.Assertions.assertThat;

/**
 * Test cases for {@link WebSocketConfig}.
 * <p>
 * Merely tests if the application context can be loaded properly and thus all beans can be
 * initialized successfully if all required configuration properties are provided.
 *
 * @author M. Grzenia
 */
@SpringBootTest(classes = {WebSocketConfig.class})
@EnableConfigurationProperties({SensorDataSubscriptionServiceProperties.class})
@TestPropertySource("classpath:application-test.properties")
class WebSocketConfigTest {

  @Autowired
  private ApplicationContext context;

  @Test
  void contextLoads() {
    assertThat(context).isNotNull();
  }
}
