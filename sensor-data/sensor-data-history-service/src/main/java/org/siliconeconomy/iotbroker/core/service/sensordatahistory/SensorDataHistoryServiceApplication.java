/*
 * Copyright Open Logistics Foundation
 *
 * Licensed under the Open Logistics Foundation License 1.3.
 * For details on the licensing terms, see the LICENSE file.
 * SPDX-License-Identifier: OLFL-1.3
 */
package org.siliconeconomy.iotbroker.core.service.sensordatahistory;

import org.siliconeconomy.iotbroker.core.service.sensordatahistory.config.SensorDataHistoryServiceProperties;
import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.boot.context.properties.EnableConfigurationProperties;

@SpringBootApplication
@EnableConfigurationProperties({SensorDataHistoryServiceProperties.class})
public class SensorDataHistoryServiceApplication {
  /**
   * Global main.
   */
  public static void main(String[] args) {
    SpringApplication.run(SensorDataHistoryServiceApplication.class, args);
  }
}
