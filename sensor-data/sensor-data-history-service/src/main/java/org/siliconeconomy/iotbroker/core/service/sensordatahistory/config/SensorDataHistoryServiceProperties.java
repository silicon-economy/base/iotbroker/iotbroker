/*
 * Copyright Open Logistics Foundation
 *
 * Licensed under the Open Logistics Foundation License 1.3.
 * For details on the licensing terms, see the LICENSE file.
 * SPDX-License-Identifier: OLFL-1.3
 */
package org.siliconeconomy.iotbroker.core.service.sensordatahistory.config;

import lombok.Getter;
import org.springframework.boot.context.properties.ConfigurationProperties;
import org.springframework.boot.context.properties.ConstructorBinding;

import java.util.List;

import static java.util.Objects.requireNonNull;

/**
 * Defines service-relevant (configuration) properties (that are bound via environment variables).
 *
 * @author M. Grzenia
 */
@ConfigurationProperties(prefix = "sensor-data-history-service")
@ConstructorBinding
@Getter
public class SensorDataHistoryServiceProperties {

  private final List<String> corsAllowedOriginPatterns;

  /**
   * Creates a new instance.
   *
   * @param corsAllowedOriginPatterns The patterns of origins for which cross-origin requests to the
   *                                  service's web API are allowed from a browser.
   */
  public SensorDataHistoryServiceProperties(List<String> corsAllowedOriginPatterns) {
    this.corsAllowedOriginPatterns = requireNonNull(corsAllowedOriginPatterns,
                                                    "corsAllowedOriginPatterns");
  }
}
