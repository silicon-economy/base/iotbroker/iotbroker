/*
 * Copyright Open Logistics Foundation
 *
 * Licensed under the Open Logistics Foundation License 1.3.
 * For details on the licensing terms, see the LICENSE file.
 * SPDX-License-Identifier: OLFL-1.3
 */
package org.siliconeconomy.iotbroker.core.service.sensordatahistory.exception;

/**
 * Thrown to indicate that a message for a device with a specific source and tenant could not be
 * found in the database.
 *
 * @author M. Grzenia
 */
public class MessageNotFoundException
    extends RuntimeException {

  public MessageNotFoundException(String source, String tenant) {
    super(String.format(
        "Could not find message for source '%s' and tenant '%s'.",
        source,
        tenant
    ));
  }
}
