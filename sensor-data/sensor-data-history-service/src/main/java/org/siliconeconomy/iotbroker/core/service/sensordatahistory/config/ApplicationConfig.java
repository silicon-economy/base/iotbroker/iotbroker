/*
 * Copyright Open Logistics Foundation
 *
 * Licensed under the Open Logistics Foundation License 1.3.
 * For details on the licensing terms, see the LICENSE file.
 * SPDX-License-Identifier: OLFL-1.3
 */
package org.siliconeconomy.iotbroker.core.service.sensordatahistory.config;

import org.siliconeconomy.iotbroker.core.service.sensordata.common.config.CouchDbConfig;
import org.siliconeconomy.iotbroker.core.service.sensordata.common.config.ObjectMapperConfig;
import org.springframework.context.annotation.Configuration;
import org.springframework.context.annotation.Import;

/**
 * Configuration of various application-specific beans.
 *
 * @author M. Grzenia
 */
@Configuration
@Import({CouchDbConfig.class, ObjectMapperConfig.class})
public class ApplicationConfig {
}
