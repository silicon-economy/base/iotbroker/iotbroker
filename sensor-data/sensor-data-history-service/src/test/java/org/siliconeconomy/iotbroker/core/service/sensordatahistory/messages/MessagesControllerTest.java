/*
 * Copyright Open Logistics Foundation
 *
 * Licensed under the Open Logistics Foundation License 1.3.
 * For details on the licensing terms, see the LICENSE file.
 * SPDX-License-Identifier: OLFL-1.3
 */
package org.siliconeconomy.iotbroker.core.service.sensordatahistory.messages;

import org.junit.jupiter.api.Test;
import org.siliconeconomy.iotbroker.core.service.sensordatahistory.exception.MessageNotFoundException;
import org.siliconeconomy.iotbroker.core.service.sensordatahistory.exception.MultipleMessagesFoundException;
import org.siliconeconomy.iotbroker.core.service.sensordatahistory.webapi.v1.MessagesController;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.autoconfigure.web.servlet.WebMvcTest;
import org.springframework.boot.test.mock.mockito.MockBean;
import org.springframework.data.domain.Sort;
import org.springframework.test.context.TestPropertySource;
import org.springframework.test.web.servlet.MockMvc;
import org.springframework.test.web.servlet.request.MockMvcRequestBuilders;
import org.springframework.test.web.servlet.result.MockMvcResultMatchers;

import java.time.Instant;

import static org.mockito.ArgumentMatchers.anyString;
import static org.mockito.Mockito.verify;
import static org.mockito.Mockito.when;

/**
 * Test cases for {@link MessagesController}.
 *
 * @author D. Ronnenberg
 * @author M. Grzenia
 */
@WebMvcTest(controllers = MessagesController.class)
@TestPropertySource("classpath:application-test.properties")
class MessagesControllerTest {

  /**
   * Test dependencies.
   */
  @MockBean
  private MessageService messageService;
  /**
   * Test environment.
   */
  @Autowired
  private MockMvc mockMvc;

  @Test
  void getMessagesBySourceAndTenant()
      throws Exception {
    // Act
    mockMvc
        .perform(MockMvcRequestBuilders
                     .get("/v1/messages/sources/{source}/tenants/{tenant}", "source1", "tenant1")
        )
        .andReturn();

    // Assert & Verify
    verify(messageService)
        .findAllBySourceAndTenant("source1", "tenant1", 0, 100, Sort.Direction.ASC,
                                  Instant.parse("1970-01-01T00:00:00Z"),
                                  Instant.parse("9999-12-31T23:59:59.999999Z"));
  }

  @Test
  void getMessagesBySourceAndTenant_invalidSort()
      throws Exception {
    // Act & Assert
    mockMvc
        .perform(MockMvcRequestBuilders
                     .get("/v1/messages/sources/{source}/tenants/{tenant}", "source1", "tenant1")
                     .queryParam("sort", "no-valid-input")
        )
        .andExpect(MockMvcResultMatchers.status().isBadRequest());
  }

  @Test
  void getLatestMessageBySourceAndTenant()
      throws Exception {
    // Act
    mockMvc
        .perform(
            MockMvcRequestBuilders
                .get("/v1/messages/sources/{source}/tenants/{tenant}/latest", "source1", "tenant1")
        )
        .andReturn();

    // Assert & Verify
    verify(messageService).findLatestBySourceAndTenant("source1", "tenant1");
  }

  @Test
  void getLatestMessageBySourceAndTenant_noMessageFound()
      throws Exception {
    // Arrange
    when(messageService.findLatestBySourceAndTenant(anyString(), anyString()))
        .thenThrow(new MessageNotFoundException("somesource", "sometenant"));

    // Act & Assert
    mockMvc
        .perform(
            MockMvcRequestBuilders
                .get("/v1/messages/sources/{source}/tenants/{tenant}/latest", "source1", "tenant1")
        )
        .andExpect(MockMvcResultMatchers.status().isNotFound());
  }

  @Test
  void getLatestMessageBySourceAndTenant_multipleMessagesFound()
      throws Exception {
    // Arrange
    when(messageService.findLatestBySourceAndTenant(anyString(), anyString()))
        .thenThrow(new MultipleMessagesFoundException("somesource", "sometenant"));

    // Act & Assert
    mockMvc
        .perform(
            MockMvcRequestBuilders
                .get("/v1/messages/sources/{source}/tenants/{tenant}/latest", "source1", "tenant1")
        )
        .andExpect(MockMvcResultMatchers.status().isInternalServerError());
  }
}
