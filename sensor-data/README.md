# IoT Broker Sensor Data

The IoT Broker Sensor Data components allow working with data in the Sensor Data Message format (which is an abstraction layer for data sent by IoT devices connected to the IoT Broker).
For this, the following components are provided:

* Sensor Data Persistence Service - Responsible for storing messages containing sensor data of devices into a database.
* Sensor Data History Service - Provides access to the Sensor Data Messages stored by the Sensor Data Persistence Service.
* Sensor Data Subscription Service - Provides a live view of messages containing sensor data of devices complementing the Sensor Data History Service.

## Documentation

For more information on the components in this project, please refer to the IoT Broker's arc42 documentation maintained in the `documentation` directory in this repository.

## Licenses of third-party dependencies

The licenses used by this project's third-party dependencies are documented in the `third-party-licenses` directory.
This is done to encourage developers to check the licenses used by the third-party dependencies to ensure they do not conflict with the license of this project itself.
The directory contains the following files:

* `third-party-licenses.txt` - Contains the licenses used by this project's third-party dependencies.
  The content of this file is/can be generated.
* `third-party-licenses-complementary.txt` - Contains entries for third-party dependencies for which the licenses cannot be determined automatically.
  The content of this file is maintained manually.

### Generating third-party license reports

This project uses the [license-maven-plugin](https://github.com/mojohaus/license-maven-plugin) to generate a file containing the licenses used by the third-party dependencies.
The content of the `mvn license:aggregate-add-third-party` Maven goal's output (`target/generated-sources/license/THIRD-PARTY.txt`) can be copied into `third-party-licenses/third-party-licenses.txt`.

Third-party dependencies for which the licenses cannot be determined automatically by the license-maven-plugin have to be documented manually in `third-party-licenses/third-party-licenses-complementary.txt`.
In the `third-party-licenses/third-party-licenses.txt` file these third-party dependencies have an "Unknown license" license.
