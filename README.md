# IoT Broker

The IoT Broker is a broker for IoT devices providing realtime and past data from and to devices.
It enables the integration of IoT devices, the encapsulation of device specifics (proprietary and binary protocols, data formats), the caching of communication and device data, as well as the conversion of IoT data into open formats (such as JSON or XML) as a basis for easy further processing of data.

This project contains the following components:

* SDK - Facilitates development of components that are intended to be deployed and run in the IoT Broker environment.
* Device Registry (Service) - Manages the registration of IoT devices with the IoT Broker.
* Sensor Data (Services) - Allow working with data in the Sensor Data Message format.
* Management application - Provides an interface to manage and administrate the IoT Broker.

## Documentation

For more information on the IoT Broker as a whole and the components in this project, please refer to the `documentation` directory.

## Running the IoT Broker locally

For information on how to run the IoT Broker locally, please refer to the _Tutorial_ section in the documentation.

## Git tags

The Git tags denominating versions used in this project do not correspond to tags for releases of individual components included in this project.
Instead, since the IoT Broker is composed of several components which are distributed across multiple repositories, the version numbers used with the tags refer to specific versions of the IoT Broker as a whole.
Thus, a tag marks a revision that belongs to the referenced version of the IoT Broker.
Therefore, the version numbers used with the tags do not necessarily match with the version numbers of the individual components.

## Continuous integration/Continuous deployment (CI/CD)

Development in this project facilitates CI/CD pipelines (e.g., for execution of builds and tests, deployment of build artifacts), tools for static code analysis, etc. currently on Fraunhofer internal infrastructure.

The corresponding deployment descriptors and pipeline scripts are provided as examples.

## Licenses of third-party dependencies

For information about licenses of third-party dependencies, please refer to the `README.md` files of the corresponding components.
